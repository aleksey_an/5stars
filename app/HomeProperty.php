<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeProperty extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'home_properties';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['property_id', 'created_by'];
}
