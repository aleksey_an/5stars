<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyImage extends Model
{
    protected $table = 'property_images';
    protected $fillable = [
        'property_id',
        'name',
        'detail',
        'sequence',
        'is_enabled',
        'created_by',
        'updated_by'
    ];
}
