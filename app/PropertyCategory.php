<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyCategory extends Model
{
    public $table = 'property_categories';

    protected $fillable = ['id',  'code', 'sequence', 'is_enabled', 'created_by', 'updated_by'];

    public function translations()
    {
        return $this->hasMany('App\PropertyCategoryTranslations', 'property_category_id', 'id');
    }

    public function categoryNameInLocale()
    {
        return $this->hasMany('App\PropertyCategoryTranslations')->where(['locale' => \App::getLocale()]);
    }

    public function categoryNameInDefaultLocale()
    {
        return $this->hasMany('App\PropertyCategoryTranslations')->where(['locale' => config('app.fallback_locale')]);
    }
}
