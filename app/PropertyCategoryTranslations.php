<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyCategoryTranslations extends Model
{
    public $table = 'property_category_translations';
    protected $fillable = ['property_category_id',  'locale', 'url', 'name', 'description'];
    public $timestamps = false;
}
