<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'links';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['url', 'sequence', 'is_enabled'];
    
    
    /**
     * Getting image's path
     *
     * @return mixed
     */
    public function getImage()
    {
        if ($this->image) {
            return $this->image;
        }

        return false;
    }
    
    
    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', 1);
    }
}
