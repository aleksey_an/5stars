<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleTranslation extends Model
{
    protected $table = 'article_translations';

    protected $fillable = ['article_id', 'title', 'short_description', 'description', 'visible', 'locale', 'viewed'];

    public $timestamps = false;

    public function lang()
    {
        return $this->hasMany('App\Language', 'locale', 'locale');
    }
}
