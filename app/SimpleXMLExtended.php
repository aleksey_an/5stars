<?php

namespace App;

use SimpleXMLElement;

class SimpleXMLExtended extends SimpleXMLElement
{
    public function addChild($name, $value = null, $namespace = null)
    {
        return parent::addChild($name, htmlspecialchars($value), $namespace);
    }

    public function addChildWithCData($name, $value = null, $namespace = null)
    {
        $newChild = parent::addChild($name, null, $namespace);

        $this->addCDataToNode($newChild, htmlspecialchars($value));

        return $newChild;
    }

    private function addCDataToNode(SimpleXMLElement $node, $value = '')
    {
        if ($domElement = dom_import_simplexml($node)) {
            $domOwner = $domElement->ownerDocument;
            $domElement->appendChild($domOwner->createCDATASection($value));
        }
    }
}
