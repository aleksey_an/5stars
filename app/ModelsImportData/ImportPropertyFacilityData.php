<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportPropertyFacilityData extends Model
{
    public $table = 'z_import_property_facility_data';
}
