<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportPropertyDetails extends Model
{
    protected $table = 'z_import_property_detail';
}
