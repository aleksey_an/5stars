<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportPropertyFacility extends Model
{
    protected $table = 'z_import_property_facility';
}
