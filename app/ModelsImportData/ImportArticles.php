<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportArticles extends Model
{
    protected $table = 'z_import_article';
}
