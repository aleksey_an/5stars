<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportPropertyTransport extends Model
{
    protected $table = 'z_import_property_transport';

}
