<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportArticlesDetails extends Model
{
    protected $table = 'z_import_article_detail';
}
