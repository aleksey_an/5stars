<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportProperty extends Model
{
    protected $table = 'z_import_property';
}
