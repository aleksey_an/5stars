<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportPropertyImage extends Model
{
    public $table = 'z_import_property_image';
}
