<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportPropertyTransportDetail extends Model
{
    protected $table = 'z_import_property_transport_detail';
}
