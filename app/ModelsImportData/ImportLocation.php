<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportLocation extends Model
{
    protected $table = 'z_import_property_location';
}
