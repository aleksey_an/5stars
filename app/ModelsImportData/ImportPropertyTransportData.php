<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportPropertyTransportData extends Model
{
    protected $table = 'z_import_property_transport_data';
}
