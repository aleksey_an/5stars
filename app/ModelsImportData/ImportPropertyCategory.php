<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportPropertyCategory extends Model
{
    public $table = 'z_import_property_category';
}
