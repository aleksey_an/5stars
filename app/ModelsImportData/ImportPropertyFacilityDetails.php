<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportPropertyFacilityDetails extends Model
{
    public $table = 'z_import_property_facility_detail';
}
