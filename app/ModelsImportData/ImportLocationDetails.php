<?php

namespace App\ModelsImportData;

use Illuminate\Database\Eloquent\Model;

class ImportLocationDetails extends Model
{
    protected $table = 'z_import_property_location_detail';
}
