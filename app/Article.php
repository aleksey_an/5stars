<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = ['id', 'image', 'is_enabled', 'created_by', 'updated_by', 'created_at', 'updated_at'];

    public function translations()
    {
        return $this->hasMany('App\ArticleTranslation');
    }
   /* public function enTranslations()
    {
        return $this->hasMany('App\ArticleTranslation')->where(['locale' => 'en']);
    }
    public function frTranslations()
    {
        return $this->hasMany('App\ArticleTranslation')->where(['locale' => 'fr']);
    }*/
    public function translationInLocale(){
        return $this->hasMany('App\ArticleTranslation')->where(['locale' => App::getLocale()]);
    }
}
