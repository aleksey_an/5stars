<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $responseLang = DB::table('languages')->where(['is_enabled_web' => 1])->get();
        $langConf = [];
        foreach($responseLang as $rl){
            $langConf[$rl->locale] = array( 'name' => $rl->name, 'script' => 'Latn', 'native' => $rl->name);
        }

        config([
            'laravellocalization.supportedLocales' => $langConf,

            'laravellocalization.useAcceptLanguageHeader' => false,

            'laravellocalization.hideDefaultLocaleInURL' => false
        ]);
    }
}
