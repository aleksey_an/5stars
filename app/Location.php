<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Location extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'locations';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'parent_id', 'code', 'video_url', 'sequence', 'is_enabled', 'lat', 'lng', 'ddproperty_code', 'created_by', 'updated_by'];

    public function translations()
    {
        return $this->hasMany('App\LocationTranslation');
    }

    /**
     * Take Location Images
     */
    public function images()
    {
        return $this->hasMany('App\LocationImage')->orderBy('sequence', 'desc');
    }
    public function imagesForWeb()
    {
        return $this->hasMany('App\LocationImage')->where(['is_enabled' => 1])->orderBy('sequence', 'asc');
    }
    /**
     * Take translation in locale
     */
    public function translationsWithLocale()
    {
        return $this->hasMany('App\LocationTranslation')->where(['locale' => \App::getLocale()]);
    }

    public function translationsWithDefaultLocale()
    {
        return $this->hasMany('App\LocationTranslation')->where(['locale' => config('app.fallback_locale')]);
    }
}
