<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTranslation extends Model
{
    protected $table = 'property_translations';
    protected $fillable = ['property_id',  'subject', 'locale', 'detail', 'visible', 'viewed'];
    public $timestamps = false;

    public function lang()
    {
        return $this->hasMany('App\Language', 'locale', 'locale');
    }
}
