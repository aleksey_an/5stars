<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public $table = 'currencies';
    protected $fillable = ['name', 'code', 'sign', 'rate', 'sequence', 'is_default', 'is_enabled'];

}
