<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetroTranslation extends Model
{
    protected $table = 'metro_translations';

    protected $fillable = ['locale', 'metro_id', 'name', 'description'];

    public $timestamps = false;

}
