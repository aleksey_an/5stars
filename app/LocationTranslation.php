<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationTranslation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'location_translations';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['location_id', 'url', 'name', 'description', 'locale', 'clicked', 'created_by', 'updated_by'];

    public $timestamps = false;

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    /**
     * Scopes
     */
    public function scopeInLocale($query, $field)
    {
        $query->where(['locale' => \App::getLocale()])->select($field);
    }
}
