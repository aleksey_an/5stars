<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqTranslation extends Model
{
    protected $table = 'faq_translations';

    protected $fillable = ['faq_id', 'question', 'answer', 'visible', 'locale'];

    public $timestamps = false;

    public function lang()
    {
        return $this->hasMany('App\Language', 'locale', 'locale');
    }
}
