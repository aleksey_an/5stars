<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'languages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'locale', 'iso_639_3', 'sequence', 'is_enabled', 'is_enabled_web'];
    
    
    /**
     * Getting image's path
     *
     * @return mixed
     */
    public function getImage()
    {
        if ($this->image) {
            return $this->image;
        }

        return false;
    }
    
    
    public function scopeEnabled($query)
    {
        return $query->where('is_enabled', 1);
    }
}
