<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFacilityTranslations extends Model
{
    public $table = 'property_facility_translations';
    protected $fillable = ['property_facility_id',  'name', 'locale'];
    public $timestamps = false;
}
