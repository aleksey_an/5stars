<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';

    protected $fillable = ['id', 'is_enabled', 'sequence', 'created_at', 'updated_at'];

    public function translations()
    {
        return $this->hasMany('App\FaqTranslation');
    }
   /* public function enTranslations()
    {
        return $this->hasMany('App\ArticleTranslation')->where(['locale' => 'en']);
    }
    public function frTranslations()
    {
        return $this->hasMany('App\ArticleTranslation')->where(['locale' => 'fr']);
    }*/
    public function translationInLocale(){
        return $this->hasMany('App\FaqTranslation')->where(['locale' => App::getLocale()]);
    }
}
