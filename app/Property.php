<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    public $isFavorite = false;
    public $isHomePage = false;
    public $propertyHref = '';
    protected $fillable = [
        'code',
        'category_id',
        'location_id',
        'metro_id',
        'user_id',
        'contact_name',
        'contact_email',
        'contact_phone',
        'contact_mobile',
        'contact_viber',
        'contact_line',
        'contact_whatsapp',
        'built_year',
        'building_name',
        'address',
        'address_1',
        'lat',
        'lng',
        'unit_number',
        'indoor_area',
        'outdoor_area',
        'bedroom',
        'bathroom',
        'furnished',
        'floor_number',
        'key_info',
        'to_rent',
        'is_rent',
        'rent_month',
        'rent_week',
        'rent_day',
        'to_sale',
        'is_sold',
        'sale_price',
        'common_fee',
        'keys_collection',
        'availability',
        'publish_date',
        'youtube_url',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     *Fo Admin
     */
    public function translations()
    {
        return $this->hasMany('App\PropertyTranslation');
    }

    //Take facilities
    public function facilities()
    {
        return $this->hasMany('App\PropertyFacilityData');
    }

    //Take transports
    public function transports()
    {
        return $this->hasMany('App\PropertyTransportData');
    }

    public function categoryNameInEn()
    {
        return $this->hasMany('App\PropertyCategoryTranslations', 'property_category_id', 'category_id')
            ->where(['locale' => 'en']);
    }

    public function locationNameInEn()
    {
        return $this->hasMany('App\LocationTranslation', 'location_id', 'location_id')
            ->where(['locale' => 'en']);
    }

    /**
     * Fo web
     */
//    Translations
    public function translationsInLocale()
    {
        return $this->hasMany('App\PropertyTranslation')
            ->where(['locale' => \App::getLocale()]);
    }

    public function translationsDefaultLocale()
    {
        return $this->hasMany('App\PropertyTranslation')->where(['locale' => config('app.fallback_locale')]);
    }

    /**
     *Category
     */
    public function categoryNameInLocale()
    {
        return $this->hasMany('App\PropertyCategoryTranslations', 'property_category_id', 'category_id')
            ->where(['locale' => \App::getLocale()]);
    }

    public function categoryNameInDefaultLocale()
    {
        return $this->hasMany('App\PropertyCategoryTranslations', 'property_category_id', 'category_id')
            ->where(['locale' => config('app.fallback_locale')]);
    }

    /**
     *Location
     */
    public function location()
    {
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function locationNameInLocale()
    {
        return $this->hasMany('App\LocationTranslation', 'location_id', 'location_id')
            ->where(['locale' => \App::getLocale()]);
    }

    public function locationNameInDefaultLocale()
    {
        return $this->hasMany('App\LocationTranslation', 'location_id', 'location_id')
            ->where(['locale' => config('app.fallback_locale')]);
    }

    /**
     * Images
     */
    public function images()
    {
        return $this->hasMany('App\PropertyImage')->orderBy('sequence', 'desc');
    }

    public function imagesForWeb()
    {
        return $this->hasMany('App\PropertyImage')->where(['is_enabled' => 1])->orderBy('sequence', 'asc');
    }

    public function imageForWebSearch()
    {
        return $this->hasMany('App\PropertyImage')->orderBy('sequence', 'asc');
    }

    /**
     *Facility
     */
    public function facility()
    {
        return $this->hasMany('App\PropertyFacilityData');
    }

    /**
     * Scopes
     */
    public function scopeBudget($query, $fieldName = null, $from = null, $to = null)
    {
//        dd('TEST', $fieldName, $from, $to);
        if ($fieldName && $from && $to) {
            $query->whereBetween($fieldName, [$from, $to])->orderBy($fieldName, 'asc');
        }
    }
}
