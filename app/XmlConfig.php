<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class XmlConfig extends Model
{
    protected $table = 'xml_config';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $casts = [
        'location_id' => 'array',
        'category_id' => 'array'
    ];

    protected $fillable = [
        'locale',
        'location_id',
        'category_id',
        'to_sale',
        'to_rent',
        'date_from',
        'date_to',
        'budget_from',
        'budget_to'
    ];
}

