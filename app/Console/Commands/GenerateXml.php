<?php

namespace App\Console\Commands;

use App\Http\Controllers\Admin\XmlController;
use App\XmlConfig;
use Illuminate\Console\Command;

class GenerateXml extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xmlgenerate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Xml';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xmlController = new XmlController();
        $request = XmlConfig::get()->first()->toArray();
        $xmlController->generate($request);
        $xmlController->generateHipflat();
        $xmlController->generateDDproperty();
        $xmlController->generateSitemap();
    }
}
