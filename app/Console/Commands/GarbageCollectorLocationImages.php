<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\LocationImage;

class GarbageCollectorLocationImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'g_c_location_images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Garbage Collector Location Images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = public_path('images/location/original');
        $images = scandir($path);
        if ($images !== false) {
            $images = preg_grep("/.(?:png|gif|jpe?g)$/i", $images);
            if (is_array($images)) {
                foreach($images as $key => $image) {
                    $checkImage = LocationImage::where(['name' => $image])->get()->toArray();
                    if(!isset($checkImage[0])){
                        \File::Delete(public_path('/images/location/original/') . $image);
                        \File::Delete(public_path('/images/location/display/') . $image);
                        \File::Delete(public_path('/images/location/small/') . $image);
                        \File::Delete(public_path('/images/location/thumbnail/') . $image);
                        \Log::info('Deleted image => ' . $image);
                    }
                }
            } else {
                \Log::info('No images found in the folder');
            }
        } else {
            \Log::info('Error ');
        }
    }
}
