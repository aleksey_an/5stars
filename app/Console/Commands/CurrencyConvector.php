<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Currency;
use Equentor\LaravelOpenExchangeRates\Client;

class CurrencyConvector extends Command
{
    private $client;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency_convector';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Currency Convector';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currencies = Currency::all()->toArray();
        $codes = '';
        foreach ($currencies as $currency) {
            $codes = $codes . $currency['code'] . ',';
        }
        $coefficients = $this->client->latest($codes);
        $coefficients = $coefficients['rates'];
        if (isset($coefficients)) {
            foreach ($coefficients as $key => $coefficient) {
                Currency::where(['code' => $key])->update(['rate' => $coefficient]);
            }
        }
        \Log::info('Currency Converted  at - '.\Carbon\Carbon::now());
    }
}
