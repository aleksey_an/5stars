<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PropertyImage;

class GarbageCollectorPropertyImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'g_c_property_images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Garbage Collector Property Images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $path = public_path('images/property/original');
        $images = scandir($path);
        if ($images !== false) {
            $images = preg_grep("/.(?:png|gif|jpe?g)$/i", $images);
            if (is_array($images)) {
                foreach($images as $key => $image) {
                    $checkImage = PropertyImage::where(['name' => $image])->get()->toArray();
                    if(!isset($checkImage[0])){
                        \Log::info('Deleted');
                        \File::Delete(public_path('/images/property/original/') . $image);
                        \File::Delete(public_path('/images/property/display/') . $image);
                        \File::Delete(public_path('/images/property/small/') . $image);
                        \File::Delete(public_path('/images/property/thumbnail/') . $image);
                    }
                }
            } else {
                \Log::info('No images found in the directory');
            }
        } else {
            \Log::info('Error ');
        }
    }
}
