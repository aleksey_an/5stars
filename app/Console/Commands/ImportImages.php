<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PropertyImage;
use Intervention\Image\Facades\Image;

class ImportImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import_images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ignor = ['property_24501_Capture_dCuycran_2016-04-24_yZ_19.19.56.png'];


        $path = public_path('images/property/original');
        $images = scandir($path);
        if ($images !== false) {
            $images = preg_grep("/.(?:png|jpeg|jpe|JPG?g)$/i", $images);
            if (is_array($images) && count($images) > 0) {
                foreach ($images as $key => $image) {
                    if (is_file(public_path('/images/property/original/') . $image)) {
                        if(in_array($image, $ignor)){
                            continue;
                        }
                        $this->saveImageWithSize(public_path('/images/property/original/') . $image, $image);
                    }
                }
            } else {
                \Log::info('No images found in the directory');
            }
        } else {
            \Log::info('Error ');
        }
    }

    private function saveImageWithSize($image, $imageName)
    {
        $max_width = 1025;

        // Take image
        $Image = Image::make($image);


        // Save as to display
        if ($Image->width() > $max_width) {
            $Image->resize($max_width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $Image->save(public_path('images/property/display/') . $imageName);

        // Save as small
        $Image->fit(443, 300);
        $Image->save(public_path('images/property/small/' . $imageName));

        // Save as thumbnail
        $Image->fit(100, 100);
        $Image->save(public_path('images/property/thumbnail/' . $imageName));

        return;
    }
}
