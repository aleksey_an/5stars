<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\GarbageCollectorPropertyImages::class,
        \App\Console\Commands\GarbageCollectorLocationImages::class,
        \App\Console\Commands\CurrencyConvector::class,
        \App\Console\Commands\ImportImages::class,
        \App\Console\Commands\ImportArticlesImage::class,
        \App\Console\Commands\GenerateXml::class,
//        \App\Console\Commands\Demo::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('currency_convector')/*->dailyAt('8:00')*/->weekly();;
        $schedule->command('g_c_property_images')->dailyAt('8:00');
        $schedule->command('g_c_location_images')->dailyAt('8:00');
        $schedule->command('xmlgenerate')->dailyAt('8:00');
//        $schedule->command('demo')->everyMinute();

    }

    /**
     * Register the Closure based commands for tcdhe application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
