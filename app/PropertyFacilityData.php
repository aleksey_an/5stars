<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFacilityData extends Model
{
    public $table = 'property_facility_data';
    protected $fillable = ['property_id', 'property_facility_id'];
    public $timestamps = false;

    public function getFacility()
    {
        return $this->belongsTo('App\PropertyFacility', 'id');
    }

    public function translationsInLocale()
    {
        return $this->hasMany('App\PropertyFacilityTranslations', 'property_facility_id', 'property_facility_id')
            ->where(['locale' => \App::getLocale()]);
    }

    public function translationsInDefaultLocale()
    {
        return $this->hasMany('App\PropertyFacilityTranslations', 'property_facility_id', 'property_facility_id')
            ->where(['locale' => config('app.fallback_locale')]);
    }

    public function getImage()
    {
        return $this->belongsTo('App\PropertyFacility', 'property_facility_id', 'id');
    }
}

