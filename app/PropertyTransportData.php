<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTransportData extends Model
{
    public $table = 'property_transport_data';
    protected $fillable = ['property_id', 'property_transport_id'];
    public $timestamps = false;

    public function getTransport()
    {
        return $this->belongsTo('App\PropertyTransport', 'id');
    }

    public function translationsInLocale()
    {
        return $this->hasMany('App\PropertyTransportTranslations', 'property_transport_id', 'property_transport_id')
            ->where(['locale' => \App::getLocale()]);
    }

    public function translationsInDefaultLocale()
    {
        return $this->hasMany('App\PropertyTransportTranslations', 'property_transport_id', 'property_transport_id')
            ->where(['locale' => config('app.fallback_locale')]);
    }

    public function getImage()
    {
        return $this->belongsTo('App\PropertyTransport', 'property_transport_id', 'id');
    }
}
