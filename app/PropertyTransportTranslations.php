<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTransportTranslations extends Model
{
    public $table = 'property_transport_translations';
    protected $fillable = ['property_transport_id',  'name', 'locale'];
    public $timestamps = false;
}
