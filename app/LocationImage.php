<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'location_images';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'type', 'location_id', 'is_enabled', 'sequence',  'created_by', 'updated_by'];

    public $timestamps = false;
}
