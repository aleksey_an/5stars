<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTransport extends Model
{
    public $table = 'property_transports';

    protected $fillable = ['id',  'image', 'sequence', 'is_enabled', 'created_by', 'updated_by'];

    public function translations()
    {
        return $this->hasMany('App\PropertyTransportTranslations');
    }
}
