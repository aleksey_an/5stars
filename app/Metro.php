<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metro extends Model
{
    protected $table = 'metro';

    protected $fillable = ['lat', 'lng', 'address', 'is_enabled', 'location_id', 'created_by', 'updated_by'];

    public function translations()
    {
        return $this->hasMany('App\MetroTranslation');
    }

    public function location (){
        return $this->belongsTo('App\Location');
    }
}
