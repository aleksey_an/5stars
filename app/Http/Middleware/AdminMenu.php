<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Lavary\Menu\Menu;

class AdminMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $menu = new Menu;
        $menu->make('AdminNavBar', function ($menu) {
            $UserRoleName = Auth::user()->roles->first()->name;

            $propertiesMenu = $menu->add('Properties', array('action' => 'Admin\PropertyController@index', 'class' => 'dropdown'));
            $menu->properties->attr(array('class' => 'dropdown'))
                ->append(' <b class="caret"></b>')
                ->prepend('<span class="glyphicon glyphicon-user"></span> ');

            $propertiesMenu->link->attr(array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'));

            $menu->properties->add('Properties', array('action' => 'Admin\PropertyController@index',));

            if ($UserRoleName == 'admin' || $UserRoleName == 'super_admin') {
                $menu->properties->add('Categories', array('action' => 'Admin\PropertyCategoryController@index'));
                $menu->properties->add('Locations', array('action' => 'Admin\LocationController@index'));
                $menu->properties->add('Facilities', array('action' => 'Admin\PropertyFacilityController@index'));
                $menu->properties->add('Main Transports', array('action' => 'Admin\PropertyTransportController@index'));
                /*$menu->properties->add('Metro', array('action' => 'Admin\MetroController@index'));*/

                /*
                 $menu->add('Link', array('action' => 'Admin\LinkController@index'))
                     ->prepend('<span class="glyphicon glyphicon-link"></span> ');
                 */
                $menu->add('Blog', array('action' => 'Admin\ArticleController@index'))
                    ->prepend('<span class="glyphicon glyphicon-th-list"></span> ');

                $userMenu = $menu->add('User', array('action' => 'Admin\UserController@index', 'class' => 'dropdown'))
                    ->append(' <b class="caret"></b>')
                    ->prepend('<span class="glyphicon glyphicon-user"></span> ');
                $userMenu->link->attr(array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'));
                $menu->user->add('Edit Users', array('action' => 'Admin\UserController@index'));
                if ($UserRoleName == 'super_admin') {
                    $menu->user->add('Edit Roles', array('action' => 'Admin\RoleController@index'));
                    $menu->user->add('Edit Permissions', array('action' => 'Admin\PermissionController@index'));
                }


                $administrationMenu = $menu->add('Administration', array('action' => 'Admin\AdministrationController@index', 'class' => 'dropdown'))
                    ->append(' <b class="caret"></b>')
                    ->prepend('<span class="glyphicon glyphicon-cog"></span> ');

                $administrationMenu->link->attr(array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'));
                $administrationMenu->add('Configuration', array('action' => 'Admin\ConfigurationController@index'));
                $administrationMenu->add('Currency', array('action' => 'Admin\CurrencyController@index'));
                $administrationMenu->add('Edit Languages', array('action' => 'Admin\LanguageController@index'));
                $administrationMenu->add('Localization', array('action' => 'Admin\TranslationController@index'));


                $editHomeMenu = $menu->add('Home Page', ['class' => 'dropdown'])
                    ->append(' <b class="caret"></b>')
                    ->prepend('<span class="glyphicon glyphicon-home"></span> ');
                $editHomeMenu->link->attr(['class' => 'dropdown-toggle', 'data-toggle' => 'dropdown']);
                $editHomeMenu->add('Upload images', ['action' => 'Admin\HomePageController@uploadImages']);
                $editHomeMenu->add('Select a property for the home page', ['action' => 'Admin\HomePageController@selectProperty']);
            }
        });
        return $next($request);
    }
}
