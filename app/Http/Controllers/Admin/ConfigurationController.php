<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Configuration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Session;

class ConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $configuration = Configuration::where('key', 'LIKE', "%$keyword%")
				->orWhere('value', 'LIKE', "%$keyword%")
				->orWhere('description', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $configuration = Configuration::paginate($perPage);
        }

        return view('admin.configuration.index', compact('configuration'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $configuration = Configuration::findOrFail($id);
//  dd($configuration);
        return view('admin.configuration.edit', compact('configuration'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'value' => 'required',
            ]
        );
        
        $requestData = $request->only('value', 'description');
        
        $configuration = Configuration::findOrFail($id);
        $configuration->update($requestData);

        Session::flash('flash_message', 'Configuration updated!');
        Cache::forget('contacts');
        return redirect('admin/configuration');
    }
}
