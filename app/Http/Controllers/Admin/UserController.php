<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\RoleUser;
use Illuminate\Support\Facades\Auth;
use App\Location;
use App\LocationUser;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.property_transport_translations
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $authUser = Auth::user();
        $isRoleSuperAdmin = $authUser->hasRole(['super_admin']);
        $isRoleAdmin = $authUser->hasRole(['admin']);
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $user = User::where('username', 'LIKE', "%$keyword%")
                ->orWhere('first_name', 'LIKE', "%$keyword%")
                ->orWhere('last_name', 'LIKE', "%$keyword%")
                ->orderBy('id', 'asc')
                ->paginate($perPage);
        } else {
            $user = User::paginate($perPage);
        }
        $roles = [];

        foreach ($user as $key => $u) {
            if ($u->username == 'super_admin' && !$authUser->hasRole('super_admin')) {
                unset($user[$key]);
            }

            $userRole = $u->roles()->get();
            $roles[$u->id] = isset($userRole[0]) ? ['display_name' => $userRole[0]->display_name, 'name' => $userRole[0]->name] : ['display_name' => '', 'name' => ''];
        }
        $userAuth = Auth::user();
        return view('admin.user.index', compact('user', 'roles', 'isRoleSuperAdmin', 'isRoleAdmin', 'userAuth'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $rolesArr = Role::all();
        $user = null;
        $userRole = 3;
        $roles = [];

        foreach ($rolesArr as $key => $role) {
            if ($role['name'] == 'super_admin') continue;
            if (Auth::user()->can(['create_' . $role['name']]))
                $roles[$role['id']] = $role['display_name'];
            if ($role['name'] === 'agent')
                $userRole = $role['id'];
        };
        /*Get Locations*/
        $locationsData = Location::with('translations')/*->where(['parent_id' => 0])*/
        ->get()->toArray();
        $locations = $this->createLocationsForAdminList($locationsData);
        return view('admin.user.create', compact('user', 'roles', 'userRole', 'locations', 'locationsData'));
    }

    /**
     * Save a new user.
     *
     * @return \Illuminate\View\View
     */
    public function createUser(Request $request)
    {
        $this->validate($request, [
                'username' => 'required|unique:users|min:3|max:255',
                'first_name' => 'required|min:3|max:255',
                'last_name' => 'required|min:3|max:255',
                'email' => 'required|unique:users|email',
                'password' => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6',
                'role' => 'required',
                'is_enabled' => 'required|bool',
                'contact_skype' => 'max:255',
                'contact_viber' => 'max:255',
                'contact_line' => 'max:255',
                'contact_whatsapp' => 'max:255',
            ]
        );
        $rAll = $request->all();

        /*Added for hide prefix*/
        $rAll['prefix'] = 'Default';

        if (!isset($rAll['contact_skype']))
            $rAll['contact_skype'] = '';
        if (!isset($rAll['contact_viber']))
            $rAll['contact_viber'] = '';
        if (!isset($rAll['contact_line']))
            $rAll['contact_line'] = '';
        if (!isset($rAll['contact_whatsapp']))
            $rAll['contact_whatsapp'] = '';

        User::create($rAll);

        $user = User::where('username', '=', $request['username'])->first();
        $user->roles()->attach($request['role']);

        /*Assign Agent/Editor to location*/
        $roleName = Role::where(['id' => $rAll['role']])->get()->toArray()[0]['name'];
        if ($roleName == 'agent' || $roleName == 'editor') {
            foreach ($rAll as $key => $r) {
                if (explode('-', $key)[0] == 'assign') {
                    LocationUser::create(['user_id' => $user->id, 'location_id' => $r]);
                }
            }
        }
        return redirect('admin/user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::with('assignLocations')->findOrFail($id);
        $rolesArr = Role::all();
        $roles = [];
        $userRole = RoleUser::where('user_id', $id)->get();
        $userRole = isset($userRole[0]) ? $userRole[0]['role_id'] : '';

        foreach ($rolesArr as $role) {
            if ($role['name'] == 'super_admin') {
                if ($userRole == 1) {
                    $roles[$role['id']] = $role['display_name'];
                    break;
                } else {
                    continue;
                }
            }
            if (Auth::user()->can(['create_' . $role['name']]))
                $roles[$role['id']] = $role['display_name'];

            if ($role['id'] == $userRole)
                $roles[$role['id']] = $role['display_name'];
        };

        /*Get Locations*/
        $locationsData = Location::with('translations')/*->where(['parent_id' => 0])*/
        ->get()->toArray();
        $locations = $this->createLocationsForAdminList($locationsData);
        /*Create assign locations array*/
        $assignLocations = [];
        foreach ($user->assignLocations as $al) {
            $assignLocations[$al['location_id']] = $al['location_id'];
        }
        return view('admin.user.edit', compact('user', 'roles', 'userRole', 'locations', 'assignLocations', 'locationsData'));
    }

    /**
     * Update user account.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function editUser($id, Request $request)
    {
        $this->validate($request, [
//                'username' => 'unique:users,username,' . $id . '|required|min:3|max:255',
                'first_name' => 'required|min:3|max:255',
                'last_name' => 'required|min:3|max:255',
                'email' => 'unique:users,email,' . $id . '|required|email',
                'is_enabled' => 'required|bool',
                'contact_skype' => 'max:255',
                'contact_viber' => 'max:255',
                'contact_line' => 'max:255',
                'contact_whatsapp' => 'max:255',
            ]
        );
        $rAll = $request->all();

        //Added for hide prefix
        $rAll['prefix'] = 'Default';

        if (!$rAll['password']) {
            unset($rAll['password']);
            unset($rAll['password_confirmation']);
        } else {
            $this->validate($request, [
                    'password' => 'required|min:6|confirmed',
                    'password_confirmation' => 'required|min:6',
                ]
            );
            unset($rAll['password_confirmation']);
            $rAll['password'] = bcrypt($rAll['password']);
        }
        /*Assign Agent/Editor to location*/
        $roleName = Role::where(['id' => $rAll['role']])->get()->toArray()[0]['name'];
        if ($roleName == 'agent' || $roleName == 'editor') {
            LocationUser::where(['user_id' => $id])->delete();
        }
        foreach ($rAll as $key => $r) {
            if (explode('-', $key)[0] == 'assign') {
                if ($roleName == 'agent' || $roleName == 'editor') {
                    LocationUser::create(['user_id' => $id, 'location_id' => $r]);
                }
                unset($rAll[$key]);
            }
        }

        unset($rAll['_token']);
        $user = User::where('id', '=', $id)->first();
        $user->roles()->sync($rAll['role']);

        if ($rAll['role'] == 1 && Auth::user()->hasRole('super_admin')){
            $rAll['is_enabled'] = '1';
        }

        unset($rAll['role']);

        if (!isset($rAll['contact_skype']))
            $rAll['contact_skype'] = '';
        if (!isset($rAll['contact_viber']))
            $rAll['contact_viber'] = '';
        if (!isset($rAll['contact_line']))
            $rAll['contact_line'] = '';
        if (!isset($rAll['contact_whatsapp']))
            $rAll['contact_whatsapp'] = '';

        User::where('id', $id)->update($rAll);
        return redirect('admin/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function deleteUser($id)
    {
        LocationUser::where(['user_id' => $id])->delete();
        RoleUser::where('user_id', $id)->delete();
        User::where('id', $id)->delete();
        return redirect('admin/user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required|max:255',
                'locale' => 'required|max:2',
                'iso_639_3' => 'required|max:3',
                'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
            ]
        );

        $requestData = $request->all();
        $user = User::create($requestData);
        $this->saveImage($request, $user);
        Session::flash('success', 'New user successfully added!');
        return redirect('admin/user');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'locale' => 'required|max:2',
            'iso_639_3' => 'required|max:3',
        ]);

        $requestData = $request->all();
        $user = User::findOrFail($id);
        $user->update($requestData);
        $this->saveImage($request, $user);
        Session::flash('success', "User $user->username  successfully updated!");

        return redirect('admin/user');
    }


    /**
     * Get validation rules
     *
     * @param bool $create
     *
     * @return array
     */
    protected function getRules($create = null)
    {
        $rules = array(
            'name' => 'required|max:255',
            'email' => 'required|email',
            'sign' => 'required|max:3',
            'rate' => 'required|max:2',
            'is_enabled' => 'required|bool'
        );
        return $rules;
    }

    public function createLocationsForAdminList($locationsData)
    {
        $locMultiArr = $this->locationsMultiArray($locationsData);
        return $this->createArrayFromMultiArray($locMultiArr);
    }
}
