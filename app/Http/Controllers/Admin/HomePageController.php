<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;
use App\Location;
use App\HomeProperty;
use App\Property;

use App\HomeImage;

class HomePageController extends Controller
{
    private $perPage = 15;

    public function uploadImages()
    {
        $savedImages = null;
        $images = [];
        $savedImages = HomeImage::orderBy('sequence', 'asc')->get();
        if ($savedImages) {
            foreach ($savedImages as $si) {
                array_push($images, ['name' => $si->name, 'id' => $si->id, 'sequence' => $si->sequence]);
            }
        }
        return view('admin.home-page.upload-images', compact('images'));
    }

    public function uploadImage(Request $request)
    {
        $response = [];
        $max_width = 1024;
        $userId = Auth::user()->id;

        if (!is_dir(public_path() . '/images/home')) {
            mkdir(public_path() . '/images/home', 0777, true);
        }
        if (!is_dir(public_path() . '/images/home/original')) {
            mkdir(public_path() . '/images/home/original', 0777, true);
        }
        if (!is_dir(public_path() . '/images/home/display')) {
            mkdir(public_path() . '/images/home/display', 0777, true);
        }

        $files = $request->file('file');

        foreach ($files as $key => $image) {
            $imageName = preg_replace('/\..+$/', '', $image->getClientOriginalName()) . '_' . $userId . '_' . time() . '_' . rand(100, 999) . '.' . $image->getClientOriginalExtension();
            // Take image
            $Image = Image::make($image);

            // Save as original
            $Image->save(public_path('images/home/original/') . $imageName);

            // Save as to display
            if ($Image->width() > $max_width) {
                $Image->resize($max_width, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $Image->save(public_path('images/home/display/') . $imageName);
//            Create sequence
            $maxSequence = HomeImage::whereRaw('sequence = (select max(`sequence`) from home_images)')->get();
            $sequence = null;
            if (isset($maxSequence[0])) {
                $sequence = +$maxSequence[0]->sequence + 1;
            } else {
                $sequence = 1;
            }

            $newImg = HomeImage::create(['name' => $imageName, 'sequence' => $sequence, 'created_by' => $userId]);

            array_push($response, $newImg);
        }
        return response()->json(['success' => $response]);
    }

    public function deleteImage(Request $request)
    {
        $rAll = $request->all();
        if (isset($rAll['imgId']) && isset($rAll['sequence'])) {
            //Change sequence
            $allImagesAfter = HomeImage::where('sequence', '>', $rAll['sequence'])->get();
            if (isset($allImagesAfter)) {
                foreach ($allImagesAfter as $afterImg) {
                    $afterImg->sequence = $afterImg->sequence - 1;
                    $afterImg->save();
                }
            }
            \File::Delete(public_path('/images/home/display/') . $rAll['name']);
            \File::Delete(public_path('/images/home/original/') . $rAll['name']);
            HomeImage::where(['id' => $rAll['imgId']])->delete();
            return response()->json([], 200);
        }
    }

    public function selectProperty()
    {
        $locationsData = Location::with('translations')->orderBy('parent_id')->get()->toArray();
        $locations = $this->createLocationsList($locationsData);
        /*Get Categories*/
        $categories = $this->createCategoriesList();
        $selectedIds = [];
        $selected = HomeProperty::select(['property_id'])->get();
        foreach ($selected as $s) {
            array_push($selectedIds, $s->property_id);
        }

        $selectedProperties = Property::with(['translationsInLocale', 'imageForWebSearch'])
            ->whereIn('id', $selectedIds)->get();

        return view('admin.home-page.select-property', compact('locations', 'categories', 'selectedProperties'));
    }

    public function selectPropertySearch(Request $request)
    {
        $properties = $this->getSearchResult($request->all(), $this->perPage);
        $locationsData = Location::with('translations')->orderBy('parent_id')->get()->toArray();
        $locations = $this->createLocationsList($locationsData);
        /*Get Categories*/
        $categories = $this->createCategoriesList();
        $selected = HomeProperty::select(['property_id'])->get();
        $properties = $this->checkSelectedProperties($properties, $selected);
        $selectedIds = [];
        foreach ($selected as $s) {
            array_push($selectedIds, $s->property_id);
        }

        $selectedProperties = Property::with(['translationsInLocale', 'imageForWebSearch'])
            ->whereIn('id', $selectedIds)->get();
        return view('admin.home-page.select-property', compact('locations', 'categories', 'properties', 'selectedProperties'));
    }

    public function checkSelectedProperties($properties, $selectedProperties)
    {
        foreach ($properties as $property) {
            foreach ($selectedProperties as $sp) {
                if ($property->id == $sp->property_id) {
                    $property->isHomePage = true;
                }
            }
        }
        return $properties;
    }

    public function addProperty(Request $request)
    {
        $rAll = $request->all();
        $userId = Auth::user()->id;
        if (isset($rAll['property'])) {
            HomeProperty::create(['property_id' => $rAll['property'], 'created_by' => $userId]);
            $property = Property::with(['translationsInLocale', 'imageForWebSearch'])
                ->where(['id' => $rAll['property']])->first();
            return view('admin.components.property-select-home', compact('property'));
        } else {
            return Response::json(['error' => 'Error missing property id'], 433);
        }
    }

    public function deleteProperty(Request $request)
    {
        $rAll = $request->all();
        if (isset($rAll['property'])) {
            HomeProperty::where(['property_id' => $rAll['property']])->delete();
            return Response::json(['success' => 'Property deleted from home page'], 200);
        } else {
            return Response::json(['error' => 'Error missing property id'], 433);
        }
    }
}
