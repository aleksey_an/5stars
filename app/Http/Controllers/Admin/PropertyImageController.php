<?php

namespace App\Http\Controllers\Admin;

use App\Configuration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PropertyImage;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\LocationTranslation;
use App\PropertyCategoryTranslations;


class PropertyImageController extends Controller
{
    private $limit = 10;
    private $max_width = 1024;

    public function changeSequence(Request $request)
    {
        $rAll = $request->json()->all();
        $data = $rAll['data'];
        foreach ($data as $r) {
            PropertyImage::where(['id' => $r['id']])->update(['sequence' => $r['sequence']]);
        }
        return 'Images Sequence changed';
    }

    public function upload(Request $request, $propertyId)
    {
        $this->limit = env("MAX_COUNT_PROPERTY_IMAGES") ? env("MAX_COUNT_PROPERTY_IMAGES") : $this->limit;
        $userId = Auth::user()->id;

        if (!is_dir(public_path() . '/images/property')) {
            mkdir(public_path() . '/images/property', 0777, true);
        }
        if (!is_dir(public_path() . '/images/property/original')) {
            mkdir(public_path() . '/images/property/original', 0777, true);
        }
        if (!is_dir(public_path() . '/images/property/display')) {
            mkdir(public_path() . '/images/property/display', 0777, true);
        }
        if (!is_dir(public_path() . '/images/property/small')) {
            mkdir(public_path() . '/images/property/small', 0777, true);
        }
        if (!is_dir(public_path() . '/images/property/thumbnail')) {
            mkdir(public_path() . '/images/property/thumbnail', 0777, true);
        }
        if ($propertyId && isset($request->location_id) && isset($request->category_id)) {
            $categoryId = $request->category_id;
            $locationId = $request->location_id;

            $imagesList = PropertyImage::where(['property_id' => $propertyId])->get()->toArray();
            $files = $request->file('file');
            if (count($imagesList) + count($files) <= $this->limit) {
                $newImages = [];
                foreach ($files as $image) {
                    $imageName = $this->createNameForImage($locationId, $categoryId, time()) . rand(100, 999) . '_' . $propertyId . '.' . $image->getClientOriginalExtension();

//                  Saving an image as original, for display, small, thumbnail
                    $this->saveImageWithSize($image, $imageName);

//                   Create sequence
                    $maxSequence = PropertyImage::whereRaw('sequence = (select max(`sequence`) from property_images where property_id = ' . $propertyId . ')')->get();
                    $sequence = null;
                    if (isset($maxSequence[0])) {
                        $sequence = +$maxSequence[0]->sequence + 1;
                    } else {
                        $sequence = 1;
                    }
                    $img = PropertyImage::create([
                        'property_id' => $propertyId,
                        'name' => $imageName,
                        'detail' => '',
                        'sequence' => $sequence,
                        'is_enabled' => 1,
                        'created_by' => $userId
                    ]);
                    array_push($newImages, $img);
                }
                return response()->json(['success' => $newImages]);
            } else {
                return;
            }
        } else {
            $files = $request->file('file');
            $newImages = [];
            foreach ($files as $key => $image) {
                /*Origin*/
                $imageName = 'p_not_confirmed_' . time() . '_' . rand(100, 999) . '.' . $image->getClientOriginalExtension();

//              Saving an image as original, for display, small, thumbnail
                $this->saveImageWithSize($image, $imageName);

                array_push($newImages, ['name' => $imageName, 'sequence' => $key, 'is_enabled' => 1]);
            }
            return response()->json(['success' => $newImages]);
        }
    }

    private function saveImageWithSize($image, $imageName)
    {
        $max_width = $this->max_width;
        $max_width_conf = Configuration::select(['value'])->where(['key' => 'property_upload_max_width'])->first();
        if(isset($max_width_conf) && $max_width_conf->value){
            $max_width = $max_width_conf->value;
        }

        // Take image
        $Image = Image::make($image);

        //Watermark
        $watermark =  Image::make(resource_path('assets/img/watermark.png'));

        $resizePercentage = 70;//70% less then an actual image (play with this value)
        $watermarkSize = round($Image->width() * ((100 - $resizePercentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image

        // resize watermark width keep height auto
        $watermark->resize($watermarkSize, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        // Save as original
        $Image->save(public_path('images/property/original/') . $imageName);

        // Save as to display
        if ($Image->width() > $max_width) {
            $Image->resize($max_width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $Image->insert($watermark, 'center');
        $Image->save(public_path('images/property/display/') . $imageName);

        // Save as small
        $Image->fit(443, 300);
        $Image->save(public_path('images/property/small/' . $imageName));

        // Save as thumbnail
        $Image->fit(100, 100);
        $Image->save(public_path('images/property/thumbnail/' . $imageName));

        return false;
    }

    public function changeEnableStatus(Request $request)
    {
        $rAll = $request->json()->all();
        PropertyImage::where(['id' => $rAll['imgId']])->update(['is_enabled' => $rAll['is_enable']]);
        return $rAll;
    }

    public function changeDescription(Request $request)
    {
        $rAll = $request->json()->all();
        PropertyImage::where(['id' => $rAll['imgId']])->update(['detail' => $rAll['detail']]);
        return $rAll;
    }

    public function delete(Request $request)
    {
        $rAll = $request->json()->all();
        \File::Delete(public_path('/images/property/original/') . $rAll['name']);
        \File::Delete(public_path('/images/property/display/') . $rAll['name']);
        \File::Delete(public_path('/images/property/small/') . $rAll['name']);
        \File::Delete(public_path('/images/property/thumbnail/') . $rAll['name']);
        PropertyImage::where(['id' => $rAll['imgId']])->delete();
        return response()->json(['id' => $rAll['imgId'], 'name' => count($rAll['name'])]);
    }

    public function createNameForImage($locationId, $categoryId, $time = 0)
    {
        $locationName = '';
        $locationData = LocationTranslation::where(['location_id' => $locationId, 'locale' => config('app.locale')])->get();
        if (isset($locationData[0]) && $locationData[0]->name) {
            $locationName = explode(' ', $locationData[0]->name);
            foreach ($locationName as $key => $l) {
                if ($l == "\\" || $l == "." || $l == "," || $l == "/") {
                    unset($locationName[$key]);
                }
            }
            $locationName = implode('_', $locationName);
        }
        $categoryName = '';
        $categoryData = PropertyCategoryTranslations::where(['property_category_id' => $categoryId, 'locale' => config('app.locale')])->get();
        if (isset($categoryData[0]) && $categoryData[0]->name) {
            $categoryName = explode(' ', $categoryData[0]->name);
            foreach ($categoryName as $key => $k) {
                if ($k == "\\" || $k == "." || $k == "," || $k == "/") {
                    unset($categoryName[$key]);
                }
            }
            $categoryName = implode('_', $categoryName);
        }
        return $locationName . '_' . $categoryName . '_' . $time;
    }
}
