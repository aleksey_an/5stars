<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language;
use App\PropertyTransport;
use App\PropertyTransportTranslations;
use App\PropertyTransportData;
use Illuminate\Support\Facades\Auth;
use Session;

class PropertyTransportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transports = PropertyTransport::with('translations')->get();
//        dd($transports);
        return view('admin.property-transport.index', compact('transports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        $transport = null;

        return view('admin.property-transport.create', compact('transport', 'languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestTranslations = $this->processingDescriptionRequest($request);
        $this->validate($request, [
                'is_enabled' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]
        );
        $rAll = $request->all();
        $sequence = null;
        $maxSequence = PropertyTransport::whereRaw('sequence = (select max(`sequence`) from property_transports )')->get();
        if (isset($maxSequence[0])) {
            $sequence = +$maxSequence[0]->sequence + 1;
        } else {
            $sequence = 1;
        }

        $rAll['sequence'] = $sequence;
        $newTransport = PropertyTransport::create($rAll);

        foreach ($requestTranslations as $keyLocale => $val) {
            PropertyTransportTranslations::create([
                'property_transport_id' => $newTransport->id,
                'locale' => $keyLocale,
                'name' => $requestTranslations[$keyLocale]['name']
            ]);
        }
        if (isset($rAll['image'])) {
            $this->uploadImage($request, $newTransport->id, false);
        }
        return redirect('admin/transport');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transport = PropertyTransport::where(['id' => $id])->with('translations')->get();
        $transport = $transport[0];
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }

        /*Create facility translations*/
        $translations = $this->createTranslationsList(isset($transport['translations']) ? $transport['translations'] : null);

        return view('admin.property-transport.edit', compact('transport', 'languages', 'translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /*Create request translations array*/
        $requestTranslations = $this->processingDescriptionRequest($request);

        $this->validate($request, [
                'is_enabled' => 'required',
            ]
        );
        $rAll = $request->all();

        $userId = Auth::user()->id;

        PropertyTransport::where(['id' => $id])->update(['is_enabled' => $rAll['is_enabled'], 'updated_by' => $userId]);

        if (isset($rAll['image'])) {
            $this->uploadImage($request, $id, true);
        }

        foreach ($requestTranslations as $keyLocale => $val) {
            $checkTranslation = PropertyTransportTranslations::where(['property_transport_id' => $id, 'locale' => $keyLocale])->get()->toArray();
            if (isset($checkTranslation[0])) {
                $checkLanguage = Language::where(['locale' => $keyLocale])->get();
                if (isset($checkLanguage[0]) && $checkLanguage[0]->is_enabled) {
                    PropertyTransportTranslations::where(['property_transport_id' => $id, 'locale' => $keyLocale])->update([
                        'name' => $requestTranslations[$keyLocale]['name']
                    ]);
                }
            } else {
                PropertyTransportTranslations::create([
                    'property_transport_id' => $id,
                    'locale' => $keyLocale,
                    'name' => $requestTranslations[$keyLocale]['name']
                ]);
            }
        }
        Session::flash('success', 'Property Transport successfully updated !');
        return redirect(redirect()->getUrlGenerator()->previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkImage = PropertyTransport::where(['id' => $id])->get()->toArray();
        if (isset($checkImage[0]['image'])) {
            \File::Delete(public_path('/images/transport/') . $checkImage[0]['image']);
        }
        PropertyTransportData::where(['property_transport_id' => $id])->delete();
        PropertyTransportTranslations::where(['property_transport_id' => $id])->delete();
        PropertyTransport::where(['id' => $id])->delete();
        return redirect(redirect()->getUrlGenerator()->previous());
    }

    public function uploadImage($request, $transportId, $isCheck)
    {
        if (!is_dir(public_path() . '/images/transport')) {
            mkdir(public_path() . '/images/transport');
        }
        if ($isCheck) {
            $checkImage = PropertyTransport::where(['id' => $transportId])->get()->toArray();
            if (isset($checkImage[0]['image'])) {
                \File::Delete(public_path('/images/transport/') . $checkImage[0]['image']);
            }
        }
        $image = $request->file('image');
        $input['imagename'] = 't_' . time() . '_orig' . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/transport');
        $image->move($destinationPath, $input['imagename']);
        PropertyTransport::where(['id' => $transportId])->update(['image' => $input['imagename']]);
    }
}
