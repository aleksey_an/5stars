<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Session;
use App\Currency;
use Equentor\LaravelOpenExchangeRates\Client;

class CurrencyController extends Controller
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $currency = Currency::where('name', 'LIKE', "%$keyword%")
                ->orWhere('code', 'LIKE', "%$keyword%")
                ->orderBy('sequence', 'asc')
                ->paginate($perPage);
        } else {
            $currency = Currency::paginate($perPage);
        }
//        $this->convert();
        return view('admin.currency.index', compact('currency'));
    }

    /**
     * Currency Convector
     */
    public function convert()
    {
        $currencies = Currency::all()->toArray();
        $codes = '';
        foreach ($currencies as $currency) {
            $codes = $codes . $currency['code'] . ',';
        }
        $coefficients = $this->client->latest($codes);
        $coefficients = $coefficients['rates'];
        if (isset($coefficients)) {
            foreach ($coefficients as $key => $coefficient) {
                Currency::where(['code' => $key])->update(['rate' => $coefficient]);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.currency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->getRules(true));

        $rAll = $request->all();
        $sequence = null;
        $maxSequence = Currency::whereRaw('sequence = (select max(`sequence`) from currencies  )')->get();

        if (isset($maxSequence[0])) {
            $sequence = +$maxSequence[0]->sequence + 1;
        } else {
            $sequence = 1;
        }
        $rAll['sequence'] = $sequence;
        Currency::create($rAll);

        Session::flash('success', 'New currency successfully added!');

        return redirect('admin/currency');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $currency = Currency::findOrFail($id);

        return view('admin.currency.show', compact('currency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $currency = Currency::findOrFail($id);

        return view('admin.currency.edit', compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $this->validate($request, $this->getRules());

        $requestData = $request->all();

        $currency = Currency::findOrFail($id);
        $currency->update($requestData);


        Session::flash('success', "$currency->name currency successfully updated!");

        return redirect('admin/currency');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $currency = Currency::findOrFail($id);
        $curr_name = $currency->name;

        Currency::destroy($id);
        Session::flash('success', "$curr_name currency successfully deleted!");

        return redirect('admin/currency');
    }

    /**
     * Get validation rules
     *
     * @param bool $create
     *
     * @return array
     */
    protected function getRules($create = null)
    {
        $rules = array(
            'name' => 'required|max:255',
            'code' => 'required|max:10',
            'sign' => 'required|max:3',
            'rate' => 'required|max:10',
            'is_enabled' => 'required|bool'
        );

        return $rules;
    }

}
