<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language;
use App\Location;
use Illuminate\Support\Facades\Auth;
use Session;

use App\Metro;
use App\MetroTranslation;

class MetroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metros = Metro::with('translations', 'location')->get();
        return view('admin.metro.index', compact('metros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        /*Get Locations*/
        $locationsData = Location::with('translations')->where(['parent_id' => 0])->get()->toArray();
        $locations = $this->createLocationsList($locationsData);
        $metro = null;
        return view('admin.metro.create', compact('metro', 'languages', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*Create request translations array*/
        $requestTranslations = $this->processingDescriptionRequest($request);
        $this->validate($request, [
                'address' => 'required|max:1000',
                'lat' => 'required|numeric',
                'lng' => 'required|numeric',
                'location_id' => 'required|numeric',
                'is_enabled' => 'required|numeric'
            ]
        );
        $rAll = $request->all();

        $newMetro = Metro::create([
            'lat' => $rAll['lat'],
            'lng' => $rAll['lng'],
            'address' => $rAll['address'],
            'is_enabled' => $rAll['is_enabled'],
            'location_id' => $rAll['location_id'],
            'created_by' => Auth::user()->id
        ]);
        foreach ($requestTranslations as $keyLocale => $val) {
            MetroTranslation::create([
                'metro_id' => $newMetro->id,
                'locale' => $keyLocale,
                'name' => $requestTranslations[$keyLocale]['name']
            ]);
        }
        return redirect('admin/metro');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $metro = Metro::with('translations')->where(['id' => $id])->get();
        $metro = $metro[0];
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        /*Get Locations*/
        $locationsData = Location::with('translations')->where(['parent_id' => 0])->get()->toArray();
        $locations = $this->createLocationsList($locationsData);

        /*Create metro translations*/
        $translations = $this->createTranslationsList(isset($metro['translations']) ? $metro['translations'] : null);
        return view('admin.metro.edit', compact('metro', 'languages', 'locations', 'translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Create request translations array
        $requestTranslations = $this->processingDescriptionRequest($request);
        $this->validate($request, [
                'lat' => 'required|numeric',
                'lng' => 'required|numeric',
                'location_id' => 'required|numeric',
                'is_enabled' => 'required|numeric'
            ]
        );
        $rAll = $request->all();

        Metro::where(['id' => $id])->update([
            'lat' => $rAll['lat'],
            'lng' => $rAll['lng'],
            'address' => $rAll['address'],
            'location_id' => $rAll['location_id'],
            'is_enabled' => $rAll['is_enabled'],
            'updated_by' => Auth::user()->id
        ]);

        foreach ($requestTranslations as $keyLocale => $val) {
            $checkTranslation = MetroTranslation::where(['metro_id' => $id, 'locale' => $keyLocale])->get()->toArray();
            if (isset($checkTranslation[0])) {
                $checkLanguage = Language::where(['locale' => $keyLocale])->get();
                if (isset($checkLanguage[0]) && $checkLanguage[0]->is_enabled) {
                    MetroTranslation::where(['metro_id' => $id, 'locale' => $keyLocale])->update([
                        'name' => $requestTranslations[$keyLocale]['name']
                    ]);
                }
            } else {
                MetroTranslation::create([
                    'metro_id' => $id,
                    'locale' => $keyLocale,
                    'name' => $requestTranslations[$keyLocale]['name']
                ]);
            }
        }
        Session::flash('success', 'Metro station successfully updated !');
        return redirect(redirect()->getUrlGenerator()->previous());
    }

    public function createLocationsList($locationsData)
    {
        $locMultiArr = $this->locationsMultiArray($locationsData);
        return $this->createArrayFromMultiArray($locMultiArr);
    }
}
