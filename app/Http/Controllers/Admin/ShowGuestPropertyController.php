<?php

namespace App\Http\Controllers\Admin;

use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Property;

use App\Http\Controllers\Web\WebController as Web;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use Symfony\Component\Routing\Annotation\Route;

class ShowGuestPropertyController extends Controller
{
    public function index(Request $request)
    {
        $web = new Web();
        $rAll = $request->all();
        $property = '';
        $favoriteProperties = '';
        $viewedProperties = '';

        if (isset($rAll['property'])) {
            $property = Property::with('translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale', 'imageForWebSearch')->where(['id' => $rAll['property']])->get();
            $property = $web->createPropertyHref($property);
            $property = $property[0];
        }

        if (isset($rAll['favorites'])) {
//            $favorites = explode(';', $rAll['favorites']);
            $favorites = json_decode($rAll['favorites']);
            $favoriteProperties = Property::with('translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale', 'imageForWebSearch')->whereIn('id', $favorites)->get();
            $favoriteProperties = $web->createPropertyHref($favoriteProperties);
        }

        if (isset($rAll['viewed'])) {
//            $viewed = explode(';', $rAll['viewed']);
            $viewed = json_decode($rAll['viewed']);
            $viewedProperties = Property::with('translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale', 'imageForWebSearch')->whereIn('id', $viewed)->get();
            $viewedProperties = $web->createPropertyHref($viewedProperties);
        }

        return view('admin.show-guest-property.index', [
            'property' => $property,
            'favoriteProperties' => $favoriteProperties,
            'viewedProperties' => $viewedProperties
        ]);
    }
}
