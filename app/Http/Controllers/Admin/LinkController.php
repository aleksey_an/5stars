<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Link;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $link = Link::where('url', 'LIKE', "%$keyword%")
                    ->orderBy('sequence', 'asc')
                    ->paginate($perPage);
        } else {
            $language = Language::paginate($perPage);
        }

        return view('admin.link.index', compact('link'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return view('admin.link.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, $this->getRules(true));

        $requestData = $request->all();
         
        $language = Language::create($requestData);
        $this->saveImage($request, $language);
        
        Session::flash('success', 'New language successfully added!');
        
        return redirect('admin/language');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $language = Language::findOrFail($id);

        return view('admin.language.show', compact('language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $language = Language::findOrFail($id);

        return view('admin.language.edit', compact('language'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {

        $this->validate($request, $this->getRules());
        
        $requestData = $request->all();

        $language = Language::findOrFail($id);
        $language->update($requestData);
        
        $this->saveImage($request, $language);
        
        Session::flash('success', "$language->name language successfully updated!");

        return redirect('admin/language');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        $language = Language::findOrFail($id);
        $lang_name = $language->name;
        
        if (file_exists($language->flag)) {
            unlink($language->flag);
        }
        
        Language::destroy($id);
        Session::flash('success', "$lang_name language successfully deleted!");

        return redirect('admin/language');
    }

    /**
     * Save Link Image in /img/link folder
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Model $link
     *
     * @return bool
     */
    protected function saveImage($request, $link) {
        // checking if language's folder exists
        if (!is_dir(public_path() . '/img/link')) {
            mkdir(public_path() . '/img/link');
        }

        return false;
    }
    
    
    /**
     * Get validation rules
     *
     * @param bool $create
     *
     * @return array
     */
    protected function getRules($create = null) {
         $rules = array(
            'name' => 'required|max:255',
            'locale' => 'required|max:2',
            'iso_639_3' => 'required|max:3',
            'is_enabled' => 'required|bool'
            );
         
        if ($create) {
            $rules['image'] =  'required|image|mimes:jpeg,png,jpg|max:2048';
        }
        return $rules;
    }
}
