<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\ArticleTranslation;
use App\Language;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Session;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::with('translations.lang')->get();
        return view('admin.article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        return view('admin.article.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestTranslations = $this->processingArticleTabsDescription($request);

        $this->validate($request, [
                'is_enabled' => 'required|numeric'
            ]
        );

        $rAll = $request->all();


        $newArticle = Article::create([
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
            'is_enabled' => $rAll['is_enabled']
        ]);

        if (isset($rAll['image'])) {
            $this->uploadImage($request, $newArticle->id, false);
        }

        foreach ($requestTranslations as $keyLocale => $val) {
            ArticleTranslation::create([
                'article_id' => $newArticle->id,
                'title' => $requestTranslations[$keyLocale]['name'],
                'description' => $requestTranslations[$keyLocale]['description'],
                'short_description' => $requestTranslations[$keyLocale]['short_description'],
                'locale' => $keyLocale,
            ]);
        }

        return redirect('admin/article');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::with('translations')->where(['id' => $id])->first();
        $languages = [];
        $responseLang = Language::where(['is_enabled' => true])->get();
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        $translations = $this->createTranslationsList(isset($article['translations']) ? $article['translations'] : null);

        return view('admin.article.edit', compact('article', 'languages', 'translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Create request translations array
        $requestTranslations = $this->processingArticleTabsDescription($request);
        $this->validate($request, [
                'is_enabled' => 'required|numeric'
            ]
        );

        $rAll = $request->all();

        Article::where(['id' => $id])->update([
            'updated_by' => Auth::user()->id,
            'is_enabled' => $rAll['is_enabled']
        ]);

        if (isset($rAll['image'])){
            $this->uploadImage($request, $id, true);
        }

        foreach ($requestTranslations as $keyLocale => $val) {
            $checkTranslation = ArticleTranslation::where(['article_id' => $id, 'locale' => $keyLocale])->get()->toArray();
            if (isset($checkTranslation[0])) {
                $checkLanguage = Language::where(['locale' => $keyLocale])->get();
                if (isset($checkLanguage[0]) && $checkLanguage[0]->is_enabled) {
                    ArticleTranslation::where(['article_id' => $id, 'locale' => $keyLocale])->update([
                        'title' => $requestTranslations[$keyLocale]['name'],
                        'description' => $requestTranslations[$keyLocale]['description'],
                        'short_description' => $requestTranslations[$keyLocale]['short_description'],
                    ]);
                }
            } else {
                ArticleTranslation::create([
                    'article_id' => $id,
                    'title' => $requestTranslations[$keyLocale]['name'],
                    'description' => $requestTranslations[$keyLocale]['description'],
                    'short_description' => $requestTranslations[$keyLocale]['short_description'],
                    'locale' => $keyLocale,
                ]);
            }
        }
        Session::flash('success', 'Article successfully updated !');
        return redirect(redirect()->getUrlGenerator()->previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkImage = Article::where(['id' => $id])->get()->toArray();
        if (isset($checkImage[0]['image'])) {
            \File::Delete(public_path('/images/article/original/') . $checkImage[0]['image']);
            \File::Delete(public_path('/images/article/display/') . $checkImage[0]['image']);
            \File::Delete(public_path('/images/article/small/') . $checkImage[0]['image']);
        }
        ArticleTranslation::where(['article_id' => $id])->delete();
        Article::where(['id' => $id])->delete();

        return redirect(redirect()->getUrlGenerator()->previous());
    }

    private $max_width = 1024;

    public function uploadImage($request, $articleId, $isCheck)
    {
        $originalPath = '/images/article/original';
        $display = '/images/article/display';
        $small = '/images/article/small';

        if (!is_dir(public_path() . $originalPath)) {
            mkdir(public_path() . $originalPath);
        }
        if (!is_dir(public_path() . $display)) {
            mkdir(public_path() . $display);
        }
        if (!is_dir(public_path() . $small)) {
            mkdir(public_path() . $small);
        }

        if ($isCheck) {
            $checkImage = Article::where(['id' => $articleId])->get()->toArray();
            if (isset($checkImage[0]['image'])) {
                \File::Delete(public_path($originalPath) . '/' . $checkImage[0]['image']);
                \File::Delete(public_path($display) . '/' . $checkImage[0]['image']);
                \File::Delete(public_path($small) . '/' . $checkImage[0]['image']);
            }
        }
        $image = $request->file('image');

        $Image = Image::make($image);
        $imageName = 'a_' . time() . '.' . $image->getClientOriginalExtension();
        // Save as original
        $Image->save(public_path($originalPath) . '/' . $imageName);

        // Save as to display
        if ($Image->width() > $this->max_width) {
            $Image->resize($this->max_width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $Image->save(public_path($display) . '/' . $imageName);

        // Save as small
        $Image->fit(443, 300);
        $Image->save(public_path($small) . '/' . $imageName);

        Article::where(['id' => $articleId])->update(['image' => $imageName]);
    }

    /**
     * Create and validate request beginning from  "description-" locale- "name, description"
     */
    public function processingArticleTabsDescription($request)
    {
        /*Create request translations array*/
        $rAll = $request->all();
        $result = [];
        foreach ($rAll as $key => $r) {
            if (explode('-', $key)[0] == 'description') {
                if (explode('-', $key)[2] == 'name') {
                    $result[explode('-', $key)[1]]['name'] = $r;
                }
                if (explode('-', $key)[2] == 'description') {
                    $result[explode('-', $key)[1]]['description'] = $r ? $r : '';
                }
                if (explode('-', $key)[2] == 'short_description') {
                    $result[explode('-', $key)[1]]['short_description'] = $r ? $r : '';
                }
            }
        }
        /*Filter property translations*/
        foreach ($result as $key => $r) {
            if (!$r['name']) {
                unset($result[$key]);
            }
        }
        /*Validate code translations*/
        if (count($result) == 0 || !isset($result[config('app.locale')])) {
            Session::flash('error', 'You must fill out a Subject in at ' . config('app.locale') . ' language');
            $this->validate($request, [
                'description-' . config('app.locale') . '-name' => 'required|min:1|max:255'
            ]);
        }
        return $result;
    }
}
