<?php

namespace App\Http\Controllers\Admin;

use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Configuration;
use App\LocationImage;

class LocationImageController extends Controller
{
    private $max_width = 1024;

    public function upload(Request $request, $locationId)
    {
        $limit = env('MAX_COUNT_LOCATION_IMAGES');
        $userId = Auth::user()->id;
        if (!is_dir(public_path() . '/images/location')) {
            mkdir(public_path() . '/images/location', 0777, true);
        }
        if (!is_dir(public_path() . '/images/location/original')) {
            mkdir(public_path() . '/images/location/original', 0777, true);
        }
        if (!is_dir(public_path() . '/images/location/display')) {
            mkdir(public_path() . '/images/location/display', 0777, true);
        }
        if (!is_dir(public_path() . '/images/location/small')) {
            mkdir(public_path() . '/images/location/small', 0777, true);
        }
        if (!is_dir(public_path() . '/images/location/thumbnail')) {
            mkdir(public_path() . '/images/location/thumbnail', 0777, true);
        }

        if ($locationId && isset($request->location_name)) {
            $imagesList = LocationImage::where(['location_id' => $locationId])->get()->toArray();
            $files = $request->file('file');
            $locationName = strtolower(str_replace(' ', '-', $request->location_name));
            if (count($imagesList) + count($files) <= $limit) {
                $newImages = [];
                foreach ($files as $image) {
                    $imageName = $locationName . '_' . time() . '_'. rand(100, 999) . '_' . $locationId . '.' . $image->getClientOriginalExtension();

                    //Saving an image as original, for display, small, thumbnail
                    $this->saveImageWithSize($image, $imageName);

                    //Create sequence
                    $maxSequence = LocationImage::whereRaw('sequence = (select max(`sequence`) from location_images where location_id = ' . $locationId . ')')->get();
                    $sequence = null;
                    if (isset($maxSequence[0])) {
                        $sequence = +$maxSequence[0]->sequence + 1;
                    } else {
                        $sequence = 1;
                    }
                    $img = LocationImage::create([
                        'location_id' => $locationId,
                        'name' => $imageName,
                        'sequence' => $sequence,
                        'is_enabled' => 1,
                        'created_by' => $userId
                    ]);
                    array_push($newImages, $img);
                }
                return response()->json(['success' => array_reverse($newImages)]);
            } else {
                return;
            }
        } else {
            $files = $request->file('file');
            $newImages = [];
            foreach ($files as $key => $image) {
                $imageName = 'l_not_confirmed_' . time() . '_' . rand(100, 999) . '.' . $image->getClientOriginalExtension();

                //Saving an image as original, for display, small, thumbnail
                $this->saveImageWithSize($image, $imageName);
                array_push($newImages, ['name' => $imageName, 'sequence' => $key, 'is_enabled' => 1]);
            }
            return response()->json(['success' => $newImages]);
        }
    }

    private function saveImageWithSize($image, $imageName)
    {
        $max_width = $this->max_width;
        $max_width_conf = Configuration::select(['value'])->where(['key' => 'location_upload_max_width'])->first();
        if(isset($max_width_conf) && $max_width_conf->value){
            $max_width = $max_width_conf->value;
        }

        // Take image
        $Image = Image::make($image);

        // Save as original
        $Image->save(public_path('images/location/original/') . $imageName);

        // Save as to display
        if ($Image->width() > $max_width) {
            $Image->resize($max_width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $Image->save(public_path('images/location/display/') . $imageName);

        // Save as small
        $Image->fit(443, 300);
        $Image->save(public_path('images/location/small/' . $imageName));

        // Save as thumbnail
        $Image->fit(100, 100);
        $Image->save(public_path('images/location/thumbnail/' . $imageName));
        return false;
    }

    public function changeEnableStatus(Request $request)
    {
        $rAll = $request->json()->all();
        LocationImage::where(['id' => $rAll['imgId']])->update(['is_enabled' => $rAll['is_enable']]);
        return $rAll;
    }

    public function changeSequence(Request $request)
    {
        $rAll = $request->json()->all();
        $data = $rAll['data'];
        foreach ($data as $r) {
            LocationImage::where(['id' => $r['id']])->update(['sequence' => $r['sequence']]);
        }
        return 'Images Sequence changed';
    }

    public function delete(Request $request)
    {
        $rAll = $request->json()->all();
        \File::Delete(public_path('/images/location/original/') . $rAll['name']);
        \File::Delete(public_path('/images/location/display/') . $rAll['name']);
        \File::Delete(public_path('/images/location/small/') . $rAll['name']);
        \File::Delete(public_path('/images/location/thumbnail/') . $rAll['name']);
        LocationImage::where(['id' => $rAll['imgId']])->delete();
        return response()->json(['id' => $rAll['imgId'], 'name' => count($rAll['name'])]);
    }
}
