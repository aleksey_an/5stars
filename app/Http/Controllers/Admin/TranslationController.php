<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;
use App\Language;
use App\Localization;
use Illuminate\Support\Facades\Cache;

class TranslationController extends Controller
{
    public function index()
    {
        $languagesNames = [];
        $translations = [];
        $languages = Language::where(['is_enabled_web' => 1])->get();

        foreach ($languages as $lang) {
            $languagesNames[$lang->locale] = $lang->name;
            if (File::exists(resource_path() . '/lang/initial/' . $lang->locale . '/common.php')) {
                $data = File::getRequire(resource_path() . '/lang/initial/' . $lang->locale . '/common.php');
                $data = $this->checkRecord($data, $lang->locale, 'common', true);
                Cache::forget('localization-' . $lang->locale);
                $translations[$lang->locale] = $data;
            } else {
                $translations[$lang->locale] = [];
            }
        }
        return view('admin.translation.index', compact('translations', 'languagesNames'));
    }

    private function checkRecord($data, $locale, $block, $checkAndDelete)
    {
        $result = [];
        $delete = [];
        $addRecord = [];
        $allRecordsArr = [];

        $allRecords = Localization::where(['block' => $block, 'locale' => $locale])->select(['key', 'value', 'locale', 'id'])->get()->toArray();
        foreach ($allRecords as $ar) {
            $allRecordsArr[$ar['key']] = $ar['value'];
        }
        foreach ($data as $key => $val) {
            foreach ($allRecords as $ar) {
                if (!isset($data[$ar['key']])) {
                    $delete[$ar['key']] = $ar;
                }
            }
        }
        if (count($delete) > 0 && $checkAndDelete) {
            $arrIds = [];
            foreach ($delete as $rId) {
                array_push($arrIds, $rId['id']);
            }
            Localization::destroy($arrIds);
        }

        foreach ($data as $key => $value) {
            if (!array_key_exists($key, $allRecordsArr)) {
                $addRecord[$key] = $value;
            } else {
                $result[$key] = $allRecordsArr[$key];
            }
        }

        if (count($addRecord) > 0) {
            foreach ($addRecord as $key => $value) {
                $isExist = Localization::firstOrCreate(['block' => $block, 'value' => $value, 'key' => $key, 'locale' => $locale]);
                $result[$key] = $isExist->value;
            }
        }
        return $result;
    }

    public function updateAll(Request $request)
    {
        $rAll = $request->all();
        if (isset($rAll['data']) && isset($rAll['block'])) {
            $data = $rAll['data'];
            $block = $rAll['block'];
            foreach ($data as $locale => $toUpload) {
                if (isset($locale) && $block && count($toUpload) > 0) {
                    $this->updateDataToTable($toUpload, $locale, $block);
                    Cache::forget('localization-' . $locale);
                }
            }
            return response()->json(['success' => 'Translations is updated '], 200);
        } else {
            return response()->json(['error' => 'Wrong data! Bad request!'], 400);
        }
    }

    private function updateDataToTable($arr, $locale, $block)
    {
        foreach ($arr as $key => $value) {
            Localization::where(['block' => $block, 'key' => $key, 'locale' => $locale])->update(['value' => $value]);
        }
    }
}
