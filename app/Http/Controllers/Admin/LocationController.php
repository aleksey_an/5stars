<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\LocationUser;
use App\Property;
use Illuminate\Http\Request;
use App\Language;
use App\Location;
use App\LocationTranslation;
use App\LocationImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Session;

class LocationController extends Controller
{
    public function index()
    {
        $locations = Location::with('translations')->where(['parent_id' => 0])->get();
        return view('admin.location.index', compact('locations'));
    }

    public function create(Request $request)
    {
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        $location = null;
        $translations = null;
        $parentId = $request->get('parent');
        return view('admin.location.create', compact('languages', 'translations', 'location', 'parentId'));
    }

    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $this->validate($request, [
                'code' => 'unique:locations|min:3|max:3',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'video_url' => 'max:999'
            ]
        );

        /*Create request translations array*/
        $requestTranslations = $this->processingTranslationsInRequest($request, 'location_translations', null, null, true);

        $rAll = $request->all();
        $sequence = null;
        $maxSequence = Location::whereRaw('sequence = (select max(`sequence`) from locations where parent_id = ' . (isset($rAll['parent_id']) ? $rAll['parent_id'] : 0) . ')')->get();

        if (isset($maxSequence[0])) {
            $sequence = +$maxSequence[0]->sequence + 1;
        } else {
            $sequence = 1;
        }

        $rAll['sequence'] = $sequence;
        $rAll['code'] = (isset($rAll['parent_id']) ? '' : strtoupper($rAll['code']));
        $rAll['parent_id'] = (isset($rAll['parent_id']) ? $rAll['parent_id'] : 0);
        $rAll['video_url'] = (isset($rAll['video_url']) ? $rAll['video_url'] : '');
        $rAll['created_by'] = $userId;
        $rAll['lat'] = (isset($rAll['lat']) ? $rAll['lat'] : 0);
        $rAll['lng'] = (isset($rAll['lng']) ? $rAll['lng'] : 0);
        $createdLocale = Location::create($rAll);

        foreach ($requestTranslations as $keyLocale => $val) {
            LocationTranslation::create([
                'location_id' => $createdLocale->id,
                'locale' => $keyLocale,
                'url' => $requestTranslations[$keyLocale]['url'],
                'name' => $requestTranslations[$keyLocale]['name'],
                'description' => $requestTranslations[$keyLocale]['description'],
                'created_by' => $userId
            ]);
        }

        if (isset($rAll['images'])) {
            $images = json_decode($rAll['images']);
            foreach ($images as $image) {
                $imageExpansion = explode('.', $image->name);
                $imageExpansion = $imageExpansion[count($imageExpansion) - 1];
                $locationName = '';
                foreach ($requestTranslations as $keyLocale => $val) {
                    if ($keyLocale == config("app.fallback_locale")) {
                        $locationName = $requestTranslations[$keyLocale]['name'];
                    }
                }

                $newImageName = $locationName . '_' . time() . '_' . rand(100, 999) . '_' . $createdLocale->id . '.' . $imageExpansion;
                rename(
                    public_path('images/location/original/' . $image->name),
                    public_path('images/location/original/' . $newImageName)
                );
                rename(
                    public_path('images/location/display/' . $image->name),
                    public_path('images/location/display/' . $newImageName)
                );
                rename(
                    public_path('images/location/small/' . $image->name),
                    public_path('images/location/small/' . $newImageName)
                );
                rename(
                    public_path('images/location/thumbnail/' . $image->name),
                    public_path('images/location/thumbnail/' . $newImageName)
                );
                LocationImage::create([
                    'location_id' => $createdLocale->id,
                    'name' => $newImageName,
                    'sequence' => $image->sequence,
                    'is_enabled' => $image->is_enabled,
                    'created_by' => $userId
                ]);
            }
        }
        Cache::forget('locations');

        if (isset($rAll['parent_id']) && $rAll['parent_id'] != 0) {
            return redirect('admin/location/' . $rAll['parent_id'] . '/edit');
        }

        return redirect('admin/location');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        $locations = Location::with('translations', 'images')->where(['id' => $id])->get();
        $location = $locations[0];
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];

        /*Create location translations*/
        $translations = $this->createTranslationsList(isset($location['translations']) ? $location['translations'] : null);
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }

        $childLocations = Location::with('translations')->where(['parent_id' => $id])->get();
        $parentId = $request->get('parent');

        return view('admin.location.edit', compact('location', 'translations', 'languages', 'childLocations', 'parentId'));
    }

    public function update(Request $request, $idLocation)
    {
        $userId = Auth::user()->id;
        $rAll = $request->all();
        /*Create request translations array*/
        $requestTranslations = $this->processingTranslationsInRequest($request, 'location_translations', 'location_id', $idLocation, true);
        $this->validate($request, [
                'code' => 'min:3|max:3',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'video_url' => 'max:999'
            ]
        );

        Location::where(['id' => $idLocation])->update([
            'is_enabled' => $rAll['is_enabled'],
            'video_url' => $rAll['video_url'] ? $rAll['video_url'] : '',
            'updated_by' => $userId,
            'lat' => (isset($rAll['lat']) ? $rAll['lat'] : 0),
            'lng' => (isset($rAll['lng']) ? $rAll['lng'] : 0),
        ]);

        if (isset($rAll['image'])) {
            $this->uploadImage($request, $idLocation, true);
        }

        foreach ($requestTranslations as $keyLocale => $val) {
            $checkLocTranslation = LocationTranslation::where(['location_id' => $idLocation, 'locale' => $keyLocale])->get()->toArray();
            if (isset($checkLocTranslation[0])) {
                $checkLanguage = Language::where(['locale' => $keyLocale])->get();
                if (isset($checkLanguage[0]) && $checkLanguage[0]->is_enabled) {
                    LocationTranslation::where(['location_id' => $idLocation, 'locale' => $keyLocale])->update([
                        'url' => $requestTranslations[$keyLocale]['url'],
                        'name' => $requestTranslations[$keyLocale]['name'],
                        'description' => $requestTranslations[$keyLocale]['description'],
                        'updated_by' => $userId
                    ]);
                }
            } else {
                LocationTranslation::create([
                    'location_id' => $idLocation,
                    'locale' => $keyLocale,
                    'name' => $requestTranslations[$keyLocale]['name'],
                    'url' => $requestTranslations[$keyLocale]['url'],
                    'description' => $requestTranslations[$keyLocale]['description'],
                    'created_by' => $userId
                ]);
            }
        }
        Session::flash('success', 'Location successfully updated !');
        Cache::forget('locations');
        return redirect(redirect()->getUrlGenerator()->previous());
    }

    public function delete(Request $request, $id)
    {
        //Check if users are bound to a location
        $isUsers = LocationUser::where(['location_id' => $id])->get();
        if (count($isUsers) > 0) {
            return response()->json(['errorUsers' => $isUsers], 200);
        }

        //Check is child locations
        $ChildLocations = Location::where(['parent_id' => $id])->get();
        if (count($ChildLocations) > 0) {
            return response()->json(['errorChildLocations' => 'true'], 200);
        }

        //Check is properties
        $properties = Property::select(['code', 'id'])->where(['location_id' => $id])->get();
        if (count($properties) > 0) {
            return response()->json(['errorProperties' => $properties], 200);
        }

        //Delete location images
        $images = LocationImage::select(['name'])->where(['location_id' => $id])->get();
        foreach ($images as $img) {
            \File::Delete(public_path('/images/location/original/') . $img->name);
            \File::Delete(public_path('/images/location/display/') . $img->name);
            \File::Delete(public_path('/images/location/small/') . $img->name);
            \File::Delete(public_path('/images/location/thumbnail/') . $img->name);
        }

        LocationImage::where(['location_id' => $id])->delete();
        LocationUser::where(['location_id' => $id])->delete();
        LocationTranslation::where(['location_id' => $id])->delete();
        Location::where(['id' => $id])->delete();
        Cache::forget('locations');
        return  response()->json(['success' => $request->root() . '/admin/location'], 200);
    }
}
