<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PropertyCategory;
use App\PropertyCategoryTranslations;
use App\Language;
use App\Property;
use Illuminate\Support\Facades\Auth;
use Session;

class PropertyCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = PropertyCategory::with('translations')->orderBy('sequence', "ASC")->get();
        return view('admin.property-category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        $category = null;
        $categoryTranslations = null;
        return view('admin.property-category.create', compact('category', 'languages', 'categoryTranslations'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rAll = $request->all();
        $requestTranslations = $this->processingTranslationsInRequest($request, 'property_category_translations');
        $this->validate($request, [
                'code' => 'required|unique:property_categories|min:1|max:1',
                'is_enabled' => 'required|bool'
            ]
        );
        $userId = Auth::user()->id;
        //Take last sequence
        $sequence = 0;
        $maxSequence = PropertyCategory::whereRaw('sequence = (select max(`sequence`) from property_categories )')->get();
        if (isset($maxSequence[0])) {
            $sequence = +$maxSequence[0]->sequence + 1;
        }
        $rAll['created_by'] = $userId;
        $rAll['sequence'] = $sequence;
        $createdCategory = PropertyCategory::create($rAll);

        foreach ($requestTranslations as $keyLocale => $val) {
            PropertyCategoryTranslations::create([
                'property_category_id' => $createdCategory->id,
                'locale' => $keyLocale,
                'url' => $requestTranslations[$keyLocale]['url'],
                'name' => $requestTranslations[$keyLocale]['name'],
                'description' => $requestTranslations[$keyLocale]['description']
            ]);
        }
        return redirect('admin/category');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = PropertyCategory::where(['id' => $id])->with('translations')->get();
        $category = $category[0];
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        /*Create category translations*/
        $categoryTranslations = $this->createTranslationsList(isset($category['translations']) ? $category['translations'] : null);
        return view('admin.property-category.edit', compact('category', 'languages', 'categoryTranslations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rAll = $request->all();
        /*Create request translations array*/
        $requestTranslations = $this->processingTranslationsInRequest($request, 'property_category_translations', 'property_category_id', $id);
        $this->validate($request, [
                'is_enabled' => 'required|bool'
            ]
        );
        /*update property_category table*/
        PropertyCategory::where(['id' => $id])->update([
            /*'code' => $rAll['code'],*/
            'is_enabled' => $rAll['is_enabled'],
            'updated_by' => Auth::user()->id
        ]);
        foreach ($requestTranslations as $keyLocale => $val) {
            $checkTranslation = PropertyCategoryTranslations::where(['property_category_id' => $id, 'locale' => $keyLocale])->get()->toArray();
            if (isset($checkTranslation[0])) {
                $checkLanguage = Language::where(['locale' => $keyLocale])->get();
                if (isset($checkLanguage[0]) && $checkLanguage[0]->is_enabled) {
                    PropertyCategoryTranslations::where(['property_category_id' => $id, 'locale' => $keyLocale])->update([
                        'url' => $requestTranslations[$keyLocale]['url'],
                        'name' => $requestTranslations[$keyLocale]['name'],
                        'description' => $requestTranslations[$keyLocale]['description']
                    ]);
                }
            } else {
                PropertyCategoryTranslations::create([
                    'property_category_id' => $id,
                    'locale' => $keyLocale,
                    'url' => $requestTranslations[$keyLocale]['url'],
                    'name' => $requestTranslations[$keyLocale]['name'],
                    'description' => $requestTranslations[$keyLocale]['description']
                ]);
            }
        }
        Session::flash('success', 'Property Category successfully updated !');
        return redirect(redirect()->getUrlGenerator()->previous());
    }


    public function delete(Request $request, $id)
    {
        //Check is properties
        $properties = Property::select(['code', 'id'])->where(['category_id' => $id])->get();
        if (count($properties) > 0) {
            return response()->json(['errorProperties' => $properties], 200);
        }

        PropertyCategoryTranslations::where(['property_category_id' => $id])->delete();
        PropertyCategory::where(['id' => $id])->delete();

        return  response()->json(['success' => $request->root() . '/admin/category'], 200);
    }

    public function upDownSort(Request $request, $sort, $id)
    {
        $sort = $sort == 'up' ? -1 : 1;

        $categories = PropertyCategory::with('translations')->orderBy('sequence', "ASC")->get();
        $sortArray = [];
        foreach ($categories as $category){
            $sortArray[] = $category->id;
        }

        $currentCat = array_search($id, $sortArray);
        $secondCat = $currentCat + $sort;

        $currentCatSeq = $categories[$currentCat]->sequence;
        $secondCatSeq = $categories[$secondCat]->sequence;

        $categories[$currentCat]->sequence = $secondCatSeq;
        $categories[$currentCat]->save();
        $categories[$secondCat]->sequence = $currentCatSeq;
        $categories[$secondCat]->save();

        return redirect()->action('Admin\PropertyCategoryController@index');
    }
}
