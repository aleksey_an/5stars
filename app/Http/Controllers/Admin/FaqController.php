<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;
use App\FaqTranslation;
use App\Language;
use Illuminate\Support\Facades\Auth;
use Session;


class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faq::with('translations.lang')->orderBy('sequence', "ASC")->get();
        return view('admin.faq.index', compact('faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        return view('admin.faq.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestTranslations = $this->processingFaqTabsDescription($request);

        $this->validate($request, [
                'is_enabled' => 'required|numeric'
            ]
        );

        $rAll = $request->all();


        $newFaq = Faq::create([
            'is_enabled' => $rAll['is_enabled']
        ]);

        $sequence = 0;
        $maxSequence = Faq::whereRaw('sequence = (select max(`sequence`) from faq )')->get();
        if (isset($maxSequence[0])) {
            $sequence = +$maxSequence[0]->sequence + 1;
        }
        $rAll['sequence'] = $sequence;

        foreach ($requestTranslations as $keyLocale => $val) {
            FaqTranslation::create([
                'faq_id' => $newFaq->id,
                'question' => $requestTranslations[$keyLocale]['question'],
                'answer' => $requestTranslations[$keyLocale]['answer'],
                'locale' => $keyLocale,
            ]);
        }

        return redirect('admin/faq');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faq::with('translations')->where(['id' => $id])->first();
        $languages = [];
        $responseLang = Language::where(['is_enabled' => true])->get();
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        $translations = $this->createTranslationsList(isset($faq['translations']) ? $faq['translations'] : null);

        return view('admin.faq.edit', compact('faq', 'languages', 'translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Create request translations array
        $requestTranslations = $this->processingFaqTabsDescription($request);
        $this->validate($request, [
                'is_enabled' => 'required|numeric'
            ]
        );

        $rAll = $request->all();

        Faq::where(['id' => $id])->update([
            'is_enabled' => $rAll['is_enabled']
        ]);

        foreach ($requestTranslations as $keyLocale => $val) {
            $checkTranslation = FaqTranslation::where(['faq_id' => $id, 'locale' => $keyLocale])->get()->toArray();
            if (isset($checkTranslation[0])) {
                $checkLanguage = Language::where(['locale' => $keyLocale])->get();
                if (isset($checkLanguage[0]) && $checkLanguage[0]->is_enabled) {
                   FaqTranslation::where(['faq_id' => $id, 'locale' => $keyLocale])->update([
                        'question' => $requestTranslations[$keyLocale]['question'],
                        'answer' => $requestTranslations[$keyLocale]['answer'],
                    ]);
                }
            } else {
                FaqTranslation::create([
                    'faq_id' => $id,
                    'question' => $requestTranslations[$keyLocale]['question'],
                    'answer' => $requestTranslations[$keyLocale]['answer'],
                    'locale' => $keyLocale,
                ]);
            }
        }
        Session::flash('success', 'Faq question successfully updated !');
        return redirect(redirect()->getUrlGenerator()->previous());
    }

    /**
     * Create and validate request beginning from  "question-" locale- "question, answer"
     */
    public function processingFaqTabsDescription($request)
    {
        /*Create request translations array*/
        $rAll = $request->all();
        $result = [];
        foreach ($rAll as $key => $r) {
            if (explode('-', $key)[0] == 'description') {
                if (explode('-', $key)[2] == 'question') {
                    $result[explode('-', $key)[1]]['question'] = $r;
                }
                if (explode('-', $key)[2] == 'answer') {
                    $result[explode('-', $key)[1]]['answer'] = $r ? $r : '';
                }
            }
        }
        /*Filter property translations*/
        foreach ($result as $key => $r) {
            if (!$r['question']) {
                unset($result[$key]);
            }
        }
        /*Validate code translations*/
        if (count($result) == 0 || !isset($result[config('app.locale')])) {
            Session::flash('error', 'You must fill out a Subject in at ' . config('app.locale') . ' language');
            $this->validate($request, [
                'description-' . config('app.locale') . '-question' => 'required|min:1|max:1000'
            ]);
        }
        return $result;
    }

    public function upDownSort(Request $request, $sort, $id)
    {
        $sort = $sort == 'up' ? -1 : 1;

        $faqs = Faq::with('translations')->orderBy('sequence', "ASC")->get();
        $sortArray = [];
        foreach ($faqs as $faq){
            $sortArray[] = $faq->id;
        }

        $currentFaq = array_search($id, $sortArray);
        $secondFaq = $currentFaq + $sort;

        $currentFaqSeq = $faqs[$currentFaq]->sequence;
        $secondFaqSeq = $faqs[$secondFaq]->sequence;

        $faqs[$currentFaq]->sequence = $secondFaqSeq;
        $faqs[$currentFaq]->save();
        $faqs[$secondFaq]->sequence = $currentFaqSeq;
        $faqs[$secondFaq]->save();

        return redirect()->action('Admin\FaqController@index');
    }
}
