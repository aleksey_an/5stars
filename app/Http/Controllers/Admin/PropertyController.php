<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\LocationTranslation;
use App\Metro;
use App\PropertyCategoryTranslations;
use App\PropertyFacility;
use App\PropertyFacilityData;
use App\PropertyTranslation;
use App\PropertyTransport;
use App\PropertyTransportData;
use Illuminate\Http\Request;
use App\Property;
use App\Location;
use App\LocationUser;
use App\PropertyCategory;
use App\Language;
use Illuminate\Support\Facades\Auth;
use App\PropertyImage;
use App\User;
use Session;
use SimpleXMLElement;

class PropertyController extends Controller
{

    private $perPage = 15;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //User role
        $roleName = Auth::user()->roles->first()->name;
        $canDelete = Auth::user()->can('delete_property');
        if ($roleName == 'agent' || $roleName == 'editor') {

            $availableLocations = $this->getAvailableLocations();

            $property = Property::with(['categoryNameInEn', 'locationNameInEn', 'translations.lang'])
                ->whereIn('location_id', $availableLocations)
                ->orderBy('updated_at', 'desc')
                ->paginate($this->perPage);
            //Get Locations
            $locationsData = Location::with('translations')->whereIn('id', $availableLocations)->orderBy('parent_id')->get()->toArray();
        } else {
            //Get Property
            $property = Property::with(['categoryNameInEn', 'locationNameInEn', 'translations.lang'])
                ->orderBy('updated_at', 'desc')
                ->paginate($this->perPage);
            //Get Locations
            $locationsData = Location::with('translations')->orderBy('parent_id')->get()->toArray();
        }
        $locations = $this->createLocationsList($locationsData);
        //Get Categories
        $categories = $this->createCategoriesList();
        return view('admin.property.index', compact('property', 'locations', 'categories', 'canDelete'));
    }

    /**
     * Search system property
     */
    public function search(Request $request)
    {
        $property = [];
        $canDelete = Auth::user()->can('delete_property');
        /*Get Categories*/
        $categories = $this->createCategoriesList();

        $roleName = Auth::user()->roles->first()->name;
        if ($roleName == 'agent' || $roleName == 'editor') {
            $availableLocations = $this->getAvailableLocations();
            /*Get Locations*/
            $locationsData = Location::with('translations')->whereIn('id', $availableLocations)->orderBy('parent_id')->get()->toArray();
            $property = $this->getSearchResult($request->all(), $this->perPage, $availableLocations);
        } else {
            /*Get Locations*/
            $locationsData = Location::with('translations')->orderBy('parent_id')->get()->toArray();
            $property = $this->getSearchResult($request->all(), $this->perPage);
        }
        $locations = $this->createLocationsList($locationsData);
        return view('admin.property.index', compact('property', 'locations', 'categories', 'canDelete'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roleName = Auth::user()->roles->first()->name;
        //Get Locations
        if ($roleName == 'agent' || $roleName == 'editor') {
            $availableLocations = $this->getAvailableLocations();
            $locationsData = Location::with('translations')->whereIn('id', $availableLocations)->orderBy('parent_id')->get()->toArray();
        } else {
            $locationsData = Location::with('translations')->orderBy('parent_id')->get()->toArray();
        }
        $locations = $this->createLocationsList($locationsData);
        //Get Categories
        $categories = $this->createCategoriesList();
        //Get Languages
        $languages = $this->createLanguagesList(false);
        //Get Facilities
        $facilities = $this->createFacilityList($languages, false);
        //Get Transport
        $transports = $this->createTransportList($languages, false);
        //Get Metro
        $metroData = Metro::with('translations')->get();
        $metroData = $this->createMetroList($metroData);
        return view('admin.property.create', compact('locations', 'locationsData', 'categories', 'languages', 'facilities', 'transports', 'metroData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rAll = $request->all();

        //Create request translations array
        $requestTranslations = [];
        foreach ($rAll as $key => $r) {
            if (explode('-', $key)[0] == 'description') {
                if (explode('-', $key)[2] == 'subject') {
                    $requestTranslations[explode('-', $key)[1]]['subject'] = $r;
                }
                if (explode('-', $key)[2] == 'detail') {
                    $requestTranslations[explode('-', $key)[1]]['detail'] = $r;
                }
            }
        }
        //Filter property translations
        foreach ($requestTranslations as $key => $r) {
            if (!$r['subject'] || !$r['detail']) {
                unset($requestTranslations[$key]);
            }
        }
        //Validate property translations
        if (count($requestTranslations) == 0 || !isset($requestTranslations[config('app.locale')])) {
            Session::flash('error', 'You must fill out a name in at ' . config('app.locale') . ' language');
            $this->validate($request, [
                'description-' . config('app.locale') . '-subject' => 'required|min:1|max:255',
                'description-' . config('app.locale') . '-detail' => 'required|min:1|max:1000000',
            ]);
        }
        $this->validate($request, [
                'status' => 'required|in:published,disabled,pending',
                'contact_name' => 'required|min:1|max:255',
                'contact_email' => 'required|email',
                'contact_phone' => 'max:250',
                'contact_mobile' => 'required|min:1|max:255',
                'contact_viber' => 'max:255',
                'contact_line' => 'max:255',
                'contact_whatsapp' => 'max:255',
                'location_id' => 'required|numeric',
                'category_id' => 'required|numeric',
                'metro_id' => 'numeric',
                'building_name' => 'required|min:1|max:255',
                'address' => 'required|min:1|max:255',
                'unit_number' => 'required|min:1|max:255',
                'floor_number' => 'required|min:1|max:255',
                'key_info' => 'required|min:1|max:255',
                'availability' => 'max:255',
                'built_year' => 'nullable|numeric',
                'indoor_area' => 'required|nullable|numeric',
                'outdoor_area' => 'nullable|numeric',
                'bedroom' => 'required|nullable|numeric',
                'bathroom' => 'nullable',
                'sale_price' => 'nullable|numeric',
                'common_fee' => 'nullable|numeric',
                'rent_day' => 'nullable|numeric',
                'rent_week' => 'nullable|numeric',
                'rent_month' => 'nullable|numeric',
                'address_1' => 'nullable|max:250'
            ]
        );

        //Get user id
        $userId = Auth::user()->id;
        $rAll['user_id'] = $userId;
        $rAll['created_by'] = $userId;

        //Check social fields
        $rAll['contact_viber'] = $rAll['contact_viber'] ? $rAll['contact_viber'] : '';
        $rAll['contact_line'] = $rAll['contact_line'] ? $rAll['contact_line'] : '';
        $rAll['contact_whatsapp'] = $rAll['contact_whatsapp'] ? $rAll['contact_whatsapp'] : '';

//        dd($rAll);

        //create XML file for old version
//        $this->createXML($rAll);


        //create new Property in Property Table
        $newProperty = Property::create($rAll);

        //Create Property Code
        $codeCategory = PropertyCategory::where(['id' => $newProperty->category_id])->get();
        $codeLocationMain = $this->getMainParentInLocation($newProperty->location_id);
        $propertyCode = $codeCategory[0]->code . $codeLocationMain . $newProperty->id;
        Property::where(['id' => $newProperty->id])->update(['code' => $propertyCode]);


        //Create a record in the table Property Translations
        foreach ($requestTranslations as $key => $val) {
            PropertyTranslation::create([
                'property_id' => $newProperty->id,
                'locale' => $key,
                'subject' => $requestTranslations[$key]['subject'],
                'detail' => $requestTranslations[$key]['detail']
            ]);
        }

        //Save checked facilities
        foreach ($rAll as $key => $r) {
            if (explode('_', $key)[0] == 'facility') {
                $pfd = new PropertyFacilityData;
                $pfd->property_facility_id = explode('_', $key)[1];
                $pfd->property_id = $newProperty->id;
                $pfd->save();
            }
        }

        //Save Checked Transport
        foreach ($rAll as $key => $r) {
            if (explode('_', $key)[0] == 'transport') {
                $ptd = new PropertyTransportData;
                $ptd->property_transport_id = explode('_', $key)[1];
                $ptd->property_id = $newProperty->id;
                $ptd->save();
            }
        }

        if ($rAll['images']) {
            $images = json_decode($rAll['images']);
            foreach ($images as $image) {
                $imageExpansion = explode('.', $image->name);
                $imageExpansion = $imageExpansion[count($imageExpansion) - 1];
                $newImageName = $this->createNameForImage($rAll['location_id'], $rAll['category_id'], time());
                $newImageName = $newImageName . rand(100, 999) . '_' . $newProperty->id . '.' . $imageExpansion;
                if (
                    is_file(public_path('images/property/original/' . $image->name)) &&
                    is_file(public_path('images/property/display/' . $image->name)) &&
                    is_file(public_path('images/property/small/' . $image->name)) &&
                    is_file(public_path('images/property/thumbnail/' . $image->name))
                ) {

                    rename(
                        public_path('images/property/original/' . $image->name),
                        public_path('images/property/original/' . $newImageName)
                    );

                    rename(
                        public_path('images/property/display/' . $image->name),
                        public_path('images/property/display/' . $newImageName)
                    );

                    rename(
                        public_path('images/property/small/' . $image->name),
                        public_path('images/property/small/' . $newImageName)
                    );

                    rename(
                        public_path('images/property/thumbnail/' . $image->name),
                        public_path('images/property/thumbnail/' . $newImageName)
                    );
                    PropertyImage::create([
                        'property_id' => $newProperty->id,
                        'name' => $newImageName,
                        'detail' => '',
                        'sequence' => $image->sequence,
                        'is_enabled' => $image->is_enabled,
                        'created_by' => $userId
                    ]);
                }
            }
        }

       /* //Redirect to page
        $roleName = Auth::user()->roles->first()->name;
        if ($roleName == 'agent' || $roleName == 'editor') {
            $availableLocations = $this->getAvailableLocations();
            $property = Property::with('translations')->whereIn('location_id', $availableLocations)->paginate($this->perPage);
        } else {
            $property = Property::with('translations')->paginate($this->perPage);
        }*/


        return redirect('/admin/property');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($propertyId)
    {
        $roleName = Auth::user()->roles->first()->name;

        $property = Property::with('translations', 'facilities', 'transports')->where(['id' => $propertyId])->get();
        $property = $property[0];

        //Get Locations
        if ($roleName == 'agent' || $roleName == 'editor') {
            $availableLocations = $this->getAvailableLocations();
            $locationsData = Location::with('translations')->whereIn('id', $availableLocations)->orderBy('parent_id')->get()->toArray();
        } else {
            $locationsData = Location::with('translations')->orderBy('parent_id')->get()->toArray();
        }
        $locations = $this->createLocationsList($locationsData);

        //Get Categories
        $categories = $this->createCategoriesList();

        //Get Languages
        $languages = $this->createLanguagesList(false);

        //Create property translations
        $propertyTranslations = $this->createPropertyTranslations(isset($property['translations']) ? $property['translations'] : null);

        //Get Facilities
        $facilities = $this->createFacilityList($languages, $property);

        //Get Transport
        $transports = $this->createTransportList($languages, $property);

        //Get Metro
        $metroData = $this->createMetroList(Metro::with('translations')->get());

        //Get Images
        $images = PropertyImage::where(['property_id' => $propertyId])->orderBy('sequence', 'desc')->get();

        $createdBy = User::where(['id' => $property->created_by])->get()->toArray();
        if (isset($createdBy[0])) {
            $createdBy = $createdBy[0]['first_name'];
        } else {
            $createdBy = '';
        }
        //Prepare name image for search
        if ($property->image_for_search_name) {
            $imgForSearchName = explode('.', $property->image_for_search_name);
            if (count($imgForSearchName) > 1) {
                array_pop($imgForSearchName);
                $imgForSearchName = implode(".", $imgForSearchName);
            } else {
                $imgForSearchName = implode(".", $imgForSearchName);
            }
        } else {
            $imgForSearchName = '';
        }

        return view('admin.property.edit', compact('property', 'propertyTranslations', 'locations', 'locationsData', 'categories', 'languages', 'facilities', 'transports', 'metroData', 'images', 'createdBy', 'imgForSearchName'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $propertyId)
    {
        $rAll = $request->all();

        //Create request translations array
        $requestTranslations = [];
        foreach ($rAll as $key => $r) {
            if (explode('-', $key)[0] == 'description') {
                if (explode('-', $key)[2] == 'subject') {
                    $requestTranslations[explode('-', $key)[1]]['subject'] = $r;
                }
                if (explode('-', $key)[2] == 'detail') {
                    $requestTranslations[explode('-', $key)[1]]['detail'] = $r;
                }
            }
        }
        //Filter property translations
        foreach ($requestTranslations as $key => $r) {
            if (!$r['subject'] || !$r['detail']) {
                unset($requestTranslations[$key]);
            }
        }
        //Validate property translations
        if (count($requestTranslations) == 0 || !isset($requestTranslations[config('app.locale')])) {
            Session::flash('error', 'You must fill out a name in at ' . config('app.locale') . ' language');
            $this->validate($request, [
                'description-' . config('app.locale') . '-subject' => 'required|min:1|max:255',
                'description-' . config('app.locale') . '-detail' => 'required|min:1|max:1000000',
            ]);
        }

        $this->validate($request, [
                'status' => 'required|in:published,disabled,pending',
                'contact_name' => 'required|min:1|max:255',
                'contact_email' => 'required|email',
                'contact_phone' => 'max:250',
                'contact_mobile' => 'required|min:1|max:255',
                'contact_viber' => 'max:255',
                'contact_line' => 'max:255',
                'contact_whatsapp' => 'max:255',
                'location_id' => 'required|numeric',
                'category_id' => 'required|numeric',
                'metro_id' => 'numeric',
                'building_name' => 'required|min:1|max:255',
                'address' => 'required|min:1|max:255',
                'address_1' => 'nullable|max:250',
                'unit_number' => 'required|min:1|max:255',
                'floor_number' => 'required|min:1|max:255',
                'key_info' => 'required|min:1|max:255',
                'availability' => 'max:255',
                'built_year' => 'nullable|numeric',
                'indoor_area' => 'nullable|numeric',
                'outdoor_area' => 'nullable|numeric',
                'bedroom' => 'nullable|numeric',
                'bathroom' => 'nullable|max:250',
                'youtube_url' => 'max:255',
                'sale_price' => 'nullable|numeric',
                'common_fee' => 'nullable|numeric',
                'rent_day' => 'nullable|numeric',
                'rent_week' => 'nullable|numeric',
                'rent_month' => 'nullable|numeric',
            ]
        );

        //Created bu user
        $userId = Auth::user()->id;

        //Update property code with category

        $newCode = '';
        $getProperty = Property::where(['id' => $propertyId])->get();
        if ($getProperty[0]->category_id != $rAll['category_id']) {
            $propertyCategory = PropertyCategory::where(['id' => $rAll['category_id']])->get();
            $newCode = $getProperty[0]->code;
            $newCode[0] = $propertyCategory[0]->code;
        } else {
            $newCode = $getProperty[0]->code;
        }

        //Update property code with location
        if ($getProperty[0]->location_id != $rAll['location_id']) {
            $parentLocationCode = $this->getMainParentInLocation($rAll['location_id']);
            $newCode[1] = $parentLocationCode[0];
            $newCode[2] = $parentLocationCode[1];
            $newCode[3] = $parentLocationCode[2];
        }

        //Update property table
        Property::where(['id' => $propertyId])->update([
            'code' => $newCode,
            'updated_by' => $userId,
//            "publish_date" => $rAll["publish_date"],
            'status' => $rAll["status"],
            'contact_name' => $rAll["contact_name"],
            'contact_email' => $rAll["contact_email"],
            'contact_phone' => $rAll["contact_phone"],
            'contact_mobile' => $rAll["contact_mobile"],

            'contact_viber' => isset($rAll["contact_viber"]) ? $rAll["contact_viber"] : '',
            'contact_line' => isset($rAll["contact_line"]) ? $rAll["contact_line"] : '',
            'contact_whatsapp' => isset($rAll["contact_whatsapp"]) ? $rAll["contact_whatsapp"] : '',

            'location_id' => $rAll["location_id"],
            'category_id' => $rAll["category_id"],
            'metro_id' => isset($rAll["metro_id"]) ? $rAll["metro_id"] : 0,
            'building_name' => $rAll["building_name"],
            'address_1' => $rAll["address_1"],
            'address' => $rAll["address"],
            'lat' => $rAll["lat"],
            'lng' => $rAll["lng"],
            'unit_number' => $rAll["unit_number"],
            'floor_number' => $rAll["floor_number"],
            'key_info' => $rAll["key_info"],
            'availability' => $rAll["availability"],
            'built_year' => $rAll["built_year"],
            'indoor_area' => $rAll["indoor_area"],
            'outdoor_area' => $rAll["outdoor_area"],
            'bedroom' => $rAll["bedroom"],
            'bathroom' => $rAll["bathroom"],
            'youtube_url' => $rAll["youtube_url"],
            'furnished' => isset($rAll["furnished"]) ? 1 : 0,
            'to_sale' => isset($rAll["to_sale"]) ? 1 : 0,
            'is_sold' => isset($rAll["is_sold"]) ? 1 : 0,
            'sale_price' => $rAll["sale_price"] ? $rAll["sale_price"] : 0,
            'common_fee' => $rAll["common_fee"] ? $rAll["common_fee"] : 0,
            'to_rent' => isset($rAll["to_rent"]) ? 1 : 0,
            'is_rent' => isset($rAll["is_rent"]) ? 1 : 0,
            'rent_month' => $rAll["rent_month"] ? $rAll["rent_month"] : 0,
            'rent_week' => $rAll["rent_week"] ? $rAll["rent_week"] : 0,
            'rent_day' => $rAll["rent_day"] ? $rAll["rent_day"] : 0
        ]);

        //Update property_facility_data table
        PropertyFacilityData::where(['property_id' => $propertyId])->delete();
        foreach ($rAll as $key => $r) {
            if (explode('_', $key)[0] == 'facility') {
                $pfd = new PropertyFacilityData;
                $pfd->property_facility_id = explode('_', $key)[1];
                $pfd->property_id = $propertyId;
                $pfd->save();
            }
        }

        //Update property_transport_data
        PropertyTransportData::where(['property_id' => $propertyId])->delete();
        foreach ($rAll as $key => $r) {
            if (explode('_', $key)[0] == 'transport') {
                $ptd = new PropertyTransportData;
                $ptd->property_transport_id = explode('_', $key)[1];
                $ptd->property_id = $propertyId;
                $ptd->save();
            }
        }

        //Update property_translations table
        //Create a record in the table Property Translations
        foreach ($requestTranslations as $keyLocale => $val) {
            $checkLocTranslation = PropertyTranslation::where(['property_id' => $propertyId, 'locale' => $keyLocale])->get()->toArray();
            if (isset($checkLocTranslation[0])) {
                $checkLanguage = Language::where(['locale' => $keyLocale])->get();
                if (isset($checkLanguage[0]) && $checkLanguage[0]->is_enabled) {
                    PropertyTranslation::where(['property_id' => $propertyId, 'locale' => $keyLocale])->update([
                        'subject' => $requestTranslations[$keyLocale]['subject'],
                        'detail' => $requestTranslations[$keyLocale]['detail']
                    ]);
                }
            } else {
                PropertyTranslation::create([
                    'property_id' => $propertyId,
                    'locale' => $keyLocale,
                    'subject' => $requestTranslations[$keyLocale]['subject'],
                    'detail' => $requestTranslations[$keyLocale]['detail']
                ]);
            }
        }

        Session::flash('success', 'Property successfully updated !');
        return redirect(redirect()->getUrlGenerator()->previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $images = PropertyImage::where(['property_id' => $id])->get();
        foreach ($images as $image) {
            if (is_file(public_path() . '/images/property/original/' . $image->name)) {
                \File::Delete(public_path('/images/property/original/') . $image->name);
            }
            if (is_file(public_path() . '/images/property/display/' . $image->name)) {
                \File::Delete(public_path('/images/property/display/') . $image->name);
            }
            if (is_file(public_path() . '/images/property/small/' . $image->name)) {
                \File::Delete(public_path('/images/property/small/') . $image->name);
            }
            if (is_file(public_path() . '/images/property/thumbnail/' . $image->name)) {
                \File::Delete(public_path('/images/property/thumbnail/') . $image->name);
            }
        }
        PropertyImage::where(['property_id' => $id])->delete();
        PropertyTransportData::where(['property_id' => $id])->delete();
        PropertyFacilityData::where(['property_id' => $id])->delete();
        PropertyTranslation::where(['property_id' => $id])->delete();
        Property::where(['id' => $id])->delete();
        return redirect(redirect()->getUrlGenerator()->previous());
    }

    public function createPropertyTranslations($propertyTranslations)
    {
        $result = [];
        if (!$propertyTranslations || count($propertyTranslations) == 0)
            return null;
        foreach ($propertyTranslations as $key => $val) {
            $result[$val['locale']] = $propertyTranslations[$key];
        }
        return $result;
    }

    public function createTransportList($languages, $property)
    {
        $transports = PropertyTransport::with('translations')->get();
        $response = [];
        $checked = false;
        foreach ($transports as $key => $transport) {
            if ($property) {
                $propertyTransport = $property->transports;
                foreach ($propertyTransport as $pt) {
                    if ($pt->property_transport_id == $transport->id) {
                        $checked = true;
                        break;
                    } else {
                        $checked = false;
                    }
                }
            }
            $response[$transport['id']] = [
                'transportName' => $this->createTranslation($transport, $languages),
                'image' => $transport['image'],
                'id' => $transport['id'],
                'checked' => $checked];
        }
        return $response;
    }

    public function createMetroList($metroData)
    {
        $response = [];
        foreach ($metroData as $key => $m) {
            for ($i = 0; $i < count($m['translations']); $i++) {
                if ($m['translations'][$i]['locale'] == 'en') {
                    array_push($response, ['name' => $m['translations'][$i]['name'], 'locationId' => $m['location_id'], 'id' => $m['id']]);
                    break;
                } else if ($m['translations'][$i]['locale'] == 'fr') {
                    array_push($response, ['name' => $m['translations'][$i]['name'], 'locationId' => $m['location_id'], 'id' => $m['id']]);
                    break;
                } else {
                    array_push($response, ['name' => $m['translations'][$i]['name'], 'locationId' => $m['location_id'], 'id' => $m['id']]);
                }
            }
        }
        return $response;
    }

    public function createFacilityList($languages, $property)
    {
        $response = [];
        $facilities = PropertyFacility::with('translations')->get();
        $checked = false;
        foreach ($facilities as $key => $facility) {
            if ($property) {
                $propertyFacility = $property->facilities;
                foreach ($propertyFacility as $pf) {
                    if ($pf->property_facility_id == $facility->id) {
                        $checked = true;
                        break;
                    } else {
                        $checked = false;
                    }
                }
            }
            $response[$facility['id']] = [
                'facilityName' => $this->createTranslation($facility, $languages),
                'image' => $facility['image'],
                'id' => $facility['id'],
                'checked' => $checked];
        }
        return $response;
    }

    public function createTranslation($data, $languages)
    {
        $response = [];
        foreach ($data['translations'] as $key => $d) {
            if (!isset($languages[$d['locale']])) {
                unset($data['translations'][$key]);
            }
            if (isset($languages[$d['locale']]) && $d['locale'] == 'en') {
                $response = $data['translations'][$key];
                break;
            } else if (isset($languages[$d['locale']])) {
                $response = $data['translations'][$key];
            }
        }
        return $response;
    }

    public function createLanguagesList($default)
    {
        $languages = Language::where(['is_enabled' => true])->get();
        $result = [];
        foreach ($languages as $value) {
            if ($default) {
                if ($value['locale'] == 'en') {
                    $result[$value['locale']] = $value['name'];
                }
            } else {
                $result[$value['locale']] = $value['name'];
            }
        }
        return $result;
    }

    public function getAvailableLocations()
    {
        $availableLocations = LocationUser::where(['user_id' => Auth::user()->id])->get();
        $result = [];
        foreach ($availableLocations as $al) {
            array_push($result, $al['location_id']);
        }
        return $result;
    }

    public function getMainParentInLocation($locationId)
    {
        $code = '';
        $location = Location::where(['id' => $locationId])->get()->toArray();
        if ($location[0]['parent_id'] != 0) {
            $code = $this->getMainParentInLocation($location[0]['parent_id']);
        } else {
            $code = $location[0]['code'];
        }
        return $code;
    }

    public function createNameForImage($locationId, $categoryId, $time = 0)
    {
        $locationName = '';
        $locationData = LocationTranslation::where(['location_id' => $locationId, 'locale' => config('app.locale')])->get();
        if (isset($locationData[0]) && $locationData[0]->name) {
            $locationName = explode(' ', $locationData[0]->name);
            foreach ($locationName as $key => $l) {
                if ($l == "\\" || $l == "." || $l == "," || $l == "/") {
                    unset($locationName[$key]);
                }
            }
            $locationName = implode('_', $locationName);
        }
        $categoryName = '';
        $categoryData = PropertyCategoryTranslations::where(['property_category_id' => $categoryId, 'locale' => config('app.locale')])->get();
        if (isset($categoryData[0]) && $categoryData[0]->name) {
            $categoryName = explode(' ', $categoryData[0]->name);
            foreach ($categoryName as $key => $k) {
                if ($k == "\\" || $k == "." || $k == "," || $k == "/") {
                    unset($categoryName[$key]);
                }
            }
            $categoryName = implode('_', $categoryName);
        }
        return $locationName . '_' . $categoryName . '_' . $time;
    }

//    public function createXML($rAll)
//    {
//        $xml = new SimpleXMLElement('<findyourspace/>');
//
//        $customerInfo = $xml->addChild('customer_info');
//            $customerId = $customerInfo->addChild('id');
//            $customerName = $rAll['contact_name'] ? $customerInfo->addChild('name', $rAll['contact_name']) : $customerInfo->addChild('name');
//            $customerType = $rAll['contact_type'] ? $customerInfo->addChild('type', $rAll['contact_type']) : ""; //agency, agent,developer, unspecified
//            $customerPhone = $rAll['contact_phone'] ? $customerInfo->addChild('phone', $rAll['contact_phone']) : $customerInfo->addChild('phone'); //need to regexp with country code
//            $customerMobilephone = $rAll['contact_mobile'] ? $customerInfo->addChild('mobilephone', $rAll['contact_mobile']) : $customerInfo->addChild('mobilephone'); //need to regexp with country code
//            $customerEmail = $rAll['contact_email'] ? $customerInfo->addChild('email', $rAll['contact_email']) : $customerInfo->addChild('email');
//            $customerUrl = $rAll['contact_url'] ? $customerInfo->addChild('url', $rAll['contact_url']) : "";
//            $customerAddress = $rAll['contact_address'] ? $customerInfo->addChild('address', $rAll['contact_address']) : "";
//            $customerLogo = $rAll['contact_logo'] ? $customerInfo->addChild('logo', $rAll['contact_logo']) : ""; //url


//        $properties = $xml->addChild('properties');

//        Header('Content-type: text/xml');
//        print($xml->asXML("xml/xml_".date("d-m-y_H-i-s").".xml"));
//
//        dd($xml);

//        $array = [
//              "_token" => "cBzQmfJG2ZB7FAl9FXauSLK4jU4Xw6itOKHXqsBP"
//              "publish_date" => "2017-12-19"
//              "status" => "published"
//              "contact_name" => "qweqwe"
//              "contact_email" => "qweqwe@sdsgf.com"
//              "contact_phone" => "wqq"
//              "contact_mobile" => "2345345345"
//              "contact_viber" => "345343453"
//              "contact_line" => "3453453453"
//              "contact_whatsapp" => "grfg45r3r3g"
//              "location_id" => "16"
//              "category_id" => "4"
//              "description-fr-subject" => "sgdfgdf"
//              "description-fr-detail" => "gd fg dfhg dh  erhe r erte rt"
//              "description-en-subject" => "wer tert et"
//              "description-en-detail" => "er te ry erye r y"
//              "description-jp-subject" => "ertertert ert ert ert e"
//              "description-jp-detail" => "rt ert erte tert qw etwerg aghtrertyytrerhrdfv vdv vfdev bfefvb bfew2dfvb bvfdewedfvb"
//              "address" => "kharkiv"
//              "lat" => "49.9935"
//              "lng" => "36.230383"
//              "building_name" => "safdgs"
//              "unit_number" => "243"
//              "floor_number" => "456"
//              "key_info" => "df gf"
//              "availability" => "sd fsdf s"
//              "built_year" => "1920"
//              "indoor_area" => "0"
//              "outdoor_area" => "0"
//              "bedroom" => "3"
//              "bathroom" => "4"
//              "youtube_url" => "http://dfhvdnkfjd.com"
//              "facility_1" => "1"
//              "facility_5" => "5"
//              "facility_7" => "7"
//              "facility_9" => "9"
//              "transport_3" => "3"
//              "transport_5" => "5"
//              "to_sale" => "1"
//              "sale_price" => "15000000"
//              "common_fee" => "12000"
//              "rent_day" => "0"
//              "rent_week" => "0"
//              "rent_month" => "0"
//              "images" => "[{"name":"p_not_confirmed_1513617722_166.jpg","sequence":0,"is_enabled":1,"id":0},{"name":"p_not_confirmed_1513617722_896.jpg","sequence":1,"is_enabled":1,"id":1},{"name":"p_not_confirmed_1513617722_934.jpg","sequence":2,"is_enabled":1,"id":2}] ◀"
//              "img_" => "2"
//              "user_id" => 2
//              "created_by" => 2
//            ];
//    }


}
