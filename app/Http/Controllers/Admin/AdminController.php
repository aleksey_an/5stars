<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $UserRoleName = \Auth::user()->roles->first()->name;
        return view('admin.index', compact('UserRoleName'));
    }
}
