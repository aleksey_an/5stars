<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\LocationTranslation;
use App\Property;
use App\XmlConfig;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language;
use App\Location;
use App\PropertyCategory;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Web\CheckCacheController;
use Illuminate\Support\Facades\URL;
use App\SimpleXMLExtended;

class XmlController extends Controller
{
    public $locale = '';
    public $locations;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->locale = \App::getLocale();
        if (isset($request['locale']) && $request['locale']) {
            $this->locale = $request['locale'];
        }

        $locationsData = Location::with(['translations' => function ($query) {
            $query->where('location_translations.locale', '=', $this->locale);
        }])
            ->orderBy('parent_id')
            ->get()
            ->toArray();

        $locations = $this->createLocationsList($locationsData);
        $categories = $this->createCategoriesArray();
        $languages = $this->createLanguagesArray();


        if (isset($request['ajax']) && $request['locale']) {
            return view('admin.xml.form', compact('categories', 'locations', 'languages'));
        } else {
            return view('admin.xml.index', compact('categories', 'locations', 'languages'));
        }
    }

    public function generateXml($cron = null, Request $request)
    {
        $this->generateHipflat();
        $this->generateDDproperty();
        $this->generateSitemap();

        if ($cron) {
            $res = $this->generate($cron);
        } else {
            $res = $this->generate($request);
        }

        return $res;
    }

    public function generate($request)
    {
        $this->locale = $request['locale'] ? $request['locale'] : \App::getLocale();
        $this->locations = Location::all();

        //Save xml configuration for CRON task
        $xmlConfig = XmlConfig::get()->first();
        if (!$xmlConfig) {
            $xmlConfig = new XmlConfig();
        }

        $xmlConfig->locale = $this->locale;
        $xmlConfig->location_id = $request['location_id'] ? $request['location_id'] : null;
        $xmlConfig->category_id = $request['category_id'] ? $request['category_id'] : null;
        $xmlConfig->to_sale = $request['to_sale'] ? $request['to_sale'] : null;
        $xmlConfig->to_rent = $request['to_rent'] ? $request['to_rent'] : null;
        $xmlConfig->date_from = $request['date_from'] ? \DateTime::createFromFormat('Y-m-d H:i:s', $request['date_from']) : null;
        $xmlConfig->date_to = $request['date_to'] ? \DateTime::createFromFormat('Y-m-d H:i:s', $request['date_to']) : null;
        $xmlConfig->budget_from = $request['budget_from'] ? $request['budget_from'] : null;
        $xmlConfig->budget_to = $request['budget_to'] ? $request['budget_to'] : null;
        $xmlConfig->save();

        $properties = Property::with(
            ['translations' => function ($query) {
                $query->where('property_translations.locale', '=', $this->locale);
            },
                'images' => function ($query) {
                    $query->where('property_images.is_enabled', '=', 1);
                },
                'translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale',
            ]
        )
            ->where('status', '=', 'published')
            ->where('is_sold', '=', 0)

            ->where(function ($query) {
                $query
                    ->where(function ($query) {
                        $locations = [];
                        // Location : Bangkok, Hua Hin, Koh Samui, Phuket
                        foreach ([1, 3, 4, 6] as $value) {
                            $locations = array_merge($locations, $this->makeChildLocationArr($value));
                        }
                        $query
                            ->where('to_sale', '=', 1)
                            ->where('sale_price', '>=', 3500000)
                            ->whereIn('location_id', $locations)
                            // Property type : condo, house, land
                            ->whereIn('category_id', [3, 4, 6])
                        ;
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->where('to_rent', '=', 1)
                            ->where('rent_month', '>=', 40000)
                            // Property type : house, land, condo
                            ->whereIn('category_id', [3, 4, 7])
                            // Location : Bangkok
                            ->whereIn('location_id', $this->makeChildLocationArr(1))
                        ;
                    })
                ;
            })
        ;

/*
        if ($request['location_id']) {
            $locationsArray = [];
            foreach ($request['location_id'] as $value) {
                $locationsArray = array_merge($locationsArray, $this->makeChildLocationArr($value));
            }
            $properties = $properties
                ->whereIn('location_id', $locationsArray);
        }
        if ($request['category_id']) {
            $properties = $properties
                ->whereIn('category_id', $request['category_id']);
        }
        if ($request['to_sale'] != $request['to_rent']) {
            if ($request['to_sale']) {
                $properties = $properties
                    ->where('to_sale', '=', 1);
            }
            if ($request['to_rent']) {
                $properties = $properties
                    ->where('to_rent', '=', 1);
            }
        }
        if ($request['date_from']) {
            $dateFrom = new \DateTime($request['date_from']);
            $properties = $properties
                ->whereDate('created_at', '>=', $dateFrom);
        }
        if ($request['date_to']) {
            $dateTo = new \DateTime($request['date_to']);
            $properties = $properties
                ->where('created_at', '<=', $dateTo);
        }
        if ($request['budget_from']) {
            if (($request['to_sale'] && $request['to_rent']) || (!$request['to_sale'] && !$request['to_rent'])) {
                $properties = $properties
                    ->where(function ($query) use ($request) {
                        $query->where('sale_price', '>=', $request['budget_from'])
                            ->orWhere('rent_month', '>=', $request['budget_from']);
                    });
            } elseif ($request['to_rent']) {
                $properties = $properties
                    ->where('rent_month', '>=', $request['budget_from']);
            } elseif ($request['to_sale']) {
                $properties = $properties
                    ->where('sale_price', '>=', $request['budget_from']);
            }
        }
        if ($request['budget_to']) {
            if (($request['to_sale'] && $request['to_rent']) || (!$request['to_sale'] && !$request['to_rent'])) {
                $properties = $properties
                    ->where(function ($query) use ($request) {
                        $query->where('sale_price', '<=', $request['budget_to'])
                            ->orWhere('rent_month', '<=', $request['budget_to']);
                    });
            } elseif ($request['to_rent']) {
                $properties = $properties
                    ->where('rent_month', '<=', $request['budget_to']);
            } elseif ($request['to_sale']) {
                $properties = $properties
                    ->where('sale_price', '<=', $request['budget_to']);
            }
        }
*/

        $properties = $properties->orderBy('publish_date', 'DESC')->get();

        $properties = $this->createPropertyHref($properties);

        //Generate XML and save it in file "public/xml/xml.xml"
        $xml = new \SimpleXMLElement('<xml/>');
        $xmlChannel = $xml->addChild('channel');
        $xmlChannel->addChild('title', 'Five Stars Thailand Real Estate');
        $xmlChannel->addChild('description', 'Five Stars Thailand Real Estate');
        $xmlChannel->addChild('link', URL::to('/'));
        $xmlChannel->addChild('lastBuildDate', date('d:m:Y H:i'));
        $xmlChannel->addChild('pubDate', date('d:m:Y H:i'));
        $xmlProperties = $xmlChannel->addChild('properties');
        foreach ($properties as $key => $property) {
            $item = $xmlProperties->addChild('item');
            $item->addChild('title', htmlspecialchars(isset($property->translations[0]) ? $property->translations[0]->subject : ''));
            $item->addChild('description', htmlspecialchars(isset($property->translations[0]) ? $property->translations[0]->detail : ''));
            $item->addChild('building', htmlspecialchars($property->building_name));
            $item->addChild('link', htmlspecialchars(url($property->propertyHref[$this->locale])));
            $item->addChild('property_id', $property->id);
            $item->addChild('reference_id', $property->code);
            $item->addChild('type', htmlspecialchars(isset($property->categoryNameInLocale[0]) ? $property->categoryNameInLocale[0]->name : ''));
            $item->addChild('published', $property->publish_date);
            $prices = $item->addChild('prices');
                $sale_price = $prices->addChild('price', $property->sale_price);
                    $sale_price->addAttribute('tenure', 'sale');
                    $sale_price->addAttribute('currency', 'THB');
                $rent_day = $prices->addChild('price', $property->rent_day);
                    $rent_day->addAttribute('tenure', 'rent');
                    $rent_day->addAttribute('currency', 'THB');
                    $rent_day->addAttribute('period', 'daily');
                $rent_week = $prices->addChild('price', $property->rent_week);
                    $rent_week->addAttribute('tenure', 'rent');
                    $rent_week->addAttribute('currency', 'THB');
                    $rent_week->addAttribute('period', 'weekly');
                $rent_month = $prices->addChild('price', $property->rent_month);
                    $rent_month->addAttribute('tenure', 'rent');
                    $rent_month->addAttribute('currency', 'THB');
                    $rent_month->addAttribute('period', 'monthly');

            $addresses = $item->addChild('addresses');
                $mainLocation = $this->getMainParentInLocation($property->locationNameInLocale[0]->location_id);
                $addresses->addChild('province', LocationTranslation::query()->where(['location_id' => $mainLocation, 'locale' => $this->locale])->first(['name'])->name);
                $addresses->addChild('city', htmlspecialchars(isset($property->locationNameInLocale[0]) ? $property->locationNameInLocale[0]->name : ''));

            $item->addChild('beds', $property->bedroom);
            $item->addChild('baths', $property->bathroom);
            $area = $item->addChild('area');
            $area->addChild('indoor_area', $property->indoor_area);
            $area->addChild('outdoor_area', $property->outdoor_area);
            $item->addChild('floor', htmlspecialchars($property->floor_number));
            $item->addChild('availability', htmlspecialchars($property->availability));
            $item->addChild('updated', $property->updated_at);
            $photos = $item->addChild('images');
            if (isset($property->images[0])) {
                foreach ($property->images->reverse() as $key => $image) {
                    $photos->addChild('image', htmlspecialchars(URL::to('/') . '/images/property/original/' . $image->name))
                        ->addAttribute('number', $key + 1);
                }
            }
        }

        if (!is_dir(public_path() . '/xml')) {
            mkdir(public_path() . '/xml');
        }
        $xmlFileOpen = fopen(public_path() . '/xml/xml.xml', 'w');
        fwrite($xmlFileOpen, $xml);

        return $xml->asXML(public_path() . '/xml/xml.xml') ? '1' : '0';
    }

    public function generateHipflat()
    {
        $this->locale = 'en';

        $this->locations = Location::all();

        $propertyTypes = [
            1 => 'retail',
            3 => 'condo',
            4 => 'house',
            6 => 'land',
            7 => 'office',
            8 => 'townhouse',
        ];

        $featuresMapping = array_flip([
            'duplex' => '',
            'penthouse' => '',
            'renovated' => '',
            'originalCondition' => '',
            'cornerUnit' => '',
            'groundFloor' => '',
            'greenView' => '',
            'cityView' => '',
            'seaView' => '',
            'mountainView' => '',
            'riverView' => '',
            'lakeView' => '',
            'petsAllowed' => 10,
            'smallPetsAllowed' => '',
            'airCon' => 11,
            'bathtub' => '',
            'privatePool' => '',
            'privateGarden' => '',
            'intercom' => '',
            'waterHeater' => '',
            'builtinKitchen' => '',
            'cookerHob' => '',
            'builtinWardrobe' => '',
            'walkinWardrobe' => '',
            'maidsRoom' => '',
            'studyRoom' => '',
            'balcony' => '',
            'patio' => '',
            'terrace' => '',
            'roofTerrace' => '',
            'garage' => '',
            'elevator' => '',
            'security' => 8,
            'cctv' => '',
            'parking' => 3,
            'openParking' => '',
            'basementParking' => '',
            'coveredParking' => '',
            'pool' => 1,
            'sauna' => 4,
            'jacuzzi' => '',
            'gym' => 2,
            'garden' => '',
            'playground' => 7,
            'shop' => '',
            'restaurant ' => '',
            'wifi' => 9,
            'clubhouse' => '',
            'lounge' => '',
            'basketball' => '',
            'billiard ' => '',
            'miniGolf ' => '',
            'puttingGreen' => '',
            'drivingRange' => '',
            'tennis' => 5,
            'squash' => 6,
            'badminton' => '',
            'library' => '',
            'functionRoom' => '',
            'karaoke' => '',
        ]);

        $properties = Property::with([
            'translations' => function ($query) {
                $query->where('property_translations.locale', '=', $this->locale);
            },
            'images' => function ($query) {
                $query->where('property_images.is_enabled', '=', 1);
            },
            'translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale',
        ])
            ->where('status', '=', 'published')
            ->where('is_sold', '=', 0)

            ->where(function ($query) {
                $query
                    ->where(function ($query) {
                        $locations = [];
                        // Location : Bangkok, Hua Hin, Koh Samui, Phuket
                        foreach ([1, 3, 4, 6] as $value) {
                            $locations = array_merge($locations, $this->makeChildLocationArr($value));
                        }
                        $query
                            ->where('to_sale', '=', 1)
                            ->where('sale_price', '>=', 5000000)
                            ->whereIn('location_id', $locations)
                            // Property type : condo, house, land, townhouse
                            ->whereIn('category_id', [3, 4, 6, 8])
                        ;
                    })
                    ->orWhere(function ($query) {
                        $query
                            ->where('to_rent', '=', 1)
                            ->where('rent_month', '>=', 45000)
                            // Property type : condo, house
                            ->whereIn('category_id', [3, 4])
                            // Location : Bangkok
                            ->whereIn('location_id', $this->makeChildLocationArr(1))
                        ;
                    })
                ;
            })

            ->orderBy('publish_date', 'DESC')->get();

        $properties = $this->createPropertyHref($properties);

        //Generate XML and save it in file "public/xml/xml.xml"
        $xml = new \SimpleXMLElement('<xml />', LIBXML_NOBLANKS);
        $rss = $xml->addChild('rss');
        $rss->addAttribute('version', '2.0');
        $rss->addAttribute(' xmlns:atom', 'http://www.w3.org/2005/Atom');
        $xmlChannel = $xml->addChild('channel');
        $xmlChannel->addChild('title', 'Five Stars Thailand Real Estate');
        $xmlChannel->addChild('description', 'Five Stars Thailand Real Estate');
        $xmlChannel->addChild('link', URL::to('/'));
        $xmlChannel->addChild('lastBuildDate', date(DATE_RFC2822));
        $xmlChannel->addChild('pubDate', date(DATE_RFC2822));
        $atom = $xmlChannel->addChild('atom:link');
        $atom->addAttribute('href', 'https://www.fivestars-thailand.com/xml/hipflat.xml');
        $atom->addAttribute('rel', 'self');
        $atom->addAttribute('type', 'application/rss+xml');
        foreach ($properties as $key => $property) {
            $item = $xmlChannel->addChild('item');
            $item->addChild('title', htmlspecialchars(isset($property->translations[0]) ? $property->translations[0]->subject : ''));
            $item->addChild('description', htmlspecialchars(isset($property->translations[0]) ? $property->translations[0]->detail : ''));
            $item->addChild('projectName', htmlspecialchars($property->building_name));
            $item->addChild('link', htmlspecialchars(url($property->propertyHref[$this->locale])));
            $item->addChild('refId', $property->code);

            $item->addChild('propertyType', isset($propertyTypes[$property->category_id]) ? $propertyTypes[$property->category_id] : '');
            $item->addChild('published', $property->publish_date);

            $vendorDetails = $item->addChild('vendorDetails');
            $vendorDetails->addChild('name', 'Five Stars Thailand Real Estate');
            $vendorDetails->addChild('phone', '081 271 7155');
            $vendorDetails->addChild('email', 'fabrice@fivestars-thailand.com');
            $vendorDetails->addChild('lineId', null);

            $item->addChild('area', $property->indoor_area);

            $coordinates = $item->addChild('coordinates');
            $coordinates->addChild('lat', $property->lat);
            $coordinates->addChild('lng', $property->lng);

            $item->addChild('salePrice', $property->sale_price ? $property->sale_price : null);
            $item->addChild('rentPrice', $property->rent_month ? $property->rent_month : null);
            $shortTermRent = $item->addChild('shortTermRent');
            $shortTermRent->addChild('dailyPrice', $property->rent_day ? $property->rent_day : null);
            $shortTermRent->addChild('weeklyPrice', $property->rent_week ? $property->rent_week : null);
            $shortTermRent->addChild('monthlyPrice', $property->rent_month ? $property->rent_month : null);

            $mainLocation = $this->getMainParentInLocation($property->locationNameInLocale[0]->location_id);
            $subregion = LocationTranslation::query()->where(['location_id' => $mainLocation, 'locale' => $this->locale])->first(['name'])->name;

            switch ($mainLocation) {
                case 3: // Hua Hin
                    $region = 'Prachuap Khiri Khan';
                    break;
                case 4:  // Koh Samui
                case 34: // Koh Phangan
                    $region = 'Surat Thani';
                    break;
                case 5: // Pattaya
                    $region = 'Chonburi';
                    break;
                default:
                    $region = $subregion;
                    $subregion = null;
            }

            $item->addChild('region', $region);
            if ($subregion) {
                $item->addChild('subregion', preg_replace(['/^Koh /i', '/Phangan/'], ['Ko ', 'Pha-ngan'], $subregion));
            }
            $item->addChild('place', htmlspecialchars(isset($property->locationNameInLocale[0]) ? $property->locationNameInLocale[0]->name : ''));

            $item->addChild('beds', $property->bedroom);

            $item->addChild('baths', $property->bathroom);

            $item->addChild('status', 'available');

            $category_bts = $item->addChild('station', $property->category_bts);
            $category_bts->addAttribute('category', 'bts');
            $category_mrt = $item->addChild('station', $property->category_mrt);
            $category_mrt->addAttribute('category', 'mrt');
            $category_arl = $item->addChild('station', $property->category_arl);
            $category_arl->addAttribute('category', 'arl');

            $features = $item->addChild('features');
            $propertyFacilities = $property->facilities;

            foreach ($propertyFacilities as $propertyFacility) {
                if (isset($featuresMapping[$propertyFacility->property_facility_id])) {
                    $features->addChild($featuresMapping[$propertyFacility->property_facility_id], 1);
                }
            }

            $item->addChild('floors', htmlspecialchars($property->floor_number));
            $item->addChild('availability', htmlspecialchars($property->availability));
            $item->addChild('updated', $property->updated_at);

            $photos = $item->addChild('photos');
            if (isset($property->images[0])) {
                foreach ($property->images->reverse() as $key => $image) {
                    $photos->addChild('photo', htmlspecialchars(URL::to('/') . '/images/property/original/' . $image->name));
                }
            }
        }

        // remove empty tags
        foreach (array_reverse($xml->xpath('//*[not(normalize-space()) and not(@*)]')) as $node) {
            unset($node[0]);
        }

        if (!is_dir(public_path() . '/xml')) {
            mkdir(public_path() . '/xml');
        }
        $xmlFileOpen = fopen(public_path() . '/xml/hipflat.xml', 'w');
        fwrite($xmlFileOpen, $xml);

        return $xml->asXML(public_path() . '/xml/hipflat.xml') ? '1' : '0';
    }

    public function generateDDproperty()
    {
        $this->locale = 'en';

        $this->locations = Location::all();

        $propertyTypesGroup = [
            1 => 'RET',
            3 => 'N',
            4 => 'B',
            6 => 'L',
            7 => 'OFF',
            8 => 'T',
        ];

        $featuresMapping = array_flip([
            'PENT' => '',
            'CORN' => '',
            'SEAV' => '',
            'CITYV' => '',
            'GREEN' => '',
            'POOLV' => 1,
            'RENO' => '',
            'ORIG' => '',
            'GFLO' => '',
            'LFLO' => '',
            'HFLO' => '',
            'COLO' => '',
            'MEET' => '',
            'VCON' => '',
            'ISDN' => '',
            'RECS' => '',
            'BROA' => 9,
            'SECR' => '',
            'RROOM' => '',
            'AVC' => '',
            'CAT' => '',
            'AIRC' => 11,
            'BATH' => '',
            'COOK' => '',
            'HAIR' => '',
            'INT' => '',
            'JACZ' => '',
            'PPOOL' => '',
            'WHEAT' => '',
            'ITSUP' => '',
            'CARP' => 3,
            'WATER' => '',
            'CFREE' => '',
            'LBAY' => '',
            'PANTR' => '',
            'KITCH' => '',
            'BGEN' => '',
            'OGEN ' => '',
            'OPLAN' => '',
            'SROOM' => '',
            'SMATV' => '',
            'GREEN ' => '',
            'OGEN' => '',
            'CCTV' => '',
            'SECU' => 8,
            'TURNS' => '',
            '24HR' => '',
            'WDEP' => '',
            'DTELE' => '',
            'FLOOR' => '',
            '450LX' => '',
            'PLATE' => '',
            'DPOW' => '',
            'BAL' => '',
            'BOMB' => '',
            'GAR' => '',
            'MAID' => '',
            'PAT' => '',
            'GARD' => '',
            'ROOF' => '',
            'TERR' => '',
            'WAR' => '',
        ]);

        $amenitiesMapping = array_flip([
            'SEC' => 8,
            'BAD' => '',
            'BCAR' => '',
            'BAS' => '',
            'BBQ' => 9,
            'BILL' => '',
            'CLUB' => '',
            'CCAR' => 3,
            'DRIV' => '',
            'FIT' => '',
            'FUNC' => '',
            'GAME' => '',
            'GYM' => 2,
            'JAC' => '',
            'KAR' => '',
            'LAU' => '',
            'LIB' => '',
            'LOU' => '',
            'MGOL' => '',
            'MMAR' => '',
            'MULTI' => '',
            'OCAR' => '',
            'PLAY' => 7,
            'PUT' => '',
            'REF' => '',
            'SAUNA' => 4,
            'SPA' => '',
            'SQU' => '',
            'STE' => '',
            'SWI' => 1,
            'TEN' => '',
            'WAD' => '',
            'PDEC' => '',
            'PAVI' => '',
            'SKYL' => '',
            'CLB' => '',
        ]);

        $propertiesMapping = array_column(
            json_decode(file_get_contents('https://services.propertyguru.com/v1/autocomplete?region=th&locale=en&limit=9999&object_type[4]=PROPERTY&query=0')),
            'objectId',
            'displayText'
        );

        $properties = Property::with([
            'translations' => function ($query) {
                $query->where('property_translations.locale', '=', $this->locale);
            },
            'images' => function ($query) {
                $query->where('property_images.is_enabled', '=', 1);
            },
            'translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale',
        ])
            ->where('status', '=', 'published')
            ->where('is_sold', '=', 0)
            ->where(function ($query) {
                $query
                    ->where('to_sale', '=', 1)
                    ->orWhereIn('location_id', $this->makeChildLocationArr(1));
            })
            ->orderBy('publish_date', 'DESC')->get();

        $xml_doc = '<?xml version="1.0"  standalone="no"?><!DOCTYPE listing-data SYSTEM "http://media.ddproperty.com/agency/feed.v.2.1.dtd"><listing-data></listing-data>';
        $xml = new SimpleXMLExtended($xml_doc);
        foreach ($properties as $key => $prop) {
            $property = $xml->addChild('property');
            $property->addChildWithCData('external-id', $prop->code);

            $location = $property->addChild('location');
            $location->addChild('streetname');
            $location->addChild('streetnumber');
            $location->addChild('post-code');
            $location->addChildWithCData('area-code', $prop->location->ddproperty_code);
            $location->addChildWithCData('district-code', mb_substr($prop->location->ddproperty_code, 0, 6));
            $location->addChildWithCData('region-code', mb_substr($prop->location->ddproperty_code, 0, 4));
            $location->addChild('longitude', $prop->lng);
            $location->addChild('latitude', $prop->lat);
            $location->addChild('property-id', $prop->building_name ? (array_values(array_filter($propertiesMapping, function($v, $k) use ($prop) { return preg_match('#'.preg_quote(trim($prop->building_name)).'#i', $k); }, ARRAY_FILTER_USE_BOTH))[0] ?? '') : '');
            $location->addChild('property-name', $prop->building_name);
            $location->addChildWithCData('property-type-group',isset($propertyTypesGroup[$prop->category_id]) ? $propertyTypesGroup[$prop->category_id] : '');

            $details = $property->addChild('details');
            $details->addChildWithCData('title', isset($prop->translations[0]) ? $prop->translations[0]->subject . ' [' . $prop->code . ']' : '');
            $details->addChildWithCData('title_en', isset($prop->translations[0]) ? $prop->translations[0]->subject . ' [' . $prop->code . ']' : '');
            $details->addChildWithCData('description', isset($prop->translations[0]) ? html_entity_decode(strip_tags($prop->translations[0]->detail)) : '');
            $details->addChildWithCData('description_en', isset($prop->translations[0]) ? html_entity_decode(strip_tags($prop->translations[0]->detail)) : '');

            $propertyFacilities = $prop->facilities;

            $result = [];
            foreach ($propertyFacilities as $propertyFacility) {
                if (isset($amenitiesMapping[$propertyFacility->property_facility_id])) {
                    $result [] = $amenitiesMapping[$propertyFacility->property_facility_id];
                }
            }

            $details->addChildWithCData('Amenities', implode(',', $result));

            $response = [];
            foreach ($propertyFacilities as $propertyFacility) {
                if (isset($featuresMapping[$propertyFacility->property_facility_id])) {
                    $response [] =  $featuresMapping[$propertyFacility->property_facility_id];
                }
            }

            $details->addChildWithCData('features', implode(',', $response));

            $price = $prop->sale_price;
            $priceType = $priceUnit = null;
            if($prop->rent_day > 0) {
                $price = $prop->rent_day;
                $priceType = 'BPD';
                $priceUnit = 'DAY';
            }
            if($prop->rent_week > 0) {
                $price = $prop->rent_week;
                $priceType = 'BPW';
                $priceUnit = 'WEEK';
            }
            if($prop->rent_month > 0) {
                $price = $prop->rent_month;
                $priceType = 'BAM';
                $priceUnit = 'MONTH';
            }
            $price_details = $details->addChild('price-details');
            $price_details->addChildWithCData('price', $price);
            $price_details->addChildWithCData('price-unit', $priceUnit);
            $price_details->addChildWithCData('price-description');
            $price_details->addChildWithCData('price-type', $priceType);
            $price_details->addChildWithCData('currency-code', 'THB');

            $rooms = $details->addChild('rooms');
            $rooms->addChildWithCData('num-bedrooms', $prop->bedroom);
            $rooms->addChildWithCData('num-bathrooms', $prop->bathroom );
            $rooms->addChildWithCData('extra-rooms');

            $details->addChildWithCData('furnishing', $prop->furnished == 1 ? 'FULL' : 'UNFUR');

            $size_details = $details->addChild('size-details');
            $size_details->addChildWithCData('floor-area', $prop->indoor_area);
            $size_details->addChildWithCData('floor-size-x');
            $size_details->addChildWithCData('floor-size-y');
            $size_details->addChildWithCData('land-area', $prop->outdoor_area);
            $size_details->addChildWithCData('land-size-x');
            $size_details->addChildWithCData('land-size-y');

            $details->addChild('numberoffloors');
            $details->addChildWithCData('floor-level', $prop->floor_number ? $prop->floor_number : null);
            $details->addChildWithCData('facing');
            $details->addChild('parking-spaces');

            $property->addChildWithCData('listing-type', $prop->to_rent == 1 ? 'RENT' : 'SALE');

            $property->addChild('agent-id', null);
            $property->addChildWithCData('custom-name', 'Five Stars Thailand Real Estate');
            $property->addChildWithCData('custom-phone', '02 652 0576');
            $property->addChildWithCData('custom-mobile', '081 271 7155');
            $property->addChildWithCData('custom-email', 'fabrice@fivestars-thailand.com');
            $property->addChildWithCData('tenure', 'F');

            $property->addChild('sold', $prop->is_sold == 1 ? 'YES' : 'NO');

            $statusProperty= $prop->status;

            switch ($statusProperty) {
                case 'published':
                    $status = 'ACTIVE';
                    break;
                case 'disabled':
                    $status = 'DELIST';
                    break;
                default:
                    $status = 'DRAFT';
            }

            $property->addChild('status', $status);

            if (isset($prop->images[0])) {
                foreach ($prop->images->reverse() as $key => $image) {
                    $photo = $property->addChild('photo');
                    $photo->addChildWithCData('picture-url', URL::to('/images/property/original/'.$image->name));
                    $photo->addChildWithCData('picture-caption', $image->name);
                }
            }
        }

        $xml->saveXML(public_path() .'/xml/ddproperty.xml');
    }

    public function generateSitemap()
    {
        $xml = new \SimpleXMLElement('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" />');

        //
        // Static pages
        //

        $pages = array(
            array(
                'en' => url('en'),
                'fr' => url('fr'),
                'changefreq' => 'monthly',
            ),
            array(
                'en' => url('en/faq'),
                'fr' => url('fr/faq'),
                'changefreq' => 'yearly',
            ),
            array(
                'en' => url('en/about-us'),
                'fr' => url('fr/about-us'),
                'changefreq' => 'yearly',
            ),
            array(
                'en' => url('en/links'),
                'fr' => url('fr/links'),
                'changefreq' => 'yearly',
            ),
            array(
                'en' => url('en/our-services'),
                'fr' => url('fr/our-services'),
                'changefreq' => 'yearly',
            ),
            array(
                'en' => url('en/agencies-in-thailand'),
                'fr' => url('fr/agencies-in-thailand'),
                'changefreq' => 'yearly',
            ),
            array(
                'en' => url('en/blog'),
                'fr' => url('fr/blog'),
                'changefreq' => 'monthly',
            ),
        );

        foreach ($pages as $page) {
            $this->addSitemap($xml, $page);
        }

        //
        // Blog posts
        //

        $articles = Article::with('translations')
            ->where(['is_enabled' => 1])
            ->orderBy('id', 'asc')
            ->get();

        foreach ($articles as $key => $article) {
            $enArticle = $article->translations->filter(function ($translation) { return $translation['locale'] === 'en'; })->first();
            $frArticle = $article->translations->filter(function ($translation) { return $translation['locale'] === 'fr'; })->first();
            $this->addSitemap($xml, array(
                'en' => ($enArticle && $enArticle->description) ? url("en/blog/{$article->id}") : null,
                'fr' => ($frArticle && $frArticle->description) ? url("fr/blog/{$article->id}") : null,
                'changefreq' => 'monthly',
            ));
        }

        //
        // Listings
        //

        $transactionTypes = array('to_rent', 'to_sale');
        $categories = PropertyCategory::with('translations')->get();
        $locations = Location::with('translations')->get();

        // By transaction type (rent, sale)
        foreach ($transactionTypes as $transactionType) {

            $this->addSitemap($xml, array(
                'en' => $enTransactionTypeUrl = url('/en/'.urlencode(trans('url.'.$transactionType, [], 'en'))),
                'fr' => $frTransactionTypeUrl = url('/fr/'.urlencode(trans('url.'.$transactionType, [], 'fr'))),
                'changefreq' => 'daily',
            ));

            // By transaction type & category (rent/condo, sale/house, ...)
            foreach ($categories as $category) {
                $enCategory = $category->translations->filter(function ($translation) { return $translation['locale'] === 'en'; })->first();
                $frCategory = $category->translations->filter(function ($translation) { return $translation['locale'] === 'fr'; })->first();
                $this->addSitemap($xml, array(
                    'en' => url($enTransactionTypeUrl.'/'.urlencode($enCategory->url)),
                    'fr' => url($frTransactionTypeUrl.'/'.urlencode($frCategory->url)),
                    'changefreq' => 'daily',
                ));
            }

        }

        // By location (bangkok, bangkok/sathorn, phuket, ...)
        foreach ($locations as $location) {

            $enLocation = $location->translations->filter(function ($translation) { return $translation['locale'] === 'en'; })->first();
            $frLocation = $location->translations->filter(function ($translation) { return $translation['locale'] === 'fr'; })->first();

            $changefreq = $location->code === 'BKK' ? 'daily' : 'weekly';

            // Discover city page
            $this->addSitemap($xml, array(
                'en' => ($enLocation && $enLocation->description) ? url('/en'.($enLocationUrl = $this->getLocationHref($location->id, 'en', $locations))).'/' : null,
                'fr' => ($frLocation && $frLocation->description) ? url('/fr'.($frLocationUrl = $this->getLocationHref($location->id, 'fr', $locations))).'/' : null,
                'changefreq' => $changefreq,
            ));

            // By transaction type & location (rent/bangkok, sale/phuket)
            foreach ($transactionTypes as $transactionType) {

                $this->addSitemap($xml, array(
                    'en' => url(($enTransactionTypeUrl = '/en/'.urlencode(trans('url.'.$transactionType, [], 'en'))).$enLocationUrl),
                    'fr' => url(($frTransactionTypeUrl = '/fr/'.urlencode(trans('url.'.$transactionType, [], 'fr'))).$frLocationUrl),
                    'changefreq' => $changefreq,
                ));

                // By transaction type & category & location (rent/condo/bangkok, sale/house-villa/phuket, ...)
                foreach ($categories as $category) {
                    $enCategory = $category->translations->filter(function ($translation) { return $translation['locale'] === 'en'; })->first();
                    $frCategory = $category->translations->filter(function ($translation) { return $translation['locale'] === 'fr'; })->first();
                    $this->addSitemap($xml, array(
                        'en' => url($enTransactionTypeUrl.'/'.urlencode($enCategory->url).$enLocationUrl),
                        'fr' => url($frTransactionTypeUrl.'/'.urlencode($frCategory->url).$frLocationUrl),
                        'changefreq' => $changefreq,
                    ));
                }

            }

        }

        //
        // Properties
        //

        $properties = Property::with('translations')->where('status', '=', 'published')->get();

        $properties = $this->createPropertyHref($properties);

        foreach ($properties as $property) {
            $this->addSitemap($xml, array(
                'en' => url($property->propertyHref['en']),
                'fr' => url($property->propertyHref['fr']),
                'changefreq' => 'monthly',
            ));
        }

        if(!is_dir(public_path().'/xml')){
            mkdir(public_path().'/xml');
        }

        $xmlFileOpen = fopen(public_path().'/xml/sitemap.xml', 'w');
        fwrite($xmlFileOpen, $xml);

        return $xml->asXML(public_path().'/xml/sitemap.xml') ? '1' : '0';
    }

    public function download()
    {
        return view('web.xml.index');
    //        return response()->download(public_path().'/xml/xml.xml');
    }

    public function createLocationsArrByParentId($parentId)
    {
        $result = [];
        $checked = [];

        $checkCache = new CheckCacheController;
        $locations = $checkCache->checkCache('locations');

        foreach ($locations as $location) {
            if ($location->parent_id == $parentId) {
                foreach ($location->translations as $ltr) {
                    if ($ltr->locale == \App::getLocale()) {
                        $result[$ltr->location_id] = ['url' => $ltr->url, 'name' => $ltr->name, 'locale' => $ltr->locale, 'id' => $ltr->location_id];
                        $checked[$ltr->location_id] = $ltr->location_id;
                    } else if ($ltr->locale == config('app.fallback_locale') && !isset($checked[$location->location_id])) {
                        $result[$ltr->location_id] = ['url' => $ltr->url, 'name' => $ltr->name, 'locale' => $ltr->locale, 'id' => $ltr->location_id];
                        $checked[$ltr->location_id] = $ltr->location_id;
                    } else if (!isset($checked[$ltr->location_id])) {
                        $result[$ltr->location_id] = ['url' => $ltr->url, 'name' => $ltr->name, 'locale' => $ltr->locale, 'id' => $ltr->location_id];
                    }
                }
            }
        }
        return $result;
    }

    public function createCategoriesArray()
    {
        $categoryData = PropertyCategory::with(['translations' => function($query){
                $query->where('property_category_translations.locale', '=', $this->locale);
            }])
            ->where(['is_enabled' => 1])
            ->get()
            ->toArray();
        $response = [];
        foreach ($categoryData as $key => $category) {
            for ($i = 0; $i < count($category['translations']); $i++) {
                if ($category['translations'][$i]['locale'] == 'en') {
                    $response[$category['id']] = $category['translations'][$i]['name'];
                    break;
                } else if ($category['translations'][$i]['locale'] == 'fr') {
                    $response[$category['id']] = $category['translations'][$i]['name'];
                    break;
                } else {
                    $response[$category['id']] = $category['translations'][$i]['name'];
                }
            }
        }
        return $response;

    }

    public function createLanguagesArray()
    {
        $languages = Language::where('is_enabled_web', '=', true)
            ->get()
            ->toArray();
        foreach ($languages as $key=>$value){
            $response[$value['locale']] = $value['name'];
        }
        return $response;
    }

    public function createPropertyHref($properties, $locations = null)
    {
        if (!isset($locations))
            $locations = Location::with('translations')->get();

        $locales = ['en', 'fr'];

        foreach ($properties as $key => $p) {
            $properties[$key]->propertyHref = [];
            $category = PropertyCategory::with('translations')->where('id', $p->category_id)->first();
            foreach ($locales as $k => $locale) {
                $propUrl = $locale;
                if ($p->to_sale) {
                    $propUrl .= '/' . urlencode(trans('url.to_sale', [], $locale));
                } else {
                    $propUrl .= '/' . urlencode(trans('url.to_rent', [], $locale));
                }
                // category
                $propUrl .= '/' . $category->translations->filter(function ($translation) use ($locale) { return $translation['locale'] === $locale; })->first()->url;
                // property
                $propUrl .= $this->getLocationHref($p->location_id, $locale, $locations);
                $properties[$key]->propertyHref[$locale] = $propUrl . '/' . $p->code;
            }
        }
        return $properties;
    }

    public function getLocationHref($locationId, $locale = null, $locations = null)
    {
        $locUrl = '';

        if (!isset($locations))
            $locations = Location::with('translations')->get();

        $locationsTranslationsUrl = $this->locationsTranslationsUrl($locations, $locationId, $locale);
        foreach ($locationsTranslationsUrl as $url) {
            $locUrl .= '/' . urlencode($url);
        }

        return $locUrl;
    }

    public function locationsTranslationsUrl($locations, $locationId, $locale = null)
    {
        if (!isset($locale))
            $locale = $this->locale;

        $result = [];
        $parentLocations = $this->createArr($locations, $locationId);

        foreach ($locations as $location) {
            foreach ($parentLocations as $pl) {
                $translation = $location->translations->filter(function ($translation) use ($locale, $pl) {
                    return ($translation['locale'] === $locale) && ($translation['location_id'] == $pl);
                })->first();
                if (isset($translation)) {
                    array_push($result, $translation->url);
                }
            }
        }

        return $result;
    }

    public function createArr($locations, $locationId)
    {
        $result = [];
        foreach ($locations as $l) {
            if ($l->id == $locationId) {
                array_push($result, $locationId);
                if ($l->parent_id !== 0) {
                    $result = array_merge($result, $this->createArr($locations, $l->parent_id));
                }
            }
        }
        return $result;
    }

    public function makeChildLocationArr($locationId)
    {
        $locations = $this->locations;
        $result = $this->getChildLocations($locationId, $locations);
        array_unshift($result, +$locationId);
        return $result;
    }

    public function getChildLocations($locationId, $locations)
    {
        $result = [];
        foreach ($locations as $location) {
            if ($location->parent_id == $locationId) {
                array_push($result, $location->id);
                $result = array_merge($result, $this->getChildLocations($location->id, $locations));
            }
        }
        return $result;
    }

    public function getMainParentInLocation($locationId)
    {
        $parent = 0;
        $location = Location::where(['id' => $locationId])->get()->toArray();
        if ($location[0]['parent_id'] != 0) {
            $parent = $this->getMainParentInLocation($location[0]['parent_id']);
        }
        else {
            $parent = $location[0]['id'];
        }

        return $parent;
    }

    private function addSitemap(\SimpleXMLElement $xml, array $links)
    {
        $links = array_filter($links);
        if (!empty($links)) {
            $url = $xml->addChild('url');
            // <changefreq>
            if (isset($links['changefreq'])) {
                $url->addChild('changefreq', $links['changefreq']);
                unset($links['changefreq']);
            }
            // <loc>
            $loc = array_shift($links);
            $url->addChild('loc', $loc);
            // Alternate links
            foreach ($links as $hreflang => $href) {
                $link = $url->addChild('xhtml:link', null, 'http://www.w3.org/1999/xhtml');
                $link->addAttribute('rel', 'alternate');
                $link->addAttribute('hreflang', $hreflang);
                $link->addAttribute('href', $href);
            }
        }
    }

}
