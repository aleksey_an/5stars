<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Session;
use App\Language;

class LanguageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $language = Language::where('name', 'LIKE', "%$keyword%")
                    ->orWhere('locale', 'LIKE', "%$keyword%")
                    ->orWhere('iso_639_3', 'LIKE', "%$keyword%")
                    ->orderBy('sequence', 'asc')
                    ->paginate($perPage);
        } else {
            $language = Language::paginate($perPage);
        }

        return view('admin.language.index', compact('language'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $language = null;
        return view('admin.language.create', compact('language'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, $this->getRules(true));

        $language = Language::orderBy('sequence', 'DESC')->first();
        $requestData = $request->only('name', 'locale', 'iso_639_3', 'is_enabled', 'is_enabled_web');
        $requestData['sequence'] = $language->sequence + 1;

        $language = Language::create($requestData);
        $this->saveImage($request, $language);
//        $this->createLangFiles($requestData['locale']);

        Session::flash('success', 'New language successfully added!');
        
        return redirect('admin/language');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $language = Language::findOrFail($id);

        return view('admin.language.show', compact('language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $language = Language::findOrFail($id);

        return view('admin.language.edit', compact('language'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request) {

        $this->validate($request, $this->getRules());
        
        $requestData = $request->only('name', 'locale', 'iso_639_3', 'is_enabled', 'is_enabled_web');
        $requestData['is_enabled_web'] = isset($requestData['is_enabled_web']) ? $requestData['is_enabled_web'] : 1;
        $language = Language::findOrFail($id);
        $language->update($requestData);
        $this->saveImage($request, $language);

        Session::flash('success', "$language->name language successfully updated!");

        return redirect('admin/language');
    }
    
     /**
     * Change the position of item - up to list
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function up($id) {
        $language = Language::findOrFail($id);
        
        $language->sequence += 1;
        $language->save(); 
        // Reorder all
        
        
        return redirect('admin/language');
    }

    /**
     * Change the position of item - down
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function down($id) {
        $language = Language::findOrFail($id);

        return redirect('admin/language');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        $language = Language::findOrFail($id);
        $lang_name = $language->name;
        \File::Delete(public_path('/images/language/') . $language->locale . '.jpg');

//        if(is_dir(resource_path("lang/" . $language->locale))) $this->deleteLangFiles(resource_path("lang/" . $language->locale));
//        if(is_dir(resource_path("lang/initial/" . $language->locale))) $this->deleteLangFiles(resource_path("lang/initial/" . $language->locale));

        Language::destroy($id);
        Session::flash('success', "$lang_name language successfully deleted!");
        return redirect('admin/language');
    }

    /**
     * Save Language flag image in /img/language folder
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Database\Eloquent\Model $language
     *
     * @return bool
     */
    protected function saveImage($request, $language) {
        // checking if language's folder exists
        if (!is_dir(public_path() . '/images/language')) {
            mkdir(public_path() . '/images/language');
        }

        if ($request->hasFile('image')) {
            $image = Image::make($request->file('image'));
            $image->fit(20, 20);
            $imageName =  $language->locale . '.' . $request->file('image')->getClientOriginalExtension();
            $imageFullPath = public_path() . '/images/language/' . $imageName;
            $image->save($imageFullPath);
            $language->image = $imageName;
            return $language->save();
        }

        return false;
    }
    
    /**
     * Get validation rules
     *
     * @param bool $create
     *
     * @return array
     */
    protected function getRules($create = null) {
        $rules = array(
            'name' => 'required|max:255',
            'locale' => 'required|size:2',
            'iso_639_3' => 'required|size:3',
            'is_enabled' => 'required|bool',
            'is_enabled_web' => 'bool'
        );

        if ($create) {
            $rules['image'] = 'required|image|mimes:jpeg,png,jpg|max:2048';
        }
        return $rules;
    }

    /**
     * Clone Language files in /resources/lang/ folder
     */
    protected function createLangFiles($locale) {
        // checking if language's folder exists
        if (!is_dir(resource_path("lang/$locale"))) {
            $this->recurseCopy(resource_path("lang/en"), resource_path("lang/$locale"));
            $this->recurseCopy(resource_path("lang/initial/en"), resource_path("lang/initial/$locale"));

            return true;
        }

        return false;
    }

    protected function recurseCopy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recurseCopy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    protected function deleteLangFiles($dir) {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->deleteLangFiles("$dir/$file") : unlink("$dir/$file");
        }
        rmdir($dir);

        return false;
    }

}
