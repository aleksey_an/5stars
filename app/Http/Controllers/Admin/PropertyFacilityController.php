<?php

namespace App\Http\Controllers\Admin;

use App\PropertyFacilityData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language;
use App\PropertyFacility;
use App\PropertyFacilityTranslations;
use Illuminate\Support\Facades\Auth;
use Session;

class PropertyFacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facilities = PropertyFacility::with('translations')->get();
        return view('admin.property-facility.index', compact('facilities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        $facility = null;
        return view('admin.property-facility.create', compact('facility', 'languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestTranslations = $this->processingDescriptionRequest($request);
        $this->validate($request, [
                'is_enabled' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]
        );
        $rAll = $request->all();
        $sequence = null;
        $maxSequence = PropertyFacility::whereRaw('sequence = (select max(`sequence`) from property_facilities )')->get();
        if (isset($maxSequence[0])) {
            $sequence = +$maxSequence[0]->sequence + 1;
        } else {
            $sequence = 1;
        }
        $rAll['sequence'] = $sequence;
        $rAll['created_by'] = Auth::user()->id;
        $newFacility = PropertyFacility::create($rAll);

        foreach ($requestTranslations as $keyLocale => $val) {
            PropertyFacilityTranslations::create([
                'property_facility_id' => $newFacility->id,
                'locale' => $keyLocale,
                'name' => $requestTranslations[$keyLocale]['name']
            ]);
        }

        if (isset($rAll['image'])) {
            $this->uploadImage($request, $newFacility->id, false);
        }
        return redirect('admin/facility');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facility = PropertyFacility::where(['id' => $id])->with('translations')->get();
        $facility = $facility[0];
        $responseLang = Language::where(['is_enabled' => true])->get();
        $languages = [];
        foreach ($responseLang as $value) {
            $languages[$value['locale']] = $value['name'];
        }
        /*Create facility translations*/
        $translations = $this->createTranslationsList(isset($facility['translations']) ? $facility['translations'] : null);
        return view('admin.property-facility.edit', compact('facility', 'languages', 'translations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*Create request translations array*/
        $requestTranslations = $this->processingDescriptionRequest($request);
        $rAll = $request->all();
        /*dd($rAll, $id);*/
        $this->validate($request, [
                'is_enabled' => 'required'
            ]
        );
        $userId = Auth::user()->id;
        $rAll['updated_by'] = $userId;

        PropertyFacility::where(['id' => $id])->update(['is_enabled' => $rAll['is_enabled'], 'updated_by' => $userId]);

        if (isset($rAll['image'])) {
            $this->uploadImage($request, $id, true);
        }

        foreach ($requestTranslations as $keyLocale => $val) {
            $checkTranslation = PropertyFacilityTranslations::where(['property_facility_id' => $id, 'locale' => $keyLocale])->get()->toArray();
            if (isset($checkTranslation[0])) {
                $checkLanguage = Language::where(['locale' => $keyLocale])->get();
                if (isset($checkLanguage[0]) && $checkLanguage[0]->is_enabled) {
                    PropertyFacilityTranslations::where(['property_facility_id' => $id, 'locale' => $keyLocale])->update([
                        'name' => $requestTranslations[$keyLocale]['name']
                    ]);
                }
            } else {
                PropertyFacilityTranslations::create([
                    'property_facility_id' => $id,
                    'locale' => $keyLocale,
                    'name' => $requestTranslations[$keyLocale]['name']
                ]);
            }
        }
        Session::flash('success', 'Property Category successfully updated !');
        return redirect(redirect()->getUrlGenerator()->previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkImage = PropertyFacility::where(['id' => $id])->get()->toArray();
        if (isset($checkImage[0]['image'])) {
            \File::Delete(public_path('/images/facility/') . $checkImage[0]['image']);
        }
        PropertyFacilityData::where(['property_facility_id' => $id])->delete();
        PropertyFacilityTranslations::where(['property_facility_id' => $id])->delete();
        PropertyFacility::where(['id' => $id])->delete();
        return redirect(redirect()->getUrlGenerator()->previous());
    }


    public function uploadImage($request, $facilityId, $isCheck)
    {
        if (!is_dir(public_path() . '/images/facility')) {
            mkdir(public_path() . '/images/facility');
        }
        if ($isCheck) {
            $checkImage = PropertyFacility::where(['id' => $facilityId])->get()->toArray();
            if (isset($checkImage[0]['image'])) {
                \File::Delete(public_path('/images/facility/') . $checkImage[0]['image']);
            }
        }
        $image = $request->file('image');
        $input['imagename'] = 'f_' . time() . '_orig' . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/facility');
        $image->move($destinationPath, $input['imagename']);
        PropertyFacility::where(['id' => $facilityId])->update(['image' => $input['imagename']]);
    }
}
