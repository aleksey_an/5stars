<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Session;
use App\Role;
use App\Permission;

class RoleController extends Controller
{
    public function __construct(){
        $this->middleware('role:super_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $role = Role::where('name', 'LIKE', "%$keyword%")
                ->orWhere('display_name', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $role = Role::paginate($perPage);
        }

        return view('admin.role.index', compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $permissions = null;
        return view('admin.role.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'name' => 'required|unique:roles|min:3|max:255',
                'display_name' => 'required|min:3|max:255',
                'description' => 'required|min:3|max:255'
            ]
        );

        $requestData = $request->all();

        Role::create($requestData);

        Session::flash('flash_message', 'Role added!');

        return redirect('admin/role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $role = Role::findOrFail($id);

        return view('admin.role.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();
        $arrRolePermissions = $role->perms()->get();
        $rolePermissions = [];
        foreach ($arrRolePermissions as $key => $value) {
            $rolePermissions[$value['id']] = $value['name'];
        }
//      dd($rolePermissions, $permissions);
        return view('admin.role.edit', compact('role', 'permissions', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
                'name' => 'unique:roles,name,' . $id . '|required|min:3|max:255',
                'display_name' => 'required|min:3|max:255',
                'description' => 'required|min:3|max:255'
            ]
        );
        $requestData = $request->all();
        $role = Role::findOrFail($id);
        $role->update($requestData);
        $arrPerm = [];
        foreach ($requestData as $key => $value) {
            if ($value == 'on') {
                array_push($arrPerm, $key);
            }
        }
        $role->perms()->sync($arrPerm);
        return redirect('admin/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
//        dd($id);
        Role::destroy($id);

        return redirect('admin/role');
    }


}
