<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\Location;
use App\Property;
use Session;

use App\PropertyCategory;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     *Create translations list
     */
    public function createTranslationsList($translations)
    {
        $result = [];
        if (!$translations || count($translations) == 0)
            return null;
        foreach ($translations as $key => $val) {
            $result[$val['locale']] = $translations[$key];
        }
        return $result;
    }

    /**
     * Create and validate request beginning from  "description-" locale- "name, description"
     */
    public function processingDescriptionRequest($request)
    {
        /*Create request translations array*/
        $rAll = $request->all();
        $result = [];
        foreach ($rAll as $key => $r) {
            if (explode('-', $key)[0] == 'description') {
                if (explode('-', $key)[2] == 'name') {
                    $result[explode('-', $key)[1]]['name'] = $r;
                }
                if (explode('-', $key)[2] == 'description') {
                    $result[explode('-', $key)[1]]['description'] = $r ? $r : '';
                }
            }
        }
        /*Filter property translations*/
        foreach ($result as $key => $r) {
            if (!$r['name']) {
                unset($result[$key]);
            }
        }
        /*Validate code translations*/
        if (count($result) == 0 || !isset($result[config('app.locale')])) {
            Session::flash('error', 'You must fill out a name in at ' . config('app.locale') . ' language');
            $this->validate($request, [
                'description-' . config('app.locale') . '-name' => 'required|min:3|max:255'
            ]);
        }
        return $result;
    }

    /**
     * Create Locations Multi Array
     */
    public function locationsMultiArray(array $elements, $parentId = 0, $chN = 0)
    {
        $branch = array();
        $chN++;
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->locationsMultiArray($elements, $element['id'], $chN);
                if ($children) {
                    foreach ($children as $key => $ch) {
                        $padding = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        $paddingLeft = '';
                        for ($i = 1; $i <= $chN; $i++) {
                            $paddingLeft = $paddingLeft . $padding;
                        }
                        $children[$key]['name'] = $paddingLeft . '-' . $children[$key]['translations'][0]['name'];
                    }
                    $element['chN'] = $chN;
                    $element['children'] = $children;
                }
                $element['name'] = $element['translations'][0]['name'];
                $branch[] = $element;
            }
        }
        return $branch;
    }

    public function createLocationsList($locationsData)
    {
        $locMultiArr = $this->locationsMultiArray($locationsData);
        return $this->createArrayFromMultiArray($locMultiArr);
    }

    public function createArrayFromMultiArray($locMultiArr)
    {
        $result = [];
        foreach ($locMultiArr as $arr) {
            if (isset($arr['children'])) {
                $result[$arr['id']] = $arr['name'];
                $ch = $this->createArrayFromMultiArray($arr['children']);
                foreach ($ch as $key => $a) {
                    $result[$key] = $a;
                }
            } else {
                $result[$arr['id']] = $arr['name'];
            }
        }
        return $result;
    }

    public function createCategoriesList()
    {
        $categoryData = PropertyCategory::with('translations')->get()->toArray();
        $response = [];
        foreach ($categoryData as $key => $category) {
            for ($i = 0; $i < count($category['translations']); $i++) {
                if ($category['translations'][$i]['locale'] == 'en') {
                    $response[$category['id']] = $category['translations'][$i]['name'];
                    break;
                } else if ($category['translations'][$i]['locale'] == 'fr') {
                    $response[$category['id']] = $category['translations'][$i]['name'];
                    break;
                } else {
                    $response[$category['id']] = $category['translations'][$i]['name'];
                }
            }
        }
        return $response;
    }

    public function cleanString($str)
    {
        $str = preg_replace('/[^\p{L}\p{N}\s]/u', ' ', $str);
        $str = str_replace(' ', '-', $str);
        return strtolower($str);
    }

    private function cleanStr($str)
    {
        $str = str_replace('\'', '`', $str);
        $str = str_replace('"', '`', $str);
        return $str;
    }

    //Processing translations with URL field check url to unique
    public function processingTranslationsInRequest($request, $table, $field = null, $parentId = null, $skipDescr = false)
    {
        /*Create request translations array*/
        $rAll = $request->all();
        $result = [];
        foreach ($rAll as $key => $r) {
            if (explode('-', $key)[0] == 'trans') {
                if (explode('-', $key)[2] == 'url') {
                    $result[explode('-', $key)[1]]['url'] = $r ? $this->cleanString($r) : '';
                }
                if (explode('-', $key)[2] == 'name') {
                    $result[explode('-', $key)[1]]['name'] = $r ? $this->cleanStr($r) : '';
                }
                if (explode('-', $key)[2] == 'description') {
                    $result[explode('-', $key)[1]]['description'] = $r ? (!$skipDescr ? $this->cleanStr($r) : $r) : '';
                }
            }
        }
        /*Filter property translations*/
        foreach ($result as $key => $r) {
            if (!$r['name']) {
                unset($result[$key]);
            }
        }

        /*Check Url like unique in table property_category_translations*/
        foreach ($result as $key => $r) {
            if ($parentId) {
                $checkUrls = DB::table($table)->where(['url' => $r['url']])->select(['id', 'url', $field])->get()->toArray();
                foreach ($checkUrls as $u) {
                    if (+$u->$field != +$parentId) {
                        $this->validate($request, [
                            'trans-' . $key . '-url' => 'required|min:1|max:255|unique:' . $table . ',url,' . $u->id
                        ]);
                    }
                }

            } else {
                $this->validate($request, [
                    'trans-' . $key . '-url' => 'required|min:1|max:255|unique:' . $table . ',url,' . $r['url']
                ]);
            }
        }

        /*Validate code translations*/
        if (count($result) == 0 || !isset($result[config('app.fallback_locale')])) {
            $this->validate($request, [
                'trans-' . config('app.fallback_locale') . '-name' => 'required|min:3|max:255'
            ]);
        }
        return $result;
    }

    public function makeChildLocationArr($locationId)
    {
        $locations = Location::all();
        $result = $this->getChildLocations($locationId, $locations);
        array_unshift($result, +$locationId);
        return $result;
    }

    public function getChildLocations($locationId, $locations)
    {
        $result = [];
        foreach ($locations as $location) {
            if ($location->parent_id == $locationId) {
                array_push($result, $location->id);
                $result = array_merge($result, $this->getChildLocations($location->id, $locations));
            }
        }
        return $result;
    }

    public function getSearchResult($rAll, $perPage, $availableLocations = null)
    {
        $searchArr = [];
        if (isset($rAll['code'])) {
            return $this->returnSearchProperties(['code' => $rAll['code']], $perPage, $availableLocations);
        }
        if (isset($rAll['contact_name'])) {
            return $this->returnSearchProperties(['contact_name' => $rAll['contact_name']], $perPage, $availableLocations);
        }
        if (isset($rAll['building_name'])) {
            return $this->returnSearchProperties(['building_name' => $rAll['building_name']], $perPage, $availableLocations);
        }

        foreach ($rAll as $key => $r) {
            if ($key == 'category_id' || $key == 'bedroom' || $key == 'to_rent' || $key == 'to_sale' || $key == 'status') {
                if ($r)
                    $searchArr[$key] = $r;
            }
        }

        if (isset($rAll['location_id'])) {
            $parentLocations = $this->makeChildLocationArr($rAll['location_id']);
            $property = Property::with(['categoryNameInEn', 'locationNameInEn', 'translations.lang'])
                ->where($searchArr)
                ->whereIn('location_id', $parentLocations)
                ->orderBy('updated_at', 'desc')
                ->paginate($perPage);
            $searchArr['location_id'] = $rAll['location_id'];
            $property->appends($searchArr);
            return $property;
        }
        return $this->returnSearchProperties($searchArr, $perPage, $availableLocations);

    }

    public function returnSearchProperties($searchArr, $perPage, $availableLocations = null)
    {
        if(isset($availableLocations)){
            $property = Property::with(['categoryNameInEn', 'locationNameInEn', 'translations.lang'])
                ->whereIn('location_id', $availableLocations)
                ->where($searchArr)
                ->orderBy('updated_at', 'desc')
                ->paginate($perPage);
            $property->appends($searchArr);
            return $property;
        }

        $property = Property::with(['categoryNameInEn', 'locationNameInEn', 'translations.lang'])
            ->where($searchArr)
            ->paginate($perPage);
        $property->appends($searchArr);
        return $property;
    }
}
