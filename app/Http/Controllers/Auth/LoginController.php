<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            $this->username() => 'required|email|max:255',
            'password' => 'required|min:6',
        ])->setAttributeNames([
            'email' => trans('common.email'),
            'password' => trans('common.password')
        ]);

        if ($validator->fails()) {
            return redirect('/login')
                ->withInput()
                ->withErrors($validator);
        } else {
            if ($this->attemptLogin($request)) {
                $user = Auth::user();
                if($user->is_enabled){
                    return redirect()->intended('/admin');
                } else {
                    Auth::logout();
                    return redirect('/access-denied');
                }
            }
        }
        return $this->sendFailedLoginResponse($request);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
