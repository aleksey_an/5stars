<?php

namespace App\Http\Controllers\Forms;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Property;
use App\LocationUser;
use App\User;
use App\Configuration;

class FormsController extends Controller
{
    protected $mailchimp;

    /**
     * Pull the Mailchimp-instance from the IoC-container.
     */
    public function __construct(\Mailchimp $mailchimp)
    {
        $this->mailchimp = $mailchimp;
    }

    public function homeContactUs(Request $request)
    {
        $this->validate($request, [
                'name' => 'required|max:100',
                'phone' => 'required|max:100',
                'email' => 'required|max:100',
                'message' => 'required|max:1000'
            ]
        );

        $rAll = $request->all();
        $data = [
            'name' => strip_tags($rAll['name']),
            'email' => $rAll['email'],
            'phone' => strip_tags($rAll['phone']),
            'text' => strip_tags($rAll['message'])
        ];
        $addressTo = Configuration::select(['key', 'value'])->where(['key' => 'company_email'])->first();
        $addressTo = $addressTo->value;

        Mail::send('emails.contact-us', $data, function ($m) use ($addressTo, $data) {
            $m->from($data['email'], $data['name']);
            $m->sender($data['email'], $data['name']);
            $m->to($addressTo, 'Receiver');
        });
        return ' Success';
    }

    public function contactWithAgent(Request $request)
    {
        $rAll = $request->all();

        $this->validate($request, [
                'name' => 'required|max:100',
                'phone' => 'required|max:100',
                'email' => 'required|max:100',
                'message' => 'required|max:1000',
                'propertyCode' => 'required',
                'viewed' => 'required',
                'favorites' => 'required'
            ]
        );
        $data = [
            'name' => $rAll['name'],
            'phone' => $rAll['phone'],
            'email' => $rAll['email'],
            'text' => $rAll['message'],
            'propertyCode' => $rAll['propertyCode'],
            'rentDate' => $rAll['rentDate'],
            'viewed' => json_decode($rAll['viewed']),
            'favorites' => json_decode($rAll['favorites']),
            'locationId' => isset($rAll['locationId']) ? $rAll['locationId'] : null
        ];

        //Get an agent tied to a location
        $users = [];
        $emailFirstSelectedUser = null;
        $addressTo = '';
        if ($data['locationId']) {
            $getUsers = LocationUser::select(['user_id'])->where(['location_id' => $data['locationId']])->get()->toArray();
            if (count($getUsers) > 0)
                foreach ($getUsers as $u) {
                    array_push($users, $u['user_id']);
                }
            if (count($users) > 0) {
                // Get first user data
                $emailFirstSelectedUser = User::select(['email'])->where(['id' => $users[0]])->first();
                $emailFirstSelectedUser = $emailFirstSelectedUser->email;
            }
        }
        if (!$emailFirstSelectedUser) {
            $addressTo = Configuration::select(['key', 'value'])->where(['key' => 'company_email'])->first();
            $addressTo = $addressTo->value;
        } else {
            $addressTo = $emailFirstSelectedUser;
        }
        $data['viewed'] = json_encode($data['viewed']);
        $data['favorites'] = json_encode($data['favorites']);

        Mail::send('emails.contact-agent', $data, function ($m) use ($addressTo, $data) {
            $m->from($data['email'], $data['name']);
            $m->sender($data['email'], $data['name']);
            $m->to($addressTo, 'Receiver');
        });

        return 'Success';
    }

    public function newsLatter(Request $request)
    {
        $listId = env('MAILCHIMP_LIST_ID');

        if (isset($request->email)) {
            try {
                $this->mailchimp
                    ->lists
                    ->subscribe(
                        $listId,
                        ['email' => $request->email],
                        null,
                        null,
                        false
                    );
                return ($request->email);
            } catch (\Mailchimp_List_AlreadySubscribed $e) {
                return response()->json(['error' =>  $request->email . 'is already subscribed to the list'], 303);
            } catch (\Mailchimp_Error $e) {
                return $e;
            }
        }
        return response()->json(['error' => 'Error data'], 303);
    }
}
