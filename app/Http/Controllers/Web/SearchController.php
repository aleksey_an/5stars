<?php

namespace App\Http\Controllers\Web;

use App\Property;
use App\Location;
use App\LocationImage;
use App\Currency;
use App\Configuration;


class SearchController extends WebController
{
    private $perPage = 12;

    /**
     * Create search from $searchDta
     */

    public function search($searchData, $favoriteProperties)
    {
        $checkCache = new CheckCacheController;
        $locations = $checkCache->checkCache('locations');

        if (isset($searchData['location'])) {
            $location = $searchData['location'];
            $result['dataForSearchPanel'] = $this->dataForSearchPanel($locations);
            $result['properties'] = $this->createPropertyHref($this->searchWithLocation($searchData, $locations), $locations);
            $result['urlPath'] = $this->createBreadCrumbs($searchData['urlPath'], $locations);
            $result['locationUrl'] = $this->createUrlDiscoverRegion($location['id'], $locations);
            $result['locationId'] = $location['id'];
            $result['locationName'] = $searchData['location']['name'];
            $result['propertyType'] = isset($searchData['category_id']) ? $result['dataForSearchPanel']['categories']['categoriesWithId'][$searchData['category_id']]['name'] : null;
            $result['searchType'] = isset($searchData['to_rent']) ? 'to_rent' : 'to_sale';
            $result['favoriteProperties'] = $favoriteProperties;
            $result['propertiesIds'] = [];
            foreach ($result['properties'] as $p) {
                array_push($result['propertiesIds'], $p->id);
            }
            if (isset($favoriteProperties))
                $result['properties'] = $this->setFavoriteProperty($result['properties'], $favoriteProperties);

            /**
             * On Infinity Scroll
             */
            if (isset($searchData['infinityScroll'])) {
                return response()->json($this->infinityScrollResponse($result['properties']));
            }
            //Take Config Data
            $result['contacts'] = $checkCache->checkCache('contacts');

            //Create meta keywords
            $metaKeyWords = [];
            foreach ($result['urlPath'] as $key) {
                if (isset($key['name'])) {
                    array_push($metaKeyWords, $key['name']);
                }
            }

            $result['metaKeyWords'] = $metaKeyWords;

            return view('web.search.index', $result);
        } else {

            $search = ['status' => 'published'];

            isset($searchData['category_id']) ? $search['category_id'] = $searchData['category_id'] : '';
            isset($searchData['to_sale']) ? $search['to_sale'] = $searchData['to_sale'] : '';
            isset($searchData['to_rent']) ? $search['to_rent'] = $searchData['to_rent'] : '';

            if (isset($searchData['rooms'])) {
                $search['bedroom'] = $searchData['rooms'];
            }
            if (isset($searchData['bedroom'])) {
                $search['bedroom'] = $searchData['bedroom'];
            }
            if (isset($searchData['bathroom'])) {
                $search['bathroom'] = $searchData['bathroom'];
            }

            $priceField = '';
            $budget = [null, null];
            if (isset($searchData['budget'])) {
                $budget = explode('-', $searchData['budget']);
                if (isset($searchData['to_sale']) && $searchData['to_sale'] == 1) {
                    $priceField = 'sale_price';
                } else {
                    $priceField = 'rent_month';
                }
            }

            $properties = Property::with('translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale', 'imageForWebSearch')
                ->budget($priceField, $budget[0], $budget[1])
                ->where($search)
                ->orderBy('updated_at', 'desc')
                ->paginate($this->perPage);

            if (isset($searchData['budget']))
                $search['budget'] = $searchData['budget'];

            $properties->appends($search);
            $properties = $this->createPropertyHref($properties, $locations);

            if (isset($favoriteProperties))
                $properties = $this->setFavoriteProperty($properties, $favoriteProperties);

            /**
             * On Infinity Scroll
             */
            if (isset($searchData['infinityScroll'])) {
                return response()->json($this->infinityScrollResponse($properties));
            }

            $urlPath = $this->createBreadCrumbs($searchData['urlPath'], $locations);
            $searchType = (isset($searchData['to_rent']) ? 'to_rent' : 'to_sale');
            $propertiesIds = [];
            foreach ($properties as $p) {
                array_push($propertiesIds, $p->id);
            }
            $dataForSearchPanel = $this->dataForSearchPanel($locations);

            $propertyType = isset($searchData['category_id']) ? $dataForSearchPanel['categories']['categoriesWithId'][$searchData['category_id']]['name'] : null;

            //Get Locations
            $locations = $this->createLocationsForSearchPage($locations);

            //Take Config Data
            $contacts = $checkCache->checkCache('contacts');

            //Create meta keywords
            $metaKeyWords = [];
            foreach ($urlPath as $key) {
                if (isset($key['name'])) {
                    array_push($metaKeyWords, $key['name']);
                }
            }

            return view('web.search.index', compact('properties', 'urlPath', 'propertyType', 'searchType', 'propertiesIds', 'favoriteProperties', 'dataForSearchPanel', 'locations', 'contacts', 'metaKeyWords'));
        }
    }

    public function infinityScrollResponse($properties)
    {
        $result = [];
        foreach ($properties as $key => $property) {
            $result['property_' . $key] = view('web.components.property', compact('property'))->render();
        }
        $result ['data'] = $properties;
        return $result;
    }

    public function searchWithLocation($searchData, $locations = null)
    {
        $childLocations = $this->getChildLocations($searchData['location']['id'], $locations);
        array_push($childLocations, +$searchData['location']['id']);
        $search = ['status' => 'published'];
        foreach ($searchData as $key => $r) {
            if ($key == 'category_id' || $key == 'to_rent' || $key == 'to_sale') {
                if ($r)
                    $search[$key] = $r;
            }
        }

        if (isset($searchData['rooms'])) {
            $search['bedroom'] = $searchData['rooms'];
        }
        if (isset($searchData['bedroom'])) {
            $search['bedroom'] = $searchData['bedroom'];
        }
        if (isset($searchData['bathroom'])) {
            $search['bathroom'] = $searchData['bathroom'];
        }


        $priceField = '';
        $budget = [null, null];
        if (isset($searchData['budget'])) {
            if (isset($searchData['to_sale']) && $searchData['to_sale'] == 1) {
                $priceField = 'sale_price';
            } else {
                $priceField = 'rent_month';
            }
            $budget = explode('-', $searchData['budget']);
        }

        $properties = Property::with(['translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale', 'imageForWebSearch'])
            ->budget($priceField, $budget[0], $budget[1])
            ->where($search)
            ->whereIn('location_id', $childLocations)
            ->orderBy('updated_at', 'desc')
            ->paginate($this->perPage);
        $search['location_id'] = $searchData['location']['id'];
        if (isset($searchData['budget']))
            $search['budget'] = $searchData['budget'];
        $properties->appends($search);
        return $properties;
    }

    /**
     * Create location page with search
     */
    protected function locationPage($searchData, $favoriteProperties = null)
    {
        $checkCache = new CheckCacheController;
        $locations = $checkCache->checkCache('locations');
//        $locations = Location::with('translations')->select(['id', 'parent_id'])->get();
        $properties = $this->createPropertyHref($this->searchWithLocation($searchData, $locations), $locations);

        if (isset($favoriteProperties))
            $properties = $this->setFavoriteProperty($properties, $favoriteProperties);

        /**
         * On Infinity Scroll
         */
        if (isset($searchData['infinityScroll'])) {
            return response()->json($this->infinityScrollResponse($properties));
        }

        $location = Location::with('translationsWithLocale', 'imagesForWeb')->where(['id' => $searchData['location']['id']])->first();
        $isShowMap = $searchData['isShowMap'];
        if (!isset($location) || empty($location->translationsWithLocale) || empty($location->translationsWithLocale[0]->description)) {
            abort(404);
        }

        //Take Config Data
        $checkCache = new CheckCacheController;
        $contacts = $checkCache->checkCache('contacts');

        //Create meta keywords
        $metaKeyWords = [];
        if (isset($searchData['location']) && isset($searchData['location']['name'])){
            $metaKeyWords = [$searchData['location']['name']];
        }

        return view('web.location.index', compact('location', 'properties', 'isShowMap', 'favoriteProperties', 'contacts', 'metaKeyWords'));
    }

    /**
     * Create property page
     */
    protected function propertyPage($property, $searchData)
    {
        $favoriteProperties = null;
        if (isset($_COOKIE['favorites'])) {
            $favoriteProperties = json_decode($_COOKIE['favorites']);
        }

        //Take property translation
        $propertyTranslation = '';
        if (isset($property->translationsInLocale[0])) {
            $propertyTranslation = $property->translationsInLocale[0];
            $propertyTranslation->viewed = $propertyTranslation->viewed + 1;
            $propertyTranslation->save();
        } else if (isset($property->translationsDefaultLocale[0])) {
            $propertyTranslation = $property->translationsDefaultLocale[0];
            $propertyTranslation->viewed = $propertyTranslation->viewed + 1;
            $propertyTranslation->save();
        }
        //Take location name
        $locationName = '';
        if (isset($property->locationNameInLocale[0])) {
            $locationName = $property->locationNameInLocale[0]->name;
        } else if (isset($property->locationNameInDefaultLocale[0])) {
            $locationName = $property->locationNameInDefaultLocale[0]->name;
        }
        //Take facilities
        $facilities = [];
        foreach ($property->facility as $facility) {
            $name = '';
            if (isset($facility->translationsInLocale[0])) {
                $name = $facility->translationsInLocale[0]->name;
            } else if (isset($facility->translationsInDefaultLocale[0])) {
                $name = $facility->translationsInDefaultLocale[0]->name;
            }
            $facilities[$facility->property_facility_id] = ['name' => $name, 'image' => $facility->getImage->image];
        }
        //Take images
        $images = $property->imagesForWeb;
        $urlPath = $property->propertyHref;

        $isShowMap = $this->propertyMap($property->id, $property->lat, $property->lng, $locationName . ' ' . $property->address);

        //Take currency
        $currencies = Currency::select(['name', 'code', 'sign', 'rate', 'is_default'])->where(['is_enabled' => 1])->get();

        //Take Config Data
        $contactsData = Configuration::select('key', 'value')->get();
        $contacts = [];
        foreach ($contactsData as $c) {
            $contacts[$c->key] = $c->value;
        }

        // For search block
        $checkCache = new CheckCacheController;
        $locations = $checkCache->checkCache('locations');
        $searchType = (isset($searchData['to_rent']) ? 'to_rent' : 'to_sale');
        $dataForSearchPanel = $this->dataForSearchPanel($locations);

        //Create meta keywords
        $metaKeyWords = [];
        foreach ($urlPath as $key) {
            if (isset($key['name'])) {
                array_push($metaKeyWords, $key['name']);
            }
        }

        return view(
            'web.properties.property',
            compact('property', 'propertyTranslation', 'locationName', 'facilities', 'images', 'urlPath', 'isShowMap', 'currencies', 'contacts', 'searchType', 'dataForSearchPanel', 'favoriteProperties', 'metaKeyWords'));
    }


    /**
     * Crete path for discover Region
     */
    public function createUrlDiscoverRegion($locationId, $locations)
    {
        $location = Location::with('translationsWithLocale')->where(['id' => $locationId])->first();
        if (!isset($location) || empty($location->translationsWithLocale) || empty($location->translationsWithLocale[0]->description)) {
            return null;
        }

        $url = '';
        $parentLocationsUrl = $this->locationsTranslationsUrl($locations, $locationId);

        foreach ($parentLocationsUrl as $plu) {
            $url = $url . $plu . '/';
        }
        return $url;
    }

    /**
     * Create data for search panel
     */
    public function dataForSearchPanel($locationsData = null)
    {
        $response = ['categories' => ['categories' => null, 'categoriesWithId' => null], 'locations' => ['locations' => null, 'locationsWithId' => '']];
        $categoriesWithId = $this->createCategoriesArray();
        $categories = [];
        $locations = [];
        foreach ($categoriesWithId as $s) {
            $categories[$s['url']] = $s['name'];
        }
        $response['categories']['categoriesWithId'] = $categoriesWithId;

        $response['categories']['categories'] = $categories;
        //Create locations
        if (!isset($locationsData))
            $locationsData = Location::with('translations')->get();
        $prepareLocationsMultiArray = $this->prepareLocationsMultiArray($locationsData->toArray());

        $locationsForSearchBlock = $this->createLocationsForSearchBlock($prepareLocationsMultiArray);
        foreach ($locationsForSearchBlock as $pma) {
            $locations[$pma['url']] = $pma['name'];
        }
        $response['locations']['locationsWithId'] = $locationsForSearchBlock;
        $response['locations']['locations'] = $locations;
        return $response;
    }

    public function prepareLocationsMultiArray(array $elements, $parentId = 0, $chN = 0, $parentUrl = '')
    {
        $branch = array();
        $chN++;
        foreach ($elements as $element) {
            if ($element['is_enabled'] == 1 && $element['parent_id'] == $parentId) {

                $pUrl = '';
                foreach ($element['translations'] as $tr) {
                    if ($tr['locale'] == \App::getLocale()) {
                        $pUrl = ($parentUrl ? $parentUrl . '/' : '') . $tr['url'];
                        break;
                    } else {
                        $pUrl = ($parentUrl ? $parentUrl . '/' : '') . $tr['url'];
                    }
                }
                $children = $this->prepareLocationsMultiArray($elements, $element['id'], $chN, $pUrl);
                if ($children) {
                    foreach ($children as $key => $ch) {
                        $padding = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        $paddingLeft = '';
                        for ($i = 1; $i <= $chN; $i++) {
                            $paddingLeft = $paddingLeft . $padding;
                        }
                        foreach ($children[$key]['translations'] as $tr) {
                            if ($tr['locale'] == \App::getLocale()) {
                                $children[$key]['name'] = $paddingLeft . '-' . $tr['name'];
                                $children[$key]['url'] = $pUrl . '/' . $tr['url'];
                                break;
                            } else {
                                $children[$key]['name'] = $paddingLeft . '-' . $tr['name'];
                                $children[$key]['url'] = $pUrl . '/' . $tr['url'];
                            }
                        }
                    }
                    $element['chN'] = $chN;
                    $element['children'] = $children;
                }
                foreach ($element['translations'] as $tr) {
                    if ($tr['locale'] == \App::getLocale()) {
                        $element['name'] = $tr['name'];
                        $element['url'] = $tr['url'];
                        break;
                    } else {
                        $element['name'] = $tr['name'];
                        $element['url'] = $tr['url'];
                    }
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

    public function createLocationsForSearchBlock($locMultiArr)
    {
        $result = [];
        foreach ($locMultiArr as $arr) {
            if (isset($arr['children'])) {
                $result[$arr['id']] = ['name' => $arr['name'], 'url' => $arr['url'], 'id' => $arr['id']];
                $ch = $this->createLocationsForSearchBlock($arr['children']);
                foreach ($ch as $key => $a) {
                    $result[$key] = $a;
                }
            } else {
                $result[$arr['id']] = ['name' => $arr['name'], 'url' => $arr['url'], 'id' => $arr['id']];
            }
        }
        return $result;
    }

    public function createLocationsForSearchPage($locations)
    {
        $result = [];
        $checked = [];
        $locationsIds = [];
        foreach ($locations as $location) {
            if ($location->parent_id == 0 && $location->is_enabled == 1) {
                foreach ($location->translations as $lt) {
                    if ($lt->locale == \App::getLocale()) {
                        $result[$location->id] = ['url' => \Request::url() . '/' . $lt->url, 'name' => $lt->name, 'locale' => $lt->locale, 'id' => $location->id];
                        $checked[$location->id] = $location->id;
                        array_push($locationsIds, $location->id);
                    } else if ($lt->locale == config('app.fallback_locale') && !isset($checked[$location->id])) {
                        $result[$location->id] = ['url' => \Request::url() . '/' . $lt->url, 'name' => $lt->name, 'locale' => $lt->locale, 'id' => $location->id];
                        $checked[$location->id] = $location->id;
                        array_push($locationsIds, $location->id);
                    } else if (!isset($checked[$location->id])) {
                        $result[$location->id] = ['url' => \Request::url() . '/' . $lt->url, 'name' => $lt->name, 'locale' => $lt->locale, 'id' => $location->id];
                    }
                }
            }
        }

        $images = LocationImage::where(['is_enabled' => 1])->whereIn('location_id', $locationsIds)->get();
        foreach ($result as $key => $r) {
            foreach ($images as $img) {
                if ($key == $img->location_id) {
                    if (!isset($result[$key]['image']))
                        $result[$key]['image'] = [];
                    array_push($result[$key]['image'], $img->name);
                }
            }
        }
        return $result;
    }

    /**
     * Create Bread Crumbs
     */
    public function createBreadCrumbs($path, $locations)
    {
        $result = [];
        $result['header'] = '';
        $url = '/';

        foreach ($path as $key => $p) {
            $url = $url . $p . '/';
            $result[$p] = ['name' => $this->getLocationName($locations, $p)];
            $result[$p]['url'] = $url;
            $result['header'] = $result['header'] . ' ' . $result[$p]['name'];
        }
        return $result;
    }

    public function getLocationName($locations, $locationUrl)
    {
        foreach ($locations as $l) {
            foreach ($l->translations as $lTr) {
                if ($lTr->url == $locationUrl && $lTr->locale == \App::getLocale()) {
                    return $lTr->name;
                }
            }
        }
        $locationUrl = str_replace('-', ' ', $locationUrl);
        return ucwords($locationUrl);
    }
}
