<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Article;
use App\Faq;

class StaticPagesController extends WebController
{
    public function pages(Request $request)
    {
        $checkCache = new CheckCacheController;
        //Take Config Data
        $contacts = $checkCache->checkCache('contacts');

        $path = explode('/', $request->path())[1];
        return view('web.static-pages.' . App::getLocale() . '.' . $path, compact('contacts', 'articles'));
    }

    public function articles($id=null){
        //Take Config Data
        $checkCache = new CheckCacheController;
        $contacts = $checkCache->checkCache('contacts');

        if($id){
            $article = Article::with('translationInLocale')->where(['id' => $id])->first();
            if (!isset($article) || empty($article->translationInLocale) || empty($article->translationInLocale[0]->description)) {
                abort(404);
            }
            return view('web.blog.page', compact('contacts', 'article'));
        }

        $articles = Article::with('translationInLocale')
            ->where(['is_enabled' => 1])
            ->orderByDesc('id')
            ->get();
        return view('web.blog.index', compact('contacts', 'articles'));
    }

    public function faq(){
        //Take Config Data
        $checkCache = new CheckCacheController;
        $contacts = $checkCache->checkCache('contacts');

        $faqs = Faq::with('translationInLocale')->where(['is_enabled' => 1])->orderBy('sequence', "ASC")->get();
        return view('web.faq.index', compact('contacts', 'faqs'));
    }
}
