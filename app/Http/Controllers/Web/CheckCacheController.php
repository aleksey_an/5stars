<?php

namespace App\Http\Controllers\Web;

use Illuminate\Support\Facades\Cache;
use App\Location;
use App\Configuration;
use App\Localization;

class CheckCacheController
{

    public function checkCache($key)
    {
        $availableKeys = ['locations', 'contacts', 'localization-en', 'localization-fr'];
        $minutes = 15000;

        if (in_array($key, $availableKeys)) {
            if (Cache::has($key)) {
                return Cache::get($key);
            } else {
                if ($key == 'locations') {
                    $locations = Location::with('translations')->select(['id', 'parent_id', 'is_enabled'])->where(['is_enabled' => 1])->get();
                    Cache::add('locations', $locations, $minutes);
                    return $locations;
                } else if ($key == 'contacts') {
                    $contactsData = Configuration::select('key', 'value')->get();
                    $contacts = [];
                    foreach ($contactsData as $c) {
                        $contacts[$c->key] = $c->value;
                    }
                    Cache::add('contacts', $contacts, $minutes);
                    return $contacts;
                } else if($key == 'localization-en'){
                    $localizations = Localization::select(['key', 'value'])->where(['block' => 'common', 'locale' => 'en'])->get()->toArray();
                    Cache::add('localization-en', $localizations, $minutes);
                    return $localizations;
                } else if($key == 'localization-fr'){
                    $localizations = Localization::select(['key', 'value'])->where(['block' => 'common', 'locale' => 'fr'])->get()->toArray();
                    Cache::add('localization-fr', $localizations, $minutes);
                    return $localizations;
                }
            }
        } else {
           dd('Cache system nothing know about this key');
        }
    }
}
