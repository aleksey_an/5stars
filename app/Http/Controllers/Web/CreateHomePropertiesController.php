<?php

namespace App\Http\Controllers\Web;

use App\HomeProperty;
use App\Property;


class CreateHomePropertiesController extends WebController
{

    public function __construct()
    {
        $this->checkCache = new CheckCacheController;
        $this->locations = $this->checkCache->checkCache('locations');
    }

    public function getProperties()
    {
        $checkCache = new CheckCacheController;
        $selected = HomeProperty::select(['property_id'])->get();
        $selectedIds = [];
        foreach ($selected as $s) {
            array_push($selectedIds, $s->property_id);
        }

        $properties = Property::with(['translationsInLocale', 'categoryNameInLocale', 'imageForWebSearch', 'locationNameInLocale'])
            ->whereIn('id', $selectedIds)->get();
        $properties = $this->createPropertyHref($properties, $checkCache->checkCache('locations'));
        $properties = $properties->sortBy(function ($model) use ($selectedIds) {
            return array_search($model->getKey(), $selectedIds);
        });

        $sortedProperties = [];
        foreach ($properties as $key => $value) {
            $lId = $this->getParentLocation($value->location_id);
            if (isset($sortedProperties[$lId])) {
                array_push($sortedProperties[$lId], $value);
            } else {
                $sortedProperties[$lId] = [];
                array_push($sortedProperties[$lId], $value);
            }
        }

        $sp = [];
        $loc = $this->createLocationsHomePageProperties($this->nameLocationsMultiArray($this->locations->toArray()));
        foreach ($sortedProperties as $key => $value) {
            foreach ($loc as $lKey => $lValue) {
                if ($key == $lKey) {
                    $sp[$loc[$lKey]['name']] = $value;
                }
            }
        }
        return $sp;
    }

    private function getParentLocation($locationId)
    {
        $locations = $this->locations->toArray();
        $result = '';
        foreach ($locations as $key => $value) {
            if ($locations[$key]['id'] == $locationId) {
                if ($locations[$key]['parent_id'] != 0) {
                    $result = $this->getParentLocation($locations[$key]['parent_id']);
                } else {
                    $result = $locations[$key]['id'];
                }
                break;
            }
        }
        return $result;
    }

    private function createLocationsHomePageProperties($locMultiArr)
    {
        $result = [];
        foreach ($locMultiArr as $arr) {
            if (isset($arr['children'])) {
                $result[$arr['id']] = ['name' => $arr['name'], 'url' => $arr['url'], 'id' => $arr['id']];
                $ch = $this->createLocationsHomePageProperties($arr['children']);
                foreach ($ch as $key => $a) {
                    $result[$key] = $a;
                }
            } else {
                $result[$arr['id']] = ['name' => $arr['name'], 'url' => $arr['url'], 'id' => $arr['id']];
            }
        }
        return $result;
    }

    private function nameLocationsMultiArray(array $elements, $parentId = 0, $chN = 0, $parentUrl = '')
    {
        $branch = array();
        $chN++;
        foreach ($elements as $element) {
            if ($element['is_enabled'] == 1 && $element['parent_id'] == $parentId) {

                $pUrl = '';
                foreach ($element['translations'] as $tr) {
                    if ($tr['locale'] == \App::getLocale()) {
                        $pUrl = ($parentUrl ? $parentUrl . '/' : '') . $tr['url'];
                        break;
                    } else {
                        $pUrl = ($parentUrl ? $parentUrl . '/' : '') . $tr['url'];
                    }
                }
                $children = $this->nameLocationsMultiArray($elements, $element['id'], $chN, $pUrl);
                if ($children) {
                    foreach ($children as $key => $ch) {
                        $padding = '';
                        $paddingLeft = '';
                        for ($i = 1; $i <= $chN; $i++) {
                            $paddingLeft = $paddingLeft . $padding;
                        }
                        foreach ($children[$key]['translations'] as $tr) {
                            if ($tr['locale'] == \App::getLocale()) {
                                $children[$key]['name'] = $paddingLeft . $tr['name'];
                                $children[$key]['url'] = $pUrl . '/' . $tr['url'];
                                break;
                            } else {
                                $children[$key]['name'] = $paddingLeft . $tr['name'];
                                $children[$key]['url'] = $pUrl . '/' . $tr['url'];
                            }
                        }
                    }
                    $element['chN'] = $chN;
                    $element['children'] = $children;
                }
                foreach ($element['translations'] as $tr) {
                    if ($tr['locale'] == \App::getLocale()) {
                        $element['name'] = $tr['name'];
                        $element['url'] = $tr['url'];
                        break;
                    } else {
                        $element['name'] = $tr['name'];
                        $element['url'] = $tr['url'];
                    }
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }
}