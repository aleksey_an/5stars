<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Property;

class AjaxController extends WebController
{
    public function takeFavorites(Request $request)
    {
        $rAll = $request->all();
        if (isset($rAll['ids'])) {
            $ids = json_decode(stripslashes($rAll['ids']));
            foreach ($ids as $id) {
                if (!is_int(+$id)) {
                    return response()->json(['error' => 'Error data'], 303);
                }
            }
            if (count($ids) == 0) {
                return response()->json([], 200);
            }

            $properties = Property::with('translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale', 'imageForWebSearch')->whereIn('id', $ids)->get();
            $properties = $properties->sortBy(function ($model) use ($ids) {
                return array_search($model->getKey(), $ids);
            });
            $properties = $this->createPropertyHref($properties);
            $properties = $this->setFavoriteProperty($properties, $ids);

            $propertiesIds = [];
            $notFound = [];
            foreach ($properties as $p) {
                array_push($propertiesIds, $p->id);
            }
            foreach ($ids as $id) {
                if (!in_array($id, $propertiesIds)) {
                    array_push($notFound, $id);
                }
            }

            if (count($ids) > 0) {
                $favoritesLink = $request->root() . '/favorites?';
                foreach ($ids as $key => $id) {
                    $favoritesLink = $favoritesLink . 'p' . $key . '=' . $id . (isset($ids[$key + 1]) ? '&' : '');
                }
            }

            return view('web.components.take-favorites', compact('properties', 'notFound', 'favoritesLink'));
        }
    }

    public function takeViewed(Request $request)
    {
        $rAll = $request->all();
        if (isset($rAll['ids'])) {
            $ids = json_decode(stripslashes($rAll['ids']));
            $favorites = json_decode(stripslashes($rAll['favorites']));
            foreach ($ids as $id) {
                if (!is_int(+$id)) {
                    return response()->json(['error' => 'Error data'], 303);
                }
            }
            if (count($ids) == 0) {
                return response()->json([], 200);
            }

            $properties = Property::with('translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale', 'imageForWebSearch')->whereIn('id', $ids)->get();
            $properties = $properties->sortBy(function ($model) use ($ids) {
                return array_search($model->getKey(), $ids);
            });
            $properties = $this->createPropertyHref($properties);
            $properties = $this->setFavoriteProperty($properties, $favorites);

            $propertiesIds = [];
            $notFound = [];
            foreach ($properties as $p) {
                array_push($propertiesIds, $p->id);
            }
            foreach ($ids as $id) {
                if (!in_array($id, $propertiesIds)) {
                    array_push($notFound, $id);
                }
            }

            return view('web.components.take-viewed', compact('properties', 'notFound'));
        }
    }

   /* public function properties(Request $request)
    {
        $rAll = $request->all();
        if (isset($rAll['ids'])) {
            $ids = json_decode(stripslashes($rAll['ids']));
            $favorites = json_decode(stripslashes($rAll['favorites']));
            foreach ($ids as $id) {
                if (!is_int(+$id)) {
                    return response()->json(['error' => 'Error data'], 303);
                }
            }
            if (count($ids) == 0) {
                return response()->json([], 200);
            }

            $properties = Property::with('translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale', 'imageForWebSearch')->whereIn('id', $ids)->get();
            $properties = $properties->sortBy(function ($model) use ($ids) {
                return array_search($model->getKey(), $ids);
            });
            $properties = $this->createPropertyHref($properties);
            $properties = $this->setFavoriteProperty($properties, $favorites);

            $propertiesIds = [];
            $notFound = [];
            foreach ($properties as $p) {
                array_push($propertiesIds, $p->id);
            }
            foreach ($ids as $id) {
                if (!in_array($id, $propertiesIds)) {
                    array_push($notFound, $id);
                }
            }

            $forFavoritePage = false;
            if (isset($rAll['forFavoritePage']) && $rAll['forFavoritePage'] == true) {
                $forFavoritePage = true;
            }

            $favoritesLink = null;
            if (count($ids) > 0 && $forFavoritePage) {
                $favoritesLink = $request->root() . '/favorites?';
                foreach ($ids as $key => $id) {
                    $favoritesLink = $favoritesLink . 'p' . $key . '=' . $id . (isset($ids[$key + 1]) ? '&' : '');
                }
            }
            return view('web.components.get-ajax-properties', compact('properties', 'notFound', 'forFavoritePage', 'favoritesLink'));
        }
    }*/

    public function checkRecaptcha(Request $request)
    {
        $rAll = $request->all();
        if (isset($rAll['g-recaptcha-response'])) {
            $response = $rAll['g-recaptcha-response'];
            $secret = env('RECAPTCHA_SECRET_KEY');
            $url = env('RECAPTCHA_URL');
            $remoteip = $_SERVER['REMOTE_ADDR'];

            $data = array(
                'secret' => $secret,
                'response' => $response,
                'remoteip' => $remoteip
            );
            $options = array(
                'http' => array (
                    'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                                "User-Agent:MyAgent/1.0\r\n",
                    'method' => 'POST',
                    'content' => http_build_query($data)
                )
            );
            $context  = stream_context_create($options);
            $verify = file_get_contents($url, false, $context);
            $captcha_success = json_decode($verify);

            return response()->json($captcha_success, 200);
        }else{
            return response()->json(['error' => 'Error data'], 200);
        }
    }
}
