<?php

namespace App\Http\Controllers\Web;

use App\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;
use App\HomeImage;
use Illuminate\Support\Facades\DB;
use Session;
use Mapper;

class WebController extends Controller
{
    public function index()
    {
        $checkCache = new CheckCacheController;
        $homeProp = new CreateHomePropertiesController;
        /**
         *Check is favorite properties in cooke
         */
        $favoriteProperties = null;
        if (isset($_COOKIE['favorites'])) {
            $favoriteProperties = json_decode($_COOKIE['favorites']);
        }

        //Take Config Data
        $contacts = $checkCache->checkCache('contacts');

        //Get Categories
        $categories = [];
        $categoriesWithId = $this->createCategoriesArray();
        foreach ($categoriesWithId as $s) {
            $categories[$s['url']] = $s['name'];
        }
        //Get Locations
        $locations = [];
        $locationsWithId = $this->createLocationsArrByParentId(0);
        foreach ($locationsWithId as $ld) {
            $locations[$ld['url']] = $ld['name'];
        }
        //Check background image
        $backgroundImage = $this->createBackgroundImage();

        //Featured properties
        $sp = $homeProp->getProperties();
        if (isset($favoriteProperties))
            foreach ($sp as $key => $p) {
                $sp[$key] = $this->setFavoriteProperty($p, $favoriteProperties);
            }

        return view('web.home.index', compact('categories', 'locations', 'categoriesWithId', 'locationsWithId', 'contacts', 'backgroundImage', 'sp'));
    }

    public function createBackgroundImage()
    {
        $backgroundImg = null;
        $cookieBackgroundImg = null;
        $numberImg = null;
        $image = null;

        if (isset($_COOKIE['background_image'])) {
            $cookieBackgroundImg = +$_COOKIE['background_image'];
        }

        if (is_dir(public_path() . '/images/home')) {
            if (isset($cookieBackgroundImg)) {
                setcookie("background_image", $cookieBackgroundImg + 1, time() + 1000000, "/");
                $image = HomeImage::select(['name'])->where(['sequence' => $cookieBackgroundImg + 1])->first();

                if (!isset($image->name)) {
                    setcookie("background_image", 1, time() + 1000000, "/");
                    $image = HomeImage::select(['name'])->where(['sequence' => 1])->first();
                }
            } else {
                setcookie("background_image", 1, time() + 1000000, "/");
                $image = HomeImage::select(['name'])->where(['sequence' => 1])->first();
            }

            if (isset($image->name) && file_exists(public_path('images/home/display/' . $image->name))) {
                $backgroundImage = 'style="background-image: url(\'/images/home/display/' . $image->name . '\')"';
            } else {
                setcookie("background_image", 1, time() + 1000000, "/");
                $backgroundImage = null;
            }
            return $backgroundImage;
        } else {
            return null;
        }
    }

    public function notFoundProperty()
    {
        return view('errors.property-deleted');
    }

    public function favorites(Request $request)
    {
        $favoritesArr = [];
        if (is_array($request->query()) && count($request->query()) > 0) {
            foreach ($request->query() as $pId) {
                array_push($favoritesArr, $pId);
            }
            $properties = Property::with('translationsInLocale', 'categoryNameInLocale', 'locationNameInLocale', 'images')->whereIn('id', $favoritesArr)->get();
            $properties = $this->createPropertyHref($properties);
            foreach ($properties as $key => $p) {
                $properties[$key]->isFavorite = true;
            }
            return view('web.favorites.index', compact('properties'));
        } else {
            return view('errors.404');
        }
    }

    public function createLocationsArrByParentId($parentId)
    {
        $result = [];
        $checked = [];

        /* $locations = DB::table('locations AS l')->where(['l.parent_id' => $parentId, 'l.is_enabled' => 1])
             ->join('location_translations AS lt', 'l.id', '=', 'lt.location_id')
             ->select(['l.id', 'lt.url', 'lt.name', 'lt.locale'])
             ->orderBy('l.id')
             ->get();*/

        $checkCache = new CheckCacheController;
        $locations = $checkCache->checkCache('locations');


        foreach ($locations as $location) {
            if ($location->parent_id == $parentId) {
                foreach ($location->translations as $ltr) {
                    if ($ltr->locale == \App::getLocale()) {
                        $result[$ltr->location_id] = ['url' => $ltr->url, 'name' => $ltr->name, 'locale' => $ltr->locale, 'id' => $ltr->location_id];
                        $checked[$ltr->location_id] = $ltr->location_id;
                    } else if ($ltr->locale == config('app.fallback_locale') && !isset($checked[$location->location_id])) {
                        $result[$ltr->location_id] = ['url' => $ltr->url, 'name' => $ltr->name, 'locale' => $ltr->locale, 'id' => $ltr->location_id];
                        $checked[$ltr->location_id] = $ltr->location_id;
                    } else if (!isset($checked[$ltr->location_id])) {
                        $result[$ltr->location_id] = ['url' => $ltr->url, 'name' => $ltr->name, 'locale' => $ltr->locale, 'id' => $ltr->location_id];
                    }
                }
            }


        }
        return $result;
    }

    /**
     * Create array categories with translations and url
     **/
    public function createCategoriesArray()
    {
        $result = [];
        $categories = DB::table('property_categories AS pc')->where(['is_enabled' => 1])
            ->join('property_category_translations AS ct', 'pc.id', '=', 'ct.property_category_id')
            ->select(['pc.id', 'ct.property_category_id', 'ct.locale', 'ct.url', 'ct.name'])
            ->orderBy('sequence', "ASC")
            ->get();
        $checked = [];
        foreach ($categories as $category) {
            if ($category->locale == \App::getLocale()) {
                $result[$category->id] = ['url' => $category->url, 'name' => $category->name, 'locale' => $category->locale, 'id' => $category->id];
                $checked[$category->id] = $category->id;
            } else if ($category->locale == config('app.fallback_locale') && !isset($checked[$category->id])) {
                $result[$category->id] = ['url' => $category->url, 'name' => $category->name, 'locale' => $category->locale, 'id' => $category->id];
                $checked[$category->id] = $category->id;
            } else if (!isset($checked[$category->id])) {
                $result[$category->id] = ['url' => $category->url, 'name' => $category->name, 'locale' => $category->locale, 'id' => $category->id];
            }
        }
        return $result;
    }

    /**
     * Create array with parent locations
     */
    public function locationsTranslationsUrl($locations, $locationId)
    {
        $result = [];
        $parentLocations = $this->createArr($locations, $locationId);

        foreach ($locations as $location) {
            foreach ($parentLocations as $pl) {
                if (isset($location->translations[0]) && $location->translations[0]->location_id == $pl) {
                    foreach ($location->translations as $lt) {
                        if ($lt->locale == \App::getLocale()) {
                            array_push($result, $lt->url);
                            break;
                        }
                    }
                }
            }
        }
        /*$checked = [];
        $parentLocations = array_reverse($parentLocations);
        foreach($parentLocations as $pl){
            foreach ($locations as $location) {
                if($location->id == $pl){
                    foreach ($location->translations as $lt) {
                        if ($lt->locale == \App::getLocale() && !isset($checked[$lt->location_id])) {
                            array_push($result, $lt->url);
                            $checked[$lt->location_id] = $lt->location_id;
                            break;
                        } else if(!isset($checked[$lt->location_id]) && !in_array($lt->url, $result)){
                            array_push($result, $lt->url);
                        }
                    }
                }
            }
        }*/
//        dd($parentLocations, $result);
        return $result;
    }

    public function createArr($locations, $locationId)
    {
        $result = [];
        foreach ($locations as $l) {
            if ($l->id == $locationId) {
                array_push($result, $locationId);
                if ($l->parent_id !== 0) {
                    $result = array_merge($result, $this->createArr($locations, $l->parent_id));
                }
            }
        }
        return $result;
    }

    /**
     * Create href for property
     */
    public function createPropertyHref($properties, $locations = null)
    {
        if (!isset($locations))
            $locations = Location::with('translations')->get();
        foreach ($properties as $key => $p) {
            $propUrl = '';
            if ($p->to_sale) {
                $propUrl = trans('url.to_sale');
            } else {
                $propUrl = trans('url.to_rent');
            }

            if (isset($p->categoryNameInLocale[0])) {
                /*location url part*/
                $propUrl = '/' . $propUrl . '/' . $p->categoryNameInLocale[0]['url'];
                $locationsTranslationsUrl = $this->locationsTranslationsUrl($locations, $p->location_id);
                // dd($locationsTranslationsUrl);
                foreach ($locationsTranslationsUrl as $url) {
                    $propUrl = $propUrl . '/' . $url;
                }
                $properties[$key]->propertyHref = $propUrl . '/' . $p->code;
            } else if (isset($p->categoryNameInDefaultLocale[0]) && isset($p->locationNameInDefaultLocale[0])) {
                /*location url part*/
                $propUrl = '/' . $propUrl . '/' . $p->categoryNameInDefaultLocale[0]['url'];
                $locationsTranslationsUrl = $this->locationsTranslationsUrl($locations, $p->location_id);
                //  dd($locationsTranslationsUrl);
                foreach ($locationsTranslationsUrl as $url) {
                    $propUrl = $propUrl . '/' . $url;
                }
                $properties[$key]->propertyHref = $propUrl . '/' . $p->code;
            } else {
//                dd('UNSET');
                unset($properties[$key]);
            }
        }
        return $properties;
    }

    function cleanString($string)
    {
        $string = str_replace(' ', '-', $string);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    public function locationMap($id, $lat = null, $lng = null, $searchName = null)
    {
        $mapConfig = ['zoom' => 10, 'center' => true, 'marker' => true, 'type' => 'ROADMAP', 'zoomControl' => false, 'streetViewControl' => false, 'mapTypeControl' => false, 'fullscreenControl' => false,];

        if (isset($lat) && isset($lng) && $lat != 0.0 && $lng != 0.0) {
            Mapper::map($lat, $lng, $mapConfig);
            return true;
        } else {
            try {
                $gMap = Mapper::location(str_replace('/', ', ', $searchName));
                $lat = $gMap->getLatitude();
                $lng = $gMap->getLongitude();
                Location::where(['id' => $id])->update(['lat' => $lat, 'lng' => $lng]);

                Mapper::map($lat, $lng, $mapConfig);
                return true;
            } catch (\Exception $exception) {
                return false;
            }
        }
    }

    public function propertyMap($id, $lat = null, $lng = null, $searchName = null)
    {
        $mapConfig = ['zoom' => 16, 'center' => true, 'marker' => false, 'type' => 'ROADMAP', 'zoomControl' => false, 'streetViewControl' => false, 'mapTypeControl' => false, 'fullscreenControl' => false,];
        $mapCircleConfig = ['strokeColor' => '#000000', 'strokeOpacity' => 0.3, 'strokeWeight' => 1, 'fillColor' => 'yellow', 'radius' => 500];

        if (isset($lat) && isset($lng) && $lat != 0.0 && $lng != 0.0) {
            Mapper::map($lat, $lng, $mapConfig)
                ->circle([['latitude' => $lat, 'longitude' => $lng]], $mapCircleConfig);
            return true;
        } else {
            try {
                $gMap = Mapper::location(json_decode(str_replace('/', ', ', $searchName)));
                $lat = $gMap->getLatitude();
                $lng = $gMap->getLongitude();
                Property::where(['id' => $id])->update(['lat' => $lat, 'lng' => $lng]);
                Mapper::map($lat, $lng, $mapConfig)
                    ->circle([['latitude' => $lat, 'longitude' => $lng]], $mapCircleConfig);
                return true;
            } catch (\Exception $exception) {
                return false;
            }
        }
    }

    public function setFavoriteProperty($properties, $favoriteProperties)
    {
        foreach ($properties as $key => $p) {
            foreach ($favoriteProperties as $fp) {
                if ($fp == $p->id) {
                    $properties[$key]->isFavorite = true;
                }
            }
        }
        return $properties;
    }

    public function success(Request $request)
    {
        $checkCache = new CheckCacheController;
        $contacts = $checkCache->checkCache('contacts');

        $response = $request->get('success') == 'ok' ? 1 : 0;

        return view('web.success.index', compact('contacts', 'articles', 'response'));
    }
}
