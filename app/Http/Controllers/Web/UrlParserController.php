<?php

namespace App\Http\Controllers\Web;

use App\Property;
use Illuminate\Http\Request;
use App\PropertyCategoryTranslations;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Lang;
use Mapper;


class UrlParserController extends SearchController
{
    public function parser(Request $request, $saleRentLocation)
    {
        /**
         * Data for search
         */
        $searchData = [];
        /**
         *Check is favorite properties in cooke
         */
        $favoriteProperties = null;
        if (isset($_COOKIE['favorites'])) {
            $favoriteProperties = json_decode($_COOKIE['favorites']);
        }
        /**
         * Take and filter url path
         */
        $urlArray = parse_url(url()->full());
        $urlPath = $urlArray['path'];
        $pathUrlArr = explode('/', $urlPath);
        array_splice($pathUrlArr, 0, 2);
        $pathArr = [];
        foreach ($pathUrlArr as $pua) {
            array_push($pathArr, urldecode($pua));
        }

        /**
         * Redirect 301
         */
        $checkEndOfPath = explode('.', end($pathArr));
        if (count($checkEndOfPath) > 1 && $checkEndOfPath[1] == 'html') {
            array_splice($pathArr, -1, 1, $checkEndOfPath[0]);
            $redirectPath = implode('/',$pathArr);
            return Redirect::to('/' . $redirectPath, 301);
//            dd($pathArr, $checkEndOfPath, $redirectPath);
        }

        $searchData['urlPath'] = $pathArr;

        /**
         * Check is search cookies
         */
        if (isset($_COOKIE['search'])) {
            $searchCookies = json_decode($_COOKIE['search']);
            if (isset($searchCookies->category->id))
                $searchData['category_id'] = $searchCookies->category->id;
            if (isset($searchCookies->location->id)) {
                $searchData['location'] = [
                    'id' => $searchCookies->location->id,
                    'url' => $searchCookies->location->url,
                    'name' => $searchCookies->location->name,
                    'locale' => \App::getLocale()];
            }
        }

        /**
         * Get sale or rent search type $saleRentLocation
         */
        $urlTranslations = Lang::get('url');
        foreach ($urlTranslations as $key => $ut) {
            if ($key == 'to_sale' || $key == 'to_rent') {
                if ($ut == $saleRentLocation) {
                    $searchData[$key] = 1;
                }
            }
        }

        if (!isset($searchData['category_id']) || !isset($searchData['location'])) {
            /**
             * Is property
             */

            $property = Property::with('locationNameInLocale', 'translationsInLocale', 'facility.translationsInLocale', 'facility.getImage')->where(['code' => end($pathArr)])->first();
            if (isset($property)) {

                $property->propertyHref = $this->createBreadCrumbsForProperty($pathArr, end($pathArr));
                return $this->propertyPage($property, $searchData);


            } else if(isset(end($pathArr)[4]) && is_numeric(end($pathArr)[4])){
                array_pop($pathArr);
                $redirectPath = implode('/',$pathArr);
                return Redirect::to('/' . $redirectPath);
            }
        }

        /**
         * Is search with sale or rent
         */

        if (isset($searchData['to_sale']) || isset($searchData['to_rent'])) {
            //Check $pathArr[1]
            if (isset($pathArr[1]) && !isset($searchData['category_id'])) {
                /**
                 * Is property category
                 */
                $categoryData = PropertyCategoryTranslations::where(['url' => $pathArr[1]])->get();
                if (isset($categoryData[0])) {
                    $searchData['category_id'] = $categoryData[0]->property_category_id;
                } else if (count($pathArr) <= 2 && !isset($searchData['location'])) {
                    $searchData['location'] = $this->getLocationByUrl($pathArr[1]);
                }
            }
            //Check $pathArr[2]
            if (count($pathArr) > 2 && !isset($searchData['location'])) {
                $searchData['location'] = $this->getLocationByUrl(end($pathArr));
            }

            /**
             * Parser search query
             */
            if (isset($urlArray['query'])) {
                $query = $urlArray['query'];
                $queryArr = explode('&', $query);

                foreach ($queryArr as $key => $q) {
                    $q = explode('=', $q);
                    if (count($q) == 2) {
                        if ($q[0] == 'budget') {
                            $searchData['budget'] = $q[1];
                        }
                        if ($q[0] == 'page') {
                            $searchData['infinityScroll'] = $q[1];
                        }
                        if ($q[0] == 'rooms'){
                            $searchData['rooms'] = $q[1];
                        }
                        if($q[0] == 'bedroom'){
                            $searchData['bedroom'] = $q[1];
                        }
                        if($q[0] == 'bathroom'){
                            $searchData['bathroom'] = $q[1];
                        }
                        if ($q[0] == 'code') {
                            $q[1] = urldecode($q[1]);
                            $property = Property::with('locationNameInLocale', 'translationsInLocale', 'facility.translationsInLocale')->where(['code' => str_replace(' ', '', $q[1])])->get();
                            if (isset($property[0])) {
                                $property = $this->createPropertyHref($property);
                                return redirect($property[0]->propertyHref);
                            }
                        }
                    }
                }
            }

            return $this->search($searchData, $favoriteProperties);
        } else {
            /**
             * Is location
             */
            $location = $this->getLocationByUrl(end($pathArr));
            if ($location) {
                setcookie("search", "", time() - 3600, "/");
                $searchData = [];
                if (isset($urlArray['query'])) {
                    $query = $urlArray['query'];
                    $queryArr = explode('&', $query);
                    foreach ($queryArr as $key => $q) {
                        $q = explode('=', $q);
                        if (count($q) == 2) {
                            if ($q[0] == 'page') {
                                $searchData['infinityScroll'] = $q[1];
                            }
                        }
                    }
                }
                $searchData['location'] = $location;
                $searchData['isShowMap'] = $this->locationMap($location['id'], $location['lat'], $location['lng'], $saleRentLocation);
                return $this->locationPage($searchData, $favoriteProperties);
            } else {
                return view('errors.404');
            }
        }
    }

    /**
     * Take location by url
     */
    public function getLocationByUrl($locationUrl)
    {
        /* var_dump($locationUrl);*/
        $location = DB::table('location_translations AS lt')
            ->join('locations AS l', 'lt.location_id', '=', 'l.id')
            ->select(['l.id', 'l.lat', 'l.lng', 'lt.url', 'lt.locale', 'lt.location_id', 'lt.name'])
            ->where(['lt.url' => $locationUrl/*, 'lt.locale' => \App::getLocale()*/])
            ->get();
        $location = collect($location)->map(function ($x) {
            return (array)$x;
        })->toArray();
        if (isset($location[0])) {
            return $location[0];
        } else {
            return null;
        }
    }

    /**
     * Create Bread Crumbs for property details page
     */
    public function createBreadCrumbsForProperty($path, $propertyCode = null)
    {
        $result = [];
        $url = '/';

        foreach ($path as $key => $p) {
            $name = str_replace('-', ' ', $p);
            $url = $url . $p . '/';
            if ($p != $propertyCode) {
                $result[$p] = ['name' => ucwords($name)];
                $result[$p]['url'] = $url;
            }
        }
        return $result;
    }
}
