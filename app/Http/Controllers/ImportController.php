<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelsImportData\ImportProperty;
use App\ModelsImportData\ImportLocation;
use App\ModelsImportData\ImportLocationDetails;
use App\ModelsImportData\ImportPropertyDetails;
use App\ModelsImportData\ImportPropertyCategory;
use App\ModelsImportData\ImportPropertyCategoryDetail;
use App\ModelsImportData\ImportPropertyFacility;
use App\ModelsImportData\ImportPropertyFacilityDetails;
use App\ModelsImportData\ImportPropertyFacilityData;
use App\ModelsImportData\ImportPropertyTransport;
use App\ModelsImportData\ImportPropertyTransportDetail;
use App\ModelsImportData\ImportPropertyTransportData;
use App\ModelsImportData\ImportPropertyImage;
use App\ModelsImportData\ImportArticles;
use App\ModelsImportData\ImportArticlesDetails;
use App\Property;
use App\Location;
use App\LocationTranslation;
use App\PropertyTranslation;
use App\PropertyCategory;
use App\PropertyCategoryTranslations;
use App\PropertyFacility;
use App\PropertyFacilityTranslations;
use App\PropertyFacilityData;
use App\PropertyTransport;
use App\PropertyTransportTranslations;
use App\PropertyTransportData;
use App\PropertyImage;
use App\Article;
use App\ArticleTranslation;


use Illuminate\Support\Facades\DB;

class ImportController extends Controller
{
    public function importAll($block)
    {
        if ($block == 'locations') {
            $this->location();
            $this->locationTranslation();
            dd('Finished');
        } else if ($block == 'properties') {
            ini_set('max_execution_time', 7000);
            $this->property();
            $this->propertyTranslations();
            dd('Finished');
        } else if ($block == 'property-images') {
            ini_set('max_execution_time', 7000);
            $this->propertyImages();
            dd('Finished');
        } else if ($block == 'categories') {
            $this->propertyCategory();
            $this->propertyCategoryTranslations();
            dd('Finished');
        } else if ($block == 'facilities') {
            ini_set('max_execution_time', 7000);
            $this->propertyFacility();
            $this->propertyFacilityTranslations();
            $this->propertyFacilityData();
            dd('Finished');
        } else if ($block == 'transport') {
            $this->propertyTransport();
            $this->propertyTransportTranslations();
            $this->propertyTransportData();
            dd('Finished');
        } else if($block == 'articles'){
            $this->articles();
        } else if($block == 'articles-trans'){
            ini_set('max_execution_time', 7000);
            $this->articleTranslations();
        } else if ($block == 'all') {
            ini_set('max_execution_time', 50000);
            $this->location();
            $this->locationTranslation();

            $this->property();
            $this->propertyTranslations();
//          $this->propertyImages();

            $this->propertyCategory();
            $this->propertyCategoryTranslations();

            $this->propertyFacility();
            $this->propertyFacilityTranslations();
            $this->propertyFacilityData();

            $this->propertyTransport();
            $this->propertyTransportTranslations();
            $this->propertyTransportData();
            $this->articles();
            $this->articleTranslations();
            dd('Finished');
        } else {
            dd('Wrong parameter');
        }
    }

    /*Locations*/
    public function location()
    {
        $locations = ImportLocation::all()->toArray();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Location::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        foreach ($locations as $key => $l) {
            $location = new Location;
            $location->id = $l['id'];
            $location->parent_id = $l['parent'];
            $location->code = $l['code'] ? $l['code'] : '';
            $location->sequence = $l['sequence'];
            $location->is_enabled = $l['status'] == 'E' ? 1 : 0;

            $location->save();
            unset($location);
        }
        echo 'Done Import Location <br>';
        return false;
    }

    public function locationTranslation()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        LocationTranslation::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $locationsDetails = ImportLocationDetails::all()->toArray();
        foreach ($locationsDetails as $key => $l) {

            $locationTranslations = new LocationTranslation;
            $locationTranslations->id = $l['id'];
            $locationTranslations->location_id = $l['property_location_id'];
            $locationTranslations->name = $l['subject'];
            $locationTranslations->description = strip_tags($l['detail']);

            $locale = '';
            if ($l['language_id'] === 1) {
                $locale = 'en';
            } else if ($l['language_id'] === 2) {
                $locale = 'fr';
            } else if ($l['language_id'] === 9) {
                $locale = 'jp';
            }
            $locationTranslations->locale = $locale;
            /*Create url*/
            $locationTranslations->url = strtolower($this->cleanString($l['subject']));

            $locationTranslations->save();
            unset($locationTranslations);
        }
        unset($locationsDetails);
        echo 'Done Import Location Translation <br>';
        return false;
    }

    /*Properties*/
    public function property()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('properties')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        ImportProperty::chunk(100, function ($importProperties) {

            foreach ($importProperties as $key => $pr) {

                $property = new Property;

                $property->id = $pr['id'];
                $property->code = $pr['code'];
                $property->category_id = $pr['property_category_id'];
                $property->location_id = $pr['property_location_id'];
                $property->user_id = $pr['user_id'];
                $property->contact_name = $pr['contact_name'];
                $property->contact_email = $pr['contact_email'];
                $property->contact_phone = $pr['contact_phone'];
                $property->contact_mobile = $pr['contact_mobile'];
                $property->built_year = $pr['built_year'];
                $property->building_name = $pr['building_name'];
                $property->address = $pr['address'];
                $property->unit_number = $pr['unit_number'];
                $property->indoor_area = $pr['indoor_area'];
                $property->outdoor_area = $pr['outdoor_area'];
                $property->bedroom = $pr['bedroom'];
                $property->bathroom = $pr['bathroom'];
                $property->furnished = $pr['furnished'] ? 1 : 0;
                $property->floor_number = $pr['floor_number'] ? $pr['floor_number'] : 0;
                $property->key_info = $pr['key_info'] ? $pr['key_info'] : '';
                $property->to_rent = $pr['to_rent'] == 'Y' ? 1 : 0;
                $property->is_rent = $pr['user_id'] ? 1 : 0;
                $property->rent_month = is_numeric($pr['rent_month']) ? $pr['rent_month'] : 0;
                $property->rent_week = $pr['rent_week'] ? $pr['rent_week'] : 0;
                $property->rent_day = $pr['rent_day'] ? $pr['rent_day'] : 0;
                $property->to_sale = $pr['to_sale'] == 'Y' ? 1 : 0;
                $property->is_sold = $pr['is_sold'] ? 1 : 0;
                $property->sale_price = is_numeric($pr['sale_price']) ? +$pr['sale_price'] : 0;
                $property->common_fee = $pr['common_fee'];
                $property->availability = $pr['availability'];
                $property->youtube_url = $pr['video'];
                $property->publish_date = $pr['publish_date'];

                $property->created_by = $pr['create_by'];
                $property->updated_by = $pr['update_by'];

                $property->save();
                unset($property);
            }
        });

        echo 'Done Import Properties <br>';
        return false;
    }

    public function propertyTranslations()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyTranslation::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');


        ImportPropertyDetails::chunk(100, function ($propertyImportTranslations) {

            foreach ($propertyImportTranslations as $key => $pd) {

                $checkProperty = Property::where(['id' => $pd['property_id']])->get()->toArray();
                if (isset($checkProperty[0]['id'])) {
                    $propertyTranslations = new PropertyTranslation;
                    $propertyTranslations->id = $pd['id'];
                    $propertyTranslations->property_id = $pd['property_id'];
                    $locale = '';
                    if ($pd['language_id'] === 1) {
                        $locale = 'en';
                    } else if ($pd['language_id'] === 2) {
                        $locale = 'fr';
                    } else if ($pd['language_id'] === 9) {
                        $locale = 'jp';
                    }
                    $propertyTranslations->locale = $locale;
                    $propertyTranslations->subject = $pd['subject'];
                    $propertyTranslations->detail = strip_tags($pd['detail']);
                    $propertyTranslations->visible = ($pd['visible'] == 'Y' ? 1 : 0);
                    $propertyTranslations->viewed = $pd['viewed'];

                    $propertyTranslations->save();
                    unset($propertyTranslations);
                }
            }
        });
        echo 'Done Import Property translations <br>';
        return false;
    }

    public function propertyImages()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyImage::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        ImportPropertyImage::chunk(100, function ($propertyImage) {
            foreach ($propertyImage as $key => $pi) {

                $checkProperty = Property::where(['id' => $pi['property_id']])->get()->toArray();
                if (isset($checkProperty[0]['id'])) {
                    $propertyImage = new PropertyImage;

                    $propertyImage->id = $pi['id'];
                    $propertyImage->property_id = $pi['property_id'];
                    $propertyImage->name = $pi['image'];
                    $propertyImage->detail = $pi['detail'];
                    $propertyImage->sequence = $pi['sequence'];
                    $propertyImage->is_enabled = $pi['status'] == 'E' ? 1 : 0;
                    $propertyImage->created_by = $pi['create_by'];
                    $propertyImage->updated_by = $pi['update_by'];

                    $propertyImage->save();
                    unset($propertyImage);
                }
            }
        });
        echo 'Done Import Property images <br>';
        return false;
    }


    public function propertyCategory()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyCategory::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $importCategories = ImportPropertyCategory::all()->toArray();
        foreach ($importCategories as $key => $cat) {
            $propertyCategory = new PropertyCategory;
            $propertyCategory->id = $cat['id'];
            $propertyCategory->code = $cat['code'];
            $propertyCategory->sequence = $cat['sequence'];
            $propertyCategory->is_enabled = ($cat['status'] == 'E' ? 1 : 0);
            $propertyCategory->created_by = $cat['create_by'];
            $propertyCategory->updated_by = $cat['update_by'];

            $propertyCategory->save();
            unset($propertyCategory);
        }
        echo 'Done Import Property Category <br>';
        return false;
    }

    public function propertyCategoryTranslations()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyCategoryTranslations::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $importCategoriesDetails = ImportPropertyCategoryDetail::all()->toArray();
        foreach ($importCategoriesDetails as $key => $cat) {
            $propertyCategoryTranslations = new PropertyCategoryTranslations();
            $propertyCategoryTranslations->id = $cat['id'];
            $propertyCategoryTranslations->property_category_id = $cat['property_category_id'];
            $locale = '';
            if ($cat['language_id'] === 1) {
                $locale = 'en';
            } else if ($cat['language_id'] === 2) {
                $locale = 'fr';
            } else if ($cat['language_id'] === 9) {
                $locale = 'jp';
            }
            $propertyCategoryTranslations->locale = $locale;
            $propertyCategoryTranslations->name = $cat['subject'];
            $propertyCategoryTranslations->description = $cat['detail'];

            $propertyCategoryTranslations->url = $this->cleanString($this->cleanString($cat['subject']));

            $propertyCategoryTranslations->save();
            unset($propertyCategoryTranslations);
        }

        echo 'Done Import Property Category Translation <br>';
        return false;
    }

    /*Property facility*/
    public function propertyFacility()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyFacility::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $importFacility = ImportPropertyFacility::all()->toArray();
        foreach ($importFacility as $key => $facility) {
            $propertyFacility = new PropertyFacility;
            $propertyFacility->id = $facility['id'];
            $propertyFacility->image = $facility['image'];
            $propertyFacility->sequence = $facility['sequence'];
            $propertyFacility->is_enabled = ($facility['status'] == 'E' ? 1 : 0);
            $propertyFacility->created_by = $facility['create_by'];
            $propertyFacility->updated_by = $facility['update_by'];
            $propertyFacility->save();
            unset($propertyFacility);
        }
        echo 'Done Import Facility <br>';
        return false;
    }

    public function propertyFacilityTranslations()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyFacilityTranslations::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $importFacilityDetails = ImportPropertyFacilityDetails::all()->toArray();
        foreach ($importFacilityDetails as $key => $facility) {
            $propertyFacilityTranslations = new PropertyFacilityTranslations;
            $propertyFacilityTranslations->id = $facility['id'];
            $propertyFacilityTranslations->property_facility_id = $facility['property_facility_id'];
            $propertyFacilityTranslations->name = $facility['subject'];
            $locale = '';
            if ($facility['language_id'] === 1) {
                $locale = 'en';
            } else if ($facility['language_id'] === 2) {
                $locale = 'fr';
            } else if ($facility['language_id'] === 9) {
                $locale = 'jp';
            }
            $propertyFacilityTranslations->locale = $locale;
            $propertyFacilityTranslations->save();
            unset($propertyFacilityTranslations);
        }
        echo 'Done Import Facility Translations <br>';
        return false;
    }

    public function propertyFacilityData()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyFacilityData::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');


        ImportPropertyFacilityData::chunk(100, function ($importPropertyFacilityData) {
            foreach ($importPropertyFacilityData as $pfd) {

                $importPropertyFacilitiesList = explode(",", $pfd['property_facility_list']);
                $importPropertyFacilitiesList = array_unique($importPropertyFacilitiesList);
                if (count($importPropertyFacilitiesList) > 0 && $importPropertyFacilitiesList[0]) {
                    foreach ($importPropertyFacilitiesList as $ipf) {
                        $checkProperty = Property::where(['id' => $pfd['property_id']])->get()->toArray();
                        if (isset($checkProperty[0]['id'])) {
                            $propertyFacilityData = new PropertyFacilityData;
                            $propertyFacilityData->property_id = $pfd['property_id'];
                            $propertyFacilityData->property_facility_id = $ipf;
                            $propertyFacilityData->save();
                            unset($propertyFacilityData);
                        }
                    }
                }
            }
        });
        echo 'Done Import Facility Data <br>';
        return false;
    }

    /*Property Transport*/
    public function propertyTransport()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyTransport::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $importTransport = ImportPropertyTransport::all()->toArray();
        foreach ($importTransport as $key => $transport) {
            $propertyTransport = new PropertyTransport;
            $propertyTransport->id = $transport['id'];
            $propertyTransport->image = $transport['image'];
            $propertyTransport->sequence = $transport['sequence'];
            $propertyTransport->is_enabled = ($transport['status'] == 'E' ? 1 : 0);
            $propertyTransport->created_by = $transport['create_by'];
            $propertyTransport->updated_by = $transport['update_by'];
            $propertyTransport->save();
            unset($propertyTransport);
        }
        echo 'Done Import Property Transport <br>';
        return false;
    }

    public function propertyTransportTranslations()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyTransportTranslations::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $importTransportDetails = ImportPropertyTransportDetail::all()->toArray();
        foreach ($importTransportDetails as $key => $transport) {
            $propertyFacilityTranslations = new PropertyTransportTranslations;
            $propertyFacilityTranslations->id = $transport['id'];
            $propertyFacilityTranslations->property_transport_id = $transport['property_transport_id'];
            $propertyFacilityTranslations->name = $transport['subject'];
            $locale = '';
            if ($transport['language_id'] === 1) {
                $locale = 'en';
            } else if ($transport['language_id'] === 2) {
                $locale = 'fr';
            } else if ($transport['language_id'] === 9) {
                $locale = 'jp';
            }
            $propertyFacilityTranslations->locale = $locale;
            $propertyFacilityTranslations->save();
            unset($propertyFacilityTranslations);
        }
        echo 'Done Import Property Facility Translations <br>';
        return false;
    }

    public function propertyTransportData()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        PropertyTransportData::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');


        ImportPropertyTransportData::chunk(100, function ($importPropertyTransportData) {

            foreach ($importPropertyTransportData as $ptd) {
                $importPropertyTransportList = explode(",", $ptd['property_transport_list']);
                $importPropertyTransportList = array_unique($importPropertyTransportList);
                if (count($importPropertyTransportList) > 0 && $importPropertyTransportList[0]) {
                    foreach ($importPropertyTransportList as $ipf) {

                        $checkProperty = Property::where(['id' => $ptd['property_id']])->get()->toArray();
                        if (isset($checkProperty[0]['id'])) {
                            $propertyTransportData = new PropertyTransportData;
                            $propertyTransportData->property_id = $ptd['property_id'];
                            $propertyTransportData->property_transport_id = $ipf;
                            $propertyTransportData->save();
                            unset($propertyTransportData);
                        }
                    }
                }

            }
        });
        echo 'Done Import Transport Data <br>';
        return false;
    }

    public function articles (){
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Article::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $articles = ImportArticles::all()->toArray();
        foreach ($articles as $article){
            $newArticle = new Article;

            $newArticle->id = $article['id'];
            $newArticle->image = $article['image'] ? $article['image'] : '';
            $newArticle->is_enabled = ($article['status'] == 'E' ? 1 : 0);
            $newArticle->created_at = $article['create_date'];
            $newArticle->created_by = $article['create_by'] || 0;
            $newArticle->updated_by = $article['update_by'] || 0;

            $newArticle->save();
        }
        echo 'Done Import Articles Data <br>';
        return false;
    }

    public function articleTranslations (){
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        ArticleTranslation::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $articleTrans = ImportArticlesDetails::all()->toArray();

        foreach ($articleTrans as $trans){
            $newArtTrans = new ArticleTranslation;

            $newArtTrans->article_id = $trans['article_id'];
            $newArtTrans->title = $trans['subject'];
            $newArtTrans->description = $trans['detail'];
            $locale = '';
            if ($trans['language_id'] === 1) {
                $locale = 'en';
            } else if ($trans['language_id'] === 2) {
                $locale = 'fr';
            } else if ($trans['language_id'] === 9) {
                $locale = 'jp';
            }
            $newArtTrans->locale = $locale;
            $newArtTrans->viewed = $trans['viewed'];

            $newArtTrans->save();
        }
        echo 'Done Import Articles Translations Data <br>';
        return false;
    }

    /*Help functions*/
    function cleanString($string)
    {
        $string = str_replace(' ', '-', $string);
        $string = str_replace([' ', '/', '--'], '-', $string);
        return $string = strtolower(str_replace('--', '-', $string));
//        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }
}
