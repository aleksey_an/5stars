<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/*Import data*/
//Route::get('/import/{block}', 'ImportController@importAll');

/*Update Currency Data*/
//Route::get('/update-currency', 'Admin\CurrencyController@convert');

Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

//Debug
Route::get('/_debugbar/assets/stylesheets', '\Barryvdh\Debugbar\Controllers\AssetController@css');
Route::get('/_debugbar/assets/javascript', '\Barryvdh\Debugbar\Controllers\AssetController@js');


Route::post('/forms/home-contact-us', 'Forms\FormsController@homeContactUs');
Route::post('/forms/form-contact-agent', 'Forms\FormsController@contactWithAgent');
Route::post('/forms/news-latter', 'Forms\FormsController@newsLatter');

Route::get('/access-denied', function () {
    return view('errors.access-denied');
});

Route::get('/xml/download', 'Admin\XmlController@download');
/**
 * Admin
 */
Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth', 'checkStatus', 'admin', 'mainMenu', 'role:admin|super_admin|agent|editor']
    ], function () {

    Route::get('/', 'Admin\AdminController@index');
    /**
     * Articles (Blog)
    */
    Route::resource('/article', 'Admin\ArticleController');
    /**
     * Property
     */
    Route::get('/property/search', 'Admin\PropertyController@search');
    Route::resource('/property', 'Admin\PropertyController');
    /**
     * Property images
     */
    Route::post('/property/image/upload/{propertyId}', ['as' => 'property.image.upload', 'uses' => 'Admin\PropertyImageController@upload']);
    Route::post('/property/image/change-sequence', 'Admin\PropertyImageController@changeSequence');
    Route::post('/property/image/change-enable', 'Admin\PropertyImageController@changeEnableStatus');
    Route::post('/property/image/change-description', 'Admin\PropertyImageController@changeDescription');
    Route::post('/property/image/delete', 'Admin\PropertyImageController@delete');
    /**
     * Location resources
     */
    Route::resource('/location', 'Admin\LocationController', ['middleware' => ['role:admin|super_admin']]);
    Route::post('/location/delete/{id}', 'Admin\LocationController@delete', ['middleware' => ['role:admin|super_admin']]);
    //Location images
    Route::post('/location/image/upload/{locationId}', ['as' => 'location.image.upload', 'uses' => 'Admin\LocationImageController@upload']);
    Route::post('/location/image/change-sequence', 'Admin\LocationImageController@changeSequence');
    Route::post('/location/image/change-enable', 'Admin\LocationImageController@changeEnableStatus');
    Route::post('/location/image/delete', 'Admin\LocationImageController@delete');

    /**
     * Metro stations
     */
    Route::resource('/metro', 'Admin\MetroController', ['middleware' => ['role:admin|super_admin']]);
    /**
     * Property Categories
     */
    Route::resource('/category', 'Admin\PropertyCategoryController', ['middleware' => ['role:admin|super_admin']]);
    Route::post('/category/delete/{id}', 'Admin\PropertyCategoryController@delete', ['middleware' => ['role:admin|super_admin']]);
    Route::get('/category/sorting/{sort}/{id}', 'Admin\PropertyCategoryController@upDownSort', ['middleware' => ['role:admin|super_admin']]);
    /**
     * Property Facility
     */
    Route::resource('/facility', 'Admin\PropertyFacilityController', ['middleware' => ['role:admin|super_admin']]);
    /**
     * Property Transport
     */
    Route::resource('/transport', 'Admin\PropertyTransportController', ['middleware' => ['role:admin|super_admin']]);

//    Route::get('/content/', 'Admin\ContentController@index');
    Route::resource('/link', 'Admin\LinkController');
    Route::get('/article/', 'Admin\ArticleController@index');

    /**
     * User
     */
    Route::get('/user', ['middleware' => ['role:admin|super_admin'], 'uses' => 'Admin\UserController@index']);
    Route::get('/user/all', ['middleware' => ['role:admin|super_admin'], 'uses' => 'Admin\UserController@index']);
    Route::get('/user/create', ['middleware' => ['role:admin|super_admin'], 'uses' => 'Admin\UserController@create']);
    Route::post('/user/save', 'Admin\UserController@createUser');
    Route::get('/user/edit/{id}', 'Admin\UserController@edit');
    Route::post('/user/edit/{id}', 'Admin\UserController@editUser');
    Route::delete('/user/delete/{id}', 'Admin\UserController@deleteUser');

    Route::get('/user/suspend/{user}', 'Admin\UserController@suspend');
    Route::get('/user/activate/{user}', 'Admin\UserController@activate');
    /**
     * Role
     */
    Route::resource('/role', 'Admin\RoleController', ['except' => 'show']);
    Route::resource('/permission', 'Admin\PermissionController');
    /**
     * Administration
     */
    Route::get('/administration/', 'Admin\AdministrationController@index');

    Route::resource('/configuration', 'Admin\ConfigurationController', ['except' => 'show', 'create', 'destroy']);

    Route::resource('/currency', 'Admin\CurrencyController', ['except' => 'show']);
//    Route::resource('/emailtemplate', 'Admin\EmailTemplateController');
    Route::resource('/language', 'Admin\LanguageController', ['except' => 'show']);

    /**
     * Localization (Translations web content)
     */
    Route::get('/translation', 'Admin\TranslationController@index');
    Route::post('/translation/update-all', 'Admin\TranslationController@updateAll');

    /**
     * Home Page
     */
    Route::get('/edit-home-page', 'Admin\HomePageController@uploadImages');
    Route::post('/edit-home-page/upload-images', 'Admin\HomePageController@uploadImage');
    Route::post('/edit-home-page/delete-image', 'Admin\HomePageController@deleteImage');

    Route::get('/edit-home-page/select-property', 'Admin\HomePageController@selectProperty');
    Route::get('/edit-home-page/select-property/search', 'Admin\HomePageController@selectPropertySearch');
    Route::post('/edit-home-page/add-property', 'Admin\HomePageController@addProperty');
    Route::post('/edit-home-page/delete-property', 'Admin\HomePageController@deleteProperty');

    /**
     * Show guest property
     */
    Route::get('/show-guest-property/', 'Admin\ShowGuestPropertyController@index');

    /**
     * XML fid
     */
    Route::get('/xml', 'Admin\XmlController@index');
    Route::post('/xml/generate', 'Admin\XmlController@generateXml');
    /**
     * FAQ
     */
    Route::get('/faq/sorting/{sort}/{id}', 'Admin\FaqController@upDownSort', ['middleware' => ['role:admin|super_admin']]);
    Route::resource('/faq', 'Admin\FaqController');
});

/**
 * Web
 */
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
], function () {
    //Debug
    Route::get('/_debugbar/assets/stylesheets', '\Barryvdh\Debugbar\Controllers\AssetController@css');
    Route::get('/_debugbar/assets/javascript', '\Barryvdh\Debugbar\Controllers\AssetController@js');

    Route::get('/', 'Web\WebController@index');
    /**
     * Static-pages
     */
    Route::get('/blog/{id?}', 'Web\StaticPagesController@articles');
    Route::get('/faq', 'Web\StaticPagesController@faq');
    Route::get('/who-we-are', 'Web\StaticPagesController@pages');
    Route::get('/about-us', 'Web\StaticPagesController@pages');
    Route::get('/links', 'Web\StaticPagesController@pages');
    Route::get('/our-services', 'Web\StaticPagesController@pages');
    Route::get('/agencies-in-thailand', 'Web\StaticPagesController@pages');
    Route::get('/success', 'Web\WebController@success');


    Route::get('/take-favorites', 'Web\AjaxController@takeFavorites');
    Route::get('/take-viewed', 'Web\AjaxController@takeViewed');
    Route::post('/check-recaptcha', 'Web\AjaxController@checkRecaptcha')->name('check-recaptcha');

    Route::get('/property-deleted', 'Web\WebController@notFoundProperty');
    Route::get('/favorites', 'Web\WebController@favorites');
    Route::get('/{saleRentLocation}/{propertyType?}/{location?}/{location1?}/{location2?}/{location3?}/{location4?}/{location5?}/{location6?}/{location7?}', 'Web\UrlParserController@parser');

});
