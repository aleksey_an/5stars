var elixir = require('laravel-elixir');

elixir(function (mix) {
    mix.sass('app.scss')
        .sass('web.scss')
        .sass('daterangepicker.scss')
        .sass('../../../node_modules/bootstrap-select/sass/bootstrap-select.scss')
        .copy('resources/assets/js/lib/intl-tel-input/build/css/intlTelInput.css', 'public/css/intlTelInput.css')
        .copy('node_modules/lightslider/dist/css/lightslider.css', 'public/css')
        .copy('node_modules/jquery-ui-dist/jquery-ui.min.css', 'public/css')
        .copy('node_modules/dropzone/dist/min/dropzone.min.css', 'public/css')
        .copy('node_modules/bootstrap-sass/assets/fonts', 'public/fonts')
        .copy('node_modules/jquery-ui-dist/images', 'public/css/images')
        .copy('resources/assets/fonts', 'public/css/fonts')
        .copy('resources/assets/img', 'public/img')
        .copy('resources/assets/js/lib/ckeditor', 'public/ckeditor')
        .browserify('admin/admin.js')
        .browserify('web/web-lib.js')
        .scripts(['web/main.js', 'web/property.js', 'web/location.js', 'web/search.js', 'web/form.js'], 'public/js/web.js')
        .browserSync({
            proxy: '5stars.loc'
        });
});