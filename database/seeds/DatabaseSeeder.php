<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        $this->call(CurrencyTableSeeder::class);
        $this->call(ConfigurationTableSeeder::class);
        $this->call(RoleUserSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(IdRoleUsersSeeder::class);
        $this->call(PermissionRoleSeeder::class);
    }
}
