<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Admin*/
        \App\Permission::create([
            'name' => 'create_admin',
            'display_name' => 'Create Admin',
            'description' => 'Create Admin'
        ]);
        \App\Permission::create([
            'name' => 'edit_admin',
            'display_name' => 'Edit Admin',
            'description' => 'Edit Admin'
        ]);
        \App\Permission::create([
            'name' => 'delete_admin',
            'display_name' => 'Delete Admin',
            'description' => 'Delete Admin'
        ]);
        /*Agent*/
        \App\Permission::create([
            'name' => 'create_agent',
            'display_name' => 'Create Agent',
            'description' => 'Create Agent'
        ]);
        \App\Permission::create([
            'name' => 'edit_agent',
            'display_name' => 'Edit Agent',
            'description' => 'Edit Agent'
        ]);
        \App\Permission::create([
            'name' => 'delete_agent',
            'display_name' => 'Delete Agent',
            'description' => 'Delete Agent'
        ]);
        /*Editor*/
        \App\Permission::create([
            'name' => 'create_editor',
            'display_name' => 'Create Editor ',
            'description' => 'Create Editor'
        ]);
        \App\Permission::create([
            'name' => 'edit_editor',
            'display_name' => 'Edit Editor',
            'description' => 'Edit Editor'
        ]);
        \App\Permission::create([
            'name' => 'delete_editor',
            'display_name' => 'Delete Editor',
            'description' => 'Delete Editor'
        ]);
        /*City*/
        \App\Permission::create([
            'name' => 'create_city',
            'display_name' => 'Create City',
            'description' => 'Create City'
        ]);
        \App\Permission::create([
            'name' => 'edit_city',
            'display_name' => 'Edit City',
            'description' => 'Edit City'
        ]);
        \App\Permission::create([
            'name' => 'delete_city',
            'display_name' => 'Delete City',
            'description' => 'Delete City'
        ]);
        /*Assign City to user*/
        \App\Permission::create([
            'name' => 'assign_city_to_user',
            'display_name' => 'Assign City To User',
            'description' => 'Assign City To User'
        ]);
        /*Area*/
        \App\Permission::create([
            'name' => 'create_area',
            'display_name' => 'Create Area',
            'description' => 'Create Area'
        ]);
        \App\Permission::create([
            'name' => 'edit_area',
            'display_name' => 'Edit Area',
            'description' => 'Edit Area'
        ]);
        \App\Permission::create([
            'name' => 'delete_area',
            'display_name' => 'Delete Area',
            'description' => 'Delete Area'
        ]);
        /*Property*/
        \App\Permission::create([
            'name' => 'view_property',
            'display_name' => 'View Property',
            'description' => 'View Property'
        ]);
        \App\Permission::create([
            'name' => 'create_property',
            'display_name' => 'Create Property',
            'description' => 'Create Property'
        ]);
        \App\Permission::create([
            'name' => 'edit_property',
            'display_name' => 'Edit Property',
            'description' => 'Edit Property'
        ]);
        \App\Permission::create([
            'name' => 'delete_property',
            'display_name' => 'Delete Property',
            'description' => 'Delete Property'
        ]);
    }
}
