<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Currency::create([
            'id' => 1,
            'name' => 'Thai Baht',
            'code' => 'THB',
            'sign' => '฿',
            'rate' => 1,
            'sequence' => 1,
            'is_default' => 1,
            'is_enabled' => 1,
        ]);
        
        \App\Currency::create([
            'id' => 2,
            'name' => 'Euro',
            'code' => 'EUR',
            'sign' => '€',
            'rate' => 	0.0257,
            'sequence' => 2,
            'is_default' => 0,
            'is_enabled' => 1,
        ]);
        
        \App\Currency::create([
            'id' => 3,
            'name' => 'Dollar',
            'code' => 'USD',
            'sign' => '$',
            'rate' => 0.0303,
            'sequence' => 3,
            'is_default' => 0,
            'is_enabled' => 1,
        ]);
    }
}
