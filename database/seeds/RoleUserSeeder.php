<?php

use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create([
            'name' => 'super_admin',
            'display_name' => 'Super Admin',
            'description' => 'Super admin'
        ]);
        \App\Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Admin'
        ]);
        \App\Role::create([
            'name' => 'agent',
            'display_name' => 'Agent',
            'description' => 'Agent'
        ]);
        \App\Role::create([
            'name' => 'editor',
            'display_name' => 'Editor',
            'description' => 'Editor'
        ]);
    }
}
