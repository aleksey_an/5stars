<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Language::create([
            'id' => 1,
            'name' => 'French',
            'locale' => 'fr',
            'iso_639_3' => 'fra',
            'sequence' => 1,
            'image' => 'fr.png',
            'is_enabled' => 1,
        ]);
        \App\Language::create([
            'id' => 2,
            'name' => 'English',
            'locale' => 'en',
            'iso_639_3' => 'eng',
            'sequence' => 2,
            'image' => 'en.png',
            'is_enabled' => 1,
        ]);
         \App\Language::create([
            'id' => 3,
            'name' => 'Japanese',
            'locale' => 'jp',
            'iso_639_3' => 'jpn',
            'sequence' => 3,
             'image' => 'jp.png',
            'is_enabled' => 1,
             'is_enabled_web' => 0
        ]);
    }
}
