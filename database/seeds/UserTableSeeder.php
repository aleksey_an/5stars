<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'username' => 'super_admin',
            'first_name' => 'Super Admin',
            'last_name' => 'Super Admin',
            'prefix' => 'Mr.',
            'is_enabled' => 1,
            'email' => 'super.admin@gmail.com',
            'password' => 'qweqwe',
        ]);
        \App\User::create([
            'username' => 'admin',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'contact_viber' => 'my-viber',
            'contact_line' => 'my-line',
            'contact_whatsapp' => 'my-whatsapp',
            'contact_skype' => 'my-skype',
            'prefix' => 'Mr.',
            'is_enabled' => 1,
            'email' => 'admin@gmail.com',
            'password' => 'qweqwe',
        ]);
        \App\User::create([
            'username' => 'editor',
            'first_name' => 'Editor',
            'last_name' => 'Editor',
            'prefix' => 'Mr.',
            'is_enabled' => 1,
            'email' => 'editor@gmail.com',
            'password' => 'qweqwe',
        ]);
        \App\User::create([
            'username' => 'agent',
            'first_name' => 'Agent',
            'last_name' => 'Agent',
            'prefix' => 'Mr.',
            'is_enabled' => 1,
            'email' => 'agent@gmail.com',
            'password' => 'qweqwe',
        ]);

    }

}
