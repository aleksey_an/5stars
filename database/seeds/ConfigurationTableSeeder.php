<?php

use Illuminate\Database\Seeder;

class ConfigurationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Configuration::create([
            'id' => 1,
            'key' => 'company_name',
            'value' => 'FIVE STARS THAILAND REAL ESTATE',
            'description' => 'Company name',
        ]);
        
        \App\Configuration::create([
            'id' => 2,
            'key' => 'company_address',
            'value' => '153/3, 4th floor, Goldenland Building, Soi Mahardlek Luang 1,Ratchadamri Rd., Lumpini, Phathumwan, Bangkok 10330, Thailand',
            'description' => 'Company Address ',
        ]);
        
        \App\Configuration::create([
            'id' => 3,
            'key' => 'company_phone',
            'value' => '+66(0)2 652 0576',
            'description' => 'Company Phone',
        ]);
        
        \App\Configuration::create([
            'id' => 4,
            'key' => 'company_fax',
            'value' => '+66(0)2 652 0577',
            'description' => 'Company Fax',
        ]);
        
        \App\Configuration::create([
            'id' => 5,
            'key' => 'company_email',
            'value' => 'fabrice@5stars-immobilier.com',
            'description' => 'Company Email',
        ]);

        \App\Configuration::create([
            'id' => 6,
            'key' => 'company_skype',
            'value' => 'fivestarsthai',
            'description' => 'Skype Account',
        ]);
        \App\Configuration::create([
            'id' => 7,
            'key' => 'company_wechat',
            'value' => 'Wechat',
            'description' => 'Wechat Account',
        ]);
        \App\Configuration::create([
            'id' => 8,
            'key' => 'company_line',
            'value' => 'Line',
            'description' => 'Line Account',
        ]);
        \App\Configuration::create([
            'id' => 9,
            'key' => 'company_whatsapp',
            'value' => 'Whatsapp',
            'description' => 'Whatsapp Account',
        ]);

        \App\Configuration::create([
            'id' => 10,
            'key' => 'facebook_url',
            'value' => 'https://www.facebook.com/pages/Five-stars-Transactions-Immobilieres/181105408567906',
            'description' => 'Facebook URL',
        ]);

        \App\Configuration::create([
            'id' => 11,
            'key' => 'youtube_url',
            'value' => 'https://www.youtube.com/user/fivestarsthailand',
            'description' => 'Youtube URL',
        ]);
        
        \App\Configuration::create([
            'id' => 12,
            'key' => 'property_upload_max_width',
            'value' => '1024',
            'description' => 'Resize uploaded images to fit width by size (px)',
        ]);

        \App\Configuration::create([
            'id' => 13,
            'key' => 'location_upload_max_width',
            'value' => '1024',
            'description' => 'Resize uploaded images to fit width by size (px)',
        ]);
    }
}
