<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_transports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('sequence')->default(0);
            $table->boolean('is_enabled')->default(true);
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->timestamps();
        });
        
        Schema::create('property_transport_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_transport_id')->unsigned();;
            $table->string('name')->nullable();
            $table->string('locale',2)->index();
            $table->unique(['property_transport_id','locale'], 'p_t_id_locale');
            $table->foreign('property_transport_id')->references('id')->on('property_transports')->onDelete('cascade');
        });

        Schema::create('property_transport_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned();
            $table->integer('property_transport_id')->unsigned();

            $table->unique(['property_id','property_transport_id'], 'p_id_p_t_id');
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->foreign('property_transport_id')->references('id')->on('property_transports')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('property_transport_data');
        Schema::dropIfExists('property_transport_translations');
        Schema::dropIfExists('property_transports');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
