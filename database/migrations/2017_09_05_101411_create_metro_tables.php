<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetroTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metro', function (Blueprint $table){
            $table->increments('id');
            $table->double('lat')->default(0);
            $table->double('lng')->default(0);
            $table->string('address')->default('');
            $table->boolean('is_enabled')->default(true);
            $table->integer('location_id')->unsigned();
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->timestamps();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
        });

        Schema::create('metro_translations', function (Blueprint $table){
            $table->increments('id');
            $table->string('locale', 2);
            $table->integer('metro_id')->unsigned();
            $table->string('name');
            $table->foreign('metro_id')->references('id')->on('metro')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('metro_translations');
        Schema::dropIfExists('metro');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
