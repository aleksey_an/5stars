<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('image')->nullable();
            $table->unsignedInteger('sequence');
            $table->boolean('is_enabled')->default(true);
            $table->timestamps();
        });
        
        Schema::create('link_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('link_id');
            $table->string('subject')->nullable();
            $table->text('detail')->nullable();
            
            $table->string('locale',2)->index();
            $table->unsignedInteger('clicked');
            $table->unique(['link_id','locale']);
            $table->foreign('link_id')->references('id')->on('links')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_translations');
        Schema::dropIfExists('links');
    }
}
