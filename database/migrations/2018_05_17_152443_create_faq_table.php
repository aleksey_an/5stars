<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq', function (Blueprint $table){
            $table->increments('id');
            $table->boolean('is_enabled')->default(1);
            $table->integer('sequence')->default(0);
            $table->timestamps();
        });

        Schema::create('faq_translations', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('faq_id');
            $table->text('question');
            $table->text('answer');
            $table->boolean('visible')->default(1);
            $table->string('locale', 2)->index();

            $table->unique(['faq_id', 'locale']);
            $table->foreign('faq_id')->references('id')->on('faq')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_translations');
        Schema::dropIfExists('faq');
    }
}
