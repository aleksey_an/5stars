<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE properties MODIFY COLUMN bathroom VARCHAR(250) DEFAULT '' ");
        DB::statement("ALTER TABLE properties ADD address_1 VARCHAR(250) DEFAULT ''");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE properties MODIFY COLUMN bathroom INTEGER (11) DEFAULT 0 ");
        DB::statement("ALTER TABLE properties DROP COLUMN address_1");
    }
}
