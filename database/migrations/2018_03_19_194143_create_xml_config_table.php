<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXmlConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xml_config', function (Blueprint $table){
            $table->increments('id');
            $table->string('locale')->nullable();
            $table->json('location_id')->nullable();
            $table->json('category_id')->nullable();
            $table->boolean('to_sale')->nullable();
            $table->boolean('to_rent')->nullable();
            $table->dateTime('date_from')->nullable();
            $table->dateTime('date_to')->nullable();
            $table->integer('budget_from')->nullable();
            $table->integer('budget_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xml_config');
    }
}
