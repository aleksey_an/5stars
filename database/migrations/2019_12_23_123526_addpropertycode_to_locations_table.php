<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddpropertycodeToLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
             $table->string('ddproperty_code')->after('lng');
        });

        DB::table('locations')->where('id', 2)->update(['ddproperty_code' => 'TH500101']);
        DB::table('locations')->where('id', 3)->update(['ddproperty_code' => 'TH770701']);
        DB::table('locations')->where('id', 4)->update(['ddproperty_code' => 'TH840401']);
        DB::table('locations')->where('id', 5)->update(['ddproperty_code' => 'TH980101']);
        DB::table('locations')->where('id', 6)->update(['ddproperty_code' => 'TH830101']);
        DB::table('locations')->where('id', 14)->update(['ddproperty_code' => 'TH103303']);
        DB::table('locations')->where('id', 15)->update(['ddproperty_code' => 'TH103303']);
        DB::table('locations')->where('id', 16)->update(['ddproperty_code' => 'TH103902']);
        DB::table('locations')->where('id', 17)->update(['ddproperty_code' => 'TH103901']);
        DB::table('locations')->where('id', 18)->update(['ddproperty_code' => 'TH100703']);
        DB::table('locations')->where('id', 19)->update(['ddproperty_code' => 'TH100703']);
        DB::table('locations')->where('id', 20)->update(['ddproperty_code' => 'TH100703']);
        DB::table('locations')->where('id', 21)->update(['ddproperty_code' => 'TH100703']);
        DB::table('locations')->where('id', 22)->update(['ddproperty_code' => 'TH100703']);
        DB::table('locations')->where('id', 23)->update(['ddproperty_code' => 'TH100404']);
        DB::table('locations')->where('id', 24)->update(['ddproperty_code' => 'TH102803']);
        DB::table('locations')->where('id', 25)->update(['ddproperty_code' => 'TH102803']);
        DB::table('locations')->where('id', 26)->update(['ddproperty_code' => 'TH102803']);
        DB::table('locations')->where('id', 27)->update(['ddproperty_code' => 'TH100404']);
        DB::table('locations')->where('id', 28)->update(['ddproperty_code' => 'TH100404']);
        DB::table('locations')->where('id', 29)->update(['ddproperty_code' => 'TH103701']);
        DB::table('locations')->where('id', 30)->update(['ddproperty_code' => 'TH103701']);
        DB::table('locations')->where('id', 31)->update(['ddproperty_code' => 'TH103701']);
        DB::table('locations')->where('id', 32)->update(['ddproperty_code' => 'TH103702']);
        DB::table('locations')->where('id', 33)->update(['ddproperty_code' => 'TH103702']);
        DB::table('locations')->where('id', 34)->update(['ddproperty_code' => 'TH840501']);
        DB::table('locations')->where('id', 35)->update(['ddproperty_code' => 'TH101701']);
        DB::table('locations')->where('id', 36)->update(['ddproperty_code' => 'TH101701']);
        DB::table('locations')->where('id', 37)->update(['ddproperty_code' => 'TH104501']);
        DB::table('locations')->where('id', 38)->update(['ddproperty_code' => 'TH103801']);
        DB::table('locations')->where('id', 39)->update(['ddproperty_code' => 'TH101701']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
             $table->dropColumn('ddproperty_code');
        });
    }
}
