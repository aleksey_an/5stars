<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->default('');

            $table->integer('category_id')->default(0);
            $table->integer('location_id')->default(0);
            $table->integer('metro_id')->default(0);
            $table->integer('user_id');
            /*Location*/
            $table->double('lat')->default(0);
            $table->double('lng')->default(0);
            /*Contact*/
            $table->string('contact_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->string('contact_mobile')->nullable();
            $table->string('contact_viber')->default('');
            $table->string('contact_line')->default('');
            $table->string('contact_whatsapp')->default('');
            /*Property description*/
            $table->string('built_year')->nullable();
            $table->string('building_name')->nullable();
            $table->string('address')->nullable();
            $table->string('unit_number')->nullable();
            $table->double('indoor_area')->default(0);
            $table->double('outdoor_area')->default(0);
            $table->integer('bedroom')->default(0);
            $table->integer('bathroom')->default(0);
            $table->boolean('furnished')->default(0);
            $table->string('floor_number')->default('');
            $table->string('key_info')->default('');

            /*Property for rent*/
            $table->boolean('to_rent')->default(0);
            $table->boolean('is_rent')->default(0);
            $table->integer('rent_month')->deefault(0);
            $table->integer('rent_week')->default(0);
            $table->integer('rent_day')->default(0);
            /*Property for sale*/
            $table->boolean('to_sale')->default(0);
            $table->boolean('is_sold')->default(0);
            $table->integer('sale_price')->default(0);


            $table->string('common_fee')->nullable();
            $table->string('availability')->nullable();
            $table->dateTime('publish_date')->default(null);
            $table->string('youtube_url')->nullable();
            $table->enum('status', ['published', 'disabled', 'pending'])->default('published');
            //create_by
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->timestamps();
        });

        Schema::create('property_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('property_id');
            $table->string('subject')->nullable();
            $table->text('detail')->nullable();
            $table->string('locale', 2)->index();
            $table->boolean('visible')->default(1);
            $table->integer('viewed')->default(0);

            $table->unique(['property_id', 'locale']);
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
        });

        Schema::create('property_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('property_id');
            $table->string('name');
            $table->string('detail')->default('');
            $table->integer('sequence')->default(1);
            $table->boolean('is_enabled')->defaul(1);
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);

            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_images');
        Schema::dropIfExists('property_translations');
        Schema::dropIfExists('properties');
    }
}
