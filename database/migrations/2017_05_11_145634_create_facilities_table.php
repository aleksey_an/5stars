<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_facilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->integer('sequence')->default(0);
            $table->boolean('is_enabled')->default(true);
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->timestamps();
        });
        
         Schema::create('property_facility_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_facility_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('locale',2)->index();
            $table->unique(['property_facility_id', 'locale'], 'p_f_id_locale');
            $table->foreign('property_facility_id')->references('id')->on('property_facilities')->onDelete('cascade');
        });

        Schema::create('property_facility_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned();
            $table->integer('property_facility_id')->unsigned();
            $table->unique(['property_facility_id', 'property_id'], 'p_f_id_p_id');
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->foreign('property_facility_id')->references('id')->on('property_facilities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('property_facility_data');
        Schema::dropIfExists('property_facility_translations');
        Schema::dropIfExists('property_facilities');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
