<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 1)->unique();
            $table->integer('sequence')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->timestamps();
        });

        Schema::create('property_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_category_id')->unsigned();
            $table->string('locale',2)->index();
            $table->string('url');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->foreign('property_category_id')->references('id')->on('property_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_category_translations');
        Schema::dropIfExists('property_categories');
    }
}
