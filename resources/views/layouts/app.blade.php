<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="/img/favicon.ico">
    <title>@yield('title','5stars-immobilier')</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link rel="stylesheet" href="/css/dropzone.min.css">
    <link href="/css/app.css" rel="stylesheet">
<!--    <link href="/css/summernote.css" rel="stylesheet">-->
    <link href="/css/jquery-ui.min.css" rel="stylesheet">

    <script src="/ckeditor/ckeditor.js"></script>
    <script src="/js/admin.js"></script>

</head>
<body id="app-layout">
@if (Auth::user() && isset($AdminNavBar))
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/admin') }}">5stars-immobilier.com</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            {!! $AdminNavBar->asUl(array('class' => 'nav navbar-nav')) !!}

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Hello, {{ Auth::user()->username }}</a></li>
                <li><a href="#">Role #{{ Auth::user()->roles->first()->name }}</a></li>
                <li><a href="{{ url('/logout') }}">Logout</a></li>
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
@endif
@if(session('success'))
<div class="container alert alert-success">{{ session('success') }}</div>
@elseif (session('error'))
<div class="container alert alert-danger">{{ session('error') }}</div>
@endif

@yield('content')

<script>
    function gUploaded() {
        var el = document.getElementById('g-m-uploaded');
        if (el)
            el.remove();
    }
</script>
<script id="g-m-uploaded"
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=gUploaded">
</script>
<script src="/js/web.js"></script>
@stack('scripts')

</body>
</html>
