<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta
          name="description"
          content="@yield('meta_description') {{ isset($propertyTranslation->subject) ? ',' . $propertyTranslation->subject : ''}} ">

    <meta
          name="keywords"
          content="Thailand{{isset($metaKeyWords) ? ',' . join(',', $metaKeyWords) : ''}}"
    />

    <title>@yield('meta_title') - {{trans('common.meta_title_fivestars')}}</title>

    <!--    <link rel="shortcut icon" href="/img/favicon.ico">-->
    <link rel="stylesheet" href="/css/bootstrap-select.css">
    <link rel="stylesheet" href="/css/daterangepicker.css">
    <link rel="stylesheet" href="/css/lightslider.css">
    <link rel="stylesheet" href="/css/web.css">
    <link rel="stylesheet" href="/css/intlTelInput.css">

</head>
<body>

@yield('content')

@include('web.components.footer.footer')

<script>
  document.addEventListener('DOMContentLoaded', function () {
    Web.main.init();
  })
</script>

<script src="/js/web-lib.js"></script>
<script src="/js/web.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{env('GOOGLE_ANALYTICS_TRACKING')}}"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{env('GOOGLE_ANALYTICS_TRACKING')}}');
</script>
</body>
</html>