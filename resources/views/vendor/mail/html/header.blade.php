<tr>
    <td class="header">
        <a href="{{ $url }}" title="{{ $slot }}">
            {{ $slot }}
        </a>
    </td>
</tr>
