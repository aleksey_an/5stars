<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<div>

    <div style="width: 100%;padding: 85px 0 100px;margin: auto;background: #ededed;">

            <div style="text-align: center; margin: 0 auto;padding-bottom: 50px;" >
                <a href="{{url('/')}}" style="width: 275px;margin: auto;" target="_blank"><img src="{{url('/')}}/img/logo_{{\App::getLocale()}}.png" alt="fivestars" style="width: 275px;"></a>
            </div>

        <div style="width: 540px;padding: 25px 75px;margin: auto;background: #ffffff;">

            <div style="border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;">
                <p style="color: #a8a8a8;font-size: 14px;">Name</p>
                <p style="color:#434343;font-size: 18px;">{{$name}}</p>
            </div>
            <div style="border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;">
                <p style="color: #a8a8a8;font-size: 14px;">Phone</p>
                <p style="color:#434343;font-size: 18px;">{{$phone}}</p>
            </div>
            <div style="border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;">
                <p style="color: #a8a8a8;font-size: 14px;">Email</p>
                <p style="color:#434343;font-size: 18px;">{{$email}}</p>
            </div>
            <div style="border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;">
                <p style="color: #a8a8a8;font-size: 14px;">Message</p>
                <p style="color:#434343;font-size: 18px;">{{$text}}</p>
            </div>
        </div>

    </div>

</div>
</body>
</html>