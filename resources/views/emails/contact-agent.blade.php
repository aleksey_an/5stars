<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
    <div style="width: 100%;padding: 85px 0 100px;margin: auto;background: #ededed;">

        <div style="width: 540px;padding: 25px 75px;margin: auto;background: #ffffff;">

            <h3 style="font-size: 20px;text-align: center; color: #434343;">Test contact agent form</h3>

            <div style="color:#434343;border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;font-size: 18px;"><p style="color: #a8a8a8;font-size: 14px;">Reference</p> {{$propertyCode}}</div>

            <div style="color:#434343;border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;font-size: 18px;"><p style="color: #a8a8a8;font-size: 14px;">User name</p> {{$name}}</div>

            <div style="color:#434343;border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;font-size: 18px;"><p style="color: #a8a8a8;font-size: 14px;">User phone</p> {{$phone}}</div>

            <div style="color:#434343;border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;font-size: 18px;"><p style="color: #a8a8a8;font-size: 14px;">User email</p> {{$email}}</div>

            <div style="color:#434343;border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;font-size: 18px;"><p style="color: #a8a8a8;font-size: 14px;">User message</p> {{$text}}</div>
            @if(isset($rentDate['from']) && isset($rentDate['to']))
                <div style="color:#434343;border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;font-size: 18px;"><p style="color: #a8a8a8;font-size: 14px;">Rent date</p> <span>From {{$rentDate['from']}}</span> <span>To {{$rentDate['to']}}</span></div>
            @endif
            <div style="color:#434343;border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;font-size: 18px;"><p style="color: #a8a8a8;font-size: 14px;">Viewed</p> {{$viewed}}</div>

            <div style="color:#434343;border-bottom: solid 1px #a8a8a8;padding-bottom: 10px;margin-bottom: 30px;font-size: 18px;"><p style="color: #a8a8a8;font-size: 14px;">Favorites</p> {{$favorites}}</div>
        </div>

    </div>
</body>
</html>