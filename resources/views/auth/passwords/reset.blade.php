@extends('layouts.app2')

@section('title', trans('common.reset_password'))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <h1 class="home-title text-center title-for-form">{{ trans('common.reset_password') }}</h1>
            <form id="password-reset-form" class="form-inscription" role="form" method="POST" action="{{ url(LaravelLocalization::getCurrentLocale().'/password/reset') }}">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('common.email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">{{ $errors->first('email') }}</span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password_register" type="password" class="form-control" name="password" placeholder="{{ trans('common.password') }}">
                    @if ($errors->has('password'))
                        <span class="help-block">{{ $errors->first('password') }}</span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" name="password_confirmation" placeholder="{{ trans('common.password_confirm') }}">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" value="{{ trans('common.reset_password') }}">
                        <i class="glyphicon glyphicon-refresh"></i> {{ trans('common.reset_password') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $("#password-reset-form").validate({
            errorClass: "help-block",
            errorElement: "span",
            rules: {
                email: {
                    required: true,
                    email: true,
                    maxlength: 255
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    equalTo: "#password_register"
                }
            },
            messages: {
                email: {
                    required: "{!! trans('validation.required', ['attribute' => trans('common.email')]) !!}",
                    email: "{!! trans('validation.email', ['attribute' => trans('common.email')]) !!}",
                    maxlength: "{!! trans('validation.max.string', ['attribute' => trans('common.email'), 'max' => 255]) !!}"
                },
                password: {
                    required: "{!! trans('validation.required', ['attribute' => trans('common.password')]) !!}",
                    minlength: "{!! trans('validation.min.string', ['attribute' => trans('common.password'), 'min' => 6]) !!}"
                },
                password_confirmation: {
                    required: "{!! trans('validation.required', ['attribute' => trans('common.password_confirmation')]) !!}",
                    equalTo: "{!! trans('validation.confirmed', ['attribute' => trans('common.password_confirmation')]) !!}"
                }
            },
            submitHandler: function(form) {
                form.submit();
            },
            highlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").removeClass('has-error');
            }
        });
    });
</script>
@endpush
