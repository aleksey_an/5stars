@extends('layouts.app2')

@section('title', trans('common.password_reset_title'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="change-em-pas">

                    <h1 class="h2-titre">{{ trans('common.password_reset_title') }}</h1>
                    <div class="change-em-pas-item">

                        <form id="password-email-form" class="form-inscription" role="form" method="POST" action="{{ url(LaravelLocalization::getCurrentLocale().'/password/email') }}">
                            {!! csrf_field() !!}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('common.email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" value="{{ trans('common.send_password_reset_link') }}">
                                    <i class="glyphicon glyphicon-envelope"></i> {{ trans('common.send_password_reset_link') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(document).ready(function() {
        $("#password-email-form").validate({
            errorClass: "help-block",
            errorElement: "span",
            rules: {
                email: {
                    required: true,
                    email: true,
                    maxlength: 255
                }
            },
            messages: {
                email: {
                    required: "{!! trans('validation.required', ['attribute' => trans('common.email')]) !!}",
                    email: "{!! trans('validation.email', ['attribute' => trans('common.email')]) !!}",
                    maxlength: "{!! trans('validation.max.string', ['attribute' => trans('common.email'), 'max' => 255]) !!}"
                }
            },
            submitHandler: function(form) {
                form.submit();
            },
            highlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").removeClass('has-error');
            }
        });
    });
</script>
@endpush
