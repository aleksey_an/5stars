@extends('layouts.app')

@section('title', trans('auth.login'))

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 vcenter">
               <div class="panel panel-default">
               <div class="panel-heading text-center"><b>Login</b></div>
               <div class="panel-body">
                    {!! Form::open(['url' =>  url('login'), 'id'=> 'login-form', 'class' => 'form-horizontal', 'method' => 'POST']) !!}
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                        <div class="col-md-6 col-md-offset-3">
                            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('auth.email_addr')]) !!}
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                        <div class="col-md-6 col-md-offset-3">
                             <input type="password" class="form-control" name="password" placeholder="{{ trans('auth.password') }}">
                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            {!! Form::submit(trans('auth.login'), ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
               </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $("#login-form").validate({
            errorClass: "help-block",
            errorElement: "p",
            rules: {
                email: {
                    required: true,
                    email: true,
                    maxlength: 255
                },
                password: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                email: {
                    required: "{!! trans('validation.required', ['attribute' => trans('common.email')]) !!}",
                    email: "{!! trans('validation.email', ['attribute' => trans('common.email')]) !!}",
                    maxlength: "{!! trans('validation.max.string', ['attribute' => trans('common.email'), 'max' => 255]) !!}"
                },
                password: {
                    required: "{!! trans('validation.required', ['attribute' => trans('common.password')]) !!}",
                    minlength: "{!! trans('validation.min.string', ['attribute' => trans('common.password'), 'min' => 6]) !!}"
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            highlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="' + element.getAttribute('name') + '"]').closest(".login").addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="' + element.getAttribute('name') + '"]').closest(".login").removeClass('has-error');
            }
        });
    });
</script>
@endpush
