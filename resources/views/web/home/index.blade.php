@extends('layouts.web')
@section('meta_title', trans('common.home_meta_title'))
@section('content')
@include('web.components.headers.home')
<!--Search block-->
<div class="home">
    <div class="home-img" id="home" {!!$backgroundImage!!}>
        @include ('web.components.search')
    </div>

    @if(isset($sp) && count($sp) > 0)
    <h2 class="title text-center">{{trans('common.home_featured')}}</h2>
    <div class="container-fluent">
        <div>
            @foreach($sp as $k => $pb)
            <div>
                <p class="text-center property-home-list-title">{{$k}}</p>
                <div class="slider-home">
                    <ul id="hp-{{str_replace(' ', '', $k)}}">
                        @foreach($pb as $property)
                        <li>
                            @include('web.components.property',['property' => $property])
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
</div>
<script>
  document.addEventListener('DOMContentLoaded', function () {
    var prop = <?php echo isset($sp) ? json_encode($sp) : json_encode([])?> ;
    if (Object.keys(prop).length > 0) {
      for (var i in prop) {
        $('#hp-' + i.replace(' ', '')).lightSlider({
          item: 3,
          loop: false,
          controls: true,
          slideMove: 2,
          pager: false,
          slideMargin: 0,
          speed: 800,
          responsive: [
            {
              breakpoint: 1600,
              settings: {
                item: 3,
                slideMove: 3,
                slideMargin: 0
              }
            },
            {
              breakpoint: 1200,
              settings: {
                item: 3,
                slideMove: 3,
                slideMargin: 0
              }
            },
            {
              breakpoint: 990,
              settings: {
                item: 2,
                slideMove: 2,
                slideMargin: 3
              }
            },
            {
              breakpoint: 580,
              settings: {
                item: 1,
                slideMove: 1
              }
            }
          ]
        });
      }
    }
  })
</script>
@endsection