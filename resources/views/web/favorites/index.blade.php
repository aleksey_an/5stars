@extends('layouts.web')
@section('content')
<div class="container" style="margin-top: 14rem">
    <h3>Favorites page</h3>

    <div class="properties-block">
        @if(isset($properties))
        @foreach($properties as $property)
        @include ('web.components.property',['property' => $property])
        @endforeach
        Add a comment to this line
        @endif
    </div>
</div>
@endsection