@extends('layouts.web')
@section('content')
    @include('web.components.headers.static-pages-header')
    <div class="container static static-success">
        @if ($response)
            <h2>{{trans('common.success_response_ok')}}</h2>
        @else
            <h2 style="color:red;">{{trans('common.success_response_error')}}</h2>
        @endif
    </div>
@endsection