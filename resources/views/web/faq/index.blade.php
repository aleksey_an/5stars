@extends('layouts.web')
@section('meta_title', trans('common.faq_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">
    <h1>FAQ</h1>

    @foreach($faqs as $faq)
        @if(isset($faq->translationInLocale[0]))
            <div class="faq-bloc">
                <h4 class="faq-title">{{$faq->translationInLocale[0]->question}}</h4>
                <div class="faq-content">{!!$faq->translationInLocale[0]->answer!!}</div>
            </div>
        @endif
    @endforeach

</div>
@endsection