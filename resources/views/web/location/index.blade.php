@extends('layouts.web')
@section('content')
@include('web.components.headers.discover-rigion')
<div class="location-page">
    <div class="location-page-slider">
        <div>
            <ul id="slider" style="display: none">
                @if(isset($location->imagesForWeb) && count($location->imagesForWeb) > 0)
                @foreach($location->imagesForWeb as $img)
                <li data-thumb="/images/location/thumbnail/{{$img->name}}">
                    <div class="slider-discover"
                         style="background-image: url('/images/location/display/{{$img->name}}'); width: 100%; background-repeat: no-repeat">
                    </div>
                </li>
                @endforeach
                @else
                <li>
                    <div class="slider-discover"
                         style="background-image: url('/img/default.png'); width: 100%; background-repeat: no-repeat">
                    </div>
                </li>
                @endif
            </ul>
            <button class="location-page-anc" id="go-to-region-details"
                    value="{{trans('common.discover_region_discover')}}"><span>{{trans('common.discover_region_discover')}}</span>
            </button>
        </div>

    </div>
    <div class="container" id="target-region-details">
        @if(isset($location->translationsWithLocale[0]))
        <h1 class="title text-center">{{trans('common.discover_region_welcome')}}
            {{$location->translationsWithLocale[0]->name}}</h1>
        @endif
        @if(isset($location->translationsWithLocale[0]))
        {!! $location->translationsWithLocale[0]->description !!}
        @endif


        @if(isset($location->video_url) && strlen($location->video_url) > 0)
        <div id="player"></div>
        @endif

    </div>
    @if(isset($isShowMap) && $isShowMap)
    @include('web.components.map')
    @endif

    <div class="container locations-properties-list">
        @if(isset($location->translationsWithLocale[0]))
        <h2 class="title text-center">{{trans('common.discover_region_properties_in')}}
            {{$location->translationsWithLocale[0]->name}}</h2>
        @endif
        @include ('web.properties.properties-block')
    </div>


</div>
<script>
  //Init slider

  var player;

  function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      width: '100%',
      videoId: Web.location.videoId,
      events: {
        'onReady': Web.location.onPlayerReady
      }
    });
  }

  document.addEventListener('DOMContentLoaded', function () {
    $('#slider').lightSlider({
      gallery: true,
      item: 1,
      //auto: true,
      loop: true,
      pauseOnHover: true,
      controls: false,
      thumbItem: '<?php echo isset($location->imagesForWeb) ? count($location->imagesForWeb) : 0?>',
      onBeforeStart: function (el) {
        $('#slider').css({'display': 'block'})
      }
    });

    Web.location.videoId = '<?php echo $location->video_url?>';
    Web.location.video();
    Web.property.countFavorites();
    Web.property.markFavoriteProperties();
    Web.property.countFavoritesBeforeUpload = Web.property.getFromLocalStorage('favorites_uploaded');
  });
</script>
@endsection