@if($properties)
<div id="inf-scroll">
    <div class="properties-block ">
        @foreach($properties as $key => $property)
        @include ('web.components.property')
        @endforeach
    </div>
    <div class="pagination-wrapper"> {!! $properties->appends(['search' =>
        Request::get('search')])->render() !!}
    </div>
</div>
<div class="center">
    <img src="/img/ajax-loading-large.gif" alt="" style="display: none" id="pr-img" width="170">
</div>
@endif


<script>

    var infinityScroll = {
        config: {
            selector: '.pagination li.active + li a ',
            addToBlock: '.properties-block',
            paginationWrapper: '.pagination-wrapper'
        },

        nextPage: null,

        init: function () {
            this.setNextPage();
            this.onNext();
            $('.pagination-wrapper').remove();
        },

        setNextPage: function () {
            var a = $('.pagination li.active + li a ');
            if (!this.nextPage && a[0]) {
                this.nextPage = a[0].href;
            }
        },

        requestInProcess: false,

        getProperties: function () {
            var _this = this;
            if (this.nextPage && !this.requestInProcess) {
                $('#pr-img').css({'display': 'block'});
                _this.requestInProcess = true;
                $.ajax({
                    url: this.nextPage,
                    type: 'GET'
                })
                    .done(function (data) {
                        $('#pr-img').css({'display': 'none'});
                        _this.requestInProcess = false;
                        if (data) {
                            if (data.data && data.data.next_page_url) {
                                _this.nextPage = data.data.next_page_url;
                            } else {
                                _this.nextPage = undefined;
                            }
                            _this.addDataToPage(data);
                        } else {
                            _this.nextPage = undefined;
                        }
                    })
                    .fail(function (xhr, status, errorThrown) {
                        _this.requestInProcess = false;
                        $('#pr-img').css({'display': 'none'});
                        console.error(xhr, status);
                    })
            } else {
                return;
            }
        },

        addDataToPage: function (data) {
            for (var i in data) {
                var splitI = i.split('_');
                if (splitI[0] === 'property') {
                    $(this.config.addToBlock).append(data[i]);
                    console.log(data[i])
                }
            }
        },

        onNext: function () {
            var _this = this;
            $(window).scroll(function () {
                if ($(window).scrollTop() == ($(document).height() - $(window).height())) {
                    _this.getProperties()
                }
            });
        }
    };
    document.addEventListener('DOMContentLoaded', function () {
        infinityScroll.init();
//     Web.property.initInfinityScroll()
    });
</script>
