@extends('layouts.web')
@section('meta_title', isset($propertyTranslation->subject) ? $propertyTranslation->subject : '')
@section('content')
@include('web.components.headers.property')

{{--
<div id="video-block" style="display: none" class="video-bl">
    <button onclick="Web.property.hidVideo()"><span>Hide Player</span></button>
    <div id="player"></div>
</div>--}}
<div class="property">
    <div class="prop__top">
        <div class="container">
            @if(isset($urlPath))
            <div class="breadcrumb">
                <a href="{{url( LaravelLocalization::getCurrentLocale())}}"
                   title="{{trans('common.bread_crumbs_home')}}">{{trans('common.bread_crumbs_home')}}</a>
                @foreach($urlPath as $key => $path)
                @if(isset($path['url']))
                <span> / </span>
                <a href="{{url( LaravelLocalization::getCurrentLocale() . $path['url'])}}" title="{{$path['name']}}">{{$path['name']}}</a>
                @endif
                @endforeach
            </div>
            @endif

            <p class="reference"><span>{{ trans('common.property_details_ref') }} </span>{{$property->code}}</p>
        </div>
    </div>
    <div class="container">
        <h1 class="title property-title text-center">
            {{ isset($propertyTranslation->subject) ? $propertyTranslation->subject : ''}}
        </h1>

        <div style="z-index:11;" class="property-slider-top">
            <ul id="slider" style="display: none">
                @if(isset($images) && count($images) > 0)
                @foreach($images as $img)
                <li>
                    <div class="property-slider"
                         style="background-image: url('/images/property/display/{{$img->name}}'); width: 100%; height: 30em; background-repeat: no-repeat">
                    </div>
                </li>
                @endforeach
                @else
                <li>
                    <div class="property-slider"
                         style="background-image: url('/img/default.png'); width: 100%; height: 30em; background-repeat: no-repeat">
                    </div>
                </li>
                @endif
            </ul>
            <div class="property-price-block">
                <div class="property-price-result">
                    <p>
                        <span id="price-result-sign"></span>
                        <span id="price-result">0</span>
                    </p>
                </div>
                <div class="property-price-currency">
                    @if(isset($currencies))
                    <select class="selectpicker" onchange="Web.property.changeCurrency(this.value)">
                        @foreach($currencies as $currency)
                        <option value="{{$currency->rate . '-' . $currency->sign}}">
                            {{$currency->code}}
                        </option>
                        @endforeach
                    </select>
                    @foreach($currencies as $currency)
                    @if($currency->is_default)
                    <input type="hidden" value="{{$currency->rate . '-' . $currency->sign}}" id="default-currency">
                    @endif
                    @endforeach
                    @endif

                    {{--if need show property-price-type - must remove this block--}}
                    @if(isset($property->sale_price) && +$property->sale_price > 0)
                    <input type="hidden" value="{{$property->sale_price}}" id="default-currency-price">
                    @endif
                    @if(isset($property->sale_price) && !+$property->sale_price > 0)
                    <input type="hidden" value="{{$property->rent_month}}" id="default-currency-price">
                    @endif
                    {{--this block--}}

                </div>
                {{--
                <div class="property-price-type">--}}
                    {{--<select class="selectpicker" onchange="Web.property.changePrice(this.value)">
                        @if(isset($property->sale_price) && +$property->sale_price > 0)
                        <option value="{{$property->sale_price}}">
                            {{trans('common.price_block_price')}}
                        </option>
                        @endif

                        @if(isset($property->rent_month) && +$property->rent_month > 0)
                        <option value="{{$property->rent_month}}">
                            {{trans('common.price_block_rent_by_month')}}
                        </option>
                        @endif

                        @if(isset($property->rent_week) && +$property->rent_week > 0)
                        <option value="{{$property->rent_week}}">
                            {{trans('common.price_block_rent_by_week')}}
                        </option>
                        @endif

                        @if(isset($property->rent_day) && +$property->rent_day > 0)
                        <option value="{{$property->rent_day}}">
                            {{trans('common.price_block_rent_by_day')}}
                        </option>
                        @endif
                    </select>--}}
                    {{--@if(isset($property->sale_price) && +$property->sale_price > 0)
                    <input type="hidden" value="{{$property->sale_price}}" id="default-currency-price">
                    @endif
                    @if(isset($property->sale_price) && !+$property->sale_price > 0)
                    <input type="hidden" value="{{$property->rent_month}}" id="default-currency-price">
                    @endif--}}
                    {{--
                </div>
                --}}
                {{-- @if(isset($property->youtube_url) && strlen($property->youtube_url) > 0)
                <div class="video-btn">
                    <button onclick="Web.property.showVideo()">--}}{{--Youtube--}}{{--</button>
                </div>
                @endif--}}
            </div>
        </div>

        @if(isset($property->youtube_url) && strlen($property->youtube_url) > 0)
           <div id="video-block" class="video-bl">
               <div id="player"></div>
           </div>
        @endif

        <div class="property-description">
            {{--<h3>Description:</h3>--}}

            <p>{!! isset($propertyTranslation->detail) ? $propertyTranslation->detail : ''!!}</p>
        </div>

        <div class="_property">
            <div class="property-information">
                <div class="property-information-item">
                    <p>{{trans('common.property_details_location')}}</p>
                    <span class="property-information--name">{{$locationName}}</span>
                </div>

                <div class="property-information-item">
                    <p>{{trans('common.property_details_beds')}}</p>
                    <span>{{$property->bedroom}}</span>
                </div>

                <div class="property-information-item">
                    <p>{{trans('common.property_details_bath')}}</p>
                    <span>{{$property->bathroom}}</span>
                </div>

                <div class="property-information-item">
                    <p>{{trans('common.property_details_sqft')}}</p>
                    <span>{{$property->indoor_area}} m&sup2;</span>
                </div>

                <div class="property-information-item">
                    <p>{{trans('common.property_details_furnished')}} </p>
                    <span>{{$property->furnished ? trans('common.property_details_full_furnished') : trans('common.property_details_not_furnished')}}</span>
                </div>
            </div>
            @if($facilities)
            <div class="property-facilities">
                <h2 class="title">{{trans('common.property_details_facilities')}}</h2>

                {{--<p class="arrow-facil"></p>--}}
                {{--
                <ul>--}}
                    <div class="property-facilities-item">
                        @foreach($facilities as $facility)
                        <div class="property-information-item"><img width="25"
                                                                    src="/images/facility/{{$facility['image']}}"
                                                                    alt="{{$facility['name']}}">{{$facility['name']}}
                        </div>
                        @endforeach
                    </div>
                    {{--
                </ul>
                --}}
            </div>
            @endif

        </div>
    </div>

    <!--    Form contact the agent  -->
    <h3 class="text-center">{{ trans('common.property_contact_us_title') }}</h3>
    <div class="container form-main__" id="form-target">
        <div class="form-main">
            <div class="form-item">

                {{--
                <div class="form-main-header">
                    <div class="contact-info">
                        @if(isset($contacts['company_phone']))
                        <p class="contact-info_tel"><a href="tel:{{$contacts['company_phone']}}"
                                                       title="{{$contacts['company_phone']}}">{{$contacts['company_phone']}}</a>
                        </p>
                        @endif
                        @if(isset($contacts['company_line']))
                        <p class="contact-info_line">{{$contacts['company_line']}}</p>
                        @endif
                        @if(isset($contacts['company_wechat']))
                        <p class="contact-info_viber">{{$contacts['company_wechat']}}</p>
                        @endif
                        @if(isset($contacts['company_whatsapp']))
                        <p class="contact-info_whatsapp"><a
                                    href="https://api.whatsapp.com/send?phone={{$contacts['company_whatsapp']}}"
                                    title="{{$contacts['company_whatsapp']}}">{{$contacts['company_whatsapp']}}</a></p>
                        @endif
                    </div>
                </div>
                --}}

                <p class="text-center"><span>{{ trans('common.property_details_property_ref') }} </span>{{$property->code}}
                </p>

                <form id="__vtigerWebForm" name="Callback form"
                      action="https://vtiger.vidax.com/modules/Webforms/capture.php" method="post" accept-charset="utf-8"
                      enctype="multipart/form-data">
                    <input type="hidden" name="__vtrftk"
                           value="sid:de1bb25a95071b47a8b38320c9d2b196580ceda6,1515161355">
                    <input type="hidden" name="publicid" value="73d8c7fb54694d162fbb678eaed21604">
                    <input type="hidden" name="urlencodeenable" value="1">
                    <input type="hidden" name="name" value="Callback form">

                    <div class="form-item__input">
                        <div>
                            <input id="form-name"
                                   type="text"
                                   name="firstname"
                                   class="label_better"
                                   placeholder="{{trans('common.form_contact_us_first_name')}}"
                                   data-label="" value="" required="">
                        </div>
                        <div>
                            <input id="form-name"
                                   type="text"
                                   name="lastname"
                                   class="label_better"
                                   placeholder="{{trans('common.form_contact_us_name')}}"
                                   data-label="" value="" required="">
                        </div>
                        <div>
                            <input id="form-email"
                                   type="email"
                                   name="email"
                                   class="label_better"
                                   placeholder="{{trans('common.form_contact_us_email')}}"
                                   data-label="" value="" required="">
                        </div>
                        <div class="form-item__input--phone">
                            <input id="form-phone"
                                   type="text"
                                   name="phone"
                                   class="label_better"
                                   placeholder="{{trans('common.form_contact_us_phone')}}"
                                   data-label=""
                                   value=""
                                   required="">
                        </div>
                        @if($property->to_rent && ($searchType == "to_rent"))
                        <div>
                            <label for="date-picker">{{trans('common.form_contact_agent_rent_date')}}</label>
                            <input id="date-picker"
                                   type="text"
                                   name="cf_864"
                                   data-label=""
                                   value="">
                        </div>
                        @endif
                    </div>
                    <div class="form-item__txt form-captch">
                        <div class="textarea-form">
                            <textarea id="form-message"
                                      name="cf_866"
                                      class="label_better"
                                      placeholder="{{trans('common.form_contact_us_message')}}"
                                      required=""></textarea>
                        </div>
                        <div class="footer-form">
                            <div class="g-recaptcha" data-sitekey="{{ env("RECAPTCHA_KEY") }}"></div>
                            <input type="hidden" name="recaptcha-url" id="recaptcha-url" data-url="{{ route('check-recaptcha') }}">
                            <p class="send">
                                <input type="hidden" name="cf_854" data-label="" value="">
                                <input type="hidden" name="leadsource" data-label="" value="Web Site">
                                <input type="hidden" name="cf_858" data-label="" value="{{$property->to_rent ? " Rent" :
                            "Buy"}}">
                                <textarea name="cf_868" hidden="">Property Ref: {{$property->code}}&#13;&#10;Property name: {{ isset($propertyTranslation->subject) ? $propertyTranslation->subject : ''}}</textarea>
                                <button onclick="sendForm()" id="send-btn" value="{{trans('common.header_home_contact_us')}}">
                                    {{trans('common.header_home_contact_us')}}
                                </button>
                                <img src="/img/form-preloader.gif" alt="" style="display: none" id="form-preloader">
                            </p>
                        </div>


                        {{--
                        <script type="text/javascript">var RecaptchaOptions = {theme: "clean"};</script>
                        <script type="text/javascript"
                                src="http://www.google.com/recaptcha/api/challenge?k=6Lchg-wSAAAAAIkV51_LSksz6fFdD2vgy59jwa38"></script>
                        <noscript>
                            <iframe src="http://www.google.com/recaptcha/api/noscript?k=6Lchg-wSAAAAAIkV51_LSksz6fFdD2vgy59jwa38"
                                    height="300" width="500" frameborder="0"></iframe>
                            <br><textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea><input
                                    type="hidden" name="recaptcha_response_field" value="manual_challenge"></noscript>
                        --}}
                    </div>
                </form>
                <script type="text/javascript">window.onload = function () {
                    var N = navigator.appName, ua = navigator.userAgent, tem;
                    var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
                    if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
                    M = M ? [M[1], M[2]] : [N, navigator.appVersion, "-?"];
                    var browserName = M[0];
                    var form = document.getElementById("__vtigerWebForm"), inputs = form.elements;
                    form.onsubmit = function () {
                      var required = [], att, val;
                      for (var i = 0; i < inputs.length; i++) {
                        att = inputs[i].getAttribute("required");
                        val = inputs[i].value;
                        type = inputs[i].type;
                        if (type == "email") {
                          if (val != "") {
                            var elemLabel = inputs[i].getAttribute("label");
                            var emailFilter = /^[_/a-zA-Z0-9]+([!"#$%&()*+,./:;<=>?\^_`{|}~-]?[a-zA-Z0-9/_/-])*@[a-zA-Z0-9]+([\_\-\.]?[a-zA-Z0-9]+)*\.([\-\_]?[a-zA-Z0-9])+(\.?[a-zA-Z0-9]+)?$/;
                            var illegalChars = /[\(\)\<\>\,\;\:\"\[\]]/;
                            if (!emailFilter.test(val)) {
                              alert("For " + elemLabel + " field please enter valid email address");
                              return false;
                            } else if (val.match(illegalChars)) {
                              alert(elemLabel + " field contains illegal characters");
                              return false;
                            }
                          }
                        }
                        if (att != null) {
                          if (val.replace(/^\s+|\s+$/g, "") == "") {
                            required.push(inputs[i].getAttribute("label"));
                          }
                        }
                      }
                      if (required.length > 0) {
                        alert("The following fields are required: " + required.join());
                        return false;
                      }
                      var numberTypeInputs = document.querySelectorAll("input[type=number]");
                      for (var i = 0; i < numberTypeInputs.length; i++) {
                        val = numberTypeInputs[i].value;
                        var elemLabel = numberTypeInputs[i].getAttribute("label");
                        var elemDataType = numberTypeInputs[i].getAttribute("datatype");
                        if (val != "") {
                          if (elemDataType == "double") {
                            var numRegex = /^[+-]?\d+(\.\d+)?$/;
                          } else {
                            var numRegex = /^[+-]?\d+$/;
                          }
                          if (!numRegex.test(val)) {
                            alert("For " + elemLabel + " field please enter valid number");
                            return false;
                          }
                        }
                      }
                      var dateTypeInputs = document.querySelectorAll("input[type=date]");
                      for (var i = 0; i < dateTypeInputs.length; i++) {
                        dateVal = dateTypeInputs[i].value;
                        var elemLabel = dateTypeInputs[i].getAttribute("label");
                        if (dateVal != "") {
                          var dateRegex = /^[1-9][0-9]{3}-(0[1-9]|1[0-2]|[1-9]{1})-(0[1-9]|[1-2][0-9]|3[0-1]|[1-9]{1})$/;
                          if (!dateRegex.test(dateVal)) {
                            alert("For " + elemLabel + " field please enter valid date in required format");
                            return false;
                          }
                        }
                      }
                    };
                  }</script>
                {{--
                <form id="form-contact-agent" onkeypress="if(event.keyCode == 13) return false;">--}}
                    {{--<input type="hidden" value="{{$property->code}}" id="property-code">--}}
                    {{--<input type="hidden" value="{{$property->location_id}}" id="form-location-id">--}}

                    {{--
                    <div class="form-item__input">--}}
                        {{--
                        <div>--}}
                            {{--<input id="form-name" --}}
                                       {{--type="text" --}}
                                       {{--name="formName" --}}
                                       {{--class="label_better" --}}
                                       {{--placeholder="{{trans('common.form_contact_us_name')}}">--}}
                            {{--
                        </div>
                        --}}
                        {{--
                        <div>--}}
                            {{--<input id="form-email" --}}
                                       {{--type="text" --}}
                                       {{--name="formEmail" --}}
                                       {{--class="label_better" --}}
                                       {{--placeholder="{{trans('common.form_contact_us_email')}}">--}}
                            {{--
                        </div>
                        --}}
                        {{--
                        <div class="form-item__input--phone">--}}
                            {{--<input id="form-phone" --}}
                                       {{--type="text" --}}
                                       {{--name="formPhone" --}}
                                       {{--class="label_better" --}}
                                       {{--placeholder="{{trans('common.form_contact_us_phone')}}">--}}
                            {{--
                        </div>
                        --}}
                        {{--@if($property->to_rent)--}}
                        {{--
                        <div>--}}
                            {{--<label for="date-picker">{{trans('common.form_contact_agent_rent_date')}}</label>--}}
                            {{--<input id="date-picker" --}}
                                       {{--type="text">--}}
                            {{--
                        </div>
                        --}}
                        {{--@endif--}}
                        {{--
                    </div>
                    --}}
                    {{--
                    <div class="form-item__txt">--}}
                        {{--
                        <div>--}}
                            {{--<textarea id="form-message" --}}
                                          {{--name="formMessage" --}}
                                          {{--class="label_better" --}}
                                          {{--placeholder="{{trans('common.form_contact_us_message')}}"></textarea>--}}
                            {{--
                        </div>
                        --}}
                        {{--
                        <p class="send">--}}
                            {{--
                            <button onclick="Web.form.sendFormToAgent()" id="send-btn">Send</button>
                            --}}
                            {{--<img src="/img/form-preloader.gif" alt="" style="display: none" id="form-preloader">--}}
                            {{--
                        </p>
                        --}}
                        {{--
                    </div>
                    --}}
                    {{--
                </form>
                --}}
                <div class="form-popup" id="form-popup" style="display: none">
                    <div>
                        <p id="form-popup-content"></p>
                        <button onclick="$('#form-popup').css({'display': 'none'})" value="Ok">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        @if(isset($isShowMap) && $isShowMap)
        @include('web.components.map')
        @endif
    </div>
    <!--viewed-properties-->
    <div class="container">
        <div id="viewed-properties" class="properties-view">
            <h3>{{ trans('common.property_details_viewed_properties') }}</h3>
        </div>
    </div>

</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    function sendForm() {
        event.preventDefault();
        var form = $('#__vtigerWebForm');
        var formData = form.serialize();
        var recaptchaUrl = $('#recaptcha-url').attr('data-url');

        $.ajax({
            url: recaptchaUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Access-Control-Allow-Credentials': true
            },
            type: 'post',
            data: formData,
            success: function (data) {
                console.log(data);
                if(data.success){
                    form.submit();
                }
            }
        });
        return false;
    }
</script>
<script>
  document.addEventListener('DOMContentLoaded', function () {
    //Init slider
    $('#slider').lightSlider({
      item: 1,
      //auto: true,
      //pauseOnHover: true,
      loop: true,
      slideMove: 1,
      pager: true,
      controls: true,
      speed: 800,
      onBeforeStart: function (el) {
        $('#slider').css({'display': 'block'})
      }
    });
    $("input.label_better").label_better({
      position: "top",
      animationTime: 150,
      easing: "ease-in-out",
      offset: 5,
      hidePlaceholderOnFocus: true
    });
    $("textarea.label_better").label_better({
      position: "top",
      animationTime: 150,
      easing: "ease-in-out",
      offset: 10,
      hidePlaceholderOnFocus: true
    });
    // Init Date Picker on property page
    $('#date-picker').daterangepicker({
      locale: {
        format: 'MM.DD.YYYY'
      }
    }, function (from, to) {
      $('#form-date-from').val(from.format('MM.DD.YYYY'));
      $('#form-date-to').val(to.format('MM.DD.YYYY'));
    });

    //Phone number in contact us form
    var telInput = $("#form-phone");
    telInput.intlTelInput({
      initialCountry: "<?php echo LaravelLocalization::getCurrentLocale() === 'en' ? 'gb' : LaravelLocalization::getCurrentLocale() ?>",
    });

    telInput.on('focus', function () {
      $(this).parents( "div" ).addClass('lb_wrap_line');
    });
    telInput.on('focusout', function () {
      $(this).parents( "div" ).removeClass('lb_wrap_line');
    });

    var isClicked = false;
    var currentDialCode = '';
    telInput.on('click', function () {
      var countryData = telInput.intlTelInput("getSelectedCountryData");
      if (!isClicked) {
        telInput.val('+' + countryData.dialCode + ' ');
        currentDialCode = countryData.dialCode;
      }
      isClicked = true;
    });

    telInput.on('countrychange', function(e, countryData) {
      if(countryData.dialCode && currentDialCode !== countryData.dialCode) {
        currentDialCode = countryData.dialCode;
        telInput.val('+' + countryData.dialCode + ' ');
      }
      isClicked = true;
    });

    $('#viewed-properties').hide();
    Web.property.propertyId = +'<?php echo $property->id ?>';
    Web.property.propertiesIds = '<?php echo "[" . $property->id . "]"?>';
    Web.property.savePropertyAsViewed();
    Web.property.markFavoriteProperties();
    //Create price
    Web.property.changeCurrency($('#default-currency').val());
    Web.property.changePrice($('#default-currency-price').val());
    //Create video
    Web.property.videoId = '<?php echo $property->youtube_url?>';
    Web.property.video();
    Web.main.setInputLine()

    $(document).ready(function () {
      var favorite_property = localStorage.getItem('favorite_property');
      var viewed_property = localStorage.getItem('viewed_property');
      $('input[name="cf_854"]').val('{{url()->to(' / admin / show - guest - property ')}}' +
      '?property={{$property->id}}' + (favorite_property ? '&favorites=' + favorite_property : '') + (viewed_property ? '&viewed=' + viewed_property : '')
    )
      ;
    });
  });


</script>
<script>
  var player;

  function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      width: '100%',
      videoId: Web.property.videoId,
      events: {
        'onReady': Web.property.onPlayerReady
      }
    });
  }

</script>
@endsection