<footer class="footer">
    <div class="container-fluent footer-main">
        <ul>
            <li>{{trans('common.footer_menu')}}</li>
            <li><a href="/{{\App::getLocale()}}/{{trans('url.to_sale')}}" title="{{trans('common.header_home_sale')}}">{{trans('common.header_home_sale')}}</a>
            </li>
            <li><a href="/{{\App::getLocale()}}/{{trans('url.to_rent')}}" title="{{trans('common.header_home_to_rent')}}">{{trans('common.header_home_to_rent')}}</a>
            </li>
            <li><a href="/{{\App::getLocale()}}/blog" title="{{trans('common.header_home_blog')}}">{{trans('common.header_home_blog')}}</a>
            </li>
            <li><a href="/{{\App::getLocale()}}/faq" title="{{trans('common.header_home_faq')}}">{{trans('common.header_home_faq')}}</a></li>
            {{--
            <li><a href="/who-we-are">{{trans('common.header_home_who_we_are')}}</a></li>
            --}}
        </ul>
        <ul>
            <li>{{trans('common.footer_useful_links')}}</li>
            <li><a href="/{{\App::getLocale()}}/about-us" title="{{trans('common.footer_about_us')}}">{{trans('common.footer_about_us')}}</a>
            </li>
            <li><a href="/{{\App::getLocale()}}/links" title="{{trans('common.footer_links')}}">{{trans('common.footer_links')}}</a></li>
            <li><a href="/{{\App::getLocale()}}/our-services" title="{{trans('common.footer_our_services')}}">{{trans('common.footer_our_services')}}</a>
            </li>
            <li><a href="/{{\App::getLocale()}}/agencies-in-thailand" title="{{trans('common.footer_our_agencies')}}">{{trans('common.footer_our_agencies')}}</a>
            </li>
        </ul>
        <ul class="footer-contact">
            <li>{{trans('common.header_home_contact_us')}}</li>
            @if(isset($contacts['company_phone']))
            <li><a href="tel:{{$contacts['company_phone']}}" title="{{$contacts['company_phone']}}"><img
                            src="/img/footer-telephone.svg" alt="telephone">{{$contacts['company_phone']}}</a></li>
            @endif
            {{--@if(isset($contacts['company_email']))--}}
            {{--<li><img src="/img/footer-email.svg" alt="email">{{$contacts['company_email']}}</li>--}}
            {{--@endif--}}
            @if(isset($contacts['company_skype']))
            <li><a href="skype:{{$contacts['company_skype']}}" title="{{$contacts['company_skype']}}"><img
                            src="/img/footer-skype.svg" alt="skype">{{$contacts['company_skype']}}</a></li>
            @endif
            @if(isset($contacts['company_address']))
            <li class="footer-pin"><img src="/img/footer-pin.svg" alt="pinterest">{!!$contacts['company_address']!!}
            </li>
            @endif
        </ul>
        <ul>
            <li>{{trans('common.footer_newsletter')}}</li>
            <li>
                <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
                <form id="__vtigerWebForm" name="Newsletter Form"
                      action="https://vtiger.vidax.com/modules/Webforms/capture.php" method="post"
                      accept-charset="utf-8" enctype="multipart/form-data">
                    <span id="form-footer-thank-message" class="thank text-center fadeInUp" style="display: none;">
                        {{trans('common.form_footer_thank_message')}}
                    </span>
                    <input type="hidden" name="__vtrftk"
                           value="sid:d4da66b6db389cc4bd3172d42393e650d49e623f,1533559253">
                    <input type="hidden" name="publicid" value="dc013578c9c1e968cfed2d3287604873">
                    <input type="hidden" name="urlencodeenable" value="1">
                    <input type="hidden" name="name" value="Newsletter Form">
                    <label>
                        <input type="email"
                               name="accountname"
                               id="news-latter-email"
                               placeholder="{{trans('common.form_contact_us_email')}}"
                               data-label=""
                               value=""
                               required="">
                    </label>

                    <p class="send">
                        <button type="button"
                                id="send-news-later"
                                onclick="Web.form.newsLatter()" value="Send Form"></button>

                        <img src="/img/form-preloader.gif" alt="preloader" style="display: none" width="60"
                             id="form-preloader-news-later">
                    </p>
                    <script type="text/javascript">window.onload = function () {
                        var N = navigator.appName, ua = navigator.userAgent, tem;
                        var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
                        if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
                        M = M ? [M[1], M[2]] : [N, navigator.appVersion, "-?"];
                        var browserName = M[0];
                        var form = document.getElementById("__vtigerWebForm"), inputs = form.elements;
                        form.onsubmit = function () {
                          var required = [], att, val;
                          for (var i = 0; i < inputs.length; i++) {
                            att = inputs[i].getAttribute("required");
                            val = inputs[i].value;
                            type = inputs[i].type;
                            if (type == "email") {
                              if (val != "") {
                                var elemLabel = inputs[i].getAttribute("label");
                                var emailFilter = /^[_/a-zA-Z0-9]+([!"#$%&()*+,./:;<=>?\^_`{|}~-]?[a-zA-Z0-9/_/-])*@[a-zA-Z0-9]+([\_\-\.]?[a-zA-Z0-9]+)*\.([\-\_]?[a-zA-Z0-9])+(\.?[a-zA-Z0-9]+)?$/;
                                var illegalChars = /[\(\)\<\>\,\;\:\"\[\]]/;
                                if (!emailFilter.test(val)) {
                                  alert("For " + elemLabel + " field please enter valid email address");
                                  return false;
                                } else if (val.match(illegalChars)) {
                                  alert(elemLabel + " field contains illegal characters");
                                  return false;
                                }
                              }
                            }
                            if (att != null) {
                              if (val.replace(/^\s+|\s+$/g, "") == "") {
                                required.push(inputs[i].getAttribute("label"));
                              }
                            }
                          }
                          if (required.length > 0) {
                            alert("The following fields are required: " + required.join());
                            return false;
                          }
                          var numberTypeInputs = document.querySelectorAll("input[type=number]");
                          for (var i = 0; i < numberTypeInputs.length; i++) {
                            val = numberTypeInputs[i].value;
                            var elemLabel = numberTypeInputs[i].getAttribute("label");
                            var elemDataType = numberTypeInputs[i].getAttribute("datatype");
                            if (val != "") {
                              if (elemDataType == "double") {
                                var numRegex = /^[+-]?\d+(\.\d+)?$/;
                              } else {
                                var numRegex = /^[+-]?\d+$/;
                              }
                              if (!numRegex.test(val)) {
                                alert("For " + elemLabel + " field please enter valid number");
                                return false;
                              }
                            }
                          }
                          var dateTypeInputs = document.querySelectorAll("input[type=date]");
                          for (var i = 0; i < dateTypeInputs.length; i++) {
                            dateVal = dateTypeInputs[i].value;
                            var elemLabel = dateTypeInputs[i].getAttribute("label");
                            if (dateVal != "") {
                              var dateRegex = /^[1-9][0-9]{3}-(0[1-9]|1[0-2]|[1-9]{1})-(0[1-9]|[1-2][0-9]|3[0-1]|[1-9]{1})$/;
                              if (!dateRegex.test(dateVal)) {
                                alert("For " + elemLabel + " field please enter valid date in required format");
                                return false;
                              }
                            }
                          }
                        };
                      }</script>

                </form>
            </li>
            <li>
                <div class="social">
                    <a href="https://www.facebook.com/FiveStarsThailandRealEstate/" class="social-fk"
                       title="facebook"></a>
                    <a href="" class="social-in" title="linkedin"></a>
                    <a href="" class="social-gl" title="google"></a>
                </div>
            </li>
        </ul>
    </div>
    <div class="container-fluent footer-bottom">
        <p>&copy; Copyright 2017 - Five Stars Thailand Real Estate, Thailand</p>
        <ul>
            <li><a href="" title="Privacy Policy">Privacy Policy</a></li>
            <li><a href="" title="Terms of Use">Terms of Use</a></li>
            <li><a href="" title="Disclaimer">Disclaimer</a></li>
        </ul>
    </div>

</footer>