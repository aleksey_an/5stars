<?php // var_dump($property->to_sale, $property->is_sold) ?>
<div class="property-block">
    <div class="property-block-main">

        <div class="property-block-top">

            @if(isset($searchType))
           <!-- <div class="info_rent-sale">
                @if($searchType == 'to_rent')
                <p>{{--trans('common.component_property_rent')--}}</p>
                @endif
                @if($searchType == 'to_sale')
                <p>{{--trans('common.component_property_sale')--}}</p>
                @endif
            </div>-->

            {{--<div class="property-block-top__bottom">
                @if($searchType == 'to_rent')
                @if($property->rent_month > 0)
                <p class="property-block-price">{{number_format($property->rent_month)}}<span> THB/{{trans('common.component_property_month')}}</span></p>
                @endif
                @endif
                @if($searchType == 'to_sale')
                @if($property->sale_price > 0)
                <p><span>{{trans('common.component_property_price')}}: </span> {{number_format($property->sale_price)}}<span> THB </span></p>
                @endif
                @endif
            </div>--}}
            @else
            <!--<div class="info_rent-sale">
                @if($property->to_rent)
                <p>{{trans('common.component_property_rent')}}</p>
                @endif

                @if($property->to_sale)
                <p>{{trans('common.component_property_sale')}}</p>
                @endif
            </div>-->
            {{--<div class="property-block-top__bottom">
                @if($property->rent_month > 0)
                <p class="property-block-price">{{number_format($property->rent_month)}}<span> THB/{{trans('common.component_property_month')}}</span></p>
                @endif
                @if($property->sale_price > 0)
                <p><span>{{trans('common.component_property_price')}}: </span> {{number_format($property->sale_price)}}<span> THB </span></p>
                @endif
            </div>--}}
            @endif

            <div onclick="Web.property.addPropertyToFavorite(+'{{$property->id}}')" class="favorit-icon {{$property->isFavorite ? 'favorit-icon-select' : 'favorit-icon-no_select'}} fi-{{$property->id}}">
                <span></span>
            </div>

            <a onclick="Web.main.deleteCookie('search'); window.open('{{url( LaravelLocalization::getCurrentLocale() . $property->propertyHref)}}', '_blank')" title="{{ (isset($property->imageForWebSearch[0]) && $property->imageForWebSearch[0]['detail']) ? $property->imageForWebSearch[0]['detail'] : '' }}">
                @if(isset($property->imageForWebSearch[0]))
                <img id="img-{{$property->id}}" src="/images/property/small/{{$property->imageForWebSearch[0]->name}}" alt="{{ $property->imageForWebSearch[0]['detail'] ? $property->imageForWebSearch[0]['detail'] : '' }}" onerror="imgError('img-{{$property->id}}')">
                @else
                <img src="/img/default.png" alt="Default">
                @endif

                @if(isset($property) && $property->to_rent && $property->is_rent)
                <span class="sold-property">
                    <img src="/img/property-rented.png" alt="Rented"/>
                </span>
                @endif
                @if(isset($property) && $property->to_sale && $property->is_sold)
                <span class="sold-property sold">
                    <img src="/img/property-sold.png" alt="Sold"/>
                </span>
                @endif
            </a>

        </div>

        <div class="property-block-bottom">

            <!--{{--@if(isset($property->categoryNameInLocale[0]))--}}
            {{--<p><span>Category:</span> {{$property->categoryNameInLocale[0]->name}}</p>--}}
            {{--@endif--}}-->

            <div class="property-block-bottom__">
                @if(isset($property->locationNameInLocale[0]))
                <p class="property-block-bottom__loc"><span><img src="/img/location-icon.svg" alt="{{trans('common.component_property_location')}}">{{--{{trans('common.component_property_location')}}--}}</span> {{$property->locationNameInLocale[0]->name}}</p>
                @endif
                <p class="property-block-bottom__beds"><span><img src="/img/bed.svg" alt="{{$property->bedroom}}">{{--Beds--}} </span> {{$property->bedroom}} </p>

                {{--<div class="property-block-top__bottom">--}}
                    @if($property->rent_month > 0)
                        <p class="property-block-price property-block-top__bottom">{{number_format($property->rent_month)}} <span> THB/{{trans('common.component_property_month')}}</span></p>
                    @endif
                    @if($property->sale_price > 0)
                        <p>{{number_format($property->sale_price)}}<span> THB </span></p>
                    @endif
                {{--</div>--}}

                {{--<p class="property-block-bottom__sqft"><span><img src="/img/sqft.svg" alt="{{$property->indoor_area}}">--}}{{--Sqft--}}{{-- </span> {{$property->indoor_area}} </p>--}}
            </div>

            @if(isset($property->translationsInLocale[0]))
            <p class="property-block-name title">{{substr($property->translationsInLocale[0]->subject, 0, 50)}}
                {{strlen($property->translationsInLocale[0]->subject) > 50 ? ' ...' : ''}}</p>
            @endif

            <!--<div class="descrip-bottom">
                {{--@if(isset($property->translationsInLocale[0]))--}}
                <p>{{--substr($property->translationsInLocale[0]->detail, 0, 100)--}}
                    {{--strlen($property->translationsInLocale[0]->detail) > 100 ? ' ...' : ''--}}</p>
                {{--@endif--}}
            </div>-->

        </div>
    </div>
</div>

<script>
   function imgError(id){
       if(document.getElementById(id)){
           document.getElementById(id).src = '/img/default.png';
       }
   }
</script>