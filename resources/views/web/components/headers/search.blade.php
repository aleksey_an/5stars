<div id="favorites-list-main" class="favorites-list-main">

    <div class="favorites-list-main-top">
        <p onclick="Web.property.createFavoritesList()" class="favorites-account favorites-account-f-s ">
            {{trans('common.header_favorite')}} (
            <span class="favorites-count">{{isset($favoriteProperties) ? count($favoriteProperties) : ''}}</span>
            )
        </p>
        <span class="close"><img src="/img/close-icon.svg" alt=""></span>
    </div>

    <div class="sharing">
        <p>Sharing options</p>
    </div>

    <div id="favorites-list" class="favorites-list"></div>


</div>
<header class="header">
    <div class="header-logo">
        <a href="/" title="Fivestars thailand real estate">
            <img src="/img/logo_{{\App::getLocale()}}.png" alt="logo_{{\App::getLocale()}}">
        </a>
    </div>
    @include('web.components.search-header')
    <div class="favorites-header">
        <p onclick="Web.property.createFavoritesList()" class="favorites-account favorites-account-f">
            {{trans('common.header_favorite')}} (
            <span class="favorites-count">{{isset($favoriteProperties) ? count($favoriteProperties) : ''}}</span>
            )
        </p>

    </div>

    <div class="burger burger-region-search-header">
        <span></span>
        <span></span>
        <span></span>
    </div>
</header>


<div class="block-back block-back-hide" id="block-back"></div>
