<header class="header header__static">


    <div class="header-logo">
        <a href="/" title="Fivestars thailand real estate">
            <img src="/img/logo_{{\App::getLocale()}}.png" alt="logo_{{\App::getLocale()}}">
        </a>
    </div>

    <div class="menu">
        <ul>
            <li><a href="/" title="{{trans('common.bread_crumbs_home')}}">{{trans('common.bread_crumbs_home')}}</a></li>
            <li><a href="/{{trans('url.to_sale')}}" title="{{trans('common.header_home_sale')}}">{{trans('common.header_home_sale')}}</a></li>
            <li><a href="/{{trans('url.to_rent')}}" title="{{trans('common.header_home_to_rent')}}">{{trans('common.header_home_to_rent')}}</a></li>
            <li><a href="/blog" title="{{trans('common.header_home_blog')}}">{{trans('common.header_home_blog')}}</a></li>
            <li><a href="/faq" title="{{trans('common.header_home_faq')}}">{{trans('common.header_home_faq')}}</a></li>
            {{--<li><a href="/who-we-are">{{trans('common.header_home_who_we_are')}}</a></li>--}}
        </ul>
    </div>
    <div class="header-right">
        <div class="header-lang">
            <div class="header-lang-item">
                <p class="arrow"></p>
                <ul>
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $locale)
                    @if (LaravelLocalization::getCurrentLocale() == $localeCode)
                    <li class="active-lang">
                        @else
                    <li>
                        @endif
                        <a rel="alternate" hreflang="{{ $localeCode }}"
                           href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" title="{{ $localeCode }}">
                            <img
                                    src="/images/language/{{App\Language::where(['name' => $locale['name']])->get()[0]->image}}"
                                    alt="">
                            <span>{{ $localeCode }}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="burger burger-static-pages">
        <span></span>
        <span></span>
        <span></span>
    </div>
</header>