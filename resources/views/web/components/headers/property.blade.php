<div id="favorites-list-main" class="favorites-list-main">

    <div class="favorites-list-main-top">
        <p onclick="Web.property.createFavoritesList()" class="favorites-account favorites-account-f-s ">
            {{trans('common.header_favorite')}} (
            <span class="favorites-count">{{isset($favoriteProperties) ? count($favoriteProperties) : '0'}}</span>
            )
        </p>
        <span class="close"><img src="/img/close-icon.svg" alt=""></span>
    </div>

    <div class="sharing">
        <p>Sharing options</p>
    </div>

    <div id="favorites-list" class="favorites-list"></div>


</div>
<header class="header">
    <div class="header-logo">
        <a href="/" title="Fivestars thailand real estate">
            <img src="/img/logo_{{\App::getLocale()}}.png" alt="">
        </a>
    </div>
    @include('web.components.search-header')
    <div class="header-right header-right-property">
        <div onclick="Web.property.addPropertyToFavorite(+'{{$property->id}}')"
             class="favorit-icon--header {{$property->isFavorite ? 'favorit-icon-select' : 'favorit-icon-no_select'}} fi-{{$property->id}}">
            <span></span>{{trans('common.header_favorite_add')}}
        </div>
        <div class="favorites-header">
            <p onclick="Web.property.createFavoritesList()" class="favorites-account favorites-account-f">
                {{trans('common.header_favorite')}} (
                <span class="favorites-count">{{isset($favoriteProperties) ? count($favoriteProperties) : '0'}}</span>
                )
            </p>

        </div>
    </div>

    <div class="burger burger-property">
        <span></span>
        <span></span>
        <span></span>
    </div>
</header>
