<div id="favorites-list-main" class="favorites-list-main">

    <div class="favorites-list-main-top">
        <p onclick="Web.property.createFavoritesList()" class="favorites-account favorites-account-f-s ">
            {{trans('common.header_favorite')}} (
            <span class="favorites-count">{{isset($favoriteProperties) ? count($favoriteProperties) : ''}}</span>
            )
        </p>
        <span class="close"><img src="/img/close-icon.svg" alt=""></span>
    </div>

    <div class="sharing">
        <p>Sharing options</p>
    </div>

    <div id="favorites-list" class="favorites-list"></div>


</div>

<header class="header">


    <div class="header-logo">
        <a href="/" title="Fivestars thailand real estate">
            <img src="/img/logo_{{\App::getLocale()}}.png" alt="logo_{{\App::getLocale()}}">
        </a>
    </div>

    <div class="menu">
        <ul>
            <li><a href="/" title="{{trans('common.bread_crumbs_home')}}">{{trans('common.bread_crumbs_home')}}</a></li>
            <li><a href="/{{trans('url.to_sale')}}" title="{{trans('common.header_home_sale')}}">{{trans('common.header_home_sale')}}</a></li>
            <li><a href="/{{trans('url.to_rent')}}" title="{{trans('common.header_home_to_rent')}}">{{trans('common.header_home_to_rent')}}</a></li>
            <li><a href="/blog" title="{{trans('common.header_home_blog')}}">{{trans('common.header_home_blog')}}</a></li>
            {{--<li><a href="/who-we-are">{{trans('common.header_home_who_we_are')}}</a></li>--}}
            <!--<li><a href="/">{{trans('common.header_home_faq')}}</a></li>-->
        </ul>
    </div>

    <div class="favorites-header header-favorites-discover">
        <p onclick="Web.property.createFavoritesList()" class="favorites-account favorites-account-f">
            {{trans('common.header_favorite')}} (
            <span class="favorites-count">{{isset($favoriteProperties) ? count($favoriteProperties) : ''}}</span>
            )
        </p>

    </div>

    <div class="burger burger-discover">
        <span></span>
        <span></span>
        <span></span>
    </div>
</header>

<div class="block-back block-back-hide" id="block-back"></div>