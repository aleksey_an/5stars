<header class="header">
    {{--
    <div class="container">--}}
        <div class="header-logo">
            <a href="/" title="Fivestars thailand real estate ">
                <img src="/img/logo_{{\App::getLocale()}}.png" alt="Logo {{\App::getLocale()}}">
            </a>
        </div>

        <div class="menu">
            <ul>
                <li><a href="/{{\App::getLocale()}}" title="{{trans('common.bread_crumbs_home')}}">{{trans('common.bread_crumbs_home')}}</a></li>
                <li><a href="/{{\App::getLocale()}}/{{trans('url.to_sale')}}" title="{{trans('common.header_home_sale')}}">{{trans('common.header_home_sale')}}</a></li>
                <li><a href="/{{\App::getLocale()}}/{{trans('url.to_rent')}}" title="{{trans('common.header_home_to_rent')}}">{{trans('common.header_home_to_rent')}}</a></li>
                <li><a href="/{{\App::getLocale()}}/blog" title="{{trans('common.header_home_blog')}}">{{trans('common.header_home_blog')}}</a></li>
                <li><a href="/{{\App::getLocale()}}/faq" title="{{trans('common.header_home_faq')}}">{{trans('common.header_home_faq')}}</a></li>
                {{--<li><a href="/who-we-are">{{trans('common.header_home_who_we_are')}}</a></li>--}}
            </ul>
        </div>

        <div class="header-right">
{{--            @if(Request::url() == config('app.url') . LaravelLocalization::getCurrentLocale())--}}
            <div class="header-lang">
                <div class="header-lang-item">
                    <ul>
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $locale)
                        @if (LaravelLocalization::getCurrentLocale() == $localeCode)
                        <li class="active-lang">
                            @else
                        <li>
                            @endif
                            <a rel="alternate" hreflang="{{ $localeCode }}"
                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" title="{{ $localeCode }}">
                                <img
                                        src="/images/language/{{App\Language::where(['name' => $locale['name']])->get()[0]->image}}"
                                        alt="Languege {{\App::getLocale()}}">
                                <span>{{ $localeCode }}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    <p class="arrow"></p>
                </div>
            </div>
{{--            @endif--}}

            <div class="header-home-contact">

                <div class="header-home-contact-i">
                    <p class="contact-btn">{{trans('common.header_home_contact_us')}}</p>
                </div>

                <div class="form-contact-us form-contact-us-hide">

                    <div class="form-contact-us-top">
                        <p class="contact-btn-i">{{trans('common.header_home_contact_us')}}</p>

                        <div class="close"><img src="/img/close-icon-c.svg" alt="Close contact"></div>
                    </div>

                    <div class="form-contact-us-bottom">
                        <div class="form-contact-us-bottom-item">

                            <div>
                                {{--<form id="form-home-contact-us" class="form-contact-us__"--}}
                                <form id="__vtigerWebForm" class="form-contact-us__" name="Callback form" action="https://vtiger.vidax.com/modules/Webforms/capture.php" method="post" accept-charset="utf-8" enctype="multipart/form-data"
                                      onkeypress="if(event.keyCode == 13) return false;">
                                    <input type="hidden" name="__vtrftk" value="sid:de1bb25a95071b47a8b38320c9d2b196580ceda6,1515161355">
                                    <input type="hidden" name="publicid" value="73d8c7fb54694d162fbb678eaed21604">
                                    <input type="hidden" name="urlencodeenable" value="1">
                                    <input type="hidden" name="name" value="Callback form">

                                    <div>
                                        <input id="form-name"
                                               type="text"
                                               name="firstname"
                                               class="label_better"
                                               placeholder="{{trans('common.form_contact_us_first_name')}}"
                                               required="">
                                    </div>
                                    <div>
                                        <input id="form-name"
                                               type="text"
                                               name="lastname"
                                               class="label_better"
                                               placeholder="{{trans('common.form_contact_us_name')}}"
                                               required="">
                                    </div>
                                    <div class="form-phone-block">
                                        <input id="form-phone"
                                               type="tel"
                                               name="phone"
                                               class="label_better"
                                               placeholder="{{trans('common.form_contact_us_phone')}}"
                                               required="">

                                    </div>
                                    <div>
                                        <input id="form-email"
                                               type="email"
                                               name="email"
                                               class="label_better"
                                               placeholder="{{trans('common.form_contact_us_email')}}"
                                               required="">
                                    </div>
                                    <div>
                                    <textarea name="cf_866"
                                              id="form-message"
                                              class="label_better"
                                              required=""
                                              placeholder="{{trans('common.form_contact_us_message')}}" cols="38" rows="5"></textarea>
                                    </div>

                                    <input type="hidden" name="cf_854" data-label="" value="">
                                    <input type="hidden" name="leadsource" data-label="" value="Web Site">

                                    <div class="g-recaptcha" data-sitekey="{{ env("RECAPTCHA_KEY") }}"></div>
                                    <input type="hidden" name="recaptcha-url" id="recaptcha-url" data-url="{{ route('check-recaptcha') }}">

                                    <div class="send">
                                        <button onclick="Web.form.sendContactUs()" id="send-btn" value="{{trans('common.form_contact_us_send')}}">{{trans('common.form_contact_us_send')}}</button>
                                        <img src="/img/form-preloader.gif" alt="preloader form" style="display: none" id="form-preloader">
                                    </div>
                                </form>
                                <script  type="text/javascript">window.onload = function() { var N=navigator.appName, ua=navigator.userAgent, tem;var M=ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];M=M? [M[1], M[2]]: [N, navigator.appVersion, "-?"];var browserName = M[0];var form = document.getElementById("__vtigerWebForm"), inputs = form.elements; form.onsubmit = function() { var required = [], att, val; for (var i = 0; i < inputs.length; i++) { att = inputs[i].getAttribute("required"); val = inputs[i].value; type = inputs[i].type; if(type == "email") {if(val != "") {var elemLabel = inputs[i].getAttribute("label");var emailFilter = /^[_/a-zA-Z0-9]+([!"#$%&()*+,./:;<=>?\^_`{|}~-]?[a-zA-Z0-9/_/-])*@[a-zA-Z0-9]+([\_\-\.]?[a-zA-Z0-9]+)*\.([\-\_]?[a-zA-Z0-9])+(\.?[a-zA-Z0-9]+)?$/;var illegalChars= /[\(\)\<\>\,\;\:\"\[\]]/ ;if (!emailFilter.test(val)) {alert("For "+ elemLabel +" field please enter valid email address"); return false;} else if (val.match(illegalChars)) {alert(elemLabel +" field contains illegal characters");return false;}}}if (att != null) { if (val.replace(/^\s+|\s+$/g, "") == "") { required.push(inputs[i].getAttribute("label")); } } } if (required.length > 0) { alert("The following fields are required: " + required.join()); return false; } var numberTypeInputs = document.querySelectorAll("input[type=number]");for (var i = 0; i < numberTypeInputs.length; i++) { val = numberTypeInputs[i].value;var elemLabel = numberTypeInputs[i].getAttribute("label");var elemDataType = numberTypeInputs[i].getAttribute("datatype");if(val != "") {if(elemDataType == "double") {var numRegex = /^[+-]?\d+(\.\d+)?$/;}else{var numRegex = /^[+-]?\d+$/;}if (!numRegex.test(val)) {alert("For "+ elemLabel +" field please enter valid number"); return false;}}}var dateTypeInputs = document.querySelectorAll("input[type=date]");for (var i = 0; i < dateTypeInputs.length; i++) {dateVal = dateTypeInputs[i].value;var elemLabel = dateTypeInputs[i].getAttribute("label");if(dateVal != "") {var dateRegex = /^[1-9][0-9]{3}-(0[1-9]|1[0-2]|[1-9]{1})-(0[1-9]|[1-2][0-9]|3[0-1]|[1-9]{1})$/;if(!dateRegex.test(dateVal)) {alert("For "+ elemLabel +" field please enter valid date in required format"); return false;}}}}; }</script>


                                <div class="form-popup" id="form-popup" style="display: none">
                                    <div>
                                        <p id="form-popup-content"></p>
                                        <button onclick="$('#form-popup').css({'display': 'none'})" value="Ok">Ok</button>
                                    </div>
                                </div>
                            </div>

                            <div class="contact-info">
                                <p class="contact-title title">{{trans('common.form_contact_us_how_to_contact')}}</p>
                                @if(isset($contacts['company_phone']))
                                    <p class="contact-info_tel"><a href="tel:{{$contacts['company_phone']}}" title="{{$contacts['company_phone']}}">{{$contacts['company_phone']}}</a></p>
                                @endif
                                @if(isset($contacts['company_skype']))
                                    <p class="contact-info_sk"><a href="skype:{{$contacts['company_skype']}}" title="{{$contacts['company_skype']}}">{{$contacts['company_skype']}}</a></p>
                                @endif
                                @if(isset($contacts['company_email']))
                                <p class="contact-info_mail">{{$contacts['company_email']}}</p>
                                @endif
                                @if(isset($contacts['company_line']))
                                <p class="contact-info_line">{{$contacts['company_line']}}</p>
                                @endif
                                @if(isset($contacts['company_wechat']))
                                <p class="contact-info_viber">{{$contacts['company_wechat']}}</p>
                                @endif
                                @if(isset($contacts['company_whatsapp']))
                                <p class="contact-info_whatsapp"><a href="https://api.whatsapp.com/send?phone={{$contacts['company_whatsapp']}}" title="{{$contacts['company_whatsapp']}}">{{$contacts['company_whatsapp']}}</a></p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="burger burger-home">
            <span></span>
            <span></span>
            <span></span>
        </div>
    {{--
    </div>
    --}}
</header>
<div class="block-back block-back-hide" id="block-back"></div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>

    document.addEventListener('DOMContentLoaded', function () {

        $('.contact-btn').click(function () {
            $('.block-back').addClass('block-back-show').removeClass('block-back-hide');
            $('.form-contact-us').addClass('form-contact-us-show').removeClass('form-contact-us-hide');
        });

        $('.block-back, .close').click(function () {
            Web.main.hideContactUs();
        });

        $("input.label_better").label_better({
            position: "top",
            animationTime: 150,
            easing: "ease-in-out",
            offset: 5,
            hidePlaceholderOnFocus: true
        });
        $("textarea.label_better").label_better({
            position: "top",
            animationTime: 120,
            easing: "ease-in-out",
            offset: 5,
            hidePlaceholderOnFocus: true
        });
        Web.main.setInputLine();


          //Phone number in contact us form
          var telInput = $("#form-phone");
          telInput.intlTelInput({
            initialCountry: "<?php echo LaravelLocalization::getCurrentLocale() === 'en' ? 'gb' : LaravelLocalization::getCurrentLocale() ?>",
          });

          telInput.on('focus', function () {
             $(this).parents( "div" ).addClass('lb_wrap_line');
          });
          telInput.on('focusout', function () {
             $(this).parents( "div" ).removeClass('lb_wrap_line');
          });

          var isClicked = false;
          var currentDialCode = '';
          telInput.on('click', function () {
            var countryData = telInput.intlTelInput("getSelectedCountryData");
            if (!isClicked) {
              telInput.val('+' + countryData.dialCode + ' ');
              currentDialCode = countryData.dialCode;
            }
            isClicked = true;
          });

          telInput.on('countrychange', function(e, countryData) {
             if(countryData.dialCode && currentDialCode !== countryData.dialCode) {
               currentDialCode = countryData.dialCode;
               telInput.val('+' + countryData.dialCode + ' ');
             }
             isClicked = true;
          });

        var favorite_property = localStorage.getItem('favorite_property');
        var viewed_property = localStorage.getItem('viewed_property');
        $('input[name="cf_854"]').val('{{url()->to('/admin/show-guest-property')}}?' + (favorite_property ? 'favorites='+favorite_property : '') + (viewed_property ? '&viewed='+viewed_property : ''));

    });
</script>