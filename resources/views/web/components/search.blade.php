<div class="search-main">
    <div class="search-main-wrap">
        <div class="search-main-block" id="search-main-block">
            <div class="search-main-head">
                <h1 class="title">{{trans('common.home_header')}}</h1>
            </div>
            <div class="search-main-bottom" style="opacity: 0.0" id="search-main-bottom">

                <form onkeypress="if(event.keyCode == 13) return false;" id="search-form">

                    <div class="form-type">
                        <select name="sale-rent"
                                id="sale-rent"
                                class="selectpicker"
                                onchange="Web.search.changeSearchType(this.value)"
                                title="{{trans('common.search_buy')}} / {{trans('common.search_rent')}}">
                            <option data-hidden="true"></option>
                            <option value="{{trans('url.to_sale')}}">{{trans('common.search_buy')}}</option>
                            <option value="{{trans('url.to_rent')}}">{{trans('common.search_rent')}}</option>
                        </select>
                    </div>

                    <div class="form-category">
                        <select name="category_url"
                                id="category_url"
                                class="selectpicker"
                                onchange="Web.search.setToStorageLastSearch('search_category', this.value)"
                                title="{{trans('common.search_title_category')}}">
                            <option data-hidden="true"></option>
                            <option value="">{{trans('common.search_type')}}</option>
                            @foreach($categories as $key => $c)
                            <option value="{{$key}}">{{$c}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!--Rooms-->
                    <div class="form-rooms">
                        <select name="rooms_url"
                                id="rooms"
                                class="selectpicker"
                                onchange="Web.search.setToStorageLastSearch('search_rooms', this.value)"
                                title="{{trans('common.search_rooms')}}">
                            <option data-hidden="true"></option>
                            <option value="">{{trans('common.search_rooms')}}</option>

                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                        </select>
                    </div>

                    <div class="form-city">
                        <select name="location_url"
                                id="location_url"
                                class="selectpicker"
                                onchange="Web.search.setToStorageLastSearch('search_location', this.value)"
                                title="{{trans('common.search_title_city')}}">
                            <option data-hidden="true"></option>
                            <option value="">{{trans('common.search_city')}}</option>
                            @foreach($locations as $key => $l)
                            <option value="{{$key}}">{{$l}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div id="budget-block" class="form-budget">
                        <select name="budget"
                                id="budget"
                                class="selectpicker"
                                title="{{trans('common.search_title_budget')}}">
                            <option data-hidden="true"></option>
                        </select>
                    </div>
                    <div class="form-code">
                        <input name="code" id="code" type="text" placeholder="{{trans('common.search_by_id')}}">
                    </div>
                    <div class="form-btn">
                        <button type="button" onclick="Web.search.goSearch()" value="{{trans('common.search_btn_search')}}">
                            {{trans('common.search_btn_search')}}
                        </button>
                    </div>

                    <div class="close-form-mob"><span class="close-form-mob-btn">Close Filter</span></div>
                </form>
            </div>

        </div>
        <div class="text-center show-search-main">
            <button class="show-search-main-btn" value="{{trans('common.search_btn_find')}}">
                {{trans('common.search_btn_find')}}
            </button>
        </div>
    </div>
</div>

<script type="text/javascript">

    document.addEventListener('DOMContentLoaded', function () {
        Web.main.deleteCookie('search');
        Web.search.setData({
            url: '{{ url("/")}}',
            categories: JSON.parse('<?php echo isset($categoriesWithId) ? json_encode($categoriesWithId) : json_encode([]); ?>'),
            locations: JSON.parse('<?php echo isset($locationsWithId) ? json_encode($locationsWithId) : json_encode([])?>'),
            toSaleTrans: '<?php echo trans("url.to_sale") ?>',
            budgetTrans: '{{trans("common.search_budget")}}',
            test: 'test'
        });
        Web.search.changeSearchType($('#sale-rent').val() || '{{trans("url.to_sale")}}');
        $("#location_url").selectpicker()
            .on('loaded.bs.select', function () {
                $('#search-main-bottom').animate({opacity: 1}, 500);
            })

    });
/*
    $(document).ready(function(){

        $( ".show-search-main-btn" ).click(function(){
            $("body").addClass("body-fix");
            $("#search-form").addClass("form-open");
        });

        $( "#search-form > div" ).click(function(){
            $("#search-form > div").removeClass("z-idx");
            $( this ).addClass( "z-idx" );
        });

        $( ".close-form-mob-btn, .burger" ).click(function(){
            $("body").removeClass("body-fix");
            $("#search-form").removeClass("form-open");
        })

    });*/
</script>