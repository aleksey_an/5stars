<div class="favorites-list-sharing">
    <div class="search-favorites-link">
        <p id="search-link">{{$favoritesLink}}</p>
        <span onclick="Web.main.selectAndCopyText('search-link')" id="btn-take-copy">Copy</span>
    </div>
    <p class="favorites-list-sharing-open-form">{{trans('common.header_home_contact_us')}}<span id="btn-hide-show-form"><span
                    class="caret"></span></span></p>

    <div id="form-block" class="favorites-list-sharing-form">
        {{--<form id="form-home-contact-us" onkeypress="if(event.keyCode == 13) return false;">--}}
            {{--<div>--}}
                {{--<input id="form-name"--}}
                       {{--type="text"--}}
                       {{--name="formName"--}}
                       {{--class="label_better"--}}
                       {{--placeholder="{{trans('common.form_contact_us_name')}}">--}}
            {{--</div>--}}
            {{--<div>--}}
                {{--<input id="form-phone"--}}
                       {{--type="text"--}}
                       {{--name="formPhone"--}}
                       {{--class="label_better"--}}
                       {{--placeholder="{{trans('common.form_contact_us_phone')}}">--}}
            {{--</div>--}}
            {{--<div>--}}
                {{--<input id="form-email"--}}
                       {{--type="email"--}}
                       {{--name="formEmail"--}}
                       {{--class="label_better"--}}
                       {{--placeholder="{{trans('common.form_contact_us_email')}}">--}}
            {{--</div>--}}
            {{--<div>--}}
                {{--<textarea name="formMessage"--}}
                          {{--id="form-message"--}}
                          {{--class="label_better"--}}
                          {{--placeholder="{{trans('common.form_contact_us_message')}}" cols="38" rows="5"></textarea>--}}
            {{--</div>--}}
            {{--<div class="send">--}}
                {{--<button onclick="Web.form.sendContactUs()" id="send-btn" value="Send">Send</button>--}}
                {{--<img src="/img/form-preloader.gif" alt="preloader" style="display: none" id="form-preloader">--}}
            {{--</div>--}}
        {{--</form>--}}
        {{--<div class="form-popup" id="form-popup" style="display: none">--}}
            {{--<div>--}}
                {{--<p id="form-popup-content"></p>--}}
                {{--<button onclick="$('#form-popup').css({'display': 'none'})" value="Ok">Ok</button>--}}
            {{--</div>--}}
        {{--</div>--}}


        <form id="__vtigerWebForm" class="" name="Callback form" action="https://vtiger.vidax.com/modules/Webforms/capture.php" method="post" accept-charset="utf-8" enctype="multipart/form-data"
              onkeypress="if(event.keyCode == 13) return false;">
            <input type="hidden" name="__vtrftk" value="sid:de1bb25a95071b47a8b38320c9d2b196580ceda6,1515161355">
            <input type="hidden" name="publicid" value="73d8c7fb54694d162fbb678eaed21604">
            <input type="hidden" name="urlencodeenable" value="1">
            <input type="hidden" name="name" value="Callback form">

            {{--<div>--}}
                {{--<input id="form-name"--}}
                       {{--type="text"--}}
                       {{--name="firstname"--}}
                       {{--class="label_better"--}}
                       {{--placeholder="{{trans('common.form_contact_us_first_name')}}"--}}
                       {{--required="">--}}
            {{--</div>--}}
            <div>
                <input id="form-name"
                       type="text"
                       name="lastname"
                       class="label_better"
                       placeholder="{{trans('common.form_contact_us_name')}}"
                       required="">
            </div>
            <div class="form-phone-block">
                <input id="form-phone"
                       type="tel"
                       name="phone"
                       class="label_better"
                       placeholder="{{trans('common.form_contact_us_phone')}}"
                       required="">

            </div>
            <div>
                <input id="form-email"
                       type="email"
                       name="email"
                       class="label_better"
                       placeholder="{{trans('common.form_contact_us_email')}}"
                       required="">
            </div>
            <div>
                <textarea name="cf_866"
                          id="form-message"
                          class="label_better"
                          required=""
                          placeholder="{{trans('common.form_contact_us_message')}}" cols="38" rows="5"></textarea>
            </div>

            <input type="hidden" name="cf_854" data-label="" value="">
            <input type="hidden" name="leadsource" data-label="" value="Web Site">

            <div class="g-recaptcha" data-sitekey="{{ env("RECAPTCHA_KEY") }}"></div>
            <input type="hidden" name="recaptcha-url" id="recaptcha-url" data-url="{{ route('check-recaptcha') }}">

            <div class="send">
                <button onclick="Web.form.sendContactUs()" id="send-btn" value="{{trans('common.form_contact_us_send')}}">{{trans('common.form_contact_us_send')}}</button>
                <img src="/img/form-preloader.gif" alt="preloader form" style="display: none" id="form-preloader">
            </div>
        </form>
        <script  type="text/javascript">window.onload = function() { var N=navigator.appName, ua=navigator.userAgent, tem;var M=ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];M=M? [M[1], M[2]]: [N, navigator.appVersion, "-?"];var browserName = M[0];var form = document.getElementById("__vtigerWebForm"), inputs = form.elements; form.onsubmit = function() { var required = [], att, val; for (var i = 0; i < inputs.length; i++) { att = inputs[i].getAttribute("required"); val = inputs[i].value; type = inputs[i].type; if(type == "email") {if(val != "") {var elemLabel = inputs[i].getAttribute("label");var emailFilter = /^[_/a-zA-Z0-9]+([!"#$%&()*+,./:;<=>?\^_`{|}~-]?[a-zA-Z0-9/_/-])*@[a-zA-Z0-9]+([\_\-\.]?[a-zA-Z0-9]+)*\.([\-\_]?[a-zA-Z0-9])+(\.?[a-zA-Z0-9]+)?$/;var illegalChars= /[\(\)\<\>\,\;\:\"\[\]]/ ;if (!emailFilter.test(val)) {alert("For "+ elemLabel +" field please enter valid email address"); return false;} else if (val.match(illegalChars)) {alert(elemLabel +" field contains illegal characters");return false;}}}if (att != null) { if (val.replace(/^\s+|\s+$/g, "") == "") { required.push(inputs[i].getAttribute("label")); } } } if (required.length > 0) { alert("The following fields are required: " + required.join()); return false; } var numberTypeInputs = document.querySelectorAll("input[type=number]");for (var i = 0; i < numberTypeInputs.length; i++) { val = numberTypeInputs[i].value;var elemLabel = numberTypeInputs[i].getAttribute("label");var elemDataType = numberTypeInputs[i].getAttribute("datatype");if(val != "") {if(elemDataType == "double") {var numRegex = /^[+-]?\d+(\.\d+)?$/;}else{var numRegex = /^[+-]?\d+$/;}if (!numRegex.test(val)) {alert("For "+ elemLabel +" field please enter valid number"); return false;}}}var dateTypeInputs = document.querySelectorAll("input[type=date]");for (var i = 0; i < dateTypeInputs.length; i++) {dateVal = dateTypeInputs[i].value;var elemLabel = dateTypeInputs[i].getAttribute("label");if(dateVal != "") {var dateRegex = /^[1-9][0-9]{3}-(0[1-9]|1[0-2]|[1-9]{1})-(0[1-9]|[1-2][0-9]|3[0-1]|[1-9]{1})$/;if(!dateRegex.test(dateVal)) {alert("For "+ elemLabel +" field please enter valid date in required format"); return false;}}}}; }</script>

        <div class="form-popup" id="form-popup" style="display: none">
            <div>
                <p id="form-popup-content"></p>
                <button onclick="$('#form-popup').css({'display': 'none'})" value="Ok">Ok</button>
            </div>
        </div>
    </div>
    <ul>
        @if(isset($properties))
        @foreach($properties as $property)
        <li>
            @include ('web.components.property',['property' => $property])
        </li>
        @endforeach
        @endif
        @if(isset($notFound))
        @foreach($notFound as $id)
        <li>
            @include ('web.components.property-deleted',['id' => $id])
        </li>
        @endforeach
        @endif
    </ul>
</div>


<script>
  document.addEventListener('DOMContentLoaded', function () {
    $("#form-block").hide();
    $("#btn-hide-show-form").click(function () {
      $('#form-block').slideToggle();
      $('#btn-hide-show-form').toggleClass('around-form');
    });
    $("input.label_better").label_better({
      position: "top",
      animationTime: 150,
      easing: "ease-in-out",
      offset: 5,
      hidePlaceholderOnFocus: true
    });
    $("textarea.label_better").label_better({
      position: "top",
      animationTime: 150,
      easing: "ease-in-out",
      offset: 10,
      hidePlaceholderOnFocus: true
    });
    Web.main.setInputLine();
  });
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>