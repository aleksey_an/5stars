<div class="search-main search-main--header">
    <div class="search-main-wrap">
        <div class="search-main-block search-main-block--header" id="search-main-block">
            <div class="search-main-bottom">
                <form onkeypress="if(event.keyCode == 13) return false;" id="search-form">
                    <input type="hidden" id="sale-rent" value="{{trans('url.' .$searchType) }}">

                    <div class="form-category">
                        {!! Form::select('category_url', $dataForSearchPanel['categories']['categories'], null,
                        [
                        'class' => 'selectpicker',
                        'placeholder' => trans('common.search_type'),
                        'id' => 'category_url',
                        'onchange' => 'Web.search.setToStorageLastSearch("search_category", this.value)'
                        ]) !!}
                    </div>
                    <!--Rooms-->
                    <div class="form-rooms">
                        <select name="rooms_url"
                                id="rooms"
                                class="selectpicker"
                                onchange="Web.search.setToStorageLastSearch('search_rooms', this.value)"
                                title="{{trans('common.search_rooms')}}"
                        >
                            <option data-hidden="true"></option>
                            <option value="">{{trans('common.search_rooms')}}</option>

                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                        </select>
                    </div>
                    <div class="form-city">
                        {!! Form::select('location_url', $dataForSearchPanel['locations']['locations'], null, [
                        'class' => 'selectpicker',
                        'placeholder' => trans('common.search_city'),
                        'id' => 'location_url',
                        'onchange' => 'Web.search.setToStorageLastSearch("search_location", this.value)'
                        ]) !!}
                    </div>
                    <div id="budget-block" class="form-budget">
                        <select name="budget" id="budget" class="selectpicker"></select>
                    </div>
                    <div class="form-btn">
                        <button type="button" onclick="Web.search.goSearch()" class="search-header-btn" value="{{trans('common.search_btn_search')}}">
                            {{trans('common.search_btn_search')}}
                        </button>
                    </div>
                    <div class="close-form-mob"><span class="close-form-mob-btn">Close Filter</span></div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        Web.search.setData({
            url: '{{ url("/")}}',
            categories: JSON.parse('<?php echo isset($dataForSearchPanel['categories']['categoriesWithId']) ? json_encode($dataForSearchPanel['categories']['categoriesWithId']) : json_encode([]); ?>'),
            locations: JSON.parse('<?php echo isset($dataForSearchPanel['locations']['locationsWithId']) ? json_encode($dataForSearchPanel['locations']['locationsWithId']) : json_encode([])?>'),
            toSaleTrans: '<?php echo trans("url.to_sale") ?>',
            budgetTrans: '{{trans("common.search_budget")}}',
            test: 'test'
        });
        Web.search.changeSearchType($('#sale-rent').val() || '{{trans("url.to_sale")}}');
        Web.main.deleteCookie('search');
        $("#category_url").selectpicker('val', localStorage.getItem('search_category'));
        $("#location_url").selectpicker('val', localStorage.getItem('search_location'))
            .on('loaded.bs.select', function () {
                var spans = $('span.filter-option');
                for (var i = 0; i < spans.length; i++) {
                    spans[i].innerHTML = spans[i].innerHTML.replace('-', '').replace(/\&nbsp\;/gi, '');
                }
            }).on('change', function () {
                var spans = $('span.filter-option');
                for (var i = 0; i < spans.length; i++) {
                    spans[i].innerHTML = spans[i].innerHTML.replace('-', '').replace(/\&nbsp\;/gi, '');
                }
            });
        $("#budget").selectpicker('val', localStorage.getItem('search_budget'));
        $("#rooms").selectpicker('val', localStorage.getItem('search_rooms'));
    });
</script>