<div>
    <ul id="responsive">
        @foreach($properties as $property)
        <li>
            @include('web.components.property',['property' => $property])
        </li>
        @endforeach
    </ul>
    <script>
      //      document.addEventListener('DOMContentLoaded', function () {
      Web.property.deleteViewedPropertiesFromStorage(JSON.parse('<?php echo isset($notFound) ? json_encode($notFound) : json_encode([]); ?>'));

      $('#responsive').lightSlider({
        item: 4,
        loop: false,
        controls: true,
//        prevHtml: 'Prev',
//        nextHtml: 'Next',
        slideMove: 2,
        pager: false,
//        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 800,
        responsive: [
          {
            breakpoint: 1600,
            settings: {
              item: 4,
              slideMove: 4,
              slideMargin: 9,
            }
          },
          {
            breakpoint: 1200,
            settings: {
              item: 3,
              slideMove: 3,
              slideMargin: 6,
            }
          },
          {
            breakpoint: 990,
            settings: {
              item: 2,
              slideMove: 2,
              slideMargin: 3,
            }
          },
          {
            breakpoint: 580,
            settings: {
              item: 1,
              slideMove: 1
            }
          }
        ]
      });
      //      });
    </script>
</div>

