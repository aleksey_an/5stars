<div class="property-block" id="remove-{{$id}}">
    <div class="property-block-main">
        <div class="property-block-top">

            <div onclick="Web.property.addPropertyToFavorite(+'{{$id}}'); $('#remove-{{$id}}' ).remove();"
                 class="favorit-icon {{isset($property) && $property->isFavorite ? 'favorit-icon-select' : 'favorit-icon-no_select'}} fi-{{$id}}">
                <span></span>
            </div>

            <a class="property-block--del" href="/property-deleted" title="This property was deleted">
                <img src="/img/no-preperty.png" alt="No property">
                <span>This property was deleted</span>
            </a>

        </div>

        <div class="property-block-bottom__">
            <div class="descrip-bottom">
              <p>This property has been removed</p>
            </div>
        </div>
    </div>
</div>