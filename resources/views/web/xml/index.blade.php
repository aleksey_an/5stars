@extends('layouts.web')
@section('content')
@include('web.components.headers.static-pages-header')

<div class="container static">
    <h1>XML fide page</h1>

    <a href="/xml/xml.xml" target="_blank">
        <h3>Click here to <b>open</b> xml</h3>
    </a>

    <a href="/xml/xml.xml" download="xml">
        <h3>Click here to <b>download</b> xml</h3>
    </a>

</div>
@endsection