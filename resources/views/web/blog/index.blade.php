@extends('layouts.web')
@section('meta_title', trans('common.blog_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">
    <h1>Blog</h1>
    @foreach($articles as $article)
        @if(!empty($article->translationInLocale[0]->description))
    <div class="article-blog">
        @if(isset($article->image) && !empty($article->image))
        <div class="article-image-block">
            <img src="/images/article/small/{!!$article->image!!}" alt="Img">
        </div>
        @endif
        <div class="article-bottom">
                <div class="article-title">
                    <h4>{{$article->translationInLocale[0]->title}}</h4>
                </div>
                <div class="article-description">
                    {!!$article->translationInLocale[0]->short_description!!}
                </div>
            <div class="article-link"><a href="/{{ app()->getLocale() }}/blog/{{$article->id}}">{{trans('common.blog_read_more')}}</a></div>
        </div>
    </div>
        @endif
    @endforeach

</div>
@endsection