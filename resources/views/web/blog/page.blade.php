@extends('layouts.web')
@section('meta_title', $article->translationInLocale[0]->title . ' - ' . trans('common.blog_meta_title'))
@section('meta_description', $article->translationInLocale[0]->short_description)
@section('content')
@include('web.components.headers.static-pages-header')

<div class="container static">
    @if(isset($article->translationInLocale[0]))
    <div class="article-title">
        <h1>{{$article->translationInLocale[0]->title}}</h1>
    </div>
    @endif
    <div>
        @if(isset($article->image) && !empty($article->image))
        <div class="article-image-block">
            <img src="/images/article/display/{!!$article->image!!}" alt="Img" style="width: 500px">
        </div>
        @endif
    </div>
    @if(isset($article->translationInLocale[0]))
    <div class="article-title">
        {!!$article->translationInLocale[0]->description!!}
    </div>
    @endif
</div>
@endsection