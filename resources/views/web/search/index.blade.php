@extends('layouts.web')
@php
    $metaTitleParams = [
        'transaction_type' => trans("common.search_meta_title_$searchType"),
        'property_type' => isset($propertyType) ? $propertyType : trans('common.search_meta_title_property'),
        'location' => isset($locationName) ? trans('common.search_meta_title_in_city', ['city' => $locationName]) : trans('common.search_meta_title_in_thailand'),
    ];
@endphp
@section('meta_title', trans('common.search_meta_title', $metaTitleParams))
@section('content')
@include('web.components.headers.search')

<div class="properties-page" id="properties-page">
    <div class="prop__top">
        <div class="container-fluent">
            @if(isset($urlPath))
            <div class="breadcrumb">
                <a href="{{url( LaravelLocalization::getCurrentLocale())}}"
                   title="{{trans('common.bread_crumbs_home')}}">{{trans('common.bread_crumbs_home')}}</a>
                @foreach($urlPath as $key => $path)
                @if(isset( $path['url']))
                <span> / </span>
                <a href="{{url( LaravelLocalization::getCurrentLocale() . $path['url'])}}" title="{{$path['name']}}">{{$path['name']}}</a>
                @endif
                @endforeach
            </div>
            @endif
            @if(isset($locationUrl))
            <div class="prop__url">
                <a href="{{url( LaravelLocalization::getCurrentLocale()) . '/' . $locationUrl}}"
                   title="{{trans('common.search_page_discover')}}">{{trans('common.search_page_discover')}}
                    {{isset($locationName) ? $locationName : trans('common.search_page_region')}} </a>
            </div>
            @endif
        </div>
    </div>

    @if(isset($locations))
    <div class="prop__place">
        <div class="container-fluent">
            <div>
                @if($searchType == 'to_rent')
                <h1>
                    {{trans('common.header_home_to_rent')}}<br>
                    <small>{{trans('common.search_page_choose_location')}}  </small>
                </h1>
                @else
                <h1>
                    {{trans('common.header_home_sale')}}<br>
                    <small>{{trans('common.search_page_choose_location')}}</small>
                </h1>
                @endif
            </div>

        </div>
    </div>

    <div class="container-fluent">
        <div class="property-location-list">
            @foreach($locations as $l)
            <div>
                <a href="{{$l['url']}}" title="Property location">
                    @if(isset($l['image']))
                    <img src="/images/location/small/{{$l['image'][0]}}" alt="Property location">
                    @else
                    <img src="/img/default.png" alt="Default">
                    @endif
                </a>

                <p>{{$l['name']}}</p>
            </div>
            @endforeach
        </div>
        <h2 class="text-center title">{{trans('common.search_page_properties')}}</h2>
    </div>
    @else

    @if($urlPath['header'])
    <div class="prop__place">
        <div class="container-fluent">
            <div>
                <h1>{{trans('common.search_meta_title', $metaTitleParams)}}</h1>
            </div>
        </div>
    </div>
    @endif
    @endif
    <div class="container-fluent">
        @include ('web.properties.properties-block')
    </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function () {
    Web.property.appUrl = '{{ url("/")}}';
    Web.property.propertiesIds = '{{isset($propertiesIds) ? json_encode($propertiesIds) : []}}';

    Web.property.countFavorites();
    Web.property.markFavoriteProperties();
    Web.property.countFavoritesBeforeUpload = Web.property.getFromLocalStorage('favorites_uploaded');
    Web.form.locationId = '<?php echo isset($locationId) ? $locationId : null?>'
  })
</script>
@endsection
