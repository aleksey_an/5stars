@extends('layouts.web')
@section('meta_title', trans('common.who-we-are_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">
    <h2>Who we are</h2>
</div>
@endsection