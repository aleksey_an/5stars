@extends('layouts.web')
@section('meta_title', trans('common.about-us_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">

    <h1 class="text-center">About Us</h1>

    <div class="about-us">

        <p class="text-center">
            Based in Bangkok Thailand since 1996, Fabrice Lore is the founder of www.fivestars-thailand.com.
            Originally from Nice, a famous city of the French Riviera by the Mediterranean sea,
            Fabrice decided to broaden his horizon by embarking on a journey around the world.
            Landing in what used to be “The Virgin Island” of Koh Samui in the early 1990’s,
            Fabrice quickly fell in love with the Thai culture and local way of life.
            Naturally, he decided to stay and built a future for himself in this promising Southeast Asian country.
        </p>

        <ul class="about-us--item">
            <li>
                <img src="/img/static-fabrice.png" alt="">
                <div class="text-center about-us-name">Fabrice Lore</div>
            </li>
            <li>
                <p>Fabrice is a true self-made man and entrepreneur. Starting as a business owner in Koh Samui,
                    he worked in various industries from resort management to hospitality business.
                    He quickly and naturally evolved to Real Estate by opening a brokerage house in Bangkok known as Fivestars-thailand.com to help his fellow
                    Europeans move to Thailand and make their dreams come true.</p>
                <p>Fabrice is a true people’s person and a born communicator with strong interpersonal skills; a reference in the Real Estate world both in Bangkok and Koh Samui.</p>
                <p>He also regularly participates in international seminars and engages in continuous education programs here in Thailand.
                    This is your guarantee that he is constantly up to date with the latest Thai valuation methods and industry know-how and good practices.</p>
            </li>
        </ul>

        <p>As a specialist of the residential market and its development,
            Fabrice is a man of terrain who enjoys finding the right property for the right price and who is committed to his client’s ongoing satisfaction.
            No wonder that a large part of his clientele is based on agent referrals, repeat business and a great word-of-month reputation.</p>
        <p>Stop by the office today and you will be greeted by a jovial professional who has a passion for real estate and people.
            Do you want to know the newest restaurant in town or the quickest way to get around Bangkok during rush hour? Look no further,
            Fabrice knows Thailand’s Real Estate and its capital in and out!</p>

        <h3 class="staff-title">Our team</h3>

        <ul class="about-us--staff">
            <li>
                <span class="about-us--staff-photo"><img src="/img/Fabrice.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Fabrice <b>Lore</b></p>
                    <p class="about-us--staff-position">General Manager</p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)81 271 7155</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">fabrice<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Website: www.fivestars-thailand.com</p>
                    <p>Skype: Fivestarsthai</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsTransactionsImmobilieres/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/fivestars-real-estate-thailand?trk=biz-companies-cym" width="30px" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a></p>
                </div>
            </li>
            <li>
                <span class="about-us--staff-photo"><img src="/img/Tanyaporn.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Tanyaporn <b>Pengpid</b></p>
                    <p class="about-us--staff-position">Sales consultant</p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)62 492 4266</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">tuk<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Website: www.fivestars-thailand.com</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsThailandRealEstate/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/fivestars-real-estate-thailand?trk=biz-companies-cym" width="30px" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a>
                    </p>
                </div>
            </li>
            <li>
                <span class="about-us--staff-photo"><img src="/img/Rung.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Rungthiwa <b>Siriwichai</b></p>
                    <p class="about-us--staff-position">Office Manager</p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)89 967 1873, +66 (0)61 389 9515</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">rung<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Website: www.fivestars-thailand.com</p>
                    <p>Skype: rungfivestars</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsThailandRealEstate/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/fivestars-real-estate-thailand?trk=biz-companies-cym" width="30px" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a>
                    </p>
                </div>
            </li>
            <li>
                <span class="about-us--staff-photo"><img src="/img/Matt.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Matthieu <b>Schwebel</b></p>
                    <p class="about-us--staff-position">Sales Director</p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)83 106 9691</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">matt<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Website: www.fivestars-thailand.com</p>
                    <p>Skype: fivestarsmatt</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsThailandRealEstate/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/fivestars-real-estate-thailand?trk=biz-companies-cym" width="30px" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a>
                    </p>
                </div>
            </li>
            <li>
                <span class="about-us--staff-photo"><img src="/img/Airin.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Airin <b>Tawesuksatien</b></p>
                    <p class="about-us--staff-position">Sales consultant</p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)83 903 9153</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">airin<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Website: www.fivestars-thailand.com</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsTransactionsImmobilieres/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/five-stars-thailand-real-estate?trk=biz-companies-cym" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a>
                    </p>
                </div>
            </li>
        </ul>

    </div>


</div>
@endsection