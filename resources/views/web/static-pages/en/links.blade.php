@extends('layouts.web')
@section('meta_title', trans('common.links_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">

    <h1 class="text-center">Thailand Real Estate Agency, Property Agents
        in Thailand, Web-Link Directory</h1>

    <div class="links">
        <ul>
            <li>
                <div>
                    <img src="/img/static-link-1.png" alt="">
                    <p class="title">
                        DirectoryVault Free Web Directory
                    </p>
                    <a href="http://www.directoryvault.com/" class="links__link">
                        http://www.directoryvault.com/
                    </a>

                    <p class="links__item"></p>

                    <a href="http://www.directoryvault.com/" class="links__btn">View website</a>
                </div>
            </li>
            <li>
                <div>
                    <img src="/img/static-link-2.png" alt="">
                    <p class="title">
                        Real Estate Directory and Real Estate Resources
                    </p>
                    <a href="http://www.directoryre.com" class="links__link">
                        http://www.directoryre.com
                    </a>

                    <p class="links__item">
                        DirectoryRE.com is a comprehensive web directory which collects all the
                        companies and organizations web sites related to Real Estate Directory.
                    </p>

                    <a href="http://www.directoryre.com" class="links__btn">View website</a>
                </div>
            </li>
            <li>
                <div>
                    <img src="/img/static-link-3.png" alt="">
                    <p class="title">
                        Real Estate Directory - Directory RealEstateAgent.com
                    </p>
                    <a href="http://www.directoryrealestateagent.com/" class="links__link">
                        http://www.directoryrealestateagent.com/
                    </a>

                    <p class="links__item">
                        The biggest real estate directory includes real estate classifieds and helps
                        you find your local real estate agents and real estate brokers easily.
                    </p>

                    <a href="http://www.directoryrealestateagent.com/" class="links__btn">View website</a>
                </div>
            </li>
            <li>
                <div>
                    <img src="/img/static-link-4.png" alt="">
                    <p class="title">
                        DesignFirms™ Directory - Find a Design Company or Freelance Designer
                    </p>
                    <a href="http://www.designfirms.org/" class="links__link">
                        http://www.designfirms.org/
                    </a>

                    <p class="links__item">
                        Since 2003, DesignFirms™ has been helping small,
                        medium and large sized businesses and organizations find the right design professionals for their marketing project.
                    </p>

                    <a href="http://www.designfirms.org/" class="links__btn">View website</a>
                </div>
            </li>
            <li>
                <div>
                    <img src="/img/static-link-5.png" alt="">
                    <p class="title">
                        Defiscalisation estate: tax relief advice - K & P Finance
                    </p>
                    <a href="http://www.kpmfinance.com/" class="links__link">
                        http://www.kpmfinance.com/
                    </a>

                    <p class="links__item">
                        Real estate tax advice (Loi Girardin, Scellier etc.). K & P Finance Tel: +33 (0) 142 566 000,
                        offers his advice on defiscalisation measure. Go to a better management of your assets and pay less tax.
                    </p>

                    <a href="http://www.kpmfinance.com/" class="links__btn">View website</a>
                </div>
            </li>
            <li>
                <div>
                    <img src="/img/static-link-6.png" alt="">
                    <p class="title">
                        Dao Holidays - Apartments Jomtien Pattaya
                    </p>
                    <a href="http://www.daoholidays.com/" class="links__link">
                        http://www.daoholidays.com/
                    </a>

                    <p class="links__item">
                        Dao Holidays - Apartments - A good plan accommodation, apartment rental for your holiday in Pattaya, Jomtien, Thailand
                    </p>

                    <a href="http://www.daoholidays.com/" class="links__btn">View website</a>
                </div>
            </li>
        </ul>
    </div>
</div>
@endsection