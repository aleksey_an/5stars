@extends('layouts.web')
@section('meta_title', trans('common.agencies-in-thailand_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">

    <h1 class="text-center">Five Stars Real Estate Agents Koh Samui</h1>

    <div class="agent">
        <ul>
            <li>
                <img src="/img/static-1.png" alt="">
            </li>
            <li>
                <span>
                    With more than 15 years’ experience and presence in the Koh Samui property market,
                    the Koh Samui office of Five Stars Thailand Real Estate is helping clients in buying,
                    selling, and renting their dream properties in Koh Samui and nearby islands.
                </span>
            </li>
            <li>
                <span>
                    Our friendly, well-experienced, and French speaking team of real estate agents have extensive knowledge of the Samui property market.
                    Not only will we be happy to assist you in the finding and selecting the finest properties in the Koh Samui area,
                    we will also support you in the negotiations of the terms of contract with your best interest in mind.
                </span>
            </li>
            <li>
                <img src="/img/static-2.png" alt="">
            </li>
            <li>
                <img src="/img/static-3.png" alt="">
            </li>
            <li>
                <span>
                     Due to our excellent contacts and reputation with local authorities,
                    we can also help you with all the necessary paperwork for smooth transaction and related procedures.
                </span>
            </li>
            <li>
                <span>
                    To receive listings of our houses for sale in the Koh Samui or apartments for sale in Koh Samui region, please use the search function on the right or simply contact our Koh Samui Chief Representative at the below contact details for further assistance.
                </span>
            </li>
            <li>
                <img src="/img/static-4.png" alt="">
            </li>
        </ul>

        <ul>
            <li><img src="/img/static-5.png" alt=""></li>
            <li><img src="/img/static-6.png" alt=""></li>
        </ul>

        <div>
            <h3>Five Stars Real Estate</h3>
            <p>Branch Office Koh Samui</p>
            <p>142/73, Moo 4, Maret, Koh Samui, Suratthani, 84310, Thailand</p>
            <p>Mobile Number: +66 (0)81 271 7155</p>
            <p>Email Address: samui@fivestars-thailand.com</p>
        </div>

        <div class="static-map text-center">
            <img src="/img/static-map.png" alt="">

            <p>Map of Koh Samui, Thailand</p>
        </div>
    </div>

</div>
@endsection