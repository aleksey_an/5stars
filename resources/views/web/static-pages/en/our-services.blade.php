@extends('layouts.web')
@section('meta_title', trans('common.our-services_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">
    <h1 class="text-center">Our Services</h1>

    <div class="service">
        <p>FiveStars Real Estate Ltd is a registered company based in Bangkok.
            Our cultural and professional expertise give us the cutting-edge in the business.
            We will find your property (land, house, condo or business) anywhere in Thailand : Bangkok, Pattaya, Phuket or Koh Samui.</p>
        <p>Our team comprises of real estate professionals in charge of a specific area.
            They are familiar with every detail regarding the property itself or the neighbourhood in their zones.</p>
        <p>Hence, our perfect knowledge of the Thai real estate market including the current prices.</p>
        <p>Every property featuring on our website match the comfort and quality standards that we have strictly set in order to offer you a “Home away from Home”.</p>
        <p>Our team will assist you to feel at home in Thailand as a new expatriate or to advise you on great investment opportunities.</p>
        <p>The procurement of a wide range of services is ABSOLUTELY FREE OF CHARGE since it is fully included in our provision of services.</p>
        <p>*In case of a rental value of a property under 20 000 bahts/month,
            we request the tenant to pay the balance between the actual rent and this minimum rental charge we have set.</p>
    </div>
</div>
@endsection