@extends('layouts.web')
@section('meta_title', trans('common.our-services_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">
    <h1 class="text-center">Nos Services</h1>

    <div class="service">
        <p>Notre agence immobilière francophone est basée à Bangkok. Grâce à une bonne connaissance du pays,
            FIVE STARS Transactions Immobilières peut également vous aider à trouver un bien immobilier tel qu'une villa, un terrain,
            un appartement ou un commerce dans les régions de Bangkok, Pattaya, Phuket, Koh Samui.</p>

        <p>Chacun de nos agents immobiliers est en charge d’un quartier ou d’une région dont il connaît parfaitement les charmes et les particularités.</p>

        <p>Ainsi, notre équipe connaît parfaitement le marché immobilier Thaïlandais ainsi que les prix qui s’y pratiquent.</p>

        <p>Chaque bien immobilier que nous proposons à travers notre site répond aux normes de qualité et de confort que nous nous sommes fixés afin que vous ayez l’assurance de vous sentir « comme à la maison ».</p>

        <p>Que ce soit pour vous aider à vous loger lors de votre expatriation dans un pays que vous ne connaissez pas ou pour vous assister lors
            d'une transaction immobilière en Thaïlande, notre équipe se fera un plaisir de vous conseiller.</p>

        <p>Grâce à notre expérience en Thaïlande, l'agence immobilière FIVE STARS Transactions Immobilières,
            peut également vous conseiller dans des placements immobiliers de premier choix dans toute la Thailande.</p>

        <p>Les engagements que nous prenons font partie intégrante de notre prestation de service en recherche immobilière et sont donc entièrement GRATUITS.</p>

        <p>*Dans le cas ou la valeur locative d'un bien est inférieur à 20 000 Bahts/mois, nous demanderons alors au LOCATAIRE de couvrir la différence entre
            le montant du loyer, et le plafond minimum (20 000 Bahts) que nous avons fixé.</p>
    </div>
</div>
@endsection