@extends('layouts.web')
@section('meta_title', trans('common.about-us_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">

    <h1 class="text-center">Qui sommes nous</h1>

    <div class="about-us">

        <ul class="about-us--item">
            <li>
                <img src="/img/static-fabrice.png" alt="">
                <div class="text-center about-us-name">Fabrice Lore</div>
            </li>
            <li>
                <p>Expatrié en Thaïlande depuis 1996, Fabrice LORE est le fondateur de l'agence immobilière Five Stars.</p>
                <p>C’est lors d’un voyage avec des amis, au début des années 90, que Fabrice, d’origine Niçoise, découvre la Thaïlande pour la première fois et tout particulièrement l’ile de Koh Samui.</p>
                <p>A cette époque, l’île est très peu connue des touristes et la vie est simple et agréable, entre mer et montagne.</p>
                <p>C’est tout naturellement qu’il décide de rester pour tenter sa chance dans cette région prometteuse de l’Asie du Sud Est.</p>
                <p>Après avoir travaillé les premières années dans la restauration puis l’hôtellerie,
                    il décide de créer à Bangkok une agence immobilière Francophone afin d’aider les familles d’expatriés
                    à se loger ou d’assister les personnes à la recherche d’un investissement immobilier en Thaïlande.</p>
                <p>Commercial de formation, Fabrice est une personne dynamique, à l’aise en communication avec une
                    connaissance solide et approfondie de la Thaïlande. Il participe régulièrement à des séminaires
                    sur l’immobilier en Thaïlande et assiste également à divers salons de l’immobilier en France afin de promouvoir la Thaïlande.</p>
            </li>
        </ul>

        <p>Pour les clients, c’est avant tout la garantie d’être informé des dernières nouveautés, de l’actualité immobilière en Thaïlande ainsi que des projets les plus récents.
            Spécialiste de l’immobilier résidentiel en Thaïlande, Fabrice est un homme de terrain qui saura vous trouver l’appartement ou la maison idéale au meilleur prix.</p>
        <p>Il n’est donc pas étonnant que sa principale clientèle soit des clients satisfaits qui cherchent à
            réaliser un investissement plus important après une première expérience sans soucis, ou grâce au bouche à oreille qui est la meilleure des publicités.</p>
        <p>Vous souhaitez vous lancer dans un projet immobilier en Thaïlande ? Fabrice vous accueillera chaleureusement en partageant
            avec vous les bons plans de la ville et en vous invitant à découvrir la Thaïlande autrement. </p>
        <p>Passionné de l’immobilier et de son pays d’expatriation, Fabrice connaît la Thaïlande par cœur.</p>

        <h3 class="staff-title">Notre équipe</h3>

        <ul class="about-us--staff">
            <li>
                <span class="about-us--staff-photo"><img src="/img/Fabrice.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Fabrice <b>Lore</b></p>
                    <p class="about-us--staff-position">Directeur Général</p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)81 271 7155</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">fabrice<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Website: www.fivestars-thailand.com</p>
                    <p>Skype: Fivestarsthai</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsTransactionsImmobilieres/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/five-stars-thailand-real-estate?trk=biz-companies-cym" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a></p>
                </div>
            </li>
            <li>
                <span class="about-us--staff-photo"><img src="/img/Tanyaporn.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Tanyaporn <b>Pengpid</b></p>
                    <p class="about-us--staff-position">Agent immobilier</p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)62 492 4266</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">tuk<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Website: www.fivestars-thailand.com</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsThailandRealEstate/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/fivestars-real-estate-thailand?trk=biz-companies-cym" width="30px" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a>
                    </p>
                </div>
            </li>
            <li>
                <span class="about-us--staff-photo"><img src="/img/Rung.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Rungthiwa <b>Siriwichai</b></p>
                    <p class="about-us--staff-position">Responsable administratif</p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)89 967 1873, +66 (0)61 389 9515</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">rung<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Site Web: www.fivestars-thailand.com</p>
                    <p>Skype: rungfivestars</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsThailandRealEstate/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/fivestars-real-estate-thailand?trk=biz-companies-cym" width="30px" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a>
                    </p>
                </div>
            </li>
            <li>
                <span class="about-us--staff-photo"><img src="/img/Matt.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Matthieu <b>Schwebel</b></p>
                    <p class="about-us--staff-position">Responsable des ventes</p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)83 106 9691</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">matt<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Website: www.fivestars-thailand.com</p>
                    <p>Skype: fivestarsmatt</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsTransactionsImmobilieres/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/five-stars-thailand-real-estate?trk=biz-companies-cym" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a>
                    </p>
                </div>
            </li>
            <li>
                <span class="about-us--staff-photo"><img src="/img/Airin.jpg" alt=""></span>
                <div class="about-us--staff-info">
                    <p class="about-us--staff-name">Airin <b>Tawesuksatien</b></p>
                    <p class="about-us--staff-position">Agent immobilier </p>
                    <p class="about-us--staff-tel"><img src="/img/staff-telephone.svg" alt="">+66 (0)83 903 9153</p>
                    <p class="about-us--staff-mail"><img src="/img/staff-email.svg" alt="">airin<span class="antispam">@</span>fivestars-thailand.com</p>
                    <p>Website: www.fivestars-thailand.com</p>
                    <p class="about-us--staff-social">
                        <a href="https://www.facebook.com/FiveStarsTransactionsImmobilieres/" target="_blank"> <img style="display: block;" src="/img/Facebook.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.linkedin.com/company/five-stars-thailand-real-estate?trk=biz-companies-cym" target="_blank"> <img style="display: block;" src="/img/Linkedin.png" alt="" width="30px" data-class="external" /> </a>
                        <a href="https://www.youtube.com/user/fivestarsthailand" target="_blank"> <img style="display: block;" src="/img/youtube1.png" alt="" width="30px" data-class="external" /> </a>
                    </p>
                </div>
            </li>
        </ul>

    </div>


</div>
@endsection