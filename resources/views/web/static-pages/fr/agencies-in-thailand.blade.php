@extends('layouts.web')
@section('meta_title', trans('common.agencies-in-thailand_meta_title'))
@section('content')
@include('web.components.headers.static-pages-header')
<div class="container static">

    <h1 class="text-center">Nos agences immobilières en Thailande</h1>

    <p>Pour acheter ou louer l’appartement idéal en Europe, les démarches sont longues, coûteuses et fastidieuses : comparer toutes les annonces immobilières, participer aux visites groupées,
        constituer un dossier avec de bonnes garanties financières, puis attendre une réponse positive et tout recommencer le cas échéant…</p>

    <p>En France, les prix ne cessent d’augmenter, notamment à Paris où il est de plus en plus difficile de se loger convenablement à des prix raisonnables. À l’achat, le prix du mètre carré à Paris,
        tout arrondissement confondu est autour de 7000 euros. À Bangkok, le mètre carré s’élève à environ 100 000 Baths (2300 €) pour un immeuble neuf avec piscine, salle de sport, sauna…</p>

    <p>Au « Pays du Sourire », le marché immobilier est très attractif et les formalités pour acheter ou louer un appartement s’avèrent beaucoup plus simples et rapides.</p>

    <p>Bienvenue en Thaïlande, ou plus de <b>8000 Français expatriés</b> ont été séduits par la qualité de vie, le fort pouvoir d’achat et son climat tropical.</p>

    <h3>Five Stars, une agence immobilière qui se met en 4 pour vous servir.</h3>

    <p>Installée depuis 2005 à Bangkok, l’agence immobilière* Five Stars, est devenue <b>la spécialiste du logement pour les expatriés en Thaïlande.</b> Avec plus de 800 biens en ligne répertoriés par quartiers,
        le site de Five Stars est l’interface idéale pour trouver rapidement son futur logement : photos, descriptif des biens, informations…Tout y est pour faciliter la recherche.</p>

    <p><i>*En Thaïlande, l’acheteur et le locataire n’ont aucun frais à verser lorsqu’ils font appel aux services d’une agence immobilière.</i></p>

    <p><b>L’équipe de Five Stars assiste les clients du début de la recherche à la négociation finale avec le propriétaire :</b> deux mois avant la date d’emménagement,
        timing approprié pour contacter l’agence, les clients et l’agent échangent les premières informations. Ensuite, une liste de biens est envoyée. Une fois arrivés en Thaïlande, les visites peuvent commencer…</p>

    <p>Chaque agent commercial de l’agence Five Stars possède son véhicule et le met à la disposition des clients.
        C’est confortablement installé qu’ils découvriront les biens sélectionnés ainsi que les points forts du quartier : restaurants, supermarchés, banques, hôpitaux, accès autoroute, Lycée Français…</p>

    <p>Après plusieurs visites et une immersion dans les quartiers de Bangkok, les clients sont alors en mesure de comparer les différentes offres et de choisir le logement idéal.</p>

    <p>Pour une location, <b>l’emménagement</b> peut se faire très rapidement: fournir une photocopie du passeport, remplir le contrat préalablement préparé par l’agence,
        prévoir le règlement de 2 mois de loyer pour la caution ainsi que le mois d’avance, assister à l’état des lieux et vous êtes officiellement chez vous…</p>

    <p>Lors d’un achat, il faut compter environ 1 mois avant d’être définitivement propriétaire. Ce n’est pas un notaire mais un avocat qui prend en charge le dossier. (pour plus d’infos : acheter un appartement en Thaïlande)</p>

    <h3>Les spécificités des logements en Thaïlande</h3>

    <p>Vivre dans un studio, un appartement de standing ou une villa avec piscine privée…Les offres de logements à Bangkok sont diverses et variées et à tous les prix.
        Tous proposent une piscine, salle de sport, sauna, sécurité 24/24, une place de parking…</p>

    <p>Pour la location, le <b>loyer</b> est généralement déterminé par son emplacement (proximité centre-ville / transports), la taille de l’unité, le nombre de chambres,
        l’ameublement, la durée du contrat (préférez le « service apartment » pour les locations de moins de 12 mois), l’âge et la catégorie du bâtiment.</p>

    <p>Les <b>charges</b> ne sont pas comprises dans le loyer : environ 20 Bath par mètre cube pour l’eau. La facture d’électricité s’élève approximativement à 3,5 bath/unit pour un condo (« propriété individuelle ») et 6 Baths/unit pour un appartement
        (« ensemble de logement locatif appartenant à une famille ou société »). Les charges de copropriété sont à la charge du propriétaire.</p>

    <p>Il faut compter 1000 Baths/mois soit une vingtaine d’euros pour avoir une bonne connexion Internet.</p>

    <p>En Thaïlande, la plupart des appartements à la location sont très souvent meublés et équipés : lit, tables, bureau, armoires, cuisine équipée, écran plasma…Tout est à disposition et conçu pour que l’on se sente chez soi.</p>

    <p>Pour ceux qui ne souhaitent pas se séparer de leur mobilier, il existe quelques résidences qui proposent des appartements non meublés sur le marché locatif.</p>

    <p><b>Conseils :</b> Nous vous recommandons donc d’éviter de venir avec vos meubles.</p>

    <p>Nos amis les bêtes ne sont malheureusement pas les bienvenus dans les appartements en Thaïlande.</p>

    <p>La qualité de ses projets immobiliers, ses prix attractifs et la simplicité des démarches font de la Thaïlande un pays très convoité par les expatriés Français.</p>

    <p>Vous souhaitez vous installer en Thaïlande prochainement ? N’hésitez pas à contacter Fabrice au +66 (0)8 12 71 71 55 ou par email : fabrice@5stars-immobilier.com</p>


    <div id="map"></div>
    <script>

        function initMap() {
            var myLatLng = {lat: -25.363, lng: 131.044};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
        }
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}=initMap">
    </script>



    {{--<div class="agent">
        <ul>
            <li>
                <img src="/img/static-1.png" alt="">
            </li>
            <li>
                <span>
                    With more than 15 years’ experience and presence in the Koh Samui property market,
                    the Koh Samui office of Five Stars Thailand Real Estate is helping clients in buying,
                    selling, and renting their dream properties in Koh Samui and nearby islands.
                </span>
            </li>
            <li>
                <span>
                    Our friendly, well-experienced, and French speaking team of real estate agents have extensive knowledge of the Samui property market.
                    Not only will we be happy to assist you in the finding and selecting the finest properties in the Koh Samui area,
                    we will also support you in the negotiations of the terms of contract with your best interest in mind.
                </span>
            </li>
            <li>
                <img src="/img/static-2.png" alt="">
            </li>
            <li>
                <img src="/img/static-3.png" alt="">
            </li>
            <li>
                <span>
                     Due to our excellent contacts and reputation with local authorities,
                    we can also help you with all the necessary paperwork for smooth transaction and related procedures.
                </span>
            </li>
            <li>
                <span>
                    To receive listings of our houses for sale in the Koh Samui or apartments for sale in Koh Samui region, please use the search function on the right or simply contact our Koh Samui Chief Representative at the below contact details for further assistance.
                </span>
            </li>
            <li>
                <img src="/img/static-4.png" alt="">
            </li>
        </ul>

        <ul>
            <li><img src="/img/static-5.png" alt=""></li>
            <li><img src="/img/static-6.png" alt=""></li>
        </ul>

        <div>
            <h3>Five Stars Real Estate</h3>
            <p>Branch Office Koh Samui</p>
            <p>142/73, Moo 4, Maret, Koh Samui, Suratthani, 84310, Thailand</p>
            <p>Mobile Number: +66 (0)81 271 7155</p>
            <p>Email Address: samui@fivestars-thailand.com</p>
        </div>

        <div class="static-map text-center">
            <img src="/img/static-map.png" alt="">

            <p>Map of Koh Samui, Thailand</p>
        </div>
    </div>--}}

</div>
@endsection