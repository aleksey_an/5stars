@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h1>Edit Translations</h1>

        <div id="tabs">

            <ul>
                @foreach($translations as $locale => $translation)
                <li><a href="#{{$locale}}">{{$languagesNames[$locale]}} / ({{$locale}})</a></li>
                @endforeach
            </ul>

            @foreach($translations as $locale => $translation)
            <div id="{{$locale}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <table class="translation">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Data</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($translation as $dKey => $data)
                    <tr>
                        <td>{{ $dKey }}</td>
                        <td>
                            <input name="{{$dKey}}"
                                   type="text"
                                   value="{{$data}}"
                                   class="select-all-{{$locale}}"
                                   oninput="pageActions.changedContent('{{$locale}}', '{{$dKey}}', this.value)"/>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <!--<button type="button"
                        id="btn-{{$locale}}"
                        onclick="pageActions.update('{{$locale}}')"
                        class="btn btn-success">Update
                </button>-->
            </div>
            @endforeach
        </div>
        <button onclick="pageActions.updateAll()" class="btn btn-success" id="btn-update-all">Update All</button>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        $("#tabs").tabs({
            collapsible: true
        });
    });
    var pageActions = {

        toUpdate: {},
        isUpdate: false,

        changedContent: function (locale, key, value) {
            /*$('#btn-' + locale).removeClass("btn-success").addClass("btn-danger");*/
            this.isUpdate = true;
            $('#btn-update-all').removeClass("btn-success").addClass("btn-danger");
            this.toUpdate[locale] = this.toUpdate[locale] || {};
            this.toUpdate[locale][key] = value;
        },

        /* getAllInputs: function (locale) {
         var result = {};
         var inputs = $("input:text.select-all-" + locale);
         for (var i = 0; i < inputs.length; i++) {
         result[inputs[i].name] = inputs[i].value;
         }
         return result;
         },*/

        updateAll: function () {
            if (this.isUpdate) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo url('/admin/translation/update-all'); ?>',
                    data: {data: this.toUpdate, block: 'common'},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        alert(data.success);
                        $('#btn-update-all').removeClass("btn-danger").addClass("btn-success");
                        pageActions.isUpdate = false;
                        pageActions.toUpdate = {};
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        console.warn(XMLHttpRequest);
                        alert(textStatus);
                    }
                });
            }
        },

        /* update: function (locale) {
         var data = this.getAllInputs(locale);
         $.ajax({
         type: 'POST',
         url: '<?php echo url('/admin/translation/update'); ?>',
         data: {data: data, locale: locale},
         headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function (data) {
         $('#btn-' + locale).removeClass("btn-danger").addClass("btn-success");
         alert(data.success);
         },
         error: function (XMLHttpRequest, textStatus, errorThrown) {
         console.warn(XMLHttpRequest);
         alert(textStatus);
         }
         });
         }*/
    }
</script>
@endsection

