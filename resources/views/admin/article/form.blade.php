<?php $display = isset($article['image']) ? 'block' : 'none'; ?>

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'Image', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-2">
        {{ HTML::image(url($display == 'block' ? ('/images/article/display/' . $article['image'] . '?' . mt_rand(10,100)) :
        ''), null,
        ['style' => 'display:' . $display , 'id' => 'image', 'height' => '60'] )}}
    </div>

    <div class="col-md-8">
        {!! Form::file('image', ['class' => 'form-control', 'onchange' => 'previewImage(this.files[0])']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div id="tabs">
    <ul>
        @foreach($languages as $key => $language)
        <li><a href="#{{$key}}">{{$language}}</a></li>
        @endforeach
    </ul>
    @foreach($languages as $key => $language)
    <div id="{{$key}}" class="article-data-block">
        <div class="form-group {{ $errors->has('trans-' . $key . '-name') ? 'has-error' : ''}}">
            {!! Form::label('description-' . $key . '-name', 'Subject', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('description-' . $key . '-name', isset($translations[$key]) ?
                $translations[$key]['title'] : '',
                ['class' => 'form-control', 'maxlength' => '250'])!!}
                {!! $errors->first('description-' . $key . '-name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('trans-' . $key . '-short_description') ? 'has-error' : ''}}">
            {!! Form::label('description-' . $key . '-short_description', 'Short description', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('description-' . $key . '-short_description', isset($translations[$key]) ?
                $translations[$key]['short_description'] : '',
                ['class' => 'form-control', 'maxlength' => '500'])!!}
                {!! $errors->first('description-' . $key . '-short_description', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('trans-' . $key . '-description') ? 'has-error' : ''}}">
            {!! Form::label('description-' . $key . '-description', 'Description', ['class' => 'col-md-2
            control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('description-' . $key . '-description', isset($translations[$key]) ?
                $translations[$key]['description'] : '',
                ['class' => 'form-control', 'maxlength' => '100000']) !!}
                {!! $errors->first('description-' . $key . '-description', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    @endforeach
</div>


<div class="form-group {{ $errors->has('is_enabled') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled', 'Is Enabled', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '1', true) !!} Yes</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '0') !!} No</label>
        </div>
        {!! $errors->first('is_enabled', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<script>
    var lang = <?php echo isset($languages) ? json_encode($languages) : json_encode([]) ?>;

    document.addEventListener('DOMContentLoaded', function () {
        $("#tabs").tabs({
            collapsible: true
        });

        initCkeditor()
    });

    function initCkeditor() {
        if (Object.keys(lang).length > 0) {
            for (var i = 0; i < Object.keys(lang).length; i++) {
                CKEDITOR.replace('description-'+ Object.keys(lang)[i] +'-description',{
                    language: 'en',
                    uiColor: '#E7E7E7',
                    toolbar: [
                        { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
                        { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
                        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting' ] },
                        { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
                        { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                        { name: 'tools', items: [ 'Maximize' ] },
                        { name: 'editing', items: [ 'Scayt' ] },
                        { name: 'insert', items: [ 'Image' ] },
                        { name: 'links', items: [ 'Link' ] },
                    ],
                });
            }
        }
    }


    function previewImage(file) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result).css({display: 'block'});
        };
        reader.readAsDataURL(file);
    }
</script>