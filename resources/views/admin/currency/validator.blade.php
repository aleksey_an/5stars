@push('scripts')
<script>
    $(document).ready(function() {
        $("#currency_form").validate({
            errorClass: "help-block",
            errorElement: "span",
            rules: {
                name: {
                    required: true,
                    maxlength: 255
                },
                code: {
                    required: true,
                    maxlength: 10
                },
                sign: {
                    required: true,
                    maxlength: 10
                },
                rate: {
                    required: true,
                     number: true
                }
                
            },
            submitHandler: function(form) {
                form.submit();
            },
            highlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").removeClass('has-error');
            }
        });
    });
</script>
@endpush