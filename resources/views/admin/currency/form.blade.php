<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Currency Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    {!! Form::label('code', 'Code', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
        {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('sign') ? 'has-error' : ''}}">
    {!! Form::label('sign', 'Sign', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('sign', null, ['class' => 'form-control']) !!}
        {!! $errors->first('sign', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('rate') ? 'has-error' : ''}}">
    {!! Form::label('rate', 'Rate', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('rate', null, ['class' => 'form-control']) !!}
        {!! $errors->first('rate', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('is_default') ? 'has-error' : ''}}">
    {!! Form::label('is_default', 'Is default?', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
    <label>{!! Form::radio('is_default', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('is_default', '0', true) !!} No</label>
</div>
        {!! $errors->first('is_default', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('is_enabled') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled', 'Status', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '1', true) !!} Enable</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '0') !!} Disable</label>
        </div>
        {!! $errors->first('is_enabled', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
