<div class="property-block">
    <div class="property-block-main">

        <div class="property-block-top">
            <div class="property-block-top__bottom">
                @if($property->rent_month > 0)
                <p class="property-block-price">{{number_format($property->rent_month)}}<span> THB/{{trans('common.component_property_month')}}</span></p>
                @endif
                @if($property->sale_price > 0)
                <p><span>{{trans('common.component_property_price')}}: </span> {{number_format($property->sale_price)}}<span> THB </span></p>
                @endif
            </div>

            <a onclick="window.location = '{{url( LaravelLocalization::getCurrentLocale() . $property->propertyHref)}}'">
                @if(isset($property->imageForWebSearch[0]))
                <img id="img-{{$prefix}}-{{$property->id}}" src="/images/property/small/{{$property->imageForWebSearch[0]->name}}" alt="" onerror="imgError('img-{{$prefix}}-{{$property->id}}')">
                @else
                <img src="/img/default.png" alt="">
                @endif
            </a>

        </div>

        <div class="property-block-bottom">
            <div class="property-block-bottom__">
                @if(isset($property->locationNameInLocale[0]))
                <p class="property-block-bottom__loc"><span><img src="/img/location-icon.svg" alt="{{trans('common.component_property_location')}}">{{--{{trans('common.component_property_location')}}--}}</span> {{$property->locationNameInLocale[0]->name}}</p>
                @endif
                <p class="property-block-bottom__beds"><span><img src="/img/bed.svg" alt="{{$property->bedroom}}">{{--Beds--}} </span> {{$property->bedroom}} </p>
                <p class="property-block-bottom__sqft"><span><img src="/img/sqft.svg" alt="{{$property->indoor_area}}">{{--Sqft--}} </span> {{$property->indoor_area}} </p>
            </div>

            @if(isset($property->translationsInLocale[0]))
            <p class="property-block-name title">{{substr($property->translationsInLocale[0]->subject, 0, 50)}}
                {{strlen($property->translationsInLocale[0]->subject) > 50 ? ' ...' : ''}}</p>
            @endif

        </div>
    </div>
</div>

<script>
   function imgError(id){
       if(document.getElementById(id)){
           document.getElementById(id).src = '/img/default.png';
       }
   }
</script>