@extends('layouts.app')

@section('content')
@if(isset($property) && $property)
<div id="inf-scroll">
    <h3>Property</h3>
    <div class="properties-block ">
        @include ('admin.show-guest-property.property', ['prefix' => 'pro'])
    </div>
</div>
@endif

@if(isset($favoriteProperties) && $favoriteProperties)
<div class="container" style="margin-top: 14rem">
    <h3>Favorites page</h3>
    <div class="properties-block">

            @foreach($favoriteProperties as $property)
                @include ('admin.show-guest-property.property',['property' => $property, 'prefix' => 'fav'])
            @endforeach

    </div>
</div>
@endif

@if(isset($viewedProperties) && $viewedProperties)
<div class="container" style="margin-top: 14rem">
    <h3>Vieved properties</h3>
    <div class="properties-block">

            @foreach($viewedProperties as $property)
                @include ('admin.show-guest-property.property',['property' => $property, 'prefix' => 'vie'])
            @endforeach

    </div>
</div>
@endif

@endsection
