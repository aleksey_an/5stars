@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Language</div>
                <div class="panel-body">
                   <!-- <a href="{{ url('/admin/language/create') }}" class="btn btn-success btn-sm"
                       title="Add New Language">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>-->

                   <!-- {!! Form::open(['method' => 'GET', 'url' => '/admin/language', 'class' => 'navbar-form
                    navbar-right', 'role' => 'search']) !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    {!! Form::close() !!}-->
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Sequence</th>
                                <th>Flag</th>
                                <th>Language</th>
                                <th>ISO 639-1</th>
                                <th>ISO 639-3</th>
                                <th>Status</th>
                                <th>Web status</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($language as $item)
                            <tr>
                                <td>{{ $item->sequence }}</td>
                                <td>@if ($item->getImage())
                                    <img id="image" src="/images/language/{{ $item->getImage() }}" alt="{{ $item->name }}" width="20"/>
                                    @endif
                                </td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->locale }}</td>
                                <td>{{ $item->iso_639_3 }}</td>
                                <td>
                                    @if ($item->is_enabled)
                                    Enabled
                                    @else
                                    Disabled
                                    @endif
                                </td>
                                <td>
                                    @if ($item->is_enabled_web)
                                    Enabled
                                    @else
                                    Disabled
                                    @endif
                                </td>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    <a href="{{ url('/admin/language/' . $item->id . '/edit') }}" title="Edit Language">
                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                  aria-hidden="true"></i> Edit
                                        </button>
                                    </a>

                                    @if($item->locale != config('app.locale') && $item->locale != 'en')
                                    {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/language', $item->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Language',
                                    'onclick'=>'return confirm("Are you sure you want to delete this item?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $language->appends(['search' =>
                            Request::get('search')])->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
