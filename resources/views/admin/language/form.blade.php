<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('locale') ? 'has-error' : ''}}">
    {!! Form::label('locale', 'ISO 639-1', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('locale', null, ['class' => 'form-control']) !!}
        {!! $errors->first('locale', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('iso_639_3') ? 'has-error' : ''}}">
    {!! Form::label('iso_639_3', 'ISO 639-3', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('iso_639_3', null, ['class' => 'form-control']) !!}
        {!! $errors->first('iso_639_3', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
     {!! Form::label('image', 'Flag', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="col-md-2">
        @if(Form::getValueAttribute('image'))
            <img id="image" src="/images/language/{{ Form::getValueAttribute('image') }}" width="20" />
        @endif
        </div>
        <div class="col-md-10">
            {!! Form::file('image',null,['class'=>'form-control']) !!}
       
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
         </div>
    </div>
</div>

<div class="form-group {{ $errors->has('is_enabled') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled', 'Status Admin', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '1', true) !!} Enable</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '0') !!} Disable</label>
        </div>
        {!! $errors->first('is_enabled', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(config('app.locale') != $language['locale'])
<div class="form-group {{ $errors->has('is_enabled_web') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled_web', 'Status Web', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled_web', '1', true) !!} Enable</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled_web', '0') !!} Disable</label>
        </div>
        {!! $errors->first('is_enabled_web', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
