@push('scripts')
<script>
    $(document).ready(function() {
        $("#language_form").validate({
            errorClass: "help-block",
            errorElement: "span",
            rules: {
                name: {
                    required: true,
                    maxlength: 255
                },
                locale: {
                    required: true,
                    exactlength: 2,
                },
                iso_639_3: {
                    required: true,
                    exactlength: 3,
                }
            },
            submitHandler: function(form) {
                form.submit();
            },
            highlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").removeClass('has-error');
            }
        });
    });
</script>
@endpush