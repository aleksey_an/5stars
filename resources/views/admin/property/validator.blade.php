@push('scripts')
<script>
    $(document).ready(function () {
        var form = $("#property-form").validate({
                errorClass: "help-block",
                errorElement: "span",
                rules: {
                    publish_date: {
                        required: true,
                        date: true
                    },
                    published: {
                        required: true
                    },
                    contact_name: {
                        required: true,
                        maxlength: 250
                    },
                    contact_email: {
                        required: true,
                        email: true
                    },
                    /*contact_phone: {
                        required: true,
                        maxlength: 125
                    },*/
                    contact_mobile: {
                        required: true,
                        maxlength: 125
                    },
                    location_id: {
                        required: true,
                        number: true
                    },
                    category_id: {
                        required: true,
                        number: true
                    },
                    locale: {
                        required: true,
                        maxlength: 2
                    },
                    /* subject: {
                         required: true,
                         maxlength: 250
                     },
                     detail: {
                         required: true,
                         maxlength: 100000
                     },*/
                    building_name: {
                        required: true,
                        maxlength: 250
                    },
                    address: {
                        required: true,
                        maxlength: 250
                    },

                    address_1: {
                        maxlength: 250
                    },
                    unit_number: {
                        required: true,
                        maxlength: 250
                    },
                    floor_number: {
                        required: true,
                        maxlength: 20
                    },
                    key_info: {
                        required: true,
                        maxlength: 250
                    },
                    availability: {
                        maxlength: 250
                    },
                    built_year: {
                        number: true,
                        maxlength: 4
                    },
                    indoor_area: {
                        required: true,
                        number: true,
                        maxlength: 5
                    },
                    outdoor_area: {
                        number: true,
                        maxlength: 5
                    },
                    bedroom: {
                        required: true,
                        number: true,
                        maxlength: 5
                    },
                    bathroom: {
                        maxlength: 250
                    },
                    /*For Sale*/
                    sale_price: {
                        required: "#to_sale:checked",
                        number: true,
                        maxlength: 9
                    },
                    common_fee: {
                        required: "#to_sale:checked",
                        maxlength: 250
                    },
                    /*For Rent*/
                    rent_day: {
                        required: "#to_rent:checked",
                        number: true,
                        maxlength: 9
                    },
                    rent_week: {
                        required: "#to_rent:checked",
                        number: true,
                        maxlength: 9
                    },
                    rent_month: {
                        required: "#to_rent:checked",
                        number: true,
                        maxlength: 9
                    },
                },
                submitHandler:

                    function (form) {
                        form.submit();
                        if ($("#property-form").valid()) {
                            $('#submit-btn').css({'display': 'none'});
                        }
                    }

                ,
                highlight: function (element, errorClass, validClass) {
                    $(element.form).find('input[name="' + element.getAttribute('name') + '"]').closest(".form-group, .checkbox").addClass('has-error');
                    $(element.form).find('select[name="' + element.getAttribute('name') + '"]').closest(".form-group, .checkbox").addClass('has-error');
                }
                ,
                unhighlight: function (element, errorClass, validClass) {
                    $(element.form).find('input[name="' + element.getAttribute('name') + '"]').closest(".form-group, .checkbox").removeClass('has-error');
                    $(element.form).find('select[name="' + element.getAttribute('name') + '"]').closest(".form-group, .checkbox").removeClass('has-error');
                }
                ,
                success: function (label) {
                    setTimeout(function () {
                        $('#submit-btn').css({'display': 'block'});
                    }, 1000)
                }
            })
        ;

    });
</script>
@endpush