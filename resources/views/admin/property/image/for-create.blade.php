<div>
    <div id="gallery-block">
        <div id="mydz" class="dropzone"></div>
        <br>

        <div class="table-responsive">
            <ul id="sortable"></ul>
        </div>
    </div>
</div>

<script>
    var countImages = 0;
    var limitForUpload = '{{env("MAX_COUNT_PROPERTY_IMAGES")}}';
    var imagesArr = [];
    var galleryActions = {
        createLi: function (data) {
            countImages = +countImages + data.length;
            var li = '';
            for (var i = 0; i < data.length; i++) {
                li = '<li id="' + data[i].id + '" class="ui-state-default">' +
                        /*'<span>' + data[i].id + '</span>' +*/
                    '<span class="gallery-img"><img src="/images/property/thumbnail/' + data[i].name + '" height="60"></span> ' +
//                        '<span>' + data[i].sequence + '</span>' +
                    '<span class="gallery-edit-desc"><button type="button" style="display: none" id="btn-edit-description-' + data[i].id + '" class="btn btn-warning btn-xs" onclick="galleryActions.changeDescription(' + data[i].id + ', $(\'#description-' + data[i].id + '\').val(), \'btn-edit-description-' + data[i].id + '\')">Edit Description </button></span>' +
                    '<span class="gallery-label"><label>Is Enabled <input id="checkbox-' + data[i].id + '"  type="checkbox" name="img_" value="' + data[i].id + '" checked="true" onchange="galleryActions.changedEnableStatus(this.value, this.checked)"></label></span>' +
                    '<span class="gallery-del"><button type="button" class="btn btn-danger btn-xs" onclick="galleryActions.delete(\'' + data[i].id + '\', \'' + data[i].name + '\')">Delete </button></span>' +
                    '</li>';
                $('#sortable').append(li);
                if (data[i].is_enabled) {
                    $("#checkbox-" + data[i].id).attr("checked", true);
                } else {
                    $("#checkbox-" + data[i].id).attr("checked", false);
                }
            }
            this.imageSortable();
        },
        /*Change Property Image Sequence*/
        imageSortable: function () {
            $("#sortable").sortable({
                update: function (event, ui) {
                    $('#sortable li').each(function (e) {
                        for (var i = 0; i < imagesArr.length; i++) {
                            if (imagesArr[i].id == $(this).attr('id')) {
                                imagesArr[i].sequence = $(this).index() + 1
                            }
                        }
                    });
                    $('#images').val(JSON.stringify(imagesArr));
                }
            });
        },

        changedEnableStatus: function (imgId, status) {
            status = status ? 1 : 0;
            for (var i = 0; i < imagesArr.length; i++) {
                if (imagesArr[i].id == imgId) {
                    imagesArr[i].is_enabled = status
                }
            }
            $('#images').val(JSON.stringify(imagesArr));
        },
        delete: function (imgId, imgName) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var posting = $.post('<?php echo url('/admin/property/image/delete'); ?>', JSON.stringify({
                imgId: imgId,
                name: imgName
            }));
            for (var i = 0; i < imagesArr.length; i++) {
                if (imagesArr[i].id == imgId) {
                    imagesArr.splice(i, 1)
                }
            }
            $('#images').val(JSON.stringify(imagesArr));
            posting.done(function (data) {
                $('#' + imgId).remove();
                countImages--;
            });
        }
    };
    document.addEventListener('DOMContentLoaded', function () {
        var responseData = null;
        var totalDataTransfer = 0;
        var limitTotalDataTransfer = '<?php echo ( +substr(ini_get("post_max_size"), 0, -1 ) * 1000000) ?>'; //(Bytes)

        $("#mydz").dropzone({
            url: "{!! url('/admin/property/image/upload/0') !!}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            paramName: "file",
            uploadMultiple: true,
            autoProcessQueue: false,
            parallelUploads: 100,
            maxFilesize: 8,
            maxFiles: limitForUpload,
            addRemoveLinks: true,
            dictRemoveFile: 'Remove',
            dictFileTooBig: 'Image is bigger than 8MB',
            acceptedFiles: ".jpeg,.jpg,.png",
            success: function (file, response) {
                this.removeFile(file);
                responseData = response;
            },
            init: function () {
                var _this = this;
                $('#upload-images').click(function () {
                    if (countImages == limitForUpload) {
                        alert('Maximum number of images uploaded (' + limitForUpload + '). First, delete the images before downloading')
                    } else if ((_this.files.length + countImages) > limitForUpload) {
                        alert('The allowed number of images for downloading' + limitForUpload + ' , uploaded ' + countImages +
                            ' you can upload no more than ' + (limitForUpload - countImages) + ' images')
                    } else {
                        $('.dz-remove').css({'display': 'none'});
                        _this.processQueue();
                    }
                });
                this.on("complete", function (file) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0 && responseData && responseData.success) {
                        var img = $('#images').val();
                        img = JSON.parse(img);
                        for (var i = 0; i < responseData.success.length; i++) {
                            responseData.success[i]['id'] = (img.length == 0 ? i : img.length);
                            imagesArr.push(responseData.success[i]);
                            img.push(responseData.success[i])
                        }
                        $('#images').val(JSON.stringify(img));
                        galleryActions.createLi(responseData.success);
                    }
                    totalDataTransfer = 0;
                });
                this.on("addedfile", function (file) {
                    if (totalDataTransfer + file.size < limitTotalDataTransfer) {
                        totalDataTransfer += file.size
                    } else {
                        this.removeFile(file);
                        alert('The allowed load is ' + limitTotalDataTransfer / 1000000 + 'mb. The total size of the images is ' + (totalDataTransfer + file.size) / 1000000 + ' mb. To load a larger size, increase the value in php_ini');
                    }
                });
                this.on("error", function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('Error loading');
                    totalDataTransfer = 0;
                    console.log(response)
                })
            }
        });
    });
</script>
