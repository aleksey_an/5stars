<div class="panel-body">
    <div id="gallery-block">
        <div id="mydz" class="dropzone"></div>
        <br>

        <div class="table-responsive">
            <ul id="sortable"></ul>
        </div>
    </div>
</div>

<script>
    var countImages = 0;
    var limitForUpload = '{{env("MAX_COUNT_PROPERTY_IMAGES")}}';
    var galleryImages = <?php echo $images ? $images : [];?>;
    var galleryActions = {
        changedDescriptionField: function (btnId) {
            $('#' + btnId).show(500);
        },
        changeDescription: function (imgId, description, btnId) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var posting = $.post('<?php echo url('/admin/property/image/change-description'); ?>',
                JSON.stringify({imgId: imgId, detail: description}));
            posting.done(function (data) {
                $('#' + btnId).hide(500);
                console.log(data)
            });
        },
        changedEnableStatus: function (imgId, status) {
            status = status ? 1 : 0;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var posting = $.post('<?php echo url('/admin/property/image/change-enable'); ?>',
                JSON.stringify({imgId: imgId, is_enable: status}));
            posting.done(function (data) {
                console.log('status changed')
            });
        },
        delete: function (imgId, imgName) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var posting = $.post('<?php echo url('/admin/property/image/delete'); ?>',
                JSON.stringify({imgId: imgId, name: imgName}));
            posting.done(function (data) {
                $('#' + imgId).remove();
                countImages--;
            });
        }
    };
    document.addEventListener('DOMContentLoaded', function () {
        createLi(galleryImages);
        var responseData = null;
        var totalDataTransfer = 0;
        var limitTotalDataTransfer = '<?php echo ( +substr(ini_get("post_max_size"), 0, -1 ) * 1000000) ?>'; //(Bytes)
        function createLi(data) {
            data = data.reverse();
            countImages = +countImages + data.length;
            var li = '';
            for (var i = 0; i < data.length; i++) {
                li = '<li id="' + data[i].id + '" class="ui-state-default">' +
//                    '<span>' + data[i].id + '</span>' +
                    '<span class="gallery-img"><img src="/images/property/thumbnail/' + data[i].name + '" height="60"></span> ' +
//                    '<span>' + data[i].sequence + '</span>' +
                    '<span class="gallery-desc"><textarea id="description-' + data[i].id + '" placeholder="Image description" oninput="galleryActions.changedDescriptionField(\'btn-edit-description-' + data[i].id + '\')">' + data[i].detail + '</textarea> </span>' +
                    '<span class="gallery-edit-desc"><button type="button" style="display: none" id="btn-edit-description-' + data[i].id + '" class="btn btn-warning btn-xs" onclick="galleryActions.changeDescription(' + data[i].id + ', $(\'#description-' + data[i].id + '\').val(), \'btn-edit-description-' + data[i].id + '\')">Edit Description </button></span>' +
                    '<span class="gallery-label"><label>Is Enabled <input id="checkbox-' + data[i].id + '"  type="checkbox" name="img_" value="' + data[i].id + '" checked="true" onchange="galleryActions.changedEnableStatus(this.value, this.checked)"></label></span>' +
                    '<span class="gallery-del"><button type="button" class="btn btn-danger btn-xs" onclick="galleryActions.delete(\'' + data[i].id + '\', \'' + data[i].name + '\')">Delete </button></span>' +
                    '</li>';
                $('#sortable').append(li);
                if(data[i].is_enabled){
                    $("#checkbox-" + data[i].id).attr("checked", true);
                } else {
                    $("#checkbox-" + data[i].id).attr("checked", false);
                }
            }
            imageSortable();
        }

        $("#mydz").dropzone({
            url: "{!! url('/admin/property/image/upload/' . $property->id) !!}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            paramName: "file",
            uploadMultiple: true,
            autoProcessQueue: false,
            parallelUploads: 100,
            maxFilesize: 8,
            maxFiles: limitForUpload,
            addRemoveLinks: true,
            dictRemoveFile: 'Remove',
            dictFileTooBig: 'Image is bigger than 8MB',
            acceptedFiles: ".jpeg,.jpg,.png",
            success: function (file, response) {
                this.removeFile(file);
                responseData = response;
            },
            init: function () {
                var _this = this;
                $('#submit-all').click(function () {
                    if (countImages == limitForUpload) {
                        alert('Maximum number of images uploaded (' + limitForUpload + '). First, delete the images before downloading')
                    } else if ((_this.files.length + countImages) > limitForUpload) {
                        alert('The allowed number of images for downloading' + limitForUpload + ' , uploaded ' + countImages +
                            ' you can upload no more than ' + (limitForUpload - countImages) + ' images')
                    } else {
                        $('.dz-remove').css({'display': 'none'});
                        _this.processQueue();
                    }
                });
                this.on("sending", function(file, xhr, formData){
                    formData.append('location_id', $('#location_id').val());
                    formData.append('category_id', $('#category_id').val());
                });
                this.on("complete", function (file) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0 && responseData && responseData.success) {
                        createLi(responseData.success);
                    }
                    totalDataTransfer = 0;
                });
                this.on("addedfile", function (file) {
                    if (totalDataTransfer + file.size < limitTotalDataTransfer) {
                        totalDataTransfer += file.size
                    } else {
                        this.removeFile(file);
                        alert('The allowed load is ' + limitTotalDataTransfer / 1000000 + 'mb. The total size of the images is ' + (totalDataTransfer + file.size) / 1000000 + ' mb. To load a larger size, increase the value in php_ini');
                    }
                });
                this.on("error", function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('Error loading');
                    totalDataTransfer = 0;
                    console.log(response)
                })
            }
        });

        /*Change Property Image Sequence*/
        function imageSortable() {
            $("#sortable").sortable({

                update: function (event, ui) {
                    var order = [];
                    $('#sortable li').each(function (e) {
                        order.push({id: $(this).attr('id'), sequence: $(this).index() + 1})
                    });
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var posting = $.post('<?php echo url('/admin/property/image/change-sequence'); ?>', JSON.stringify({data: order}));
                    posting.done(function (data) {
                        console.log(data)
                    });
                }
            });
        }
    });
</script>
