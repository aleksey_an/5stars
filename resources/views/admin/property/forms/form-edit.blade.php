<?php
$viewed = 0;
foreach ($property->translations as $key => $translation) {
    $viewed += $translation->viewed;
};
?>

<div class="col-md-9">
    <p>Information</p>

    <div class="form-group mt30px {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', 'Status', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            <div class="checkbox col-md-4">
                <label>Published {!! Form::radio('status', 'published', $property->status == 'published' ? true :
                    false) !!}</label>
            </div>
            <div class="checkbox col-md-4">
                <label>Disabled {!! Form::radio('status', 'disabled', $property->status == 'disabled' ? true :
                    false)!!}</label>
            </div>
            <div class="checkbox col-md-4">
                <label>Pending {!! Form::radio('status', 'pending', $property->status == 'pending' ? true : false)
                    !!}</label>
            </div>
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

</div>

<div class="col-md-3 info__right">

    <div class="form-group ">
        {!! Form::label('create-by', 'Created By:', ['class' => 'col-md-6 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('create-by', $createdBy, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
        </div>

    </div>

    <div class="form-group {{ $errors->has('publish_date') ? 'has-error' : ''}}">
        {!! Form::label('publish_date', 'Publish Date:', ['class' => 'col-md-6 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('publish_date', explode(" ", $property->publish_date)[0], ['class' => 'form-control', 'id' =>
            'datepicker', 'disabled' => 'disabled']) !!}
            {!! $errors->first('publish_date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
        {!! Form::label('code', 'Reference:', ['class' => 'col-md-6 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('code', $property->code, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
            {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('viewed') ? 'has-error' : ''}}">
        {!! Form::label('viewed', 'Viewed:', ['class' => 'col-md-6 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('viewed', $viewed, ['class' => 'form-control', 'disabled' => 'disabled'])
            !!}
            {!! $errors->first('viewed', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<!--Contact/Owner-->
<div>
    <div class="col-md-12 mt30px">
        <p> Owner Contact</p>

        <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
            {!! Form::label('contact_name', 'Name', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_name', $property->contact_name, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
            {!! Form::label('contact_email', 'Email', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_email', $property->contact_email, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
            {!! Form::label('contact_phone', 'Phone', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_phone', $property->contact_phone, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_phone', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_mobile') ? 'has-error' : ''}}">
            {!! Form::label('contact_mobile', 'Mobile', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_mobile', $property->contact_mobile, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_mobile', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('contact_viber') ? 'has-error' : ''}}">
            {!! Form::label('contact_viber', 'Viber', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_viber', $property->contact_viber, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_viber', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_line') ? 'has-error' : ''}}">
            {!! Form::label('contact_line', 'Line', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_line', $property->contact_line, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_line', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_whatsapp') ? 'has-error' : ''}}">
            {!! Form::label('contact_whatsapp', 'Whatsapp', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_whatsapp', $property->contact_whatsapp, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_whatsapp', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <p> Description</p>

    <div class="form-group {{ $errors->has('location_id') ? 'has-error' : ''}}">
        {!! Form::label('location_id', 'Locations', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::select('location_id', $locations, $property->location_id,
            ['class' => 'form-control', 'id' => 'location_id', 'onchange' =>
            'pageActions.selectedLocation(this.value)']) !!}
            {!! $errors->first('location_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('metro_id') ? 'has-error' : ''}}" id="metro-block">
        {!! Form::label('metro_id', 'Metro', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            <select name="metro_id" class="form-control" id="metro_id"></select>
            {!! $errors->first('metro_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
        {!! Form::label('category_id', 'Categories', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::select('category_id', $categories, $property->category_id, ['class' => 'form-control', 'id' =>
            'category_id']) !!}
            {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div id="tabs">
        <ul>
            @foreach($languages as $key => $language)
            <li id="tr-tab-li-{{$key}}"><a href="#{{$key}}">{{$language}}</a></li>
            @endforeach
        </ul>
        @foreach($languages as $key => $language)
        <div id="{{$key}}">
            <div class="form-group">
                {!! Form::label('viewed-in-location', 'Viewed in (' .$key. ') translation', ['class' => 'col-md-3
                control-label']) !!}
                <div class="col-md-9">
                    {!! Form::text('viewed-in-location', isset($propertyTranslations[$key]) ?
                    $propertyTranslations[$key]['viewed'] : '',
                    ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'viewed-in-location'])!!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description-' . $key . '-subject') ? 'has-error' : ''}}">
                {!! Form::label('description-' . $key . '-subject', 'Subject', ['class' => 'col-md-3 control-label'])
                !!}
                <div class="col-md-9">
                    {!! Form::text('description-' . $key . '-subject', isset($propertyTranslations[$key]) ?
                    $propertyTranslations[$key]['subject'] : '',
                    ['class' => 'form-control', 'maxlength' => '250'])!!}
                    {!! $errors->first('description-' . $key . '-subject', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('description-' . $key . '-detail') ? 'has-error' : ''}}">
                {!! Form::label('description-' . $key . '-detail', 'Description', ['class' => 'col-md-3 control-label'])
                !!}
                <div class="col-md-9">
                    {!! Form::textarea('description-' . $key . '-detail', isset($propertyTranslations[$key]) ?
                    $propertyTranslations[$key]['detail'] : '',
                    ['class' => 'form-control', 'maxlength' => '100000']) !!}
                    {!! $errors->first('description-' . $key . '-detail', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <br>
</div>
<div class="col-md-12">
    <div class="row">
        <p> Property Address</p>

        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
            {!! Form::label('address', 'Address', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-7 input-group">
                {!! Form::text('address', $property->address, ['class' => 'form-control col-md-3']) !!}
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" onclick="googleMap.getLatLong()">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div id="map" style="width: 100%; height: 300px"></div>
        <br>

        <div class="form-group {{ $errors->has('lat') ? 'has-error' : ''}}">
            {!! Form::label('lat', 'Latitude', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-8">
                {!! Form::number('lat', $property->lat, ['class' => 'form-control', 'step' => 'any', 'id' => 'lat']) !!}
                {!! $errors->first('lat', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('lng') ? 'has-error' : ''}}">
            {!! Form::label('lng', 'Longitude', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-8">
                {!! Form::number('lng', $property->lng, ['class' => 'form-control', 'step' => 'any', 'id' => 'lng']) !!}
                {!! $errors->first('lng', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <p> Property Details</p>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group {{ $errors->has('building_name') ? 'has-error' : ''}}">
                {!! Form::label('building_name', 'Building Name', ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-9">
                    {!! Form::text('building_name', $property->building_name, ['class' => 'form-control']) !!}
                    {!! $errors->first('building_name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('address_1') ? 'has-error' : ''}}">
                {!! Form::label('address_1', 'Property Address', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-10">
                    {!! Form::text('address_1', $property->address_1, ['class' => 'form-control'])!!}
                    {!! $errors->first('address_1', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('unit_number') ? 'has-error' : ''}}">
                {!! Form::label('unit_number', 'Unit Number', ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-9">
                    {!! Form::text('unit_number', $property->unit_number, ['class' => 'form-control']) !!}
                    {!! $errors->first('unit_number', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('floor_number') ? 'has-error' : ''}}">
                        {!! Form::label('floor_number', 'Floor Number', ['class' => 'col-md-6 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('floor_number', $property->floor_number, ['class' => 'form-control']) !!}
                            {!! $errors->first('floor_number', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('key_info') ? 'has-error' : ''}}">
                        {!! Form::label('key_info', 'Key\'s Collection', ['class' => 'col-md-6 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('key_info', $property->key_info, ['class' => 'form-control']) !!}
                            {!! $errors->first('key_info', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group {{ $errors->has('youtube_url') ? 'has-error' : ''}}">
                {!! Form::label('youtube_url', 'Youtube ID', ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-9">
                    {!! Form::text('youtube_url', $property->youtube_url, ['class' => 'form-control'])
                    !!}
                    {!! $errors->first('youtube_url', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('built_year') ? 'has-error' : ''}}">
                        {!! Form::label('built_year', 'Build Year', ['class' => 'col-md-6 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::number('built_year', $property->built_year, ['class' => 'form-control', 'min' => 0]) !!}
                            {!! $errors->first('built_year', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('furnished') ? 'has-error' : ''}}">
                        {!! Form::label('furnished', 'Furnished', ['class' => 'col-md-9 control-label']) !!}
                        <div class="col-md-3">
                            {!! Form::checkbox('furnished', 1, $property->furnished == 1 ? true : false, ['class' =>
                            'form-control']) !!}
                            {!! $errors->first('furnished', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('bedroom') ? 'has-error' : ''}}">
                        {!! Form::label('bedroom', 'Bedrooms', ['class' => 'col-md-6 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::number('bedroom', $property->bedroom, ['class' => 'form-control', 'min' => 0]) !!}
                            {!! $errors->first('Bedroom', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('bathroom') ? 'has-error' : ''}}">
                        {!! Form::label('bathroom', 'Bathrooms', ['class' => 'col-md-6 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('bathroom', $property->bathroom, ['class' => 'form-control']) !!}
                            {!! $errors->first('Bathroom', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group {{ $errors->has('availability') ? 'has-error' : ''}}">
                {!! Form::label('availability', 'Availability', ['class' => 'col-md-3 control-label']) !!}
                <div class="col-md-9">
                    {!! Form::text('availability', $property->availability, ['class' => 'form-control']) !!}
                    {!! $errors->first('availability', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('indoor_area') ? 'has-error' : ''}}">
                        {!! Form::label('indoor_area', 'Indoor Area', ['class' => 'col-md-6 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::number('indoor_area', $property->indoor_area, ['class' => 'form-control', 'min' => 0]) !!}
                            {!! $errors->first('indoor_area', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('outdoor_area') ? 'has-error' : ''}}">
                        {!! Form::label('outdoor_area', 'Outdoor Area', ['class' => 'col-md-6 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::number('outdoor_area', $property->outdoor_area, ['class' => 'form-control', 'min' => 0]) !!}
                            {!! $errors->first('outdoor_area', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    {{--
    <div class="row">--}}
        <p> Facilities</p>
        @foreach($facilities as $facility)
        <div class="col-xs-6 col-md-4 mb20px p0">
            <label class="label-new">
                {!! Form::checkbox('facility_' . $facility['id'], $facility['id'], $facility['checked'] , ['class' =>
                'form-control']) !!}
                {{ HTML::image(url('/images/facility/' . $facility['image'] . '?' . mt_rand(10,100)), null,
                ['style' => 'display: inline-block ' , 'id' => 'image', 'height' => '60'] )}}
                <span>{{$facility['facilityName']['name']}}</span>
            </label>
        </div>
        @endforeach
        {{--
    </div>
    --}}
</div>

<div class="col-md-12">
    {{--
    <div class="row">--}}
        <p> Transport</p>
        @foreach($transports as $transport)
        <div class="col-xs-6 col-md-4 mb20px p0">
            <label class="label-new">
                {!! Form::checkbox('transport_' . $transport['id'], $transport['id'], $transport['checked'], ['class' =>
                'form-control']) !!}
                {{ HTML::image(url('/images/transport/' . $transport['image'] . '?' . mt_rand(10,100)), null,
                ['style' => 'display: inline-block ' , 'id' => 'image', 'height' => '60'] )}}
                <span>{{$transport['transportName']['name']}}</span>
            </label>
        </div>
        @endforeach
        {{--
    </div>
    --}}
</div>
<div class="col-md-12">
    <p> Sales Information</p>

    <div class="form-group {{ $errors->has('to_sale') ? 'has-error' : ''}}">
        {!! Form::label('to_sale', 'For Sale', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::checkbox('to_sale', 1, $property->to_sale == 1 ? true : false , ['class' => 'form-control', 'id'
            => 'to_sale', 'onclick' => 'pageActions.showHideSaleProperty()'])
            !!}
            {!! $errors->first('to_sale', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('is_sold') ? 'has-error' : ''}} to-sale">
        {!! Form::label('is_sold', 'Sold out', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::checkbox('is_sold', 1, $property->is_sold == 1 ? true : false , ['class' => 'form-control'])
            !!}
            {!! $errors->first('is_sold', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group {{ $errors->has('sale_price') ? 'has-error' : ''}} to-sale">
        {!! Form::label('sale_price', 'Sale Price', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::number('sale_price', $property->sale_price, ['class' => 'form-control', 'min' => 0]) !!}
            {!! $errors->first('sale_price', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('common_fee') ? 'has-error' : ''}} to-sale">
        {!! Form::label('common_fee', 'Common Fee/sqm', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::text('common_fee', $property->common_fee ? $property->common_fee : 0, ['class' => 'form-control'])
            !!}
            {!! $errors->first('common_fee', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="col-md-12">
    <p> Rent Information</p>

    <div class="form-group {{ $errors->has('to_rent') ? 'has-error' : ''}}">
        {!! Form::label('to_rent', 'For Rent', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::checkbox('to_rent', 1, $property->to_rent == 1 ? true : false , ['class' => 'form-control',
            'onclick' => 'pageActions.showHideRentProperty()'])
            !!}
            {!! $errors->first('to_rent', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('is_rent') ? 'has-error' : ''}} to-rent">
        {!! Form::label('is_rent', 'Rent out', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::checkbox('is_rent', 1, $property->is_rent == 1 ? true : false , ['class' => 'form-control'])
            !!}
            {!! $errors->first('is_rent', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('rent_month') ? 'has-error' : ''}} to-rent">
        {!! Form::label('rent_month', 'Rent for month', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::number('rent_month', $property->rent_month, ['class' => 'form-control', 'min' => 0, 'max' => '999999999']) !!}
            {!! $errors->first('rent_month', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('rent_week') ? 'has-error' : ''}} to-rent">
        {!! Form::label('rent_week', 'Rent for week', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::number('rent_week', $property->rent_week, ['class' => 'form-control', 'min' => 0, 'max' => '999999999']) !!}
            {!! $errors->first('rent_week', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('rent_day') ? 'has-error' : ''}} to-rent">
        {!! Form::label('rent_day', 'Rent for day', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::number('rent_day', $property->rent_week, ['class' => 'form-control', 'min' => 0, 'max' => '999999999']) !!}
            {!! $errors->first('rent_day', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-2">
        {!! Form::submit( 'Update', ['class' => 'btn btn-primary', 'id' => 'submit-btn']) !!}
    </div>
</div>


<script>
    var lang = <?php echo isset($languages) ? json_encode($languages) : json_encode([]) ?>;

    $(function () {
        $("#tabs").tabs({
            collapsible: true
        });
        googleMap.initMap();
        $('#metro-block').hide();
        pageActions.isSelectedMetro();
        pageActions.chaneDescriptionTabColor();

        initCkeditor()
    });

    function initCkeditor() {
      if (Object.keys(lang).length > 0) {
        for (var i = 0; i < Object.keys(lang).length; i++) {
          CKEDITOR.replace('description-'+ Object.keys(lang)[i] +'-detail',{
            language: 'en',
            uiColor: '#E7E7E7',
            toolbar: [
              { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
              { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
              { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting' ] },
              { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
              { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
              { name: 'tools', items: [ 'Maximize' ] },
              { name: 'editing', items: [ 'Scayt' ] }
            ],
          });
        }
      }
    }

    var pageActions = {
        isShowSaleProperty: true,
        isShowRentProperty: true,
        isShowGallery: false,
        locationsData: <?php echo json_encode($locationsData)?>,
        metroData: <?php echo json_encode($metroData)?>,
        selectedMetro: <?php echo $property->metro_id?>,
        translations: <?php echo json_encode($propertyTranslations)?>,
        languages: <?php echo json_encode($languages)?>,
        showHideSaleProperty: function () {
            if (this.isShowSaleProperty) {
                $('.to-sale').show(300);
                this.isShowSaleProperty = false;
            } else {
                $(".to-sale").hide(300);
                this.isShowSaleProperty = true;
            }
        },

        showHideRentProperty: function () {
            if (this.isShowRentProperty) {
                $('.to-rent').show(300);
                this.isShowRentProperty = false;
            } else {
                $(".to-rent").hide(300);
                this.isShowRentProperty = true;
            }
        },

        showGallery: function () {
            if (this.isShowGallery) {
                $('#gallery-block').show(300);
                this.isShowGallery = false;
            } else {
                $('#gallery-block').hide(300);
                this.isShowGallery = true;
            }
        },

        previewImage: function (file) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#search-image').attr('src', e.target.result).css({display: 'block'});
                $('#image-for-search-name-block').show(300);
            };
            reader.readAsDataURL(file);
        },

        getMainParentLocation: function (locationId) {
            var result = '';
            for (var i = 0; i < this.locationsData.length; i++) {
                if (this.locationsData[i].id == locationId) {
                    if (this.locationsData[i]['parent_id'] != 0) {
                        result = this.getMainParentLocation(this.locationsData[i]['parent_id'])
                    } else {
                        result = this.locationsData[i]['id']
                    }
                    break;
                }
            }
            return result;
        },

        selectedLocation: function (locationId) {
            var checkArr = [];
            var metrosOptions = '<option value="0">Select Metro Station</option>';
            $('#metro_id').find('option').remove();
            locationId = pageActions.getMainParentLocation(locationId);
            for (var i = 0; i < pageActions.metroData.length; i++) {
                if (pageActions.metroData[i]['locationId'] == locationId) {
                    if (pageActions.metroData[i]['id'] == pageActions.selectedMetro) {
                        metrosOptions += '<option value="' + pageActions.metroData[i].id + '" selected>' + pageActions.metroData[i].name + '</option>';
                    } else {
                        metrosOptions += '<option value="' + pageActions.metroData[i].id + '">' + pageActions.metroData[i].name + '</option>';
                    }
                    checkArr.push(pageActions.metroData[i]['locationId'])
                }
            }
            if (checkArr.length > 0) {
                $('#metro-block').show(300);
                $('#metro_id').append(metrosOptions)
            } else {
                $('#metro-block').hide(300);
            }
        },
        isSelectedMetro: function () {
            var selectedLocation = +$('#location_id').val();
            this.selectedLocation(selectedLocation);
        },
        chaneDescriptionTabColor: function () {
            for(var i in pageActions.languages){
                if(!pageActions.translations[i]){
                    $('#tr-tab-li-' + i).css("border", "2px solid red");
                } else if(!pageActions.translations[i].subject ) {
                    $('#tr-tab-li-' + i).css("border", "2px solid red");
                }
            }
        }
    };

    var googleMap = {
        map: null,
        marker: null,
        defaultCoordinates: null,
        coordinates: {
            lat: <?php echo $property->lat ? $property->lat : 0 ?>,
            lng: <?php echo $property->lng ? $property->lng : 0 ?>
        },

        initMap: function () {
            this.defaultCoordinates = window.DEFAULT_COORD;
            if (document.getElementById('map')) {
                if (!this.coordinates.lat && !this.coordinates.lng) {
                    this.coordinates = this.defaultCoordinates
                } else {
                    $('#lat').val(this.coordinates.lat)
                    $('#lng').val(this.coordinates.lng)
                }
                this.map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 10,
                    center: this.coordinates
                });
                this.map.addListener('click', function (e) {

                    $.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + e.latLng.lat() + ',' + e.latLng.lng() + '&key=' + GOOGLE_KEY, function (response) {
                        if (response.results) {
                            googleMap.addMarker({lat: e.latLng.lat(), lng: e.latLng.lng()});
                            $('#address').val(response.results[0].formatted_address)
                        } else {
                            console.log('Noting to found')
                            return false;
                        }
                    })
                });
                this.marker = new google.maps.Marker({
                    position: this.coordinates,
                    map: this.map
                });
            }
        },

        addMarker: function (latLng) {
            this.map.setCenter(latLng);
            this.marker.setMap(null);
            var marker = new google.maps.Marker({
                position: latLng,
                title: "Location"
            });
            marker.setMap(this.map);
            this.marker = marker;
            $('#lat').val(latLng.lat);
            $('#lng').val(latLng.lng);
        },

        getLatLong: function () {
            var address = $('#address').val();
            $.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + GOOGLE_KEY, function (response) {
                if (response.results) {
                    googleMap.addMarker(response.results[0].geometry.location);
                    return false;
                } else {
                    console.log('Noting to found');
                    return false;
                }
            })
        }
    };

    var toSale = <?php echo ($property['to_sale'] ? true : '\'\'');?>;
    if (toSale) {
        $(".to-sale").show();
        pageActions.isShowSaleProperty = false;
    } else {
        $(".to-sale").hide();
        pageActions.isShowSaleProperty = true;
    }
    var toRent = <?php echo ($property['to_rent'] ? true : '\'\'');?>;
    if (toRent) {
        $(".to-rent").show()
        pageActions.isShowRentProperty = false;
    } else {
        $(".to-rent").hide();
        pageActions.isShowRentProperty = true;
    }

</script>