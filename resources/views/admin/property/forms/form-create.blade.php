<div class="col-md-12">
    <div class="row">
        <div class="form-group {{ $errors->has('publish_date') ? 'has-error' : ''}}">
            {!! Form::label('publish_date', 'Publish Date', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('publish_date', null, ['class' => 'form-control', 'id' => 'datepicker']) !!}
                {!! $errors->first('publish_date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
            {!! Form::label('status', 'Status:', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                <div class="checkbox col-md-2">
                    <label>Published {!! Form::radio('status', 'published', true) !!} </label>
                </div>
                <div class="checkbox col-md-2">
                    <label>Disabled {!! Form::radio('status', 'disabled', false)!!}</label>
                </div>
                <div class="checkbox col-md-2">
                    <label>Pending {!! Form::radio('status', 'pending', false) !!}</label>
                </div>
                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

    </div>
</div>
<!--Contact/Owner-->
<div class="col-md-12">
    <div class="row">
        <p> Owner Contact</p>

        <div class="form-group {{ $errors->has('contact_name') ? 'has-error' : ''}}">
            {!! Form::label('contact_name', 'Name', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
            {!! Form::label('contact_email', 'Email', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_email', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_phone') ? 'has-error' : ''}}">
            {!! Form::label('contact_phone', 'Phone', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_phone', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_phone', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_mobile') ? 'has-error' : ''}}">
            {!! Form::label('contact_mobile', 'Mobile', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_mobile', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_mobile', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('contact_viber') ? 'has-error' : ''}}">
            {!! Form::label('contact_viber', 'Viber', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_viber', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_viber', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_line') ? 'has-error' : ''}}">
            {!! Form::label('contact_line', 'Line', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_line', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_line', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('contact_whatsapp') ? 'has-error' : ''}}">
            {!! Form::label('contact_whatsapp', 'Whatsapp', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('contact_whatsapp', null, ['class' => 'form-control']) !!}
                {!! $errors->first('contact_whatsapp', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="row">
        <p> Description</p>

        <div class="form-group {{ $errors->has('location_id') ? 'has-error' : ''}}">
            {!! Form::label('location_id', 'Locations', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('location_id', $locations, null,
                ['class' => 'form-control', 'placeholder' => 'Select City', 'onchange' =>
                'pageActions.selectedLocation(this.value)']) !!}
                {!! $errors->first('location_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('metro_id') ? 'has-error' : ''}}" id="metro-block">
            {!! Form::label('metro_id', 'Metro', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                <select name="metro_id" class="form-control" id="metro_id"></select>
                {!! $errors->first('metro_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
            {!! Form::label('category_id', 'Categories', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('category_id', $categories, null,
                ['class' => 'form-control', 'placeholder' => 'Select Category']) !!}
                {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div id="tabs">
            <ul>
                @foreach($languages as $key => $language)
                <li><a href="#{{$key}}">{{$language}}</a></li>
                @endforeach
            </ul>

            @foreach($languages as $key => $language)
            <div id="{{$key}}">
                <div class="form-group {{ $errors->has('description-' . $key . '-subject') ? 'has-error' : ''}}">
                    {!! Form::label('description-' . $key . '-subject', 'Subject', ['class' => 'col-md-2
                    control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::text('description-' . $key . '-subject', null, ['class' => 'form-control', 'maxlength'
                        => '250'])!!}
                        {!! $errors->first('description-' . $key . '-subject', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('description-' . $key . '-detail') ? 'has-error' : ''}}">
                    {!! Form::label('description-' . $key . '-detail', 'Description', ['class' => 'col-md-2
                    control-label']) !!}
                    <div class="col-md-10">
                        {!! Form::textarea('description-' . $key . '-detail', null, ['class' => 'form-control',
                        'maxlength' => '100000']) !!}
                        {!! $errors->first('description-' . $key . '-detail', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <br>
        <br>
    </div>
</div>

<div class="col-md-12">
    <div class="row">
        <p> Property Address</p>

        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
            {!! Form::label('address', 'Address', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10 input-group p15--l-r">
                {!! Form::text('address', null, ['class' => 'form-control col-md-3']) !!}
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" onclick="googleMap.getLatLong()">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div id="map" style="width: 100%; height: 300px"></div>
        <br>

        <div class="form-group {{ $errors->has('lat') ? 'has-error' : ''}}">
            {!! Form::label('lat', 'Latitude', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::number('lat', 0, ['class' => 'form-control', 'step' => 'any', 'id' => 'lat']) !!}
                {!! $errors->first('lat', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('lng') ? 'has-error' : ''}}">
            {!! Form::label('lng', 'Longitude', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::number('lng', 0, ['class' => 'form-control', 'step' => 'any', 'id' => 'lng']) !!}
                {!! $errors->first('lng', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="row">
        <p> Property Details</p>

        <div class="form-group {{ $errors->has('building_name') ? 'has-error' : ''}}">
            {!! Form::label('building_name', 'Building Name', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('building_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('building_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('address_1') ? 'has-error' : ''}}">
            {!! Form::label('address_1', 'Property Address', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('address_1', '', ['class' => 'form-control'])!!}
                {!! $errors->first('address_1', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('unit_number') ? 'has-error' : ''}}">
            {!! Form::label('unit_number', 'Unit Number', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('unit_number', null, ['class' => 'form-control']) !!}
                {!! $errors->first('unit_number', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('floor_number') ? 'has-error' : ''}}">
            {!! Form::label('floor_number', 'Floor Number', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('floor_number', null, ['class' => 'form-control']) !!}
                {!! $errors->first('floor_number', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('key_info') ? 'has-error' : ''}}">
            {!! Form::label('key_info', 'Key\'s Collection', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('key_info', null, ['class' => 'form-control']) !!}
                {!! $errors->first('key_info', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('availability') ? 'has-error' : ''}}">
            {!! Form::label('availability', 'Availability', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('availability', null, ['class' => 'form-control']) !!}
                {!! $errors->first('availability', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('built_year') ? 'has-error' : ''}}">
            {!! Form::label('built_year', 'Build Year', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::number('built_year', 0, ['class' => 'form-control', 'min' => 0]) !!}
                {!! $errors->first('built_year', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('indoor_area') ? 'has-error' : ''}}">
            {!! Form::label('indoor_area', 'Indoor Area', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('indoor_area', '', ['class' => 'form-control']) !!}
                {!! $errors->first('indoor_area', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('outdoor_area') ? 'has-error' : ''}}">
            {!! Form::label('outdoor_area', 'Outdoor Area', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('outdoor_area', 0, ['class' => 'form-control']) !!}
                {!! $errors->first('outdoor_area', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('bedroom') ? 'has-error' : ''}}">
            {!! Form::label('bedroom', 'Number of bedrooms', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::number('bedroom', '', ['class' => 'form-control', 'min' => 0]) !!}
                {!! $errors->first('bedroom', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('bathroom') ? 'has-error' : ''}}">
            {!! Form::label('bathroom', 'Number of bathrooms', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('bathroom', '', ['class' => 'form-control']) !!}
                {!! $errors->first('bathroom', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('youtube_url') ? 'has-error' : ''}}">
            {!! Form::label('youtube_url', 'Youtube ID', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('youtube_url', '', ['class' => 'form-control'])!!}
                {!! $errors->first('youtube_url', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('furnished') ? 'has-error' : ''}}">
            {!! Form::label('furnished', 'Furnished', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::checkbox('furnished', 1, false, ['class' =>
                'form-control']) !!}
                {!! $errors->first('furnished', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="row">
        <p> Facilities</p>
        @foreach($facilities as $facility)
        <div class="col-xs-6 col-md-4 mb20px p0">
            <label class="label-new">
                {!! Form::checkbox('facility_' . $facility['id'], $facility['id'], $facility['checked'] , ['class' =>
                'form-control']) !!}
                {{ HTML::image(url('/images/facility/' . $facility['image'] . '?' . mt_rand(10,100)), null,
                ['style' => 'display: inline-block ' , 'id' => 'image', 'height' => '60'] )}}
                <span>{{$facility['facilityName']['name']}}</span>
            </label>
        </div>
        @endforeach
    </div>
</div>
<div class="col-md-12">
    <div class="row">
        <p> Transport</p>
        @foreach($transports as $transport)
        <div class="col-xs-6 col-md-4 mb20px p0">
            <label class="label-new">
                {!! Form::checkbox('transport_' . $transport['id'], $transport['id'], $transport['checked'], ['class' =>
                'form-control']) !!}
                {{ HTML::image(url('/images/transport/' . $transport['image'] . '?' . mt_rand(10,100)), null,
                ['style' => 'display: inline-block ' , 'id' => 'image', 'height' => '60'] )}}
                <span>{{$transport['transportName']['name']}}</span>
            </label>
        </div>
        @endforeach
    </div>
</div>
<div class="col-md-12">
    <!--For Sales-->
    <div class="row">
        <div class="form-group {{ $errors->has('to_sale') ? 'has-error' : ''}}">
            {!! Form::label('to_sale', 'For Sale', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::checkbox('to_sale', 1, false , ['class' => 'form-control', 'onchange' =>
                'pageActions.showHideSaleProperty()', 'id' => 'to_sale'])
                !!}
                {!! $errors->first('to_sale', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('sale_price') ? 'has-error' : ''}} sale-price">
            {!! Form::label('sale_price', 'Sale Price', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::number('sale_price', 0, ['class' => 'form-control', 'min' => 0, 'max' => '999999999']) !!}
                {!! $errors->first('sale_price', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('common_fee') ? 'has-error' : ''}} sale-price">
            {!! Form::label('common_fee', 'Common Fee/sqm', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('common_fee', 0, ['class' => 'form-control']) !!}
                {!! $errors->first('common_fee', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <!--For Rent-->
        <div class="form-group {{ $errors->has('to_rent') ? 'has-error' : ''}}">
            {!! Form::label('to_rent', 'For Rent', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::checkbox('to_rent', 1, false , ['class' => 'form-control', 'onchange' =>
                'pageActions.showHideRentProperty()', 'id' => 'to_rent'])
                !!}
                {!! $errors->first('to_rent', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('rent_day') ? 'has-error' : ''}} rent-price">
            {!! Form::label('rent_day', 'Rent Price/Day', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::number('rent_day', 0, ['class' => 'form-control', 'min' => 0, 'max' => '999999999']) !!}
                {!! $errors->first('rent_day', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('rent_week') ? 'has-error' : ''}} rent-price">
            {!! Form::label('rent_week', 'Rent Price/Week', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::number('rent_week', 0, ['class' => 'form-control', 'min' => 0, 'max' => '999999999']) !!}
                {!! $errors->first('rent_week', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('rent_month') ? 'has-error' : ''}} rent-price">
            {!! Form::label('rent_month', 'Rent Price/Month ', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::number('rent_month', 0, ['class' => 'form-control', 'min' => 0, 'max' => '999999999']) !!}
                {!! $errors->first('rent_month', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<input name="images" type="hidden" id="images" value="[]"/>
<div class="col-md-12">
    <div class="row">

        <p>Gallery
            <button type="button" class="btn btn-success btn-xs" id="upload-images">Upload Images</button>
        </p>
        <div>
            @include ('admin.property.image.for-create')
        </div>
    </div>
</div>
<br>
<br>
<div class="form-group">
    <div class="col-md-offset-2">
        {!! Form::submit('Create', ['class' => 'btn btn-primary', 'id' => 'submit-btn']) !!}
    </div>
</div>


<script>
  var lang = <?php echo isset($languages) ? json_encode($languages) : json_encode([]) ?>;

    $(function () {
        $("#tabs").tabs({
            collapsible: true
        });
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", new Date());
        $(".sale-price").hide();
        $(".rent-price").hide();
        googleMap.initMap();
        $('#metro-block').hide();

        initCkeditor()
    });

  function initCkeditor() {
    if (Object.keys(lang).length > 0) {
      for (var i = 0; i < Object.keys(lang).length; i++) {
        CKEDITOR.replace('description-'+ Object.keys(lang)[i] +'-detail',{
          language: 'en',
          uiColor: '#E7E7E7',
          toolbar: [
            { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
            { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting' ] },
            { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
            { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
            { name: 'tools', items: [ 'Maximize' ] },
            { name: 'editing', items: [ 'Scayt' ] }
          ],
        });
      }
    }
  }

    var pageActions = {
        isShowSaleProperty: true,
        isShowRentProperty: true,
        locationsData: <?php echo json_encode($locationsData)?>,
        metroData: <?php echo json_encode($metroData)?>,
        showHideSaleProperty: function () {
            if (this.isShowSaleProperty) {
                $('.sale-price').show(300);
                this.isShowSaleProperty = false;
            } else {
                $(".sale-price").hide(300);
                this.isShowSaleProperty = true;
            }
        },

        showHideRentProperty: function () {
            if (this.isShowRentProperty) {
                $('.rent-price').show(300);
                this.isShowRentProperty = false;
            } else {
                $(".rent-price").hide(300);
                this.isShowRentProperty = true;
            }
        },

        previewImage: function (file) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#search-image').attr('src', e.target.result).css({display: 'block'});
                $('#image-for-search-name-block').show(300);
            };
            reader.readAsDataURL(file);
        },

        getMainParentLocation: function (locationId) {
            var result = '';
            for (var i = 0; i < this.locationsData.length; i++) {
                if (this.locationsData[i].id == locationId) {
                    if (this.locationsData[i]['parent_id'] != 0) {
                         result = this.getMainParentLocation(this.locationsData[i]['parent_id'])
                    } else {
                        result = this.locationsData[i]['id']
                    }
                    break;
                }
            }
            return result;
        },

        selectedLocation: function (locationId) {
            var checkArr = [];
            var metrosOptions = '<option value="" disabled selected>Select Metro Station</option>';
            $('#metro_id').find('option').remove();
            locationId = pageActions.getMainParentLocation(locationId);
            for (var i = 0; i < pageActions.metroData.length; i++) {
                if (pageActions.metroData[i]['locationId'] == locationId) {
                    metrosOptions += '<option value="' + pageActions.metroData[i].id + '">' + pageActions.metroData[i].name + '</option>';
                    checkArr.push(pageActions.metroData[i]['locationId'])
                }
            }
            if (checkArr.length > 0) {
                $('#metro-block').show(300);
                $('#metro_id').append(metrosOptions)
            } else {
                $('#metro-block').hide(300);
            }
        }
    };

    var googleMap = {
        map: null,
        marker: null,
        defaultCoordinates: null,
        coordinates: {
            lat: null,
            lng: null
        },

        initMap: function () {
            this.defaultCoordinates = window.DEFAULT_COORD;
            if (document.getElementById('map')) {
                if (!this.coordinates.lat && !this.coordinates.lng) {
                    this.coordinates = this.defaultCoordinates
                } else {
                    $('#lat').val(this.coordinates.lat)
                    $('#lng').val(this.coordinates.lng)
                }


                this.map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 10,
                    center: this.coordinates
                });

                this.map.addListener('click', function (e) {

                    $.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + e.latLng.lat() + ',' + e.latLng.lng() + '&key=' + GOOGLE_KEY, function (response) {
                        if (response.results) {
                            googleMap.addMarker({lat: e.latLng.lat(), lng: e.latLng.lng()});
                            $('#address').val(response.results[0].formatted_address)
                        } else {
                            console.log('Noting to found')
                            return false;
                        }
                    })
                });

                this.marker = new google.maps.Marker({
                    position: this.coordinates,
                    map: this.map
                });
            }
        },

        addMarker: function (latLng) {
            this.map.setCenter(latLng);
            this.marker.setMap(null);
            var marker = new google.maps.Marker({
                position: latLng,
                title: "Location"
            });
            marker.setMap(this.map);
            this.marker = marker;
            $('#lat').val(latLng.lat);
            $('#lng').val(latLng.lng);
        },

        getLatLong: function () {
            var address = $('#address').val();
            $.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + GOOGLE_KEY, function (response) {
                if (response.results) {
                    googleMap.addMarker(response.results[0].geometry.location)
                    return false;
                } else {
                    console.log('Noting to found')
                    return false;
                }
            })
        }
    }
</script>