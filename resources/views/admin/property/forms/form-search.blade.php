<!--<div class="input-group">
    <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
</div>-->

<div class="col-md-12">
    <div class="row panel panel-default">
        <p>Filter
            <a href="{{ url('/admin/property') }}" class="btn btn-success btn-sm" title="Back"
               id="back-to-properties">
                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back To All Properties
            </a>
        </p>

        <div class="form-group panel-body" id="search-block">

            <div class="col-md-4">
                {!! Form::label('code', 'Reference', ['class' => 'col-md-5 control-label', 'id' => 'code']) !!}
                <div class="col-md-7">
                    {!! Form::text('code', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('contact_name', 'Owner', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::text('contact_name', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('building_name', 'Building Name', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::text('building_name', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('category_id', 'Type', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('category_id', $categories, null,
                    ['class' => 'form-control', 'placeholder' => 'Select Category']) !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('location_id', 'City', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('location_id', $locations, null,
                    ['class' => 'form-control', 'placeholder' => 'Select City']) !!}
                </div>
            </div>

            <div class="col-md-4">
                {!! Form::label('bedroom', 'Bedrooms', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::number('bedroom', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-md-4">
                {!! Form::label('status', 'Status', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-7">
                    {!! Form::select('status', ['published' => 'Published', 'disabled' => 'Disabled', 'pending' =>
                    'Pending'],
                    null, ['class' => 'form-control', 'placeholder' => 'Choose status']) !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('to_sale', 'For Sale', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-3">
                    {!! Form::checkbox('to_sale', 1, 0 , ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('to_rent', 'For Rent', ['class' => 'col-md-5 control-label']) !!}
                <div class="col-md-3">
                    {!! Form::checkbox('to_rent', 1, 0 , ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12 text-center">
                <button class="btn btn-sm btn-success" type="submit"> Search</button>
            </div>
        </div>
    </div>
</div>

<script>
//    console.log(window.location.pathname);
    if (window.location.pathname !== '/admin/property/search') {
        $('#back-to-properties').hide();
    }
</script>