@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Property</div>
                <div class="panel-body">
                    <a href="{{ url('/admin/property') }}" title="Back">
                        <button class="btn btn-warning btn-xs" value="arrow left"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif
                    {!! Form::open([
                    'method' => 'PATCH',
                    'url' => ['admin/property', $property->id],
                    'class' => 'form-horizontal',
                    'id' => 'property-form',
                    'onkeypress' => 'if(event.keyCode == 13) return false;',
                    'files' => true])
                    !!}

                    @include ('admin.property.forms.form-edit')

                    {!! Form::close() !!}
                    <div class="col-md-12">
                        <p> Property Gallery
                            <button type="button" class="btn btn-warning btn-xs" onclick="pageActions.showGallery()" value="hide-show gallery">Hide/Show Gallery</button>
                            <button type="button" class="btn btn-success btn-xs" id="submit-all" value="upload images">Upload Images</button>
                        </p>
                    </div>
                    @include ('admin.property.image.for-edit')
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@include ('admin.property.validator')