@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Properties</div>
                <div class="panel-body">
                    <a href="{{ url('/admin/property/create') }}" class="btn btn-success btn-sm" title="Add New User">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Property
                    </a>
                    <br/>
                    <br/>
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/property/search', 'class' => 'form-horizontal',
                    'role' => 'search']) !!}
                    @include ('admin.property.forms.form-search')
                    {!! Form::close() !!}
                    <br/>

                    @if(count($property) > 0)
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <!--<th>Id</th>-->
                                <th>Reference</th>
                                <th>Category</th>
                                <th>Location</th>
                                <th>Contact name</th>
                                <th>Visible</th>
                                <th>Updated at</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($property as $item)
                            <tr>
                                <!--<td>{{ $item->id }}</td>-->
                                <td>{{ $item->code}}</td>
                                <td>{{ isset($item->categoryNameInEn[0]) ? $item->categoryNameInEn[0]->name : ''}}
                                </td>
                                <td>{{ isset($item->locationNameInEn[0]) ? $item->locationNameInEn[0]->name : ''}}</td>
                                <td>{{ $item->contact_name }}</td>
                                <td>
                                    @foreach($item->translations as $translation)
                                    @if($translation->subject && isset($translation) && isset($translation->lang[0]))
                                    <img src="/images/language/{{$translation->lang[0]->image}}" alt="">
                                    @endif
                                    @endforeach
                                </td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    <a href="{{ url('/admin/property/' . $item->id . '/edit') }}" title="Edit Property">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                        </button>
                                    </a>
                                    @if($canDelete)
                                    {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/property', $item->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Property',
                                    'onclick'=>'return confirm("Are you sure you want to delete this Property?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $property->appends(['search' =>
                            Request::get('search')])->render() !!}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
