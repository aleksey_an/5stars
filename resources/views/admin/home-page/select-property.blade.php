@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Add Property to Home Page</div>
                <div class="properties-block">
                    @if(isset($selectedProperties) && count($selectedProperties) > 0)
                    @foreach($selectedProperties as $property)
                    @include('admin.components.property-select-home')
                    @endforeach
                    @endif
                </div>
                <div class="panel-body">
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/edit-home-page/select-property/search', 'class'
                    => 'form-horizontal',
                    'role' => 'search']) !!}
                    @include ('admin.home-page.form-search')
                    {!! Form::close() !!}
                </div>
                @if(isset($properties) && count($properties) > 0)
                <div class="table-responsive">
                    <table class="table table-borderless">
                        <thead>
                        <tr>
                            <!--<th>Id</th>-->
                            <th>Reference</th>
                            <th>Category</th>
                            <th>Location</th>
                            <th>Contact name</th>
                            <th>Visible</th>
                            <th>Add</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($properties as $item)
                        <tr>
                            <!--<td>{{ $item->id }}</td>-->
                            <td>{{ $item->code}}</td>
                            <td>{{ isset($item->categoryNameInEn[0]) ? $item->categoryNameInEn[0]->name : ''}}
                            </td>
                            <td>{{ isset($item->locationNameInEn[0]) ? $item->locationNameInEn[0]->name : ''}}</td>
                            <td>{{ $item->contact_name }}</td>
                            <td>
                                @foreach($item->translations as $translation)
                                @if($translation->subject && isset($translation->lang[0]))
                                <img src="/images/language/{{$translation->lang[0]->image}}" alt="">
                                @endif
                                @endforeach
                            </td>
                            <td>
                                {!! Form::checkbox('property-' . $item->id, $item->id, $item->isHomePage,
                                ['onclick' => 'selectProperty.addRemoveProperty(this.value, this.checked)',
                                'class' => 'control-ajax', 'id' => 'checkbox-for-property-' . $item->id]) !!}
                                <img src="/img/form-preloader.gif" width="15" style="display: none" class="preloader">
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper"> {!! $properties->appends(['search' =>
                        Request::get('search')])->render() !!}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        selectProperty.countProperties = '<?php echo count($selectedProperties)?>'
    });

    var selectProperty = {
        limit: '{{env("MAX_COUNT_PROPERTY_ON_HOME_PAGE")}}',
        countProperties: null,
        addRemoveProperty: function (propertyId, checked) {
            if (checked) {
                this.addProperty(propertyId)
            } else {
                this.deleteProperty(propertyId)
            }
        },

        addProperty: function (id) {
            console.log(this.countProperties, this.limit)
            if (+this.countProperties < +this.limit) {
                selectProperty.showPreloader();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'post',
                    url: "{!! url('/admin/edit-home-page/add-property') !!}",
                    data: {property: id},
                    success: function (data) {
                        $('.properties-block').append(data);
                        selectProperty.countProperties++;
                        selectProperty.hidePreloader();
                    },
                    error: function (err) {
                        if (document.getElementById('checkbox-for-property-' + id))
                            document.getElementById('checkbox-for-property-' + id).checked = false;
                        console.error(err)
                        selectProperty.hidePreloader();
                    }
                })
            } else {
                if (document.getElementById('checkbox-for-property-' + id))
                    document.getElementById('checkbox-for-property-' + id).checked = false;
                alert('The limit of properties displayed on the home page is ' + this.limit)
            }
        },

        deleteProperty: function (id) {
            selectProperty.showPreloader();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'post',
                url: "{!! url('/admin/edit-home-page/delete-property') !!}",
                dataType: 'json',
                data: {property: id},
                success: function (data) {
                    selectProperty.hidePreloader();
                    selectProperty.countProperties--;
                    if (document.getElementById('checkbox-for-property-' + id))
                        document.getElementById('checkbox-for-property-' + id).checked = false;
                    $('#selected-property-block-' + id).remove();
                },
                error: function (err) {
                    selectProperty.hidePreloader();
                    console.error(err)
                }
            })
        },

        showPreloader: function () {
            $('.control-ajax').css({'display': 'none'});
            $('.preloader').css({'display': 'block'});
        },

        hidePreloader: function () {
            $('.control-ajax').css({'display': 'block'});
            $('.preloader').css({'display': 'none'});
        }
    }
</script>
@endsection
