@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Upload Images</div>
                <div class="panel-body">
                    <!--Upload Images-->
                    <div id="gallery-block">
                        <div id="mydz" class="dropzone">
                            <button id="submit-all" type="button" class="btn btn-sm btn-success">Upload</button>
                        </div>
                        <br>

                        <div class="table-responsive">
                            <ul id="sortable" class="home-images-list"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    var countImages = 0;
    var imagesArr = JSON.parse('<?php echo isset($images) ?  json_encode($images) : json_encode([]) ?>');
    var pageAction = {
        createLi: function (data) {
            countImages = +countImages + data.length;
            var li = '';
            for (var i = 0; i < data.length; i++) {
                li = '<li id="' + data[i].id + '" class="ui-state-default">' +
                    '<span class="gallery-img"><img src="/images/home/display/' + data[i].name + '" wight="100"></span> ' +
                    '<span class="gallery-del"><button type="button" class="btn btn-danger btn-xs" onclick="pageAction.deleteImage(\'' + data[i].id + '\', \'' + data[i].sequence + '\' , \'' + data[i].name + '\')">Delete </button></span>' +
                    '<img src="/img/form-preloader.gif" alt="" style="display: none" class="del-preloader">' +
                        // '<span>' + data[i].id + '</span>'+
                    '</li>';
                $('#sortable').append(li);
            }
        },

        deleteImage: function (imgId, sequence, name) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var posting = $.post('{{url("/admin/edit-home-page/delete-image")}}', {
                imgId: imgId,
                sequence: sequence,
                name: name
            });
            $('.gallery-del').css({'display': 'none'});
            $('.del-preloader').css({'display': 'block'});
            posting.done(function (data) {
                $('#' + imgId).remove();
                countImages--;
                $('.gallery-del').css({'display': 'block'});
                $('.del-preloader').css({'display': 'none'});
            });
            posting.fail(function (xhr, status, errorThrown) {
                $('.gallery-del').css({'display': 'block'});
                $('.del-preloader').css({'display': 'none'});
                console.error(xhr, status);
            })
            //When creating a location
            for (var i = 0; i < imagesArr.length; i++) {
                if (imagesArr[i].id == imgId) {
                    imagesArr.splice(i, 1)
                }
            }
            $('#images').val(JSON.stringify(imagesArr));
        }
    };

    document.addEventListener('DOMContentLoaded', function () {
        var totalDataTransfer = 0;
        var limitTotalDataTransfer = '<?php echo ( +substr(ini_get("post_max_size"), 0, -1 ) * 1000000) ?>'; //(Bytes)
        $("#mydz").dropzone({
            responseData: null,
            url: "{!! url('/admin/edit-home-page/upload-images/') !!}/",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            paramName: "file",
            uploadMultiple: true,
            autoProcessQueue: false,
            parallelUploads: 100,
            maxFilesize: 10,
            maxFiles: 10,
            addRemoveLinks: true,
            dictRemoveFile: 'Remove',
            dictFileTooBig: 'Image is bigger than 10MB',
            acceptedFiles: ".jpeg,.jpg,.png",
            success: function (file, response) {
                this.removeFile(file);
                this.responseData = response;
            },
            init: function () {
                var _this = this;
                $('#submit-all').click(function () {
                    _this.processQueue();
                });
                this.on("addedfile", function (file) {
                    if (totalDataTransfer + file.size < limitTotalDataTransfer) {
                        totalDataTransfer += file.size
                    } else {
                        this.removeFile(file);
                        alert('The allowed load is ' + limitTotalDataTransfer / 1000000 + 'mb. The total size of the images is ' + (totalDataTransfer + file.size) / 1000000 + ' mb. To load a larger size, increase the value in php_ini');
                    }
                });
                this.on("complete", function (file) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0 && this.responseData && this.responseData.success) {
                        pageAction.createLi(this.responseData.success);
                        totalDataTransfer = 0;
                    }
                });
                this.on("error", function (file, response) {
                    $(file.previewElement).find('.dz-error-message').text('Error loading');
                    totalDataTransfer = 0;
                    console.log(response)
                })
            }
        });
        if (imagesArr.length > 0) {
            pageAction.createLi(imagesArr)
        }
    });
</script>
@endsection
