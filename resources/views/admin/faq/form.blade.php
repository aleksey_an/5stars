<div id="tabs">
    <ul>
        @foreach($languages as $key => $language)
        <li><a href="#{{$key}}">{{$language}}</a></li>
        @endforeach
    </ul>
    @foreach($languages as $key => $language)
    <div id="{{$key}}" class="faq-data-block">
        <div class="form-group {{ $errors->has('trans-' . $key . '-question') ? 'has-error' : ''}}">
            {!! Form::label('description-' . $key . '-question', 'Question', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('description-' . $key . '-question', isset($translations[$key]) ?
                $translations[$key]['question'] : '',
                ['class' => 'form-control', 'maxlength' => '500'])!!}
                {!! $errors->first('description-' . $key . '-question', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('trans-' . $key . '-answer') ? 'has-error' : ''}}">
            {!! Form::label('description-' . $key . '-answer', 'Answer', ['class' => 'col-md-2
            control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('description-' . $key . '-answer', isset($translations[$key]) ?
                $translations[$key]['answer'] : '',
                ['class' => 'form-control', 'maxlength' => '100000']) !!}
                {!! $errors->first('description-' . $key . '-answer', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    @endforeach
</div>


<div class="form-group {{ $errors->has('is_enabled') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled', 'Is Enabled', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '1', true) !!} Yes</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '0') !!} No</label>
        </div>
        {!! $errors->first('is_enabled', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<script>
    var lang = <?php echo isset($languages) ? json_encode($languages) : json_encode([]) ?>;

    document.addEventListener('DOMContentLoaded', function () {
        $("#tabs").tabs({
            collapsible: true
        });

        initCkeditor()
    });

    function initCkeditor() {
        if (Object.keys(lang).length > 0) {
            for (var i = 0; i < Object.keys(lang).length; i++) {
                CKEDITOR.replace('description-'+ Object.keys(lang)[i] +'-answer',{
                    language: 'en',
                    uiColor: '#E7E7E7',
                    toolbar: [
                        { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
                        { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
                        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting' ] },
                        { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
                        { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                        { name: 'tools', items: [ 'Maximize' ] },
                        { name: 'editing', items: [ 'Scayt' ] }
                    ],
                });
            }
        }
    }
</script>