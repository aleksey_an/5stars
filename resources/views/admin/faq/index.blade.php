@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">FAQ questions</div>
                <div class="panel-body">
                    <a href="{{ url('/admin/faq/create') }}" class="btn btn-success btn-sm" title="Add New FAQ Question">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Visible</th>
                                <th>Question</th>
                                <th>Status</th>
                                <th>Updated At</th>
                                <th>Actions</th>
                                <th>Sorting</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($faqs))
                            @foreach($faqs as $item)
                            <tr>
                                <td>
                                    @foreach($item->translations as $translation)
                                    @if($translation->question && isset($translation->lang[0]))
                                    <img src="/images/language/{{$translation->lang[0]->image}}" alt="">
                                    @endif
                                    @endforeach
                                </td>
                                <td style="max-width: 500px;">
                                    {{$item->translations[0]->question}}
                                </td>
                                <td>
                                    @if ($item->is_enabled)
                                    Enabled
                                    @else
                                    Disabled
                                    @endif
                                </td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    <a href="{{ url('/admin/faq/' . $item->id . '/edit') }}" title="Edit Link">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            Edit
                                        </button>
                                    </a>
                                    {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/faq', $item->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Link',
                                    'onclick'=>'return confirm("Confirm delete?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                                <td>
                                    @if (!$loop->first)
                                        <a href="{{ url('/admin/faq/sorting/up/'. $item->id) }}" title="Up">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                    @endif
                                    @if (!$loop->last)
                                        <a href="{{ url('/admin/faq/sorting/down/'. $item->id) }}" title="Down">
                                            <button class="btn btn-primary btn-xs">
                                                <i class="fa fa fa-arrow-down" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.upDownSort').on('click', function(){
                $.post($(this).attr('data-url'));
            });
        });

    </script>
@endpush
