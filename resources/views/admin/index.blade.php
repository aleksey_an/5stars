@extends('layouts.app')
@section('content')

<?php $user = Auth::user();
//var_dump($user->hasRole('admin')) ?>
<div class="container index">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h1>Admin index page</h1>
        <ul class="df fw admin-list">
            <li><a href="{{url('/admin/property')}}" title="Properties"><img src="/img/house.svg" alt="Properties">Properties</a></li>
            @if($UserRoleName == 'super_admin' || $UserRoleName == 'admin')
            <li><a href="{{url('/admin/category')}}" title="Categories"><img src="/img/category.svg" alt="Categories">Categories</a></li>
            <li><a href="{{url('/admin/location')}}" title="Locations"><img src="/img/location.svg" alt="Locations">Locations</a></li>
            <!--<li><a href="{{url('/admin/metro')}}"><img src="/img/metro.svg" alt="">Metro</a></li>-->
            <li><a href="{{url('/admin/facility')}}" title="Facilities"><img src="/img/move-object.svg" alt="Facilities">Facilities</a></li>
            <li><a href="{{url('/admin/transport')}}" title="Main Transports"><img src="/img/transports.svg" alt="Main Transports">Main Transports</a>
            </li>
            <li><a href="{{url('/admin/user/all')}}" title="Edit Users"><img src="/img/edit-user.svg" alt="Edit Users">Edit Users</a></li>
            <li><a href="{{url('/admin/configuration')}}" title="Configuration"><img src="/img/configurations.svg" alt="Configuration">Configuration</a></li>
            <li><a href="{{url('/admin/currency')}}" title="Currency"><img src="/img/currency.svg" alt="Currency">Currency</a></li>
            <li><a href="{{url('/admin/language')}}" title="Edit Languages"><img src="/img/languages.svg" alt="Edit Languages">Edit Languages</a></li>
            <li><a href="{{url('/admin/translation')}}" title="Localization"><img src="/img/localization.svg" alt="Localization">Localization</a></li>
            <li><a href="{{url('/admin/article')}}" title="Blog"><img src="/img/blog.svg" alt="Blog">Blog</a></li>
            <li><a href="{{url('/admin/xml')}}" title="XML"><img src="/img/xml.svg" alt="XML">XML</a></li>
            <li><a href="{{url('/admin/faq')}}" title="XML"><img src="/img/faq.svg" alt="FAQ">FAQ</a></li>
            @endif
            @if($UserRoleName == 'super_admin')
            <li><a href="{{url('/admin/role')}}" title="Edit Roles"><img src="/img/edit-roles.svg" alt="Edit Roles">Edit Roles</a></li>
            <li><a href="{{url('/admin/permission')}}" title="Edit Permissions"><img src="/img/edit-permission.svg" alt="Edit Permision">Edit
                    Permissions</a></li>
            @endif

        </ul>
    </div>
</div>
@endsection
