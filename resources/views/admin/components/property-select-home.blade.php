<div class="selected-property-block" id="selected-property-block-{{$property->id}}">
    <div>
        <button class="btn-danger btn" onclick="selectProperty.deleteProperty('{{$property->id}}')">
            Remove
        </button>
    </div>
    <div>
        @if(isset($property->imageForWebSearch[0]))
        <img src="/images/property/small/{{$property->imageForWebSearch[0]->name}}" alt="">
        @else
        <img src="/img/default.png" alt="">
        @endif
    </div>
    <div>
        <p>{{$property->code}}</p>
        @if(isset($property->translationsInLocale[0]))
        <p>{{substr($property->translationsInLocale[0]->subject, 0, 25)}}
            {{strlen($property->translationsInLocale[0]->subject) > 25 ? ' ...' : ''}}</p>
        @endif
        @if(isset($property->translationsInLocale[0]))
        <p>{{substr($property->translationsInLocale[0]->detail, 0, 100)}}
            {{strlen($property->translationsInLocale[0]->detail) > 100 ? ' ...' : ''}}</p>
        @endif
    </div>
</div>