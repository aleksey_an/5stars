<?php $display = isset($facility['image']) ? 'block' : 'none'; ?>

<div id="tabs">
    <ul>
        @foreach($languages as $key => $language)
        <li><a href="#{{$key}}">{{$language}}</a></li>
        @endforeach
    </ul>
    @foreach($languages as $key => $language)
    <div id="{{$key}}">
        <div class="form-group {{ $errors->has('description-' . $key . '-name') ? 'has-error' : ''}}">
            {!! Form::label('description-' . $key . '-name', 'Name', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('description-' . $key . '-name', isset($translations[$key]) ? $translations[$key]['name'] : '',
                ['class' => 'form-control', 'id' => 'form-name']) !!}
                {!! $errors->first('description-' . $key . '-name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    @endforeach
</div>
<br>

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'Image', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-2">
        {{ HTML::image(url($display == 'block' ? ('/images/facility/' . $facility['image'] . '?' . mt_rand(10,100)) :
        ''), null,
        ['style' => 'display:' . $display , 'id' => 'image', 'height' => '60'] )}}
    </div>

    <div class="col-md-8">
        {!! Form::file('image', ['class' => 'form-control', 'onchange' => 'previewImage(this.files[0])']) !!}
        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('is_enabled') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled', 'Is Enabled', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '1', $facility['is_enabled'] ? true : false ) !!} Yes</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '0', $facility['is_enabled'] ? false : true) !!} No</label>
        </div>
        {!! $errors->first('is_enabled', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        {!! Form::submit(isset($translations) ? 'Edit' : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<script>
    $(function () {
        $("#tabs").tabs({
            collapsible: true
        });
    });
    function previewImage(file) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result).css({display: 'block'});
        };
        reader.readAsDataURL(file);
    }
</script>