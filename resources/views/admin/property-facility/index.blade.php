@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Facility</div>
                <div class="panel-body">
                    <a href="{{ url('admin/facility/create') }}" class="btn btn-success btn-sm"
                       title="Add New Property Location">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Facility
                    </a>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>image</th>
                                <th>Enable</th>
                                <th>Updated At</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($facilities as $item)
                            <tr>
                                <td>{{ $item->id }}</td>

                                <td>{{ isset($item->translations[0]) ? $item->translations[0]->name : ''}}</td>
                                <td>
                                    {{ HTML::image(url(isset($item->image) ? ('/images/facility/' . $item->image .'?' .
                                    mt_rand(10,100)) : ''), null,['style' => 'display:' . isset($item->image) ? 'block'
                                    : 'none' , 'id' => 'image', 'height' => '40'] )}}
                                </td>
                                <td>
                                    @if ($item->is_enabled)
                                    Enabled
                                    @else
                                    Disabled
                                    @endif
                                </td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    <a href="{{ url('/admin/facility/'. $item->id .'/edit') }}" title="Edit Facility">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                        </button>
                                    </a>
                                    {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/admin/facility', $item->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete User',
                                    'onclick'=>'return confirm("Are you sure you want to delete this facility?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
