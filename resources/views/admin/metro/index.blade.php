@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Metro</div>
                <div class="panel-body">

                    <a href="{{ url('admin/metro/create') }}" title="Metro Station">
                        <button class="btn btn-success btn-sm">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </button>
                    </a>

                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Enable</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($metros as $item)
                            <tr>
                                <td>{{ $item->id }}</td>

                                <td>{{ isset($item->translations[0]) ? $item->translations[0]->name : ''}}</td>

                                <td>
                                    @if ($item->is_enabled)
                                    Enabled
                                    @else
                                    Disabled
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('/admin/metro/'. $item->id .'/edit') }}" title="Edit Metro Station">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
