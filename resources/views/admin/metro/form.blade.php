<div id="tabs">
    <ul>
        @foreach($languages as $key => $language)
        <li><a href="#{{$key}}">{{$language}}</a></li>
        @endforeach
    </ul>
    @foreach($languages as $key => $language)
    <div id="{{$key}}">
        <div class="form-group {{ $errors->has('description-' . $key . '-name') ? 'has-error' : ''}}">
            {!! Form::label('description-' . $key . '-subject', 'Name', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('description-' . $key . '-name', isset($translations[$key]) ? $translations[$key]['name']
                : '',
                ['class' => 'form-control', 'maxlength' => '250'])!!}
                {!! $errors->first('description-' . $key . '-name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    @endforeach
</div>
<br>

<div class="form-group {{ $errors->has('location_id') ? 'has-error' : ''}}">
    {!! Form::label('location_id', 'Locations', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::select('location_id', $locations, $metro['location_id'],
        ['class' => 'form-control', 'placeholder' => 'Select City', 'onchange' =>
        'addLocationNameToAddress(this.value)']) !!}
        {!! $errors->first('location_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', 'Address', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10 input-group p15--l-r">
        {!! Form::text('address', $metro['address'], ['class' => 'form-control', 'id' => 'address']) !!}
        <span class="input-group-btn">
            <button class="btn btn-default" type="button" onclick="googleMap.getLatLong()">
                <i class="fa fa-search"></i>
            </button>
        </span>

        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div id="map" style="width: 100%; height: 300px"></div>
<br>
<div class="form-group {{ $errors->has('lat') ? 'has-error' : ''}}">
    {!! Form::label('lat', 'Latitude', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('lat', $metro['latitude'], ['class' => 'form-control', 'step' => 'any', 'id' => 'lat']) !!}
        {!! $errors->first('lat', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('lng') ? 'has-error' : ''}}">
    {!! Form::label('lng', 'Longitude', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('lng', $metro['longitude'], ['class' => 'form-control', 'step' => 'any', 'id' => 'lng']) !!}
        {!! $errors->first('lng', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('is_enabled') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled', 'Is Enabled', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '1', $metro ? $metro['is_enabled'] ? true : false : true ) !!}
                Yes</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '0', $metro ? $metro['is_enabled'] ? false : true : false) !!}
                No</label>
        </div>
        {!! $errors->first('is_enabled', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        {!! Form::submit($metro ? 'Edit' :'Submit', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<script>
    var locationsData = <?php echo json_encode($locations);?>;
    function previewImage(file) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image').attr('src', e.target.result).css({display: 'block'});
        };
        reader.readAsDataURL(file);
    }

    document.addEventListener('DOMContentLoaded', function () {
        $("#tabs").tabs({
            collapsible: true
        });
        if (!<?php echo $metro ? $metro : 0 ?>)
            $('#address').val('');
        googleMap.initMap();
    });

    function addLocationNameToAddress(locationId) {
        $('#address').val(locationsData[locationId] + '  ')
    }

    var googleMap = {
        map: null,
        marker: null,
        defaultCoordinates: null,
        coordinates: {
            lat: <?php echo $metro['lat'] ? $metro['lat'] : 0 ?>,
            lng: <?php echo $metro['lng'] ? $metro['lng'] : 0 ?>
        },

        initMap: function () {
            this.defaultCoordinates = window.DEFAULT_COORD;
            if (document.getElementById('map')) {
                if (!this.coordinates.lat && !this.coordinates.lng) {
                    this.coordinates = this.defaultCoordinates
                } else {
                    $('#lat').val(this.coordinates.lat)
                    $('#lng').val(this.coordinates.lng)
                }


                this.map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 10,
                    center: this.coordinates
                });

                this.map.addListener('click', function (e) {

                    $.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + e.latLng.lat() + ',' + e.latLng.lng() + '&key=' + GOOGLE_KEY, function (response) {
                        if (response.results) {
                            googleMap.addMarker({lat: e.latLng.lat(), lng: e.latLng.lng()});
                            $('#address').val(response.results[0].formatted_address)
                        } else {
                            console.log('Noting to found')
                            return false;
                        }
                    })
                });

                this.marker = new google.maps.Marker({
                    position: this.coordinates,
                    map: this.map
                });
            }
        },

        addMarker: function (latLng) {
            this.map.setCenter(latLng);
            this.marker.setMap(null);
            var marker = new google.maps.Marker({
                position: latLng,
                title: "Location"
            });
            marker.setMap(this.map);
            this.marker = marker;
            $('#lat').val(latLng.lat);
            $('#lng').val(latLng.lng);
        },

        getLatLong: function () {
            var address = $('#address').val()
            $.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + GOOGLE_KEY, function (response) {
                if (response.results) {
                    googleMap.addMarker(response.results[0].geometry.location)
                    return false;
                } else {
                    console.log('Noting to found')
                    return false;
                }
            })
        }
    }

</script>