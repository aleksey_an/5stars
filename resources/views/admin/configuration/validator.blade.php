@push('scripts')
<script>
    $(document).ready(function() {
        $("#configuration-form").validate({
            errorClass: "help-block",
            errorElement: "span",
            rules: {
                value: {
                    required: true,
                    maxlength: 255
                },
                description: {
                    required: true,
                    maxlength: 255
                }

            },
            submitHandler: function(form) {
                form.submit();
            },
            highlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").removeClass('has-error');
            }
        });
    });
</script>
@endpush