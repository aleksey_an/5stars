@push('scripts')
<script>
    $(document).ready(function() {
        $("#user-form").validate({
            errorClass: "help-block",
            errorElement: "span",
            rules: {
                username: {
                    required: true,
                    maxlength: 125
                },
                first_name: {
                    required: true,
                    maxlength: 125
                },
                last_name: {
                    required: true,
                    maxlength: 125
                },
                email: {
                    required: true,
                     email: true
                },
                password: {
                    minlength: 6,
                    maxlength: 125
                },
                password_confirmation: {
                    equalTo: "#password",
                    minlength: 6,
                    maxlength: 125
                }
            },
            submitHandler: function(form) {
                form.submit();
            },
            highlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="'+element.getAttribute('name')+'"]').closest(".form-group, .checkbox").removeClass('has-error');
            }
        });
    });
</script>
@endpush