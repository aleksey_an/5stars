@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>
                <div class="panel-body">
                    <a href="{{ url('/admin/user/create') }}" class="btn btn-success btn-sm" title="Add New User">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>

                    {!! Form::open(['method' => 'GET', 'url' => '/admin/user', 'class' => 'navbar-form navbar-right',
                    'role' => 'search']) !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    {!! Form::close() !!}
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Username</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Created</th>
                                <th>Updated</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($user as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->username }}</td>
                                <td>{{ $item->first_name }}</td>
                                <td>{{ $item->last_name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $roles ? $roles[$item->id]['display_name'] : ''}}</td>
                                <td>
                                    @if ($item->is_enabled)
                                    Enabled
                                    @else
                                    Disabled
                                    @endif
                                </td>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    @if($isRoleSuperAdmin || $isRoleAdmin)

                                       @if($roles[$item->id]['name'] != 'super_admin')
                                       <a href="{{ url('/admin/user/edit/' . $item->id) }}"
                                          title="Edit User"
                                          class="btn btn-primary btn-xs">
                                          <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                       </a>
                                       @elseif($isRoleSuperAdmin)
                                        <a href="{{ url('/admin/user/edit/' . $item->id) }}"
                                           title="Edit User"
                                           class="btn btn-primary btn-xs">
                                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                        </a>
                                       @endif

                                       @if($isRoleSuperAdmin && $roles[$item->id]['name'] != 'super_admin')

                                          {!! Form::open(['method'=>'DELETE','url' => ['/admin/user/delete', $item->id],'style' => 'display:inline', 'onkeypress' => 'if(event.keyCode == 13) return false;']) !!}
                                          {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','title' => 'Delete User',
                                          'onclick'=>'return confirm( "Are you sure you want to delete this user?" )' ]) !!}
                                          {!! Form::close() !!}

                                       @elseif($roles[$item->id]['name']=='agent' || $roles[$item->id]['name']=='editor' && $userAuth->can(['delete_agent']) && $userAuth->can(['delete_editor']))

                                          {!! Form::open(['method'=>'DELETE','url' => ['/admin/user/delete', $item->id],'style' => 'display:inline', 'onkeypress' => 'if(event.keyCode == 13) return false;']) !!}
                                          {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'title' => 'Delete User',
                                          'onclick'=>'return confirm( "Are you sure you want to delete this user?" )' ]) !!}
                                          {!! Form::close() !!}
                                       @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $user->appends(['search' =>
                            Request::get('search')])->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
