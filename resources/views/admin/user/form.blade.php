<div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
    {!! Form::label('username', 'User Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('username', $user['username'], ['class' => 'form-control', $user ? 'disabled' : '' => $user ? 'disabled' : '']) !!}
        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    {!! Form::label('first_name', 'First Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('first_name', $user['first_name'], ['class' => 'form-control']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    {!! Form::label('last_name', 'Last Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('last_name', $user['last_name'], ['class' => 'form-control']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::email('email', $user['email'], ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::password('password', null, ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
    {!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::password('password_confirmation', null, ['class' => 'form-control']) !!}
        {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('role') ? 'has-error' : ''}}">
    {!! Form::label('role', 'Role', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::select('role', $roles, $userRole, ['class' => 'form-control', 'id' => 'role', 'onchange' =>
        'pageActions.chosenRole()']) !!}
        {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('assign_to_city') ? 'has-error' : ''}}" id="assign-to-city-block">
    {!! Form::label('assign_to_city', 'Assign To City', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">

        @foreach($locations as $key => $location)
        @foreach($locationsData as $ld)
        @if($ld['id'] == $key)
        @if($ld['parent_id'] > 0 && !isset($assignLocations[$key]) && !isset($assignLocations[$ld['parent_id']]))
        <label class="label-new" style="display:none" id="assign-location-block-{{$key}}">
            {!! Form::checkbox('assign-location-' . $key, $key, isset($assignLocations[$key]) ? true : false,
            ['class' => 'form-control', 'onclick' => 'pageActions.checkedLocation(this.value, this.checked)',
            'id' => 'assign-' . $key]) !!}
            <span>{{$location}}</span>
        </label>
        @else
        <label class="label-new" style="display:block" id="assign-location-block-{{$key}}">
            {!! Form::checkbox('assign-location-' . $key, $key, isset($assignLocations[$key]) ? true : false,
            ['class' => 'form-control', 'onclick' => 'pageActions.checkedLocation(this.value, this.checked)',
            'id' => 'assign-' . $key]) !!}
            <span>{{$location}}</span>
        </label>
        @endif
        @endif
        @endforeach
        @endforeach

        {!! $errors->first('assign_to_city', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('is_enabled') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled', 'Is Enabled', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '1', $user ? $user['is_enabled'] ? true : false : true ) !!} Yes</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '0', $user ? !$user['is_enabled'] ? true : false : false) !!} No</label>
        </div>
        {!! $errors->first('is_enabled', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!--<div class="form-group {{ $errors->has('prefix') ? 'has-error' : ''}}">
    {!! Form::label('prefix', 'You are', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
            <label>{!! Form::radio('prefix', 'Mr.', true) !!} Mr.</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('prefix', 'Ms.') !!} Ms.</label>
        </div>
        {!! $errors->first('prefix', '<p class="help-block">:message</p>') !!}
    </div>
</div>-->
<div class="form-group">
    <p>Contacts</p>
    {!! Form::label('contact_skype', 'Skype', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('contact_skype', $user['contact_skype'], ['class' => 'form-control']) !!}
        {!! $errors->first('contact_skype', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('contact_viber', 'Viber', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('contact_viber', $user['contact_viber'], ['class' => 'form-control']) !!}
        {!! $errors->first('contact_viber', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('contact_line', 'Line', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('contact_line', $user['contact_line'], ['class' => 'form-control']) !!}
        {!! $errors->first('contact_line', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('contact_whatsapp', 'Whatsapp', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('contact_whatsapp', $user['contact_whatsapp'], ['class' => 'form-control']) !!}
        {!! $errors->first('contact_whatsapp', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-2">
        {!! Form::submit($user ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        $('#assign-to-city-block').hide();
        pageActions.chosenRole();
    })

    var pageActions = {
        roles: <?php echo json_encode($roles)?>,
        locations: <?php echo json_encode($locationsData)?>,
        chosenRole: function () {
            var role = $('#role').val();
            for (var i in this.roles) {
                if (i == role) {
                    if (this.roles[i] == 'Agent' || this.roles[i] == 'Editor') {
                        $('#assign-to-city-block').show(300);
                        break;
                    } else {
                        $('#assign-to-city-block').hide(300);
                    }
                }
            }
        },
        checkedLocation: function (value, isChecked) {
            var checkedInputsParent = this.getParent(value);
            var checkedInputsChild = this.getChild(value);

            for (var i = 0; i < checkedInputsParent.length; i++) {
                if (isChecked) {
                    document.getElementById('assign-' + checkedInputsParent[i]).checked = true;

                } else {

                }
            }

            for (var i = 0; i < checkedInputsChild.length; i++) {
                if (!isChecked) {
                    document.getElementById('assign-' + checkedInputsChild[i]).checked = false;
                    $('#assign-location-block-' + checkedInputsChild[i]).hide(300)
                } else {
                    $('#assign-location-block-' + checkedInputsChild[i]).show(300)
                }
            }

        },

        getParent: function (locationId) {
            var result = [];
            for (var i in this.locations) {
                if (this.locations[i].id == +locationId) {
                    result.push(+locationId)
                    if (this.locations[i]['parent_id'] !== 0) {
                        result = result.concat(this.getParent(this.locations[i]['parent_id']))
                    }
                }
            }
            return result;
        },

        getChild: function (locationId) {
            var result = [];
            for (var i in this.locations) {
                if (this.locations[i]['parent_id'] == locationId) {
                    result.push(this.locations[i]['id']);
                    result = result.concat(this.getChild(this.locations[i]['id']))
                }
            }
            return result
        }
    }
</script>