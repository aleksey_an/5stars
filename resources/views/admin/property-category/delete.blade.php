@push('scripts')
<script>
    var deleteCategory = {
        delete: function (id) {
            if (confirm("Are you sure you want to delete this category?")) {
                $.ajax({
                    url: '/admin/category/delete/' + id,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {

                        if (response.errorProperties) {
                            var properties = response.errorProperties;
                            var propertiesCodes = 'This category is tied to ' + properties.length + ' properties! To continue deleting, edit all the properties in this category so that they refer to another category. \n\t';
                            for (var i = 0; i < properties.length; i++) {
                                propertiesCodes += i + ' ' + properties[i].code + '\n\t '
                            }
                            alert(propertiesCodes);
                        }
                        if (response.success) {
                            window.location.href = response.success;
                        }
                        console.log(response)
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });
            } else {
                return;
            }
        }
    }
</script>
@endpush