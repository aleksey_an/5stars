<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('code', 'Code', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('code', $category['code'], ['class' => 'form-control', $categoryTranslations ? 'disabled' : '' => $categoryTranslations ? 'disabled' : '']) !!}
        {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div id="tabs">
    <ul>
        @foreach($languages as $key => $language)
        <li><a href="#{{$key}}">{{$language}}</a></li>
        @endforeach
    </ul>
    @foreach($languages as $key => $language)
    <div id="{{$key}}">
        <div class="form-group {{ $errors->has('trans-' . $key . '-url') ? 'has-error' : ''}}">
            {!! Form::label('trans-' . $key . '-url', 'Url', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('trans-' . $key . '-url', isset($categoryTranslations[$key]) ? $categoryTranslations[$key]['url'] : '',
                ['class' => 'form-control', 'maxlength' => '250'])!!}
                {!! $errors->first('trans-' . $key . '-url', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('trans-' . $key . '-name') ? 'has-error' : ''}}">
            {!! Form::label('trans-' . $key . '-subject', 'Subject', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('trans-' . $key . '-name', isset($categoryTranslations[$key]) ? $categoryTranslations[$key]['name'] : '',
                ['class' => 'form-control', 'maxlength' => '250'])!!}
                {!! $errors->first('trans-' . $key . '-name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('trans-' . $key . '-description') ? 'has-error' : ''}}">
            {!! Form::label('trans-' . $key . '-description', 'Description', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('trans-' . $key . '-description', isset($categoryTranslations[$key]) ? $categoryTranslations[$key]['description'] : '',
                ['class' => 'form-control', 'maxlength' => '100000']) !!}
                {!! $errors->first('trans-' . $key . '-description', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    @endforeach
</div>
<br>

<div class="form-group {{ $errors->has('is_enabled') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled', 'Is Enabled', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', 1 , $category ? $category['is_enabled'] ? true : false : true ) !!} Yes</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', 0, $category ? $category['is_enabled'] ?  false : true : false) !!} No</label>
        </div>
        {!! $errors->first('is_enabled', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        {!! Form::submit(isset($category) ? 'Edit' : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<script>
    $(function () {
        $("#tabs").tabs({
            collapsible: true
        });
    })
</script>