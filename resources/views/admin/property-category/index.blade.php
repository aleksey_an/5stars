@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Categories</div>
                <div class="panel-body">
                    <a href="{{ url('admin/category/create') }}" class="btn btn-success btn-sm"
                       title="Add New Property Location">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Category
                    </a>
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Enable</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Actions</th>
                                <th>Sorting</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $item)
                            <tr>
                                <td>{{ $item->id }}</td>

                                <td>{{ isset($item->translations[0]) ? $item->translations[0]->name : ''}}</td>
                                <td>{{ $item->code }}</td>
                                <td>
                                    @if ($item->is_enabled)
                                    Enabled
                                    @else
                                    Disabled
                                    @endif
                                </td>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    <a href="{{ url('/admin/category/'. $item->id .'/edit') }}" title="Edit Category">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-xs"
                                            onclick="deleteCategory.delete('{{$item->id}}')">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                    </button>
                                </td>
                                <td>
                                    @if (!$loop->first)
                                    <a href="{{ url('/admin/category/sorting/up/'. $item->id) }}" title="Up">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                        </button>
                                    </a>
                                    @endif
                                    @if (!$loop->last)
                                    <a href="{{ url('/admin/category/sorting/down/'. $item->id) }}" title="Down">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa fa-arrow-down" aria-hidden="true"></i>
                                        </button>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@include ('admin.property-category.delete')

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.upDownSort').on('click', function(){
                $.post($(this).attr('data-url'));
            });
//            $( "#sortable" ).sortable();
        });

    </script>
@endpush