<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('display_name') ? 'has-error' : ''}}">
    {!! Form::label('display_name', 'Display Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('display_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if($permissions)
@foreach($permissions as $permission)
<div class="form-group">
    {!! Form::label($permission->id, $permission->display_name, ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">

        {!! Form::checkbox($permission->id, null, isset($rolePermissions[$permission->id]) ? true : false, ['class' => 'form-control']) !!}
    </div>
</div>
@endforeach
@endif
<!--<div class="form-group">
    <div class="col-md-offset-4 col-md-2">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>-->
