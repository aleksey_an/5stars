@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Select language</div>
                <div class="panel-body">

                    <div class="col-md-12">
                        {!! Form::label('locale', 'Language', ['class' => 'col-md-6 control-label text-right']) !!}
                        <div class="col-md-3">
                            {!! Form::select('locale', $languages, null,
                            ['class' => 'form-control', 'id' => 'select-lang']) !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Generate XML feed</div>
                <div class="panel-body filters">

                    @include('admin.xml.form')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            datepicker();

            $('#select-lang').change(function () {
                if($(this).val()) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'get',
                        data: {locale: $(this).val(), ajax: true},
                        success: function (data) {
                            $('.filters').html(data);
                            datepicker();
                        },
                        error: function (err) {
                            console.error(err);
                        }
                    });
                }
            });

            $('.filters').on('click', '.btn-success', function () {
                if($('#select-lang').val()) {
                    if ($('#budget_from').val() <= 999999999 && $('#budget_to').val() <= 999999999){
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'post',
                            url: $('#xml-form').attr('action'),
                            data: {
                                location_id: $('#location_id').val(),
                                category_id: $('#category_id').val(),
                                to_sale: $('#to_sale').is(':checked') ? 1 : 0,
                                to_rent: $('#to_rent').is(':checked') ? 1 : 0,
                                budget_from: $('#budget_from').val(),
                                budget_to: $('#budget_to').val(),
                                date_from: $('#date_from').val(),
                                date_to: $('#date_to').val(),
                                locale: $('#select-lang').val()
                            },
                            success: function (data) {
                                if (data == '1') {
                                    $('#xml-form').append('<div class="col-md-12"><span class="success">XML feed successfully generated. To download it follow the link <a href="{{ url('/') }}/xml/download" target="_blank">{{ url('/') }}/xml/download</a></span></div>');
                                } else {
                                    $('#xml-form').append('<div class="col-md-12"><span class="error">XML feed not generated. Try to regenerate it later</span></div>');
                                }
                            },
                            error: function (err) {
                                console.error(err);
                            }
                        });
                    }else {
                        $('#xml-form').append('<div class="col-md-12"><span class="error">Budget from and budget to may be max 999999999</span></div>');
                    }
                }else{
                    alert('Please, select a language!');
                }
                return false;
            });
        });

        function datepicker() {
            $('.filters').find('.date').datepicker({
                format: 'dd-mm-yyyy'
            });
        }
    </script>
@endpush