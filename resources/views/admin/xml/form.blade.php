{!! Form::open(['method' => 'POST', 'url' => '/admin/xml/generate', 'class' => 'form-horizontal', 'role' => 'generate', 'id' => 'xml-form']) !!}

    <div class="col-md-6">
        {!! Form::label('location_id', 'City', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::select('location_id', $locations, null,
            ['class' => 'form-control', 'id' => 'location_id', 'multiple' => true]) !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('category_id', 'Category', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::select('category_id', $categories, null,
            ['class' => 'form-control', 'id' => 'category_id', 'multiple' => true]) !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('to_sale', 'For Sale', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-3">
            {!! Form::checkbox('to_sale', 1, 0 , ['class' => 'form-control', 'id' => 'to_sale']) !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('to_rent', 'For Rent', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-3">
            {!! Form::checkbox('to_rent', 1, 0 , ['class' => 'form-control', 'id' => 'to_rent']) !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('budget_from', 'Budget from (฿)', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::number('budget_from', null, ['class' => 'form-control', 'id' => 'budget_from', 'min' => '0', 'max' => '999999999']) !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('budget_to', 'Budget to (฿)', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::number('budget_to', null, ['class' => 'form-control', 'id' => 'budget_to', 'min' => '0', 'max' => '999999999']) !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('date_from', 'Date from', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::text('date_from', null, ['class' => 'form-control date', 'id' => 'date_from']) !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('date_to', 'Date to', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::text('date_to', null, ['class' => 'form-control date', 'id' => 'date_to']) !!}
        </div>
    </div>

    <div class="col-md-12 text-center">
        <button class="btn btn-sm btn-success">Generate</button>
    </div>

{!! Form::close() !!}