@extends('layouts.app')

<?php //var_dump($parentId)?>

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Create New Location</div>
                <div class="panel-body">
                    <a onclick="window.history.back();" title="Back">
                        <button class="btn btn-warning btn-xs">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    <br />
                    <br />

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::open([
                    'url' => 'admin/location',
                    'class' => 'form-horizontal',
                    'id'=>'location-form',
                    'files' => true,
                    'onkeypress' => 'if(event.keyCode == 13) return false;'
                    ]) !!}

                    @include ('admin.location.form')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@include ('admin.location.validator')