@push('scripts')
<script>
    var deleteLocation = {

        delete: function (id) {
            if (confirm("Are you sure you want to delete this location?")) {
                $.ajax({
                    url: '/admin/location/delete/' + id,
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        if (response.errorUsers) {
                            var users = response.errorUsers;
                            var text = users.length + ' users are assigned to the location! To continue deleting, untie the users from this location. \n\t';
                            alert(text);
                        }
                        if (response.errorChildLocations) {
                            alert('This location can not be deleted! This location has child locations! To continue the deletion, you need to delete all child locations!')
                        }
                        if (response.errorProperties) {
                            var properties = response.errorProperties;
                            var propertiesCodes = 'This location is tied to ' + properties.length + ' properties! To continue deleting, edit all the properties in this location so that they refer to another location. \n\t';
                            for (var i = 0; i < properties.length; i++) {
                                propertiesCodes += i + ' ' + properties[i].code + '\n\t '
                            }
                            alert(propertiesCodes);
                        }
                        if (response.success) {
                            window.location.href = response.success;
                        }
                        console.log(response)
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });
            } else {
                return;
            }
        }
    }
</script>
@endpush