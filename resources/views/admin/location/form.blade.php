@if($parentId || $parentId == '0')
{!! Form::hidden('parent_id', $parentId, ['class' => 'form-control', 'type' => 'hidden']) !!}
@endif

@if(!$parentId)
<div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    {!! Form::label('code', 'Code', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('code', $location['code'], ['class' => 'form-control', $translations ? 'disabled' : '' =>
        $translations ? 'disabled' : '']) !!}
        {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
<div id="tabs">
    <ul>
        @foreach($languages as $key => $language)
        <li><a href="#{{$key}}">{{$language}}</a></li>
        @endforeach
    </ul>
    @foreach($languages as $key => $language)
    <div id="{{$key}}">
        <div class="form-group {{ $errors->has('trans-' . $key . '-url') ? 'has-error' : ''}}">
            {!! Form::label('trans-' . $key . '-url', 'Url', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('trans-' . $key . '-url', isset($translations[$key]) ? $translations[$key]['url'] : '',
                ['class' => 'form-control', 'maxlength' => '250'])!!}
                {!! $errors->first('trans-' . $key . '-url', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('trans-' . $key . '-name') ? 'has-error' : ''}}">
            {!! Form::label('trans-' . $key . '-name', 'Name', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('trans-' . $key . '-name', isset($translations[$key]) ? $translations[$key]['name'] : '',
                ['class' => 'form-control', 'maxlength' => '250'])!!}
                {!! $errors->first('trans-' . $key . '-name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('trans-' . $key . '-description') ? 'has-error' : ''}}">
            {!! Form::label('trans-' . $key . '-description', 'Description', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('trans-' . $key . '-description', isset($translations[$key]) ?
                $translations[$key]['description'] : '',
                ['class' => 'form-control', 'maxlength' => '100000']) !!}
                {!! $errors->first('trans-' . $key . '-description', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    @endforeach
</div>
<br>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', 'Address', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10 input-group p15--l-r">
        {!! Form::text('address', $location['address'], ['class' => 'form-control', 'id' => 'address']) !!}
        <span class="input-group-btn">
            <button class="btn btn-default" type="button" onclick="googleMap.getLatLong()">
                <i class="fa fa-search"></i>
            </button>
        </span>
    </div>
</div>
<div id="map" style="width: 100%; height: 300px"></div>
<br>
<div class="form-group {{ $errors->has('lat') ? 'has-error' : ''}}">
    {!! Form::label('lat', 'Latitude', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('lat', $location['latitude'], ['class' => 'form-control', 'step' => 'any', 'id' => 'lat']) !!}
        {!! $errors->first('lat', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('lng') ? 'has-error' : ''}}">
    {!! Form::label('lng', 'Longitude', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('lng', $location['longitude'], ['class' => 'form-control', 'step' => 'any', 'id' => 'lng']) !!}
        {!! $errors->first('lng', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('video_url') ? 'has-error' : ''}}">
    {!! Form::label('video_url', 'YouTube Video ID', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('video_url', $location['video_url'], ['class' => 'form-control', 'maxlength' => '1000']) !!}
        {!! $errors->first('video_url', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('is_enabled') ? 'has-error' : ''}}">
    {!! Form::label('is_enabled', 'Is Enabled', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '1', $location ? $location['is_enabled'] ? true : false : true) !!}
                Yes</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('is_enabled', '0', $location ? $location['is_enabled'] ? false : true : false) !!}
                No</label>
        </div>
        {!! $errors->first('is_enabled', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div id="gallery-block">
    <div id="mydz" class="dropzone">
        <button id="submit-all" type="button" class="btn btn-sm btn-success">Upload</button>
    </div>
    <br>

    <div class="table-responsive">
        <ul id="sortable"></ul>
    </div>
</div>
<br>
<div class="form-group">
    <div class="col-md-offset-4 col-md-6">
        {!! Form::submit($location ? 'Edit' : 'Submit', ['class' => 'btn btn-primary', 'id' => 'submit-btn']) !!}
    </div>
</div>
<input name="images" type="hidden" id="images" value="[]"/>
<script>
    var lang = <?php echo isset($languages) ? json_encode($languages) : json_encode([]) ?>;

    document.addEventListener('DOMContentLoaded', function () {
        $("#tabs").tabs({
            collapsible: true
        });

        initCkeditor()
    });

    function initCkeditor() {
        if (Object.keys(lang).length > 0) {
            for (var i = 0; i < Object.keys(lang).length; i++) {
                CKEDITOR.replace('trans-'+ Object.keys(lang)[i] +'-description',{
                    language: 'en',
                    uiColor: '#E7E7E7',
                    toolbar: [
                        { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
                        { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
                        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting' ] },
                        { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
                        { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                        { name: 'tools', items: [ 'Maximize' ] },
                        { name: 'editing', items: [ 'Scayt' ] },
                        { name: 'insert', items: [ 'Image' ] },
                        { name: 'links', items: [ 'Link' ] },
                    ],
                });
            }
        }
    }

    var countImages = 0;
    var imagesArr = [];
    var locationId = '<?php echo (isset($location) ?  $location->id :  0 )?>';

    var pageAction = {
        images: JSON.parse('<?php echo (isset($location->images) && count($location->images) > 0 ? json_encode($location->images) : json_encode([]))?>'),
        createLi: function (data) {
            countImages = +countImages + data.length;
            var li = '';
            for (var i = 0; i < data.length; i++) {
                li = '<li id="' + data[i].id + '" class="ui-state-default">' +
                        /*'<span>' + data[i].id + '</span>' +*/
                    '<span class="gallery-img"><img src="/images/location/thumbnail/' + data[i].name + '" height="60"></span> ' +
                        /*'<span>' + data[i].sequence + '</span>' +*/
                    '<span class="gallery-label"><label>Is Enabled <input id="checkbox-' + data[i].id + '"  type="checkbox" name="img_" value="' + data[i].id + '" checked="true" onchange="pageAction.changedEnableStatus(this.value, this.checked)"></label></span>' +
                    '<span class="gallery-del"><button type="button" class="btn btn-danger btn-xs" onclick="pageAction.deleteImage(\'' + data[i].id + '\', \'' + data[i].name + '\')">Delete </button></span>' +
                    '</li>';
                $('#sortable').prepend(li);
                if (data[i].is_enabled) {
                    $("#checkbox-" + data[i].id).attr("checked", true);
                } else {
                    $("#checkbox-" + data[i].id).attr("checked", false);
                }
            }
            this.imageSortable();
        },

        imageSortable: function () {
            $("#sortable").sortable({

                update: function (event, ui) {
                    var order = [];
                    if (!+locationId) {
                        $('#sortable li').each(function (e) {
                            for (var i = 0; i < imagesArr.length; i++) {
                                if (imagesArr[i].id == $(this).attr('id')) {
                                    imagesArr[i].sequence = $(this).index() + 1
                                }
                            }
                        });
                        $('#images').val(JSON.stringify(imagesArr));
                    } else {
                        $('#sortable li').each(function (e) {
                            order.push({id: $(this).attr('id'), sequence: $(this).index() + 1})
                        });
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        var posting = $.post('<?php echo url('/admin/location/image/change-sequence'); ?>', JSON.stringify({data: order}));
                        posting.done(function (data) {
                            console.log(data)
                        });
                    }
                }
            });
        },

        changedEnableStatus: function (imgId, status) {
            status = status ? 1 : 0;
            if (!+locationId) {
                for (var i = 0; i < imagesArr.length; i++) {
                    if (imagesArr[i].id == imgId) {
                        imagesArr[i].is_enabled = status
                    }
                }
                $('#images').val(JSON.stringify(imagesArr));
            } else {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var posting = $.post('<?php echo url('/admin/location/image/change-enable'); ?>',
                    JSON.stringify({imgId: imgId, is_enable: status}));
                posting.done(function (data) {
                    console.log('status changed')
                });
            }
        },

        deleteImage: function (imgId, imgName) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var posting = $.post('<?php echo url('/admin/location/image/delete'); ?>',
                JSON.stringify({imgId: imgId, name: imgName}));
            posting.done(function (data) {
                $('#' + imgId).remove();
                countImages--;
            });
            //When creating a location
            console.log(imagesArr, imgId);
            for (var i = 0; i < imagesArr.length; i++) {
                if (imagesArr[i].id == imgId) {
                    imagesArr.splice(i, 1)
                }
            }
            $('#images').val(JSON.stringify(imagesArr));
        },

        previewImage: function (file) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image').attr('src', e.target.result).css({display: 'block'});
            };
            reader.readAsDataURL(file);
        }
    };
    var googleMap = {
        map: null,
        marker: null,
        defaultCoordinates: null,
        coordinates: {
            lat: <?php echo $location['lat'] ? $location['lat'] : 0 ?>,
            lng: <?php echo $location['lng'] ? $location['lng'] : 0 ?>
        },

        initMap: function () {
            this.defaultCoordinates = window.DEFAULT_COORD;
            if (document.getElementById('map')) {
                if (!this.coordinates.lat && !this.coordinates.lng) {
                    this.coordinates = this.defaultCoordinates
                } else {
                    $('#lat').val(this.coordinates.lat)
                    $('#lng').val(this.coordinates.lng)
                }


                this.map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 10,
                    center: this.coordinates
                });

                this.map.addListener('click', function (e) {

                    $.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + e.latLng.lat() + ',' + e.latLng.lng() + '&key=' + GOOGLE_KEY, function (response) {
                        if (response.results) {
                            googleMap.addMarker({lat: e.latLng.lat(), lng: e.latLng.lng()});
                            $('#address').val(response.results[0].formatted_address)
                        } else {
                            console.log('Noting to found')
                            return false;
                        }
                    })
                });

                this.marker = new google.maps.Marker({
                    position: this.coordinates,
                    map: this.map
                });
            }
        },

        addMarker: function (latLng) {
            this.map.setCenter(latLng);
            this.marker.setMap(null);
            var marker = new google.maps.Marker({
                position: latLng,
                title: "Location"
            });
            marker.setMap(this.map);
            this.marker = marker;
            $('#lat').val(latLng.lat);
            $('#lng').val(latLng.lng);
        },

        getLatLong: function () {
            var address = $('#address').val();
            $.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + GOOGLE_KEY, function (response) {
                if (response.results) {
                    googleMap.addMarker(response.results[0].geometry.location);
                    return false;
                } else {
                    console.log('Noting to found');
                    return false;
                }
            })
        }
    };

    //Upload image
    var limitForUpload = '{{env("MAX_COUNT_LOCATION_IMAGES")}}';
    var totalDataTransfer = 0;
    var limitTotalDataTransfer = '<?php echo ( +substr(ini_get("post_max_size"), 0, -1 ) * 1000000) ?>'; //(Bytes)
    $("#mydz").dropzone({
        responseData: null,
        url: "{!! url('/admin/location/image/upload/') !!}/" + locationId,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        paramName: "file",
        uploadMultiple: true,
        autoProcessQueue: false,
        parallelUploads: 100,
        maxFilesize: 8,
        maxFiles: 10,
        addRemoveLinks: true,
        dictRemoveFile: 'Remove',
        dictFileTooBig: 'Image is bigger than 8MB',
        acceptedFiles: ".jpeg,.jpg,.png",
        success: function (file, response) {
            this.removeFile(file);
            this.responseData = response;
        },
        init: function () {
            var _this = this;
            $('#submit-all').click(function () {
                if (countImages == limitForUpload) {
                    alert('Maximum number of images uploaded (' + limitForUpload + '). First, delete the images before downloading')
                } else if ((_this.files.length + countImages) > limitForUpload) {
                    alert('The allowed number of images for downloading' + limitForUpload + ' , uploaded ' + countImages +
                        ' you can upload no more than ' + (limitForUpload - countImages) + ' images')
                } else {
                    $('.dz-remove').css({'display': 'none'});
                    _this.processQueue();
                }
            });
            this.on("sending", function (file, xhr, formData) {
                formData.append('location_name', $('#trans-' + '{{config("app.fallback_locale")}}' + '-name').val());
            });
            this.on("complete", function (file) {
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0 && this.responseData && this.responseData.success) {
                    //When creating a location
                    if (!+locationId) {
                        var img = $('#images').val();
                        img = JSON.parse(img);
                        for (var i = 0; i < this.responseData.success.length; i++) {
                            this.responseData.success[i]['id'] = (img.length == 0 ? i : img.length);
                            imagesArr.push(this.responseData.success[i]);
                            img.push(this.responseData.success[i])
                        }
                        this.responseData.success.reverse();
                        $('#images').val(JSON.stringify(img));
                    }
                    pageAction.createLi(this.responseData.success);
                }
                totalDataTransfer = 0;
            });
            this.on("addedfile", function (file) {
                if (totalDataTransfer + file.size < limitTotalDataTransfer) {
                    totalDataTransfer += file.size
                } else {
                    this.removeFile(file);
                    alert('The allowed load is ' + limitTotalDataTransfer / 1000000 + 'mb. The total size of the images is ' + (totalDataTransfer + file.size) / 1000000 + ' mb. To load a larger size, increase the value in php_ini');
                }
            });
            this.on("error", function (file, response) {
                $(file.previewElement).find('.dz-error-message').text('Error loading');
                totalDataTransfer = 0;
                console.log(response)
            })
        }
    });

    $(function () {
        $("#tabs").tabs({
            collapsible: true
        });
        googleMap.initMap();
        if (pageAction.images.length > 0) {
            pageAction.createLi(pageAction.images);
            countImages = pageAction.images.length;
        }
    })
</script>