@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Location #{{ $location->id }}</div>
                <div class="panel-body">
                    <a onclick="window.history.back();" title="Back">
                        <button class="btn btn-warning btn-xs">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>

                    <a href="{{ url('admin/location/create?parent=' . $location->id) }}" title="Child Locations">
                        <button class="btn btn-success btn-xs">
                            <i class="fa fa-plus" aria-hidden="true"></i> Child Locations
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    @endif

                    {!! Form::open([
                    'method' => 'PATCH',
                    'url' => ['admin/location', $location['id']],
                    'class' => 'form-horizontal',
                    'files' => true,
                    'id'=>'location-form',
                    'onkeypress' => 'if(event.keyCode == 13) return false;'
                    ]) !!}

                    @include ('admin.location.form')

                    {!! Form::close() !!}

                    @if (isset($childLocations[0]))
                    <div class="table-responsive">
                        <h3>Child Locations</h3>
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Enable</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($childLocations as $item)
                            <tr>
                                <td>{{ $item->id }}</td>

                                <td>{{ $item->translations[0]->name}}</td>
                                <td>
                                    @if ($item->is_enabled)
                                    Enabled
                                    @else
                                    Disabled
                                    @endif
                                </td>
                                <td>{{ $item->created_at }}</td>
                                <td>{{ $item->updated_at }}</td>
                                <td>
                                    <a href="{{ url('/admin/location/'. $item->id .'/edit?parent=' . $location->id) }}" title="Edit Location">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-xs"
                                            onclick="deleteLocation.delete('{{$item->id}}')">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@include ('admin.location.validator')
@include ('admin.location.delete')