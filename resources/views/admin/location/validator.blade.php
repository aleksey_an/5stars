@push('scripts')
<script>
    $(document).ready(function () {
        $("#location-form").validate({
            errorClass: "help-block",
            errorElement: "span",
            rules: {
                locale: {
                    required: true
                },
                code: {
                    maxlength: 3
                },
                is_enabled: {
                    required: true
                }
            },
            submitHandler: function (form) {
                form.submit();
                if($("#location-form").valid()){
                    $('#submit-btn').css({'display': 'none'});
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="' + element.getAttribute('name') + '"]').closest(".form-group, .checkbox").addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element.form).find('input[name="' + element.getAttribute('name') + '"]').closest(".form-group, .checkbox").removeClass('has-error');

            },
            success: function (label) {
                setTimeout(function () {
                    $('#submit-btn').css({'display': 'block'});
                }, 1000)
            }
        });
    });
</script>
@endpush