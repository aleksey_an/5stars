window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
require('jquery-validation');
require('jquery');
require('jquery-datepicker');
require('jquery-ui-dist/jquery-ui.js');
var Dropzone = require('dropzone');
Dropzone.autoDiscover = false;

window.GOOGLE_KEY = 'AIzaSyAXld6Rao1mspTYI1rJd5NOoyr_tfKcU8E';
window.DEFAULT_COORD = {lat: 48.848089, lng: 2.356567};

document.addEventListener('DOMContentLoaded', function () {

  window.setTimeout(function () {
    $(".alert").fadeTo(500, 0).slideUp(500, function () {
      $(this).remove();
    });
  }, 2000);

  jQuery.validator.addMethod("exactlength", function (value, element, param) {
    return this.optional(element) || value.length == param;
  }, $.validator.format("Please enter exactly {0} characters."));
});

