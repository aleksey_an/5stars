window.Web = window.Web || {};

Web.form = {
  locationId: null,
  /**
   * Send contact us form
   * */
  sendContactUs: function () {
    $('#__vtigerWebForm').validate({
      rules: {
        'firstname': {
          required: true
        },
        'lastname': {
          required: true
        },
        'phone': {
          required: true
        },
        'email': {
          required: true
        },
        'cf_866': {
          required: true,
          maxlength: 500
        }
      },
      messages: {
        firstname: 'Please put your full firstname.',
        lastname: 'Please put your full lastname.',
        email: 'Please enter a valid email.',
        phone: 'Please put your phone.',
        cf_866: {
          required: 'Please put your message',
          maxlength: 'The maximum message length is 500 characters'
        }
      },

      submitHandler: function (form) {
        var url = $('#__vtigerWebForm').attr('action');
        var formData = $('#__vtigerWebForm').serialize();
        var recaptchaUrl = $('#recaptcha-url').attr('data-url');

        $.ajax({
          url: recaptchaUrl,
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
              'Access-Control-Allow-Credentials': true
          },
          type: 'post',
          data: formData,
          success: function (data) {
            if(data.success){
              $('#send-btn').css({'display': 'none'});
              $('#form-preloader').css({'display': 'block'});

              $.ajax({
                url: url,
                crossDomain: true,
                dataType: 'jsonp',
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                  'Access-Control-Allow-Credentials': true
                },
                type: 'post',
                data: formData,
                success: function (data) {
                  $('#send-btn').css({'display': 'block'});
                  $('#form-preloader').css({'display': 'none'});
                  $('#form-popup-content').html('Form has been sent');
                  $('#form-popup').css({'display': 'block'});
                  console.log('Email successfully sent');
                  console.log(data);
                },
                error: function (err) {
                  $('#send-btn').css({'display': 'block'});
                  $('#form-preloader').css({'display': 'none'});
                  $('#form-popup-content').html('Form has been sent');
                  $('#form-popup').css({'display': 'block'});

                  // $('#send-btn').css({'display': 'block'});
                  // $('#form-preloader').css({'display': 'none'});
                  // $('#form-popup-content').css({'color': '#ffbfbf'}).html('The problem with sending a message. We are working on it !');
                  // $('#form-popup').css({'display': 'block'});
                  console.log(err);
                }
              });
            }
          }
        });
        return false;
      }
    });
  },

  /**
   * Send Form to agent (from favorites block)
   * */
  sendFormToAgent: function () {
    $('#form-contact-agent').validate({
      rules: {
        'formName': {
          required: true,
          maxlength: 100
        },
        'formPhone': {
          required: true,
          maxlength: 100
        },
        'formEmail': {
          required: true,
          maxlength: 100
        },
        formMessage: {
          required: true,
          maxlength: 500
        }
      },
      messages: {
        formName: 'Please put your full name.',
        formEmail: 'Please enter a valid email.',
        formPhone: 'Please put your phone.',
        formMessage: {
          required: 'Please put your message',
          maxlength: 'The maximum message length is 500 characters'
        }
      },
      submitHandler: function (form) {
        var name = $('#form-name').val();
        var phone = $('#form-phone').val();
        var email = $('#form-email').val();
        var message = $('#form-message').val();
        var rentDate = null;
        var dataForRentDate = $('#date-picker').val();
        if (dataForRentDate) {
          rentDate = {
            'from': dataForRentDate.split('-')[0] || '-',
            'to': dataForRentDate.split('-')[1] || '-'
          };
        }
        var propertyCode = $('#property-code').val();
        var locationId = $('#form-location-id').val();

        var viewed = Web.property.getFromLocalStorage('viewed_property');
        var favorites = Web.property.getFromLocalStorage('favorite_property');

        $('#send-btn').css({'display': 'none'});
        $('#form-preloader').css({'display': 'block'});

        $.ajax({
          url: window.location.origin + '/forms/form-contact-agent',
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'post',
          data: {
            name: name,
            phone: phone,
            email: email,
            message: message,
            rentDate: rentDate,
            propertyCode: propertyCode,
            viewed: JSON.stringify(viewed),
            favorites: JSON.stringify(favorites),
            locationId: locationId
          },
          success: function (data) {
            $('#send-btn').css({'display': 'block'});
            $('#form-preloader').css({'display': 'none'});
            $('#form-popup-content').html('Form has been sent');
            $('#form-popup').css({'display': 'block'});
            console.log('Email successfully sent');
          },
          error: function () {
            $('#send-btn').css({'display': 'block'});
            $('#form-preloader').css({'display': 'none'});
            $('#form-popup-content').css({'color': '#ffbfbf'}).html('The problem with sending a message. We are working on it !');
            $('#form-popup').css({'display': 'block'});
          }
        });
        return false;
      }
    });
  },

  /**
   * Send news latter*/
  newsLatter: function(){
    var form = $('#news-latter-email').closest('form');
    var email = $('#news-latter-email').val();
    if (email) {
      $('#send-news-later').css({'display': 'none'});
      $('#form-preloader-news-later').css({'display': 'block'});
      $.ajax({
        url: form.attr('action'),
        crossDomain: true,
        dataType: 'jsonp',
        type: 'post',
        data: form.serialize(),
        success: function (data) {
          console.log(data);
          $('#send-news-later').css({'display': 'block'});
          $('#form-preloader-news-later').css({'display': 'none'});
          $('#form-footer-thank-message').css({'display': 'block'});
          $('#news-latter-email').val('');
          setTimeout(function() {$('#form-footer-thank-message').css({'display': 'none'});}, 5000)
        },
        error: function (err) {
          $('#send-news-later').css({'display': 'block'});
          $('#form-preloader-news-later').css({'display': 'none'});
          console.error(err);
        }
      });
    }
  }
};