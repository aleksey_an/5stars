window.Web = window.Web || {};

Web.location = {
  videoId: null,

  video: function () {
    if (this.videoId) {
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    } else {
      console.log('Video Id ' + this.videoId)
    }
  },

  onPlayerReady: function (event) {
    event.target.pauseVideo();
  },

  startVideo: function () {
    if (window.player)
      window.player.playVideo();
  },

  stopVideo: function () {
    if (window.player)
      window.player.stopVideo();
  }
};

