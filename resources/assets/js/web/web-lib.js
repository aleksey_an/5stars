window.$ = window.jQuery = require('jquery');
require('jquery-validation');
require('bootstrap-sass');
require('bootstrap-daterangepicker');
require('../lib/intl-tel-input/build/js/intlTelInput.js');
require('bootstrap-select');
require('../lib/infinity-scroll/infinite-scroll.pkgd.js');
require('../lib/label_better-master/jquery.label_better.js');
require('lightslider');