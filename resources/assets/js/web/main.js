window.Web = window.Web || {};

Web.main = {

  init: function () {
    var autoScrollAnimationTime = 1200;
    //Auto-scroll  property page
    $("#contact-form").click(function (event) {
      event.preventDefault();
      var top = $('#form-target').offset().top;
      $('body,html').animate({scrollTop: top - 100}, autoScrollAnimationTime);
    });
    //Auto-scroll location page
    $("#go-to-region-details").click(function (event) {
      event.preventDefault();
      var top = $('#target-region-details').offset().top;
      $('body,html').animate({scrollTop: top - 30}, autoScrollAnimationTime);
    });
  },

  /**
   * Take data from local storage by field name*/
  getArrayFromLocaleStorage: function (fieldName) {
    var data = localStorage.getItem(fieldName);
    if (!data) {
      return [];
    } else {
      return JSON.parse(data);
    }
  },

  compareArrays: function (arr, arr2) {
    if (arr.length != arr2.length) return false;
    var on = 0;
    for (var i = 0; i < arr.length; i++) {
      for (var j = 0; j < arr2.length; j++) {
        if (arr[i] === arr2[j]) {
          on++;
          break
        }
      }
    }
    return on == arr.length ? true : false
  },

  createBudgetOptions: function (searchType, transSearchType, placeholder) {
    $('#budget').find('option').remove();
    var budget = [];
    var options = '<option value="">' + placeholder + '</option>';

    var trans = 'and above';

    if(searchType == 'à-vendre' || searchType == 'à-louer'){
      trans = 'et plus'
    }

    if (searchType == transSearchType) {
      budget =
        /*this.createBudgetArr(50, 1000000)*/
      {
        '1-4000001': "3M - 4M THB ",
        '3999999-6000001': "4M - 6M THB",
        '5999999-8000001': "6M - 8M THB",
        '7999999-10000001': "8M - 10M THB",
        '9999999-12000001': "10M - 12M THB",
        '11999999-15000001': "12M - 15M THB",
        '14999999-20000001': "15M - 20M THB",
        '19999999-30000001': "20M - 30M THB",
        '29999999-40000001': "30M - 40M THB",
        '39999999-50000001': "40M - 50M THB",
        '49999999-10000000000': "50M THB - " + trans,
      }
      // console.log(this.createBudgetArr(50, 1000000))
    } else {
      budget =
        /*this.createBudgetArr(90, 1000)*/
      {
        '1-40001': '30K - 40K THB',
        '39999-50001': '40K - 50K THB',
        '49999-60001': '50K - 60K THB',
        '59999-70001': '60K - 70K THB',
        '69999-80001': '70K - 80K THB',
        '79999-90001': '80K - 90K THB',
        '89999-100001': '90K - 100K THB',
        '99999-120001': '100K - 120K THB',
        '119999-150001': '120K - 150K THB',
        '149999-100000000': '150K - ' + trans,
      };
      // console.log(this.createBudgetArr(90, 1000))
    }
    for (var i in budget) {
      options += '<option value="' + i + '">' + budget[i] + '</option>'
    }
    $('#budget').append(options);
    return false;
  },

  createBudgetArr: function (to, multiplyBy) {
    var currentUnit = '';
    if (multiplyBy > 1 && multiplyBy < 1001) {
      currentUnit = 'K';
    } else {
      currentUnit = 'M';
    }
    var numbersForPrice = [1];
    for (var i = 1; i <= to; i++) {
      if (i >= 4 && i <= 10 && i % 2 == 0) {
        numbersForPrice.push(i);
      } else if (i > 10 && i <= 20 && i % 5 == 0) {
        numbersForPrice.push(i);
      } else if (i % 10 == 0) {
        numbersForPrice.push(i);
      }
    }
    var priceFromTo = [];
    for (i = 0; i < numbersForPrice.length; i = i + 2) {
      priceFromTo[i] = [];
      priceFromTo[i].push(numbersForPrice[i] * multiplyBy + (i != 0 ? 1 : 0));
      priceFromTo[i].push(numbersForPrice[i + 1] * multiplyBy);
      priceFromTo[i].push(numbersForPrice[i] + currentUnit);
      priceFromTo[i].push(numbersForPrice[i + 1] + currentUnit);

      priceFromTo[+i + 1] = [];
      priceFromTo[+i + 1].push(numbersForPrice[+i + 1] * multiplyBy + 1);
      if (numbersForPrice[+i + 2]) {
        priceFromTo[+i + 1].push(numbersForPrice[+i + 2] * multiplyBy);
      }
      priceFromTo[+i + 1].push(numbersForPrice[+i + 1] + currentUnit);
      if (numbersForPrice[+i + 2]) {
        priceFromTo[+i + 1].push(numbersForPrice[+i + 2] + currentUnit);
      }
    }
    var budget = [];
    for (i = 0; i <= priceFromTo.length - 1; i++) {
      if (priceFromTo[i][2]) {
        budget[priceFromTo[i][0] + '-' + priceFromTo[i][1]] = priceFromTo[i][2] + ' - ' + priceFromTo[i][3] + ' THB ';
      } else if (priceFromTo[i][0]) {
        budget[priceFromTo[i][0] + '-' + 100000000000] = priceFromTo[i][1] + ' THB   - up';
      }
    }
    return budget;
  },

  deleteCookie: function (name) {
    document.cookie = name + '=; Expires=Thu, 01 Jan 1970 00:00:01 GMT; Path=/';
  },

  /**
   * Select and copy text
   * */
  selectAndCopyText: function (containerid) {
    var range = '';
    if (document.selection) {
      range = document.body.createTextRange();
      range.moveToElementText(document.getElementById(containerid));
      range.select();
      this.copySelectedText()
    } else if (window.getSelection) {
      range = document.createRange();
      range.selectNode(document.getElementById(containerid));
      window.getSelection().removeAllRanges();
      window.getSelection().addRange(range);
      this.copySelectedText()
    }
  },

  copySelectedText: function () {
    try {
      var successful = document.execCommand('copy');
      $('#btn-take-copy').addClass('copied').html('Copied');
    } catch (err) {
      console.log('Oops, unable to copy');
    }
  },

  hideContactUs: function () {
    $('.block-back').addClass('block-back-hide').removeClass('block-back-show');
    $('.form-contact-us').addClass('form-contact-us-hide').removeClass('form-contact-us-show');
  },

  setInputLine: function () {
    var inputs = $('.label_better');
    for (var i = 0; i < inputs.length; i++) {
      $(inputs[i]).on('focus', function () {
        $(this).parent('div').addClass('lb_wrap_line');
      });
      $(inputs[i]).on('focusout', function () {
        $(this).parent('div').removeClass('lb_wrap_line');
      });
    }
  },

};

$(document).ready(function () {
  /* $('.arrow').on('click', function(){
   $('.header-lang ul li').css('display','block');
   });*/
  $('.arrow').click(function () {
    $('.header-lang ul li, .property-price-block ul li').toggleClass('show-li');
    $(this).toggleClass('rotate-arrow');
  });
  $('.property-price-block ul').click(function () {
    $('.header-lang ul li, .property-price-block ul li').toggleClass('show-li');
    $('.property-price-block .arrow').toggleClass('rotate-arrow');
  });
  /* $('.property-facilities').click(function () {
     $('.property-facilities ul li').toggleClass('show-li');
     $('.property-facilities .arrow-facil').toggleClass('rotate-arrow');
   });*/
  $('.favorites-account-f').click(function () {
    $('.block-back').addClass('block-back-show').removeClass('block-back-hide');
    $('.favorites-list-main').addClass('show-favorite').removeClass('hide-favorite');
  });
  $('.block-back, .close').click(function () {
    $('.block-back').addClass('block-back-hide').removeClass('block-back-show');
    $('.favorites-list-main').addClass('hide-favorite').removeClass('show-favorite');
  });
  $('.burger-home, .burger-region-search-header, .burger-static-pages, .burger-discover, .burger-property').click(function () {
    $(this).toggleClass('burger-open');
    $('.header').toggleClass('header-mob');
  });


  // $(document).ready(function(){

  $(".burger-static-pages").click(function () {
    $(".menu").toggleClass("menu-open");
  });


  $(".burger-property").click(function () {
    $("body").toggleClass("body-fix");
    $("#search-form").toggleClass("form-open");
  });

  $(".burger-region-search-header").click(function () {
    $("body").toggleClass("body-fix");
    $("#search-form").toggleClass("form-open");
    $(".favorites-header").toggleClass("favorites-header-open");
  });

  $(".burger-discover").click(function () {
    $("body").toggleClass("body-fix");
    $(".menu").toggleClass("menu-open-discover");
    $(".favorites-header").toggleClass("favorites-header-open");
  });

  $(".show-search-main-btn").click(function () {
    $("body").addClass("body-fix");
    $("#search-form").addClass("form-open");
  });

  $("#search-form > div").click(function () {
    $("#search-form > div").removeClass("z-idx");
    $(this).addClass("z-idx");
  });

  $(".burger-home").click(function () {
    $("body").removeClass("body-fix");
    $("#search-form").removeClass("form-open");
  });

  $(".close-form-mob-btn").click(function () {
    $("body").removeClass("body-fix");
    $("#search-form").removeClass("form-open");
    $('.header').removeClass('header-mob');
    $('.burger-region-search-header').removeClass('burger-open');
    $('.burger-property').removeClass('burger-open');
    $(".favorites-header").removeClass("favorites-header-open");
  });


  /*=== accordion for FAQ page ===*/

  $('.faq-content:first').show('slow');
  $('.faq-title').click(function () {
    $('.faq-content').not($(this).next()).slideUp('slow');
    $(this).next().slideToggle(2000);
  });

  /*=== accordion for FAQ page ===*/

  // });
});

