window.Web = window.Web || {};


Web.search = {

  url: '',
  categories: [],
  locations: [],
  toSaleTrans: '',
  budgetTrans: '',

  setData: function(obj){
    for(var i in obj){
      if (typeof Web.search[i] !== "undefined") {
        Web.search[i] = obj[i];
      }
    }
  },

  changeSearchType: function (saleOrRent) {
    Web.main.createBudgetOptions(saleOrRent, this.toSaleTrans, this.budgetTrans);
    $('#budget').selectpicker('refresh');
  },

  goSearch: function () {
    var url = this.url;
    var type = $('#sale-rent').val() || this.toSaleTrans;
    var category = $('#category_url').val();
    var location = $('#location_url').val();
    var budget = $('#budget').val();
    var rooms = $('#rooms').val();
    var propertyCode = $('#code').val();

    this.setToStorageLastSearch("search_category", category);
    this.setToStorageLastSearch("search_location", location);
    this.setToStorageLastSearch("search_budget", budget);
    this.setToStorageLastSearch("search_rooms", rooms);

    var searchUrl = '';
    if (budget) {
      searchUrl += (searchUrl ? '&' : '') + (budget ? 'budget=' + budget : '');
    }
    if (propertyCode) {
      searchUrl += (searchUrl ? '&' : '') + (propertyCode ? 'code=' + propertyCode : '');
    }
    if(rooms){
      searchUrl += (searchUrl ? '&' : '') + (rooms ? 'rooms=' + rooms : '')
    }

    if (!propertyCode)
      this.addToCookie(type, (category ? category : null), (location ? location : null), budget ? budget : null);

    url += '/' + type + (category ? ('/' + category) : '') + '/' + location + (searchUrl ? '?' + searchUrl : '');
    window.location = url;
  },

  addToCookie: function (type, category, location, budjet) {
    var categoryInf = null;
    var locationInf = null;

    if (category) {
      for (var i in this.categories) {
        if (this.categories[i]['url'] == category) {
          categoryInf = this.categories[i];
        }
      }
    }
    if (location) {
      for (var i in this.locations) {
        if (this.locations[i]['url'] == location) {
          locationInf = this.locations[i];
          locationInf.name = locationInf.name.replace(/\&nbsp\;/gi, '');
          locationInf.name = locationInf.name.replace('-', '');
        }
      }
    }
    var data = {};
    data['type'] = type;
    data['category'] = categoryInf;
    data['location'] = locationInf;
    data['budget'] = budjet;
    data = JSON.stringify(data);
    document.cookie = 'search' + "=" + data + "; path=/";
  },

  setToStorageLastSearch: function (fieldName, data) {
    localStorage.setItem(fieldName, data);
  }
};