window.Web = window.Web || {};

Web.property = {

    appUrl: null,
    propertyId: null,
    propertiesIds: null,

    favoritesLimit: 20,
    viewedLimit: 10,

    initInfinityScroll: function () {
        $('ul.pagination').hide();
        if ($('.pagination li.active + li a').length) {
            var container = $('#inf-scroll').infiniteScroll({
                path: '.pagination li.active + li a',
                append: '.properties-block',
                history: false,
                loadOnScroll: false
            });
            container.on('request.infiniteScroll', function (event, path) {
                $('#pr-img').css({'display': 'block'});
            });
            container.on('load.infiniteScroll', function (event, response, path) {
                $('#pr-img').css({'display': 'none'});
                $('.pagination-wrapper').remove();
            });
            $(window).scroll(function () {
                if ($(window).scrollTop() == ($(document).height() - $(window).height())) {
                    container.infiniteScroll('loadNextPage');
                }
            });
        }
    },


    getFromLocalStorage: function (fieldName) {
        var data = localStorage.getItem(fieldName);
        if (!data) {
            return [];
        } else {
            return JSON.parse(data);
        }
    },

    savePropertyAsViewed: function () {
        if (this.propertyId) {
            this.getViewedProperty();
            var viewed = this.getFromLocalStorage('viewed_property');
            for (var i = 0; i < viewed.length; i++) {
                if (viewed[i] == this.propertyId) {
                    viewed.splice(i, 1);
                    break;
                }
            }
            if (viewed.length >= this.viewedLimit) {
                viewed.pop();
            }
            viewed.unshift(this.propertyId);
            localStorage.setItem('viewed_property', JSON.stringify(viewed));
        } else {
            console.log('Property Id is -> ' + this.propertyId);
        }
    },

    getViewedProperty: function () {
        var propertiesId = this.getFromLocalStorage('viewed_property');
        var favorites = this.getFromLocalStorage('favorite_property');
        if (propertiesId.length > 0) {
            $.ajax({
                url: '/take-viewed',
                data: {ids: JSON.stringify(propertiesId), favorites: JSON.stringify(favorites)},
                type: 'GET'
            })
                .done(function (data) {
                    if (data)
                        $('#viewed-properties').show().append(data);
                })
                .fail(function (xhr, status, errorThrown) {
                    console.error(xhr, status);
                })
        }
    },

    setCookie: function (data, days) {
        var expires = "";
        var value = JSON.stringify(data);
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
        document.cookie = 'favorites' + "=" + value + expires + "; path=/";
    },

    checkIsSelectedProperty: function () {
        var favorite = this.getFromLocalStorage('favorite_property');
        for (var i = 0; i < favorite.length; i++) {
            if (favorite[i] == this.propertyId) {
                $('#property-is-selected').css({'display': 'block'});
                $('#button-add-to-favorites').css({'display': 'none'});
                return;
            }
        }
        $('#property-is-selected').css({'display': 'none'});
        $('#button-add-to-favorites').css({'display': 'block'});
    },

    favoritePage: function () {
        var favorites = this.getFromLocalStorage('favorite_property');
        if (this.appUrl) {
            var url = this.appUrl + '/favorites?';
            var p = '';
            for (var i = 0; i < favorites.length; i++) {
                p += 'p' + i + '=' + favorites[i] + (favorites[i + 1] ? '&' : '');
            }
            location.href = url + p;
        }
    },

    countFavorites: function () {
        var favorites = this.getFromLocalStorage('favorite_property');
        $('.favorites-count').html(favorites.length);
    },

    countFavoritesBeforeUpload: null,

    takeFavoriteProperties: function (propertiesId, cb) {
        if (propertiesId.length > 0) {
            $.ajax({
                url: '/take-favorites',
                data: {
                    ids: JSON.stringify(propertiesId),
                    favorites: JSON.stringify(propertiesId),
                    'forFavoritePage': true
                },
                type: 'GET'
            }).done(function (data) {
                if (data) {
                    return cb(data)
                }
            })
                .fail(function (xhr, status, errorThrown) {
                    console.error(xhr, status);
                })
        }
    },

    createFavoritesList: function () {
        var favoritesIds = this.getFromLocalStorage('favorite_property');
        if (favoritesIds.length > 0) {
            // $('.favorites-header').addClass('show-favorite').removeClass('hide-favorite');
            var savedFavoritesHtml = localStorage.getItem('favorites_html');
            if (savedFavoritesHtml) {
                $('#favorites-list').html(savedFavoritesHtml);
            }
            //TODO  !!!! (On cashing on client)
            if (/*!Web.main.compareArrays(this.countFavoritesBeforeUpload, favoritesIds)*/ true) {
                this.takeFavoriteProperties(favoritesIds, function (data) {
                    localStorage.setItem('favorites_html', data);
                    localStorage.setItem('favorites_uploaded', JSON.stringify(favoritesIds));
                    $('#favorites-list').html(data);
                    Web.property.countFavoritesBeforeUpload = favoritesIds;
                })
            }
        }
    },

    addPropertyToFavorite: function (id) {
        var favorite = this.getFromLocalStorage('favorite_property');
        for (var i = 0; i < favorite.length; i++) {
            if (favorite[i] == id) {
                favorite.splice(i, 1);
                this.setCookie(favorite, 90);
                localStorage.setItem('favorite_property', JSON.stringify(favorite));
                $('.fi-' + id).addClass('favorit-icon-no_select').removeClass('favorit-icon-select');
                this.countFavorites();
                return;
            }
        }
        if (favorite.length >= this.favoritesLimit) {
            favorite.pop();
        }
        favorite.unshift(id);
        this.setCookie(favorite, 120);
        localStorage.setItem('favorite_property', JSON.stringify(favorite));
        $('.fi-' + id).addClass('favorit-icon-select').removeClass('favorit-icon-no_select');
        this.countFavorites();
    },

    markFavoriteProperties: function () {
        var favorites = this.getFromLocalStorage('favorite_property');
        if (this.propertiesIds && favorites.length > 0) {
            var propertiesIds = JSON.parse(this.propertiesIds);
            for (var i = 0; i < propertiesIds.length; i++) {
                for (var j = 0; j < favorites.length; j++) {
                    $('.fi-' + propertiesIds[i]).addClass('favorit-icon-no_select').removeClass('favorit-icon-select');
                    if (propertiesIds[i] == favorites[j]) {
                        $('.fi-' + propertiesIds[i]).addClass('favorit-icon-select').removeClass('favorit-icon-no_select');
                        break;
                    }
                }
            }
        }
    },

    deleteViewedPropertiesFromStorage: function (notFound) {
        var viewedProperties = Web.property.getFromLocalStorage('viewed_property');
        if (notFound.length > 0) {
            for (var i = 0; i < notFound.length; i++) {
                for (var j = 0; j < viewedProperties.length; j++) {
                    if (viewedProperties[j] == notFound[i]) {
                        viewedProperties.splice(j, 1)
                    }
                }
            }
        }
        localStorage.setItem('viewed_property', JSON.stringify(viewedProperties));
    },

    currencyRate: null,
    price: null,

    changeCurrency: function (data) {
        var data = data.split('-');
        var currencyRate = data[0];
        var sign = data[1];
        this.currencyRate = currencyRate || 0;
        $('#price-result-sign').html(sign || '');
        this.recountPrice();
    },

    changePrice: function (price) {
        this.price = price || 0;
        this.recountPrice();
    },

    recountPrice: function () {
        var result = this.price * this.currencyRate;
        result = +result.toFixed();
        result = (result + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,');
        $('#price-result').html(result);
    },

    videoId: null,

    video: function () {
        if (this.videoId) {
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        } else {
            console.log('Video Id ' + this.videoId)
        }
    },

    onPlayerReady: function (event) {
        event.target.pauseVideo();
    },

    startVideo: function () {
        if (window.player)
            window.player.playVideo();
    },

    stopVideo: function () {
        if (window.player)
            window.player.stopVideo();
    },

    showVideo: function () {
        $('#video-block').css({'display': 'flex'});
    }

   /* hidVideo: function () {
        $('#video-block').css({'display': 'none'});
        this.stopVideo();
    }*/
};