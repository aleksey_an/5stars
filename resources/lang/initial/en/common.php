<?php
return array(
    'meta_title_fivestars' => 'FiveStars Thailand',

    'home_meta_title' => 'Real estate agency in Thailand',
    'home_header' => 'Live in Thailand',
    'home_featured' => 'Featured Properties',

    'about-us_meta_title' => 'About Us',

    'faq_meta_title' => 'FAQ',

    'links_meta_title' => 'Web-Link Directory',

    'who-we-are_meta_title' => 'Who we are',

    'our-services_meta_title' => 'Our Services',

    'agencies-in-thailand_meta_title' => 'Five Stars Real Estate Agents Koh Samui',

    'search_meta_title' => ':property_type for :transaction_type :location',
    'search_meta_title_property' => 'Property',
    'search_meta_title_to_sale' => 'sale',
    'search_meta_title_to_rent' => 'rent',
    'search_meta_title_in_city' => 'in :city',
    'search_meta_title_in_thailand' => 'in Thailand',

    'search_buy' => 'Buy',
    'search_rent' => 'Rent',
    'search_type' => 'Property type',
    'search_city' => 'Cities',
    'search_budget' => 'Budget',
    'search_by_id' => 'By Ref',
    'search_for_sale' => 'Sale',
    'search_i_wish' => 'I wish',
    'search_btn_search' => 'Search',
    'search_btn_find' => 'Find a property',
    'search_title_category' => 'Property type',
    'search_title_city' => 'Cities',
    'search_title_budget' => 'Budget',
    'search_rooms' => 'Num of rooms',

    'search_page_discover' => 'Discover',
    'search_page_region' => 'Region',
    'search_page_choose_location' => 'Choose a Location',
    'search_page_properties' => 'Properties',

    'header_home_blog' => 'Blog',
    'header_home_sale' => 'Sale',
    'header_home_to_rent' => 'Rent',
    'header_home_faq' => 'FAQ',
    'header_home_who_we_are' => 'Who we are',
    'header_home_contact_us' => 'Contact Us',

    'header_favorite' => 'Favorite',
    'header_favorite_add' => 'Add To Favorite',
    'header_btn_contact_agent' => 'Contact the agent',

    'component_property_rent' => 'Rent',
    'component_property_sale' => 'Sale',
    'component_property_price' => 'Price',
    'component_property_month' => 'Month',
    'component_property_location' => 'Location',

    'bread_crumbs_home' => 'Home',

    'form_contact_us_first_name' => 'Your First Name',
    'form_contact_us_name' => 'Your Last Name',
    'form_contact_us_phone' => 'Your Phone',
    'form_contact_us_email' => 'Your Email',
    'form_contact_us_message' => 'Leave us a message',
    'form_contact_us_send' => 'Send',
    'form_contact_us_how_to_contact' => 'How to contact us',

    'form_contact_agent_rent_date' => 'Rent date',

    'discover_region_discover' => 'Discover',
    'discover_region_welcome' => 'Welcome to',
    'discover_region_properties_in' => 'Real estate agency',

    'price_block_price' => 'Sell',
    'price_block_rent_by_month' => 'Rent by month',
    'price_block_rent_by_week' => 'Rent by week',
    'price_block_rent_by_day' => 'Rent by day',

    'property_details_location' => 'Location',
    'property_details_beds' => 'Bedrooms',
    'property_details_bath' => 'Bathroom',
    'property_details_sqft' => 'Area',
    'property_details_facilities' => 'Facilities include',
    'property_details_furnished' => 'Furnished',
    'property_details_full_furnished' => 'Full Furnished',
    'property_details_not_furnished' => 'Not Furnished',
    'property_details_ref' => 'Ref:',
    'property_details_property_ref' => 'Property Reference:',
    'property_details_viewed_properties' => 'Properties I looked at before',
    'property_contact_us_title' => 'Contact us',

    'footer_menu' => 'Menu',
    'footer_useful_links' => 'Useful Links',
    'footer_about_us' => 'About us',
    'footer_links' => 'Links',
    'footer_our_services' => 'Our services',
    'footer_our_agencies' => 'Our Agencies in Thailand',
    'footer_newsletter' => 'Newsletter',

    'blog_read_more' => 'Read More',
    'blog_meta_title' =>'Blog',

    'success_response_ok' => 'Thank you for your message!',
    'success_response_error' => 'Something went wrong!',

    'form_footer_thank_message' => 'Thank you for your information'

);



