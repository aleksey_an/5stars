<?php
return array(
    'meta_title_fivestars' => 'FiveStars Immobilier Thaïlande',

    'home_meta_title' => 'Agence immobilière en Thaïlande',
    'home_header' => 'Immobilier en Thaïlande',
    'home_featured' => 'Nos dernières annonces',

    'about-us_meta_title' => 'Qui sommes nous',

    'faq_meta_title' => 'FAQ',

    'links_meta_title' => 'Annuaire de liens',

    'who-we-are_meta_title' => 'Qui sommes nous',

    'our-services_meta_title' => 'Nos services',

    'agencies-in-thailand_meta_title' => 'Nos agences immobilières en Thaïlande',

    'search_meta_title' => ':transaction_type :property_type :location',
    'search_meta_title_property' => 'immobilier',
    'search_meta_title_to_sale' => 'Acheter',
    'search_meta_title_to_rent' => 'Louer',
    'search_meta_title_in_city' => 'à :city',
    'search_meta_title_in_thailand' => 'en Thaïlande',

    'search_buy' => 'Acheter',
    'search_rent' => 'Louer',
    'search_type' => 'Type de bien',
    'search_city' => 'Villes',
    'search_budget' => 'Budget',
    'search_by_id' => 'Par Réf',
    'search_for_sale' => 'Vendre',
    'search_i_wish' => 'Je souhaite',
    'search_btn_search' => 'Chercher',
    'search_btn_find' => 'Consultez nos annonces immobilières',
    'search_title_category' => 'Type de bien',
    'search_title_city' => 'Villes',
    'search_title_budget' => 'Budget',
    'search_rooms' => 'Nbre de chambres',

    'search_page_discover' => 'Découvrir',
    'search_page_region' => 'Région',
    'search_page_choose_location' => 'Choisissez un emplacement',
    'search_page_properties' => 'Propriétés',

    'header_home_blog' => 'Blog',
    'header_home_sale' => 'Acheter',
    'header_home_to_rent' => 'Louer',
    'header_home_faq' => 'FAQ',
    'header_home_who_we_are' => 'Qui sommes nous',
    'header_home_contact_us' => 'Contactez nous',

    'header_favorite' => 'Préféré',
    'header_favorite_add' => 'Ajouter aux Favoris',
    'header_btn_contact_agent' => 'Contacter l\'agent',

    'component_property_rent' => 'Location',
    'component_property_sale' => 'Vente',
    'component_property_price' => 'Prix',
    'component_property_month' => 'Mois',
    'component_property_location' => 'Quartier',

    'bread_crumbs_home' => 'Accueil',

    'form_contact_us_first_name' => 'Votre Prénom',
    'form_contact_us_name' => 'Votre nom de famille',
    'form_contact_us_phone' => 'Votre Téléphone',
    'form_contact_us_email' => 'Votre Email',
    'form_contact_us_message' => 'Laissez-nous un message',
    'form_contact_us_send' => 'Envoyer',
    'form_contact_us_how_to_contact' => 'Comment nous contacter',

    'form_contact_agent_rent_date' => 'Date de location',

    'discover_region_discover' => 'Découvrir',
    'discover_region_welcome' => 'Bienvenue à',
    'discover_region_properties_in' => 'Agence immobilier',

    'price_block_price' => 'Prix',
    'price_block_rent_by_month' => 'Louer par mois',
    'price_block_rent_by_week' => 'Louer par semaine',
    'price_block_rent_by_day' => 'Louer par jour',

    'property_details_location' => 'Quartier',
    'property_details_beds' => 'Chambres',
    'property_details_bath' => 'Salle de bain',
    'property_details_sqft' => 'Surface',
    'property_details_facilities' => 'Les installations comprennent',
    'property_details_furnished' => 'Meublé',
    'property_details_full_furnished' => 'Entièrement meublé',
    'property_details_not_furnished' => 'Pas meublé',
    'property_details_ref' => 'Réf:',
    'property_details_property_ref' => 'Référence du bien:',
    'property_details_viewed_properties' => 'Vos derniers biens consultés',
    'property_contact_us_title' => 'Contactez nous',

    'footer_menu' => 'Menu',
    'footer_useful_links' => 'Liens utiles',
    'footer_about_us' => 'À propos de nous',
    'footer_links' => 'Liens',
    'footer_our_services' => 'Nos services',
    'footer_our_agencies' => 'Nos agences en Thaïlande',
    'footer_newsletter' => 'Bulletin',

    'blog_read_more' => 'Lire la suite',
    'blog_meta_title' =>'Blog',

    'success_response_ok' => 'Merci pour votre message!',
    'success_response_error' => 'Quelque chose s\'est mal passé!',

    'form_footer_thank_message' => 'Merci pour ces informations'

);


