<?php

return [
    
     /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
	'email_addr' => 'Email Address',
	'failed' => 'Incorrect Email or Password. Please try again.',
	'forgot_password' => 'Forgot Your Password?',
	'login' => 'Login',
	'password' => 'Password',
	'register' => 'Register',
	'remember_me' => 'Remember Me',
	'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
];
