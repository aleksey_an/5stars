<?php

use App\Http\Controllers\Web\CheckCacheController;

/*Get data from db */
$checkCache = new CheckCacheController;
$localizations = $checkCache->checkCache('localization-en');
$result = [];
if (isset($localizations) && count($localizations) > 0) {
    foreach ($localizations as $l) {
        $result[$l['key']] = $l['value'];
    }
}

if (count($result) > 0) {
    return $result;
} else {
    return File::getRequire(resource_path() . '/lang/initial/en/common.php');
}


